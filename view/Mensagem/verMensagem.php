<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<style type="text/css">

.nav-tabs .glyphicon:not(.no-margin) { margin-right:10px; }
.tab-pane .list-group-item:first-child {border-top-right-radius: 0px;border-top-left-radius: 0px;}
.tab-pane .list-group-item:last-child {border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
.tab-pane .list-group .checkbox { display: inline-block;margin: 0px;margin-top:-100px; }
.tab-pane .list-group input[type="checkbox"]{ margin-top: 2px; }
.tab-pane .list-group .glyphicon { margin-right:5px; }
.tab-pane .list-group .glyphicon:hover { color:#FFBC00; }
a.list-group-item.read { color: #222;background-color: #F3F3F3; }
hr { margin-top: 5px;margin-bottom: 10px; }
.nav-pills>li>a {padding: 5px 10px;}

.ad { padding: 5px;background: #F5F5F5;color: #222;font-size: 80%;border: 1px solid #E5E5E5; }
.ad a.title {color: #15C;text-decoration: none;font-weight: bold;font-size: 110%;}
.ad a.url {color: #093;text-decoration: none;}

</style>
<script type="text/javascript">
function submitform(){
    n = confirm('Tem certeza que quer excluir?');
    if(n)
        document.forms["mensagens"].submit();
}
</script>
<?php
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>
<fieldset>
<legend>Ver Mensagem</legend>

<div class="container">
<form method="post" action="?task=Mensagem&action=deletarMensagemAction" name="mensagens">
    <div class="row">
        <div class="col-sm-9 col-md-10">
            <!-- Single button -->
            <div class="btn-group">
                <a href="javascript:history.back(-2)" type="button" class="btn btn-default dropdown-toggle">
                    <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                </a>
            </div>            
            <div class="btn-group">
                <a href="#" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" onclick="submitform()">
                    Deletar
                </a>
            </div>
            <div class="btn-group">
                <a href="?task=Mensagem&action=escreverMensagemPage&usuario=<?php echo $Result['responder'];?>" type="button" class="btn btn-default dropdown-toggle">
                    Responder
                </a>
            </div>            
        </div>
    </div>  
    <input type="hidden" name="msgs[]" value="<?php echo $_GET['mensagem']?>">
    <input type="hidden" name="tipo" value="<?php echo $_GET['tipo']?>">
    <input type="hidden" name="ver" value="truee">
</form>
<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">               
                <!-- Form Name -->
                <h3><?php echo $Result['mensagem']['assunto'];?></h3>
                <!-- Text input-->

                <div class="control-group">
                  <b>De:</b>  <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['mensagem']['de'][0]['id'];?>"><?php echo $Result['mensagem']['de'][0]['login'];?></a><br>
                  <b>Para:</b>  <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['mensagem']['para'][0]['id'];?>"><?php echo $Result['mensagem']['para'][0]['login'];?></a><br>                  
                  <b>Data:</b> <?php echo  implode ( "/",array_reverse(explode("-",$Result['mensagem']['data'])))." ".$Result['mensagem']['hora'];?><br>
                  <b>Assunto:</b> <?php echo  $Result['mensagem']['assunto'];?><br>
                  <b>Mensagem:</b></br> </br>
                  <div style="width:900px;">
                        <?php echo $Result['mensagem']['mensagem'];?>
                  </div>
                  <br>
                </div>

                </fieldset>
                <input type="hidden" name="lance" value="<?php echo $Result['lance'];?>">
                <input type="hidden" name="leilao" value="<?php echo $Result['leilao']['id'];?>">
      </div><!--\div col-md6 col-sm-12 -->

</div>    

</div>


<fieldset>