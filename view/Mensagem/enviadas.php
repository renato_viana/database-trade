<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<style type="text/css">

.nav-tabs .glyphicon:not(.no-margin) { margin-right:10px; }
.tab-pane .list-group-item:first-child {border-top-right-radius: 0px;border-top-left-radius: 0px;}
.tab-pane .list-group-item:last-child {border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
.tab-pane .list-group .checkbox { display: inline-block;margin: 0px;margin-top:-100px; }
.tab-pane .list-group input[type="checkbox"]{ margin-top: 2px; }
.tab-pane .list-group .glyphicon { margin-right:5px; }
.tab-pane .list-group .glyphicon:hover { color:#FFBC00; }
a.list-group-item.read { color: #222;background-color: #F3F3F3; }
hr { margin-top: 5px;margin-bottom: 10px; }
.nav-pills>li>a {padding: 5px 10px;}

.ad { padding: 5px;background: #F5F5F5;color: #222;font-size: 80%;border: 1px solid #E5E5E5; }
.ad a.title {color: #15C;text-decoration: none;font-weight: bold;font-size: 110%;}
.ad a.url {color: #093;text-decoration: none;}

</style>

<script type="text/javascript">
function submitform(){
    n = confirm('Tem certeza que quer excluir?');
    if(n)
        document.forms["mensagens"].submit();
}
</script>

<?php
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>
<fieldset>
<legend>Enviadas</legend>

<div class="container">

    <div class="row">
        <div class="col-sm-9 col-md-10">
            <!-- Split button -->

            <a href="?task=Mensagem&action=enviadasPage"  type="button" class="btn btn-default" data-toggle="tooltip" title="atualizar">
                   <span class="glyphicon glyphicon-refresh"></span>   </a>
            <!-- Single button -->
            <div class="btn-group">
                <a href="#" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" onclick="submitform()">
                    Deletar
                </a>
            </div>
            <div class="pull-right">
                <span class="text-muted"><b><?php echo $paginacao->primeiro_registro+1; ?></b>–<b><?php echo count($Result['entrada']); ?></b> de <b><?php echo count($Result['entrada']);?></b></span>
                <div class="btn-group btn-group-sm">
                    <?php if($paginacao->pagina > 1){?>
                        <a href="?task=Mensagem&action=entradaPage&pagina=<?php echo $paginacao->pagina-1?>" type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                    <?php }else{ ?>
                        <a href="#" type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                    <?php } ?>

                    <?php if($paginacao->pagina < $paginacao->paginas){?>
                        <a href="?task=Mensagem&action=entradaPage&pagina=<?php echo $paginacao->pagina+1?>" type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    <?php }else{ ?>
                        <a href="#" type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    <?php } ?>                    

                </div>
            </div>
        </div>
    </div>  

    <div class="row">

        <div class="col-sm-9 col-md-10">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-send">
                </span>Enviadas</a></li>
            </ul>
            <!-- Tab panes -->
             <form method="post" action="?task=Mensagem&action=deletarMensagemAction" name="mensagens">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">
                    <div class="list-group">
                     <?php
                    $pergunta = $Result['entrada'];
                    $n = count($pergunta);
                    $n = ($n >= 25) ? 25 : $n;                        
                    for($i=$paginacao->primeiro_registro;$i<$paginacao->primeiro_registro+$n;$i++){

                     $entrada = $Result['entrada'][$i];

                     $aux     = explode(" ",$entrada['mensagem']);
                     $entrada['mensagem'] = $aux[0]." ".$aux[1]." ".$aux[2]." ".$aux[3]." ".$aux[4]."...";

                     if($entrada['aberto'] == 0){
                        $star = "glyphicon glyphicon-star";
                        $read = "list-group-item read";
                     }else{
                        $star = "glyphicon glyphicon-star-empty";
                        $read = "list-group-item";
                     }

                     ?>   
                        <a href="?task=Mensagem&action=verMensagemPage&mensagem=<?php echo $entrada['id'];?>&tipo=enviada" class="<?php echo $read;?>">
                            <div class="checkbox">
                                <div style="float:left;margin-top:-5px;">
                                <label>
                                    <input type="checkbox" name="msgs[]" value="<?php echo $entrada['id'];?>">
                                </label>
                              </div>
                            </div>

                            <span class="<?php echo $star;?>"></span>
                            <span class="name" style="min-width: 120px;display: inline-block;"><?php echo $entrada['para']['login'];?></span>
                            <span class=""><?php echo $entrada['assunto'];?></span>
                            <span class="text-muted" style="font-size: 11px;">- <?php echo $entrada['mensagem'];?></span> 
                            <span class="badge"><?php echo implode ( "/",array_reverse(explode("-",$entrada['data'])))." ".$entrada['hora'];?></span>
                        </a>
                        <?php } ?>

                    </div>
                </div>
            </div>
            <input type="hidden" name="tipo" value="enviada">
        </form>

        </div>
    </div>
</div>


<fieldset>