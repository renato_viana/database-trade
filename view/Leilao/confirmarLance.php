<script src="libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

function mudarSituacao(){
      situacao = document.getElementById("situacao").value;
      negociacao = document.getElementById("negociacao").value;
      url = "index.php?task=Negociacao&action=mudarSituacaoAction&negociacao="+negociacao+"&situacao="+situacao;
      window.location.assign(url);
}

</script>

<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<fieldset>
<legend>Confirmar Lance</legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meuLeilaoAdminPage&leilao=<?php echo $Result['leilao']['id'];?>">&larr; Voltar</a></li>
</ul>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <form method="post" action="?task=Leilao&action=addLanceAction">
                <fieldset class="bs-callout bs-callout-danger">               
                <!-- Form Name -->
                <h3>Confirmação Lance:</h3>
                <!-- Text input-->

                <div class="control-group">
                  <b>Leilao:</b>  <a href="?task=Leilao&action=perfilPage&id=<?php echo $Result['leilao']['id'];?>"><?php echo $Result['leilao']['nome'];?></a><br>
                  <b>Vendedor:</b> <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['usuario']['id'];?>"><?php echo $Result['usuario']['login'];?></a><br>
                  <b>Meu Lance:</b> <?php echo number_format($Result['lance'], 2, ',', '.');?><br>
                  <b>Frete:</b> <?php echo number_format($Result['leilao']['frete'], 2, ',', '.');?><br>
                  <b>Total:</b> <?php echo number_format($Result['lance']+$Result['leilao']['frete'], 2, ',', '.');?><br>
                  <!--<b>Perguntas:</b> 31<br>-->
                  <center>
                  <div class="" style="margin-top:3px;margin-bottom:3px;">
                    <button id="salvar" name="salvar" class="btn-sm btn-primary" value="submit">Confirmo meu lance</button>
                  </div>                   
                </div></center>

                </fieldset>
                <input type="hidden" name="lance" value="<?php echo $Result['lance'];?>">
                <input type="hidden" name="leilao" value="<?php echo $Result['leilao']['id'];?>">
              </form>
      </div><!--\div col-md6 col-sm-12 -->

</div>

</div>

<fieldset>