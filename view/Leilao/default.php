<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Leilões</legend>

<style type="text/css">
#mytable a{
  color: black;
}
</style>

<?php 
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>

  <div class="row">
    <div class="col-md-5">  
      <div style="float:right;">

          <form class="navbar-form navbar-left" role="search" method="post" action="?task=Leilao&action=defaultPage">
          <div class="form-group">
            <input name="nome" type="text" class="form-control" placeholder="Conteúdo do Leilão" style="width:250px;">
          </div>
          <button type="submit" class="btn btn-default">Pesquisar</button>
         </form> 

        </div>
     </div>
  </div>

<div class="row">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Leilao&action=defaultPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Leilao&action=defaultPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Leilao&action=defaultPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>

<div class="row">
      <div class="col-md-12 col-xs-12" style="height:590px;">
                <fieldset class="bs-callout bs-callout-danger">
        
                <!-- Text input-->
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th></th>
                                      <th>Nome do leilão</th>
                                      <th>Vendedor</th>
                                      <th>Lance Inicial</th>
                                      <th>Maior Lance</th>
                                      <th>Data Fim</th>
                                      <!--<th>Criado em</th>-->
                                      <th>Situação</th>
                                      <th></th>
                                      <th></th>
                                     </thead>            
                                <tbody >
                                        <?php 
                                          $pergunta = $Result['leiloes'];
                                          $n = count($pergunta);
                                          $n = ($n >= 10) ? 10 : $n;            
                                          for($i=$paginacao->primeiro_registro;$i<$paginacao->primeiro_registro+$n;$i++){
                                          if($Result['leiloes'][$i]['id'] != ""){
                                          $leilao  = $Result['leiloes'][$i];  
                                         ?>
                                      <tr>
                                          <th style="width:100px;">
                                            <center>
                                              <?php if(file_exists('upload/leilao/'.$leilao['id'].".".$leilao['imagem'])){?>
                                                  <img src="<?php echo 'upload/leilao/thumb/'.$leilao['id'].".".$leilao['imagem'];?>" width="50%">
                                              <?php }else{ ?>
                                                 <img src="<?php echo 'upload/leilao/thumb/imagem.jpg';?>" width="50%">
                                              <?php }?>
                                              </center>
                                          </th>
                                          <td><a href="?task=Leilao&action=leilaoPage&leilao=<?php echo $leilao['id'];?>"><?php echo $leilao['nome'];?></a></td>
                                          <td><a href="?task=Usuario&action=perfilPage&id=<?php echo $leilao['usuario_id'];?>"><?php echo $leilao['vendedor'];?></a></td>
                                          <td>R$ <?php echo number_format($leilao['lance_inicial'], 2, ',', '.');?></td>
                                          <td>R$ <?php echo number_format($leilao['maior_lance'][0]['lance'], 2, ',', '.');?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-", $leilao['data_fim'])))." ".$leilao['data_fim-hora'];?></td>
                                          <!--<td><?php echo implode ( "/",array_reverse(explode("-", $leilao['criado'])))." ".$leilao['criado-hora'];?></td>-->
                                          <td><?php echo $leilao['status'];?></td>
                                          <td><a href="?task=Leilao&action=leilaoPage&leilao=<?php echo $leilao['id'];?>" class="btn-sm btn-primary" style="color:white;"><b>Entrar<b></a></td>
                                      </tr>
                                      <?php }} ?>
                                </tbody>
                            </table>         
                              
                              
                          </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
</div>

<div class="row" style="margin-bottom:20px;">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Leilao&action=defaultPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Leilao&action=defaultPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Leilao&action=defaultPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>


                  <div class="container">
                    <div class="row">

                    </div>
                  </div>
<fieldset>

