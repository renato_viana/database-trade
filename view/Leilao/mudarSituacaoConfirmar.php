<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<fieldset>
<legend>Confirmar Ativação do Leilão</legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meuLeilaoAdminPage&leilao=<?php echo $_GET['leilao'];?>">&larr; Voltar</a></li>
</ul>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <form method="post" action="?task=Leilao&action=mudarSituacaoAtivarAction">
                <fieldset class="bs-callout bs-callout-danger">               
                <!-- Form Name -->
                <h3>Confirmação:</h3>
                <!-- Text input-->

               <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  Ao ativar o leilão você não poderá mais fazer edições nele. E so será possivel fazer o cancelamento do leilão se não houver nenhum 
                  lance nele, caso tenha e você queira cancelar, será enviado uma solicitação para a administração para analisar a situação e se não for
                  por algum motivo de força maior o dono do leilão irá ganhar uma reputaçao negativa.
                </div>  

                <center>
                <div class="control-group">
                  <div class="" style="margin-top:3px;margin-bottom:3px;">
                    <button id="salvar" name="salvar" class="btn btn-primary" value="submit">Confirmar ativação</button>
                  </div>                   
                </div>
              </center>

                </fieldset>
                <input type="hidden" name="leilao" value="<?php echo $_GET['leilao'];?>">
              </form>
      </div><!--\div col-md6 col-sm-12 -->

</div>


<fieldset>