<fieldset>
<legend>Tags Para Anúncio</legend>

<style type="text/css">
.table-responsive{
  position: relative;
  overflow: auto ;
  overflow-x: hidden; 
  height: 650px;
  margin-bottom: 40px;
  border: 1px solid #CCC;
}
</style>

<script type="text/javascript">

function addAnuncio(anuncio,carta){
       qtd = document.getElementById(carta+"-qtd").value;
       url = "index.php?task=Leilao&action=addAnuncioListAction&anuncio="+anuncio+"&carta="+carta+"&qtd="+qtd;
       window.location.assign(url);
}

</script>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meuAnuncioAdminPage&anuncio=<?php echo $_GET['anuncio'];?>">&larr; Voltar</a></li>
</ul>


<div class="container">
	<div class="row">
	  <div class="col-md-7">	
	  	<div style="float:right;">

	        <form class="navbar-form navbar-left" role="search" method="get" action="?task=Leilao&action=addTagsAnuncioPage">
	        <input type="hidden" value="Leilao" name="task">
	        <input type="hidden" value="addTagsAnuncioPage" name="action">
	        <input type="hidden" value="<?php echo $_GET['anuncio']?>" name="anuncio">	        	
	        <div class="form-group">
	          <input name="nome" type="text" class="form-control" placeholder="Nome da Carta" style="width:250px;" required>
	        </div>
	        <button type="submit" class="btn btn-default">Pesquisar</button>
	       </form> 

        </div>
	   </div>
	</div>
	<div class="row">
	<div class="e-search">
        <div class="col-md-7">
        <div class="table-responsive">
        	<form name="list-cartas">
              <table id="mytable" class="table table-bordred table-striped">
                   <thead>
                   
				 	<th></th>
                   <th>Nome</th>
                    <th>Coleção</th>
                     <th>Quantidade</th>
                      <th>Add</th>
                   </thead>
				    <tbody>
			    	<?php for ($i=0; $i < count($Result['cartas']) && $i < 50; $i++) { 
			    	?>
				    <tr>
				    <th><a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['cartas'][$i]['id'];?>"><img src="<?php echo 'img/cartas/'.$Result['cartas'][$i]['sigla'].'/thumb/'.$Result['cartas'][$i]['numero'].".png";?>" width="50"></a></th>
				    <td><?php echo $Result['cartas'][$i]['nome'];?></td>
				    <td><?php echo $Result['cartas'][$i]['sigla'];?></td>
				    <td><input type="text" value="1" size="1" name="<?php echo $Result['cartas'][$i]['id']."-qtd";?>" id="<?php echo $Result['cartas'][$i]['id']."-qtd";?>"></td>
				    <th><p data-placement="top" data-toggle="tooltip" title="Edit"><a class="btn btn-primary btn-xs" data-title="Edit" href="#" onClick="<?php echo "addAnuncio('".$_GET['anuncio']."','".$Result['cartas'][$i]['id']."');"?>"><span class="glyphicon glyphicon-plus"></span></a></p></th>
				    </tr>
				    <?php } ?>
				    </tbody>
				</table>
				</form>
            </div>
        </div>
         <div class="col-md-4">
				<div class="col-md-12">
				 	<div class="panel panel-primary">
				 		<div class="panel-heading">
				 			<h3 class="panel-title"><b>Conteúdo do Anúncio</b></h3>
				 		</div>
				 		<div class="panel-body">


			    	<?php for ($j=6; $j >= 1; $j--) { 
			    	if(count($Result['wantlist'][$j]) == 0)
			    		continue;
			    	switch ($j) {
			    			case 6:
			    				echo "<div style='border-bottom:1px solid #999;'><center><h5><b>Exclusiva</b></h5></center></div>";
			    				break;			    							    		
			    			case 5:
			    				echo "<div style='border-bottom:1px solid #999;'><center><h5><b>Ultra Rara</b></h5></center></div>";
			    				break;
			    			case 4:
			    				echo "<div style='border-bottom:1px solid #999;'><center><h5><b>Super Rara</b></h5></center></div>";
			    				break;

			    			case 3:
			    				echo "<div style='border-bottom:1px solid #999;'><center><h5><b>Rara</b></h5></center></div>";
			    				break;			    
			    			case 2:
			    				echo "<div style='border-bottom:1px solid #999;'><center><h5><b>Incomum</b></h5></center></div>";
			    				break;			    		
			    			case 1:
			    				echo "<div style='border-bottom:1px solid #999;'><center><h5><b>Comum</b></h5></center></div>";
			    				break;
			    		}	
			    	?>				 			

			  <p>	 			
              <table id="mytable" class="" border="0">

				    <tbody>
						<tr>
			    	<?php 
			    	$c=0;
			    	for ($i=0; $i < count($Result['wantlist'][$j]) && $i < 50; $i++) { 
			    	?>
				    	<th wigth="2">
				    		<a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['wantlist'][$j][$i]['id'];?>"><img src="<?php echo 'img/cartas/'.$Result['wantlist'][$j][$i]['sigla'].'/thumb/'.$Result['wantlist'][$j][$i]['numero'].".png";?>" width="50"></a>
				    		<center><?php echo $Result['wantlist'][$j][$i]['qtd']."x";?><p data-placement="top" data-toggle="tooltip" title="Edit"><a class="btn btn-danger btn-xs" data-title="Edit" href="?task=Leilao&action=rmvCartaAnuncioAction&anuncio=<?php echo $_GET['anuncio'];?>&carta=<?php echo $Result['wantlist'][$j][$i]['id'];?>"><span class="glyphicon glyphicon-trash"></span></a></p></center>
				    	</th>

				    <?php 
				    $c++;
				    if($c==6){
				    	echo "</tr><tr>";
				    	$c=0;
				    }
				    } ?>
				    	</tr>				    
				    </tbody>
				</table>
			   </p>

				    <?php } ?>
				 		</div>
				 	</div>
				 </div>

         </div>
        </div>
        </div>
</div>

<fieldset>

