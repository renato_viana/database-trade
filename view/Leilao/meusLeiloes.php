<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Meus Leilões</legend>

<style type="text/css">
#mytable a{
  color: black;
}
</style>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
        
                <!-- Text input-->
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Codigo</th>
                                     <th>Nome</th>
                                      <th>Lance Inicial</th>
                                      <th>Data Inicio</th>
                                      <th>Data Fim</th>
                                      <!--<th>Criado em</th>-->
                                      <th>Situação</th>
                                      <th></th>
                                      <th></th>
                                     </thead>            
                                <tbody >
                                        <?php 
                                        for ($i=0; $i < count($Result['leiloes']); $i++) { 
                                          $leilao  = $Result['leiloes'][$i];  
                                         ?>
                                      <tr>
                                          <td>#<?php echo $leilao['id'];?></td>
                                          <td><?php echo $leilao['nome'];?></td>
                                          <td>R$ <?php echo number_format($leilao['lance_inicial'], 2, ',', '.');?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-",$leilao['data_inicio'])));?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-", $leilao['data_fim'])))." ".$leilao['data_fim-hora'];?></td>
                                          <!--<td><?php echo implode ( "/",array_reverse(explode("-", $leilao['criado'])))." ".$leilao['criado-hora'];?></td>-->
                                          <td><?php echo $leilao['status'];?></td>
                                          <td><a href="?task=Leilao&action=meuLeilaoAdminPage&leilao=<?php echo $leilao['id'];?>" class="btn-sm btn-danger" style="color:white;"><b>Ver<b></a></td>
                                          <td><a href="?task=Leilao&action=editarPage&leilao=<?php echo $leilao['id'];?>" class="btn-sm btn-primary" style="color:white;"><b>Editar<b></a></td>
                                          <td><?php echo $leilao['negociacao'];?></td>
                                      </tr>
                                      <?php } ?>
                                </tbody>
                            </table>         
                              
                              
                          </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
</div>

<fieldset>

