<script src="libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

function mudarSituacao(){
      situacao = document.getElementById("situacao").value;
      anuncio = document.getElementById("anuncio").value;
      url = "index.php?task=Leilao&action=mudarSituacaoAnuncioAction&anuncio="+anuncio+"&situacao="+situacao;              
      window.location.assign(url);
}

</script>
  <script>
  function moeda(z){  
    v = z.value;
    v=v.replace(/\D/g,"")  //permite digitar apenas números
  v=v.replace(/[0-9]{12}/,"inválido")   //limita pra máximo 999.999.999,99
  v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos últimos 8 digitos
  v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos últimos 5 digitos
  v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")  //coloca virgula antes dos últimos 2 digitos
    z.value = v;
  }
</script>

<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<fieldset>
<legend>Administrar Anuncio #<?php echo $_GET['anuncio'];?></legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meusAnunciosPage">&larr; Voltar</a></li>
</ul>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                  <div style="float:right;">
                      <div class="form-group">
                        <div class="col-md-12">
                          <form>
                          <?php if( $Result['anuncio']['status-id'] == 2 || $Result['anuncio']['status-id'] == 1 || $Result['anuncio']['status-id'] == 4 || $Result['leilao']['status-id'] == 5 || $Tvar->verificarAdmin()){ ?>    
                          <select name="situacao" class="form-control" onchange="mudarSituacao()" id="situacao">  
                            <option value="" selected="selected"> Mudar Situação </option>   
                            <option value="2"> Ativo </option>  
                            <option value="4"> Cencelado </option>  
                          </select>   
                          <?php } ?>
                          <input type="hidden" value="<?php echo $_GET['anuncio']?>" name="anuncio" id="anuncio">
                         </form>
                        </div>
                      </div>
                  </div>              

                <!-- Text input-->

                <div class="control-group">
                  <b>Situação Atual:</b> <?php echo $Result['anuncio']['status'];?><br>
                  <b>Ver Anúncio publicado:</b> <a href="?task=Leilao&action=anuncioPage&anuncio=<?php echo $_GET['anuncio']?>">clique aqui</a><br>
                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

</div>


<div class="row">
      <div class="col-md-12 col-xs-12">
            <center><a href="?task=Leilao&action=addTagsAnuncioPage&anuncio=<?php echo $_GET['anuncio']?>" id="salvar" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Adicionar Tags Conteúdo</a></center>
      </div>
</div>


<div class="row">
      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                <!-- Form Name -->
                <div style="width:350px;margin:0 auto;">
                    <?php if(file_exists('upload/anuncio/'.$Result['anuncio']['id'].".".$Result['anuncio']['img'])){?>
                        <a target="_blank" href="<?php echo 'upload/anuncio/'.$Result['anuncio']['id'].".".$Result['anuncio']['img'];?>"><img src="<?php echo 'upload/anuncio/thumb/'.$Result['anuncio']['id'].".".$Result['anuncio']['img'];?>"></a>
                    <?php }else{ ?>
                        <a target="_blank" href="<?php echo 'upload/anuncio/imagem.jpg';?>"><img src="<?php echo 'upload/anuncio/thumb/imagem.jpg';?>"></a>
                    <?php }?>
                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-warning">  
                <!-- Text input-->
                 <b>Nome:</b> <?php echo $Result['anuncio']['nome'];?><br>
                 <b>Oferta:</b> R$ <?php echo number_format($Result['anuncio']['oferta'], 2, ',', '.');?><br>
                 <b>Criado:</b>  <?php echo implode ( "/",array_reverse(explode("-",$Result['anuncio']['criado'])));?><br>
                 <b>Expira em:</b>  <?php echo implode ( "/",array_reverse(explode("-",$Result['anuncio']['expiraem'])))." ".$Result['anuncio']['expiraem-hora'];?><br>
              </fieldset>
        </div>

      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-warning">
                <h3>Vendedor</h3>   
                <!-- Text input-->
                 <b>Login:</b> <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['login']['id'];?>"><?php echo $Result['login']['login'];?></a><br>
                 <b>Reputação:</b> <?php echo $Result['reputacao'];?> <span class="glyphicon glyphicon-star"></span><br>
                 <b>Estado:</b> <?php echo $Result['login']['cidade']." - ".$Result['login']['estado'];?><br>
                 <b>Registaro em:</b> <?php echo  implode ( "/",array_reverse(explode("-",$Result['login']['data_cadastro'])));?><br>               
              </fieldset>
        </div>
</div>


<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                  <?php if($Result['proprietario']['id'] != NULL){ ?>
                  <div style="float:right;">
                      <div class="form-group">
                        <div class="col-md-12">
                          <form>
                          <?php if( $Result['negociacao'][0]['status-id'] != 10 ){ ?>    
                          <select name="situacao" class="form-control" onchange="mudarSituacao()" id="situacao">  
                            <option value="" selected="selected"> Mudar Situação </option>  
                            <option value="1"> Pendente </option>  
                            <option value="2"> Ativo </option>  
                            <option value="3"> Concluido </option>  
                            <option value="4"> Cencelado </option>  
                          </select>   
                          <?php } ?>
                          <input type="hidden" value="<?php echo $_GET['negociacao']?>" name="negociacao" id="negociacao">
                         </form>
                        </div>
                      </div>
                  </div>
                  <?php } ?>                  
                <!-- Form Name -->
                <h3>Informações</h3>
                <!-- Text input-->

                <div class="control-group">


                                <?php echo $Result['anuncio']['informacoes']; ?>

                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

</div>

<fieldset>