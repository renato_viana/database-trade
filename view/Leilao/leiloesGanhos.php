<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Leilões Ganhos</legend>

<style type="text/css">
#mytable a{
  color: black;
}
</style>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
        
                <!-- Text input-->
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Codigo</th>
                                     <th>Nome do leilão</th>
                                     <th>Vendedor</th>
                                      <th>Lance Inicial</th>
                                      <th>Arrematado por</th>
                                      <th>Data</th>
                                      <th>Situação</th>
                                      <th></th>
                                     </thead>            
                                <tbody >
                                        <?php 
                                        for ($i=0; $i < count($Result['leiloes']); $i++) { 
                                          $leilao  = $Result['leiloes'][$i];  
                                         ?>
                                      <tr>
                                          <td><?php echo $leilao['id'];?></td>
                                          <td><?php echo $leilao['nome'];?></td>
                                          <td><?php echo $leilao['vendedor']['login'];?></td>
                                          <td>R$ <?php echo number_format($leilao['lance_inicial'], 2, ',', '.');?></td>
                                          <td>R$ <?php echo number_format($leilao['lance'][0]['lance'], 2, ',', '.');?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-", $leilao['data_ultimo_lance'])))." ".$leilao['data_ultimo_lance-hora'];?></td>
                                          <td><?php echo $leilao['status'];?></td>
                                          <td><?php echo $leilao['negociacao'];?></td>
                                      </tr>
                                      <?php } ?>
                                </tbody>
                            </table>         
                              
                              
                          </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
</div>


                  <div class="container">
                    <div class="row">

                    </div>
                  </div>
<fieldset>

