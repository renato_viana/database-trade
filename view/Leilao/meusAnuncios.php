<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Meus Anúncios</legend>

<style type="text/css">
#mytable a{
  color: black;
}
</style>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
        
                <!-- Text input-->
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Codigo</th>
                                     <th>Nome</th>
                                      <th>Oferta</th>
                                      <th>Criado</th>
                                      <th>Expira Em</th>
                                      <!--<th>Criado em</th>-->
                                      <th>Situação</th>
                                      <th></th>
                                      <th></th>
                                     </thead>            
                                <tbody >
                                        <?php 
                                        for ($i=0; $i < count($Result['leiloes']); $i++) { 
                                          $anuncio  = $Result['leiloes'][$i];  
                                         ?>
                                      <tr>
                                          <td>#<?php echo $anuncio['id'];?></td>
                                          <td><?php echo $anuncio['nome'];?></td>
                                          <td>R$ <?php echo number_format($anuncio['oferta'], 2, ',', '.');?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-",$anuncio['criado'])));?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-", $anuncio['expiraem'])))." ".$anuncio['expiraem-hora'];?></td>
                                          <td><?php echo $anuncio['status'];?></td>
                                          <td><a href="?task=Leilao&action=meuAnuncioAdminPage&anuncio=<?php echo $anuncio['id'];?>" class="btn-sm btn-danger" style="color:white;"><b>Ver<b></a></td>
                                          <td><a href="?task=Leilao&action=editarAnuncioPage&anuncio=<?php echo $anuncio['id'];?>" class="btn-sm btn-primary" style="color:white;"><b>Editar<b></a></td>
                                          <td><?php echo $anuncio['negociacao'];?></td>
                                      </tr>
                                      <?php } ?>
                                </tbody>
                            </table>         
                              
                              
                          </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
</div>

<fieldset>

