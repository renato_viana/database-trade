<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<script src="libs/ckeditor/ckeditor.js"></script>

<script type="text/javascript">
function Trim(str){
  return str.replace(/^\s+|\s+$/g,"");
}

function validarForm(form){
 validate('f1');
   //checa a validacao
   if(validateState){
      // alert('validado');
      //$( "#f1" ).submit();
      form.submit();
    }
  }
  </script>
  <script>
  function moeda(z){  
    v = z.value;
    v=v.replace(/\D/g,"")  //permite digitar apenas números
  v=v.replace(/[0-9]{12}/,"inválido")   //limita pra máximo 999.999.999,99
  v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos últimos 8 digitos
  v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos últimos 5 digitos
  v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")  //coloca virgula antes dos últimos 2 digitos
    z.value = v;
  }
</script>

<fieldset>
<legend>Cadastrar Anúncio</legend>
<span style="color:red;"><p id="validate_message">&nbsp;</p></span>
</br>
  <div class="row">
    <div class="col-md-11">
      <form class="form-horizontal" role="form" enctype="multipart/form-data" name="f1" action="?task=Leilao&action=cadastrarAnuncioAction" method="post">
        <fieldset>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-sm-2 control-label" for="textinput">Nome do Anúncio</label>
            <div class="col-sm-10">
              <input type="text" placeholder="Nome do Anúncio" class="form-control" name="nome" required>
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-sm-2 control-label" for="textinput">Valor da Oferta</label>
            <div class="col-sm-2">
              <input type="text" placeholder="R$" class="form-control" name="oferta" required onKeyUp="moeda(this);">
            </div>

          </div>    

          <!-- Text input-->
          <div class="form-group">
            <label class="col-sm-2 control-label" for="frete">Expira em</label>
            <div class="col-sm-2">
              <input type="text" id='data_inicial' placeholder="dd/mm/aaaa" class="form-control" name="expiraem" required readonly value="<?php echo $Result['expiraem']; ?>">
            </div>            
          </div>                                 


          <!-- Text input-->
          <div class="form-group">
            <label class="col-sm-2 control-label" for="frete">Informações</label>
            <div class="col-sm-10">
             <textarea name="informacoes" id="InputMessage" class="form-control" rows="5" required></textarea>
              <script> 
              CKEDITOR.replace( 'informacoes', {
                toolbar: [
                { name: 'document' }, 
                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
                ],
                height: '200px',
                enterMode : CKEDITOR.ENTER_BR
              });
              </script>              
            </div>
          </div>  



          <!-- File Button --> 
          <div class="form-group">
            <label class="col-md-2 control-label" for="filebutton">Imagem</label>
            <div class="col-md-4">
              <input id="filebutton" name="imagem" class="input-file" type="file">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary">Salvar</button>
              </div>
            </div>
          </div>

        </fieldset>
      </form>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->


<fieldset>