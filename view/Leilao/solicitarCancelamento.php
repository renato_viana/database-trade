<script src="libs/ckeditor/ckeditor.js"></script>
<fieldset>
<legend>Solicitação de Cancelamento de Leilão</legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meuLeilaoAdminPage&leilao=<?php echo $_GET['leilao'];?>">&larr; Voltar</a></li>
</ul>


<div class="container">
	<div class="row">
      <div class="col-md-10">
        <div class="well well-sm">
          <form class="form-horizontal" action="?task=Leilao&action=enviarSolicitacaoCancelamentoAction" method="post">
          <fieldset>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Leilao</label>
              <div class="col-md-9">
                <input id="name" name="nome" type="text" placeholder="Your name" class="form-control" value="<?php echo $Result['leilao']['nome'];?>" readonly>
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Vendedor</label>
              <div class="col-md-9">
                <input id="email" name="vendedor" type="text" placeholder="Your email" class="form-control" value="<?php echo $Result['vendedor']['login'];?>" readonly>
              </div>
            </div>        
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="mensagem">Motivo do Cancelamento</label>
              <div class="col-md-9">
                <textarea class="form-control" id="mensagem" name="mensagem" placeholder="..." rows="5"></textarea>
                  <script> 
                  CKEDITOR.replace( 'mensagem', {
                    toolbar: [
                    { name: 'document' }, 
                    { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
                    ],
                    height: '200px',
                    enterMode : CKEDITOR.ENTER_BR
                  });
                  </script>                 
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-sm">Enviar</button>
              </div>
            </div>
          </fieldset>
          <input id="estande" name="leilao" type="hidden" value="<?php echo $Result['leilao']['id'];?>">
          </form>
        </div>
      </div>
	</div>
</div>

<fieldset>

