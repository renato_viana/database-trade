<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<fieldset>
<legend>Confirmar Conclusão do Leilão</legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meuLeilaoAdminPage&leilao=<?php echo $_GET['leilao'];?>">&larr; Voltar</a></li>
</ul>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <form method="post" action="?task=Leilao&action=mudarSituacaoConcluidoAction">
                <fieldset class="bs-callout bs-callout-danger">               
                <!-- Form Name -->
                <h3>Confirmação:</h3>
                <!-- Text input-->

               <div class="alert alert-success" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  Ao concluir o leilão a administração irá ver o processo de negociação para ver se ocorreu tudo como combinado e se tiver tudo
                  de acordo, os dois envolvido irão ganhar pontos de reputação positivas. E lembrando que uma vez finalizado o processo de reputação 
                  esta negociação do leilão não poderá ter sua situação alterada novamente.
                </div>  

                <center>
                <div class="control-group">
                  <div class="" style="margin-top:3px;margin-bottom:3px;">
                    <button id="salvar" name="salvar" class="btn btn-primary" value="submit">Confirmar Conclusao</button>
                  </div>                   
                </div>
              </center>

                </fieldset>
                <input type="hidden" name="leilao" value="<?php echo $_GET['leilao'];?>">
              </form>
      </div><!--\div col-md6 col-sm-12 -->

</div>


<fieldset>