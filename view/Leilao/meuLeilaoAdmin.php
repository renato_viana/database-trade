<script src="libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

function mudarSituacao(){
      situacao = document.getElementById("situacao").value;
      leilao = document.getElementById("leilao").value;
      if(situacao == 2)
          url = "index.php?task=Leilao&action=mudarSituacaoConfirmarPage&leilao="+leilao+"&situacao="+situacao;
      if(situacao == 3)
          url = "index.php?task=Leilao&action=mudarSituacaoConcluirPage&leilao="+leilao+"&situacao="+situacao;        
      if(situacao == 4)
          url = "index.php?task=Leilao&action=cancelarAction&leilao="+leilao+"&situacao="+situacao;                
      window.location.assign(url);
}

</script>
  <script>
  function moeda(z){  
    v = z.value;
    v=v.replace(/\D/g,"")  //permite digitar apenas números
  v=v.replace(/[0-9]{12}/,"inválido")   //limita pra máximo 999.999.999,99
  v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos últimos 8 digitos
  v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos últimos 5 digitos
  v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")  //coloca virgula antes dos últimos 2 digitos
    z.value = v;
  }
</script>

<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<fieldset>
<legend>Administrar Leilão</legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=meusLeiloesPage">&larr; Voltar</a></li>
</ul>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                  <div style="float:right;">
                      <div class="form-group">
                        <div class="col-md-12">
                          <form>
                          <?php if( $Result['leilao']['status-id'] == 2 || $Result['leilao']['status-id'] == 1 || $Result['leilao']['status-id'] == 4 || $Result['leilao']['status-id'] == 5 || $Tvar->verificarAdmin()){ ?>    
                          <select name="situacao" class="form-control" onchange="mudarSituacao()" id="situacao">  
                            <option value="" selected="selected"> Mudar Situação </option>   
                            <option value="2"> Ativo </option>  
                            <option value="4"> Cencelado </option>  
                          </select>   
                          <?php } ?>
                          <input type="hidden" value="<?php echo $_GET['leilao']?>" name="leilao" id="leilao">
                         </form>
                        </div>
                      </div>
                  </div>              

                <!-- Text input-->

                <div class="control-group">
                  <b>Situação Atual:</b> <?php echo $Result['leilao']['status'];?><br>
                  <b>Leilão publicado:</b> <a href="?task=Leilao&action=leilaoPage&leilao=<?php echo $_GET['leilao']?>">clique aqui</a><br>
                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

</div>

<div class="row">

      <div class="col-md-12 col-xs-12">
        
                <fieldset class="bs-callout bs-callout-warning">  



            <fieldset>
                <div style="float:right;margin-top:-30px;">
               </div>   
               
               <?php if($Result['vencedor'] == 10){ ?>
                <h4><b>R$ <?php echo number_format($Result['ultimo-lance']['valor'], 2, ',', '.');?></b><br><small> <b>Vencedor: <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['ultimo-lance']['usuario']['id'];?>"><?php echo $Result['ultimo-lance']['usuario']['login'];?></a></b></small></h4>
  

              <?php }else{ ?>          
                              <h4> <b>Sem Vencedor</b></h4>
              <?php } ?>          



            </fieldset>
        
      </fieldset>            
        </div>
</div>

<div class="row">
      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                <!-- Form Name -->
          
                <div style="width:350px;margin:0 auto;">
                    <?php if(file_exists('upload/leilao/'.$Result['leilao']['id'].".".$Result['leilao']['imagem'])){?>
                        <a target="_blank" href="<?php echo 'upload/leilao/'.$Result['leilao']['id'].".".$Result['leilao']['imagem'];?>"><img src="<?php echo 'upload/leilao/thumb/'.$Result['leilao']['id'].".".$Result['leilao']['imagem'];?>"></a>
                    <?php }else{ ?>
                        <a target="_blank" href="<?php echo 'upload/leilao/imagem.jpg';?>"><img src="<?php echo 'upload/leilao/thumb/imagem.jpg';?>"></a>
                    <?php }?>
                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-warning">  
                <!-- Text input-->
                 <b>Lance Inicial:</b> <?php echo number_format($Result['leilao']['lance_inicial-home'], 2, ',', '.');?><br>
                 <b>Incremento mínimo:</b> <?php echo number_format($Result['leilao']['incremento'], 2, ',', '.');?><br>
                 <b>Frete:</b> <?php echo number_format($Result['leilao']['frete'], 2, ',', '.');?><br>
                 <b>Data Inicio:</b>  <?php echo implode ( "/",array_reverse(explode("-",$Result['leilao']['data_inicio'])));?><br>
                 <b>Data Fim:</b>  <?php echo implode ( "/",array_reverse(explode("-",$Result['leilao']['data_fim'])))." ".$Result['leilao']['data_fim-hora'];?><br>
              </fieldset>
        </div>

      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-warning">
                <h3>Vendedor</h3>   
                <!-- Text input-->
                 <b>Login:</b> <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['login']['id'];?>"><?php echo $Result['login']['login'];?></a><br>
                 <b>Reputação:</b> <?php echo $Result['login']['reputacao'];?><br>
                 <b>Estado:</b> <?php echo $Result['login']['cidade']." - ".$Result['login']['estado'];?><br>
                 <b>Registaro em:</b> <?php echo  implode ( "/",array_reverse(explode("-",$Result['login']['data_cadastro'])));?><br>               
              </fieldset>
        </div>
</div>


<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                  <?php if($Result['proprietario']['id'] != NULL){ ?>
                  <div style="float:right;">
                      <div class="form-group">
                        <div class="col-md-12">
                          <form>
                          <?php if( $Result['negociacao'][0]['status-id'] != 10 ){ ?>    
                          <select name="situacao" class="form-control" onchange="mudarSituacao()" id="situacao">  
                            <option value="" selected="selected"> Mudar Situação </option>  
                            <option value="1"> Pendente </option>  
                            <option value="2"> Ativo </option>  
                            <option value="3"> Concluido </option>  
                            <option value="4"> Cencelado </option>  
                          </select>   
                          <?php } ?>
                          <input type="hidden" value="<?php echo $_GET['negociacao']?>" name="negociacao" id="negociacao">
                         </form>
                        </div>
                      </div>
                  </div>
                  <?php } ?>                  
                <!-- Form Name -->
                <h3>Informações</h3>
                <!-- Text input-->

                <div class="control-group">


                                <?php echo $Result['leilao']['informacoes']; ?>

                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

</div>


<a name="lances"></a>
<div class="row">

      <div class="col-md-12 col-xs-12">
        
                <fieldset class="bs-callout bs-callout-warning">  
        <div class="comentarios">

            <fieldset>
              <legend>Lances</legend>
 
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Comprador</th>
                                      <th>Valor do Lance</th>
                                      <th>Data</th>                                      
                                      <th></th>
                                     </thead>            
                                <tbody >
                                        <?php 
                                        for ($i=0; $i < count($Result['lances']); $i++) { 
                                          $lance    = $Result['lances'][$i];  
                                          $usuario  = $lance['usuario'];
                                         ?>
                                      <tr>
                                          <td><a href="?task=Usuario&action=perfilPage&id=<?php echo $usuario['id'];?>"><?php echo $usuario['login'];?></a></td>
                                          <td>R$ <?php echo number_format($lance['valor'], 2, ',', '.');?></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-", $lance['data'])))." ".$lance['data-hora'];?></td>
                                          <td></td>
                                      </tr>
                                      <?php } ?>
                                </tbody>
                            </table>         
                              
                              
                          </div>

            </fieldset>
        </div>
      </fieldset>            
        </div>
</div>
<fieldset>