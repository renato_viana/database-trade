<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Anúncios</legend>

<style type="text/css">
#mytable a{
  color: black;
}
</style>
<style type="text/css">
/**** LAYOUT ****/
.list-inline>li {
    padding: 0 10px 0 0;
}
.container-pad {
    padding: 30px 15px;
}


/**** MODULE ****/
.bgc-fff {
    background-color: #fff!important;
}
.box-shad {
    -webkit-box-shadow: 1px 1px 0 rgba(0,0,0,.2);
    box-shadow: 1px 1px 0 rgba(0,0,0,.2);
}
.brdr {
    border: 1px solid #ededed;
}

/* Font changes */
.fnt-smaller {
    font-size: .9em;
}
.fnt-lighter {
    color: #bbb;
}

/* Padding - Margins */
.pad-10 {
    padding: 10px!important;
}
.mrg-0 {
    margin: 0!important;
}
.btm-mrg-10 {
    margin-bottom: 10px!important;
}
.btm-mrg-20 {
    margin-bottom: 20px!important;
}

/* Color  */
.clr-535353 {
    color: #535353;
}




/**** MEDIA QUERIES ****/
@media only screen and (max-width: 991px) {
    #property-listings .property-listing {
        padding: 5px!important;
    }
    #property-listings .property-listing a {
        margin: 0;
    }
    #property-listings .property-listing .media-body {
        padding: 10px;
    }
}

@media only screen and (min-width: 992px) {
    #property-listings .property-listing img {
        max-width: 180px;
    }
}

</style>
<?php 
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>

  <div class="row">
    <div class="col-md-5">  
      <div style="float:right;">

          <form class="navbar-form navbar-left" role="search" method="post" action="?task=Leilao&action=defaultPage">
          <div class="form-group">
            <input name="nome" type="text" class="form-control" placeholder="Conteúdo do Leilão" style="width:250px;">
          </div>
          <button type="submit" class="btn btn-default">Pesquisar</button>
         </form> 

        </div>
     </div>
  </div>

<div class="row">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Leilao&action=defaultPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Leilao&action=defaultPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Leilao&action=defaultPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>

<!-- Layout Anuncios-->
    <div class="container-fluid" >
        <div class="container container-pad" id="property-listings" style="margin-left:0px;">
            
            <div class="row">
                <div class="col-sm-10"> 
                  <?php 
                    $pergunta = $Result['anuncio'];
                    $n = count($pergunta);
                    $n = ($n >= 10) ? 10 : $n;            
                    for($i=$paginacao->primeiro_registro;$i<$paginacao->primeiro_registro+$n;$i++){
                    if($Result['anuncio'][$i]['id'] != ""){
                    $anuncio  = $Result['anuncio'][$i];  
                   ?>
                    <!-- Begin Listing: 609 W GRAVERS LN-->
                    <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
                        <div class="media">
                            <?php if(file_exists('upload/anuncio/'.$anuncio['id'].".".$anuncio['img'])){?>
                                <a  class="pull-left" target="_blank" href="?task=Leilao&action=anuncioPage&anuncio=<?php echo $anuncio['id'];?>"><img  class="img-responsive"  src="<?php echo 'upload/anuncio/thumb/'.$anuncio['id'].".".$anuncio['img'];?>"></a>
                            <?php }else{ ?>
                                <a  class="pull-left" target="_blank" href="?task=Leilao&action=anuncioPage&anuncio=<?php echo $anuncio['id'];?>"><img  class="img-responsive"  src="<?php echo 'upload/anuncio/thumb/imagem.jpg';?>"></a>
                            <?php }?>                            

                            <div class="clearfix visible-sm"></div>

                            <div class="media-body fnt-smaller">
                                <a href="#" target="_parent"></a>

                                <h4 class="media-heading">
                                  <a href="?task=Leilao&action=anuncioPage&anuncio=<?php echo $anuncio['id'];?>" target="_parent">R$ <?php echo  number_format($anuncio['oferta'], 2, ',', '.');?> <small class="pull-right">Até <?php echo  $anuncio['expiraem'];?></small></a></h4>


                                <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                    <li>Vendedor: <a href="?task=Usuario&action=perfilPage&id=<?php echo $anuncio['dono']['id'];?>"><?php echo $anuncio['dono']['login'];?></a></li>
                                </ul>

                                <p class="hidden-xs"><?php echo $anuncio['informacoes'];?></p>
                            </div>
                        </div>
                    </div><!-- End Listing-->
                    <?php }} ?>
                </div><!-- End Col -->
            </div><!-- End row -->
        </div><!-- End container -->
    </div>


<fieldset>

