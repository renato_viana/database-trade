  <script src="libs/ckeditor/ckeditor.js"></script>

  <div class="header-bar">
  <div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
    <h3>Cadastrar Carta</h3>
  </div> 

<form class="form-horizontal" method="post" enctype="multipart/form-data" action="?task=AdminCarta&action=admCartaAction">

  <div id="img" style="margin:0 auto;width:100px;">
    <table>
        <tr>
            <td style="padding-right:10px;"><?php echo $Result['img'];?></td>
            <td><?php echo $Result['pequena'];?></td>
        </tr>
    </table>
  </div>
  <br>
<fieldset>

<!-- files -->
<div class="form-group">
  <label class="col-md-4 control-label" for="tipo_carta">Imagem</label>
  <div class="col-md-4">
    <input type="file" name='img' size='40' min="5"/>
  </div>
</div>


<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="tipo_carta">To Search</label>
  <div class="col-md-4">
    <?php echo $FORM->tosearch;?>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="tipo_carta">Tipo Carta</label>
  <div class="col-md-4">
    <?php echo $FORM->tipo;?>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="tipo_acao">Tipo de Acao</label>
  <div class="col-md-4">
    <?php echo $FORM->tipo_acao;?>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nome">Nome</label>  
  <div class="col-md-5">

    <textarea name="nome2"><?php echo $Result['nome'];?></textarea>

    <script> 
    CKEDITOR.replace( 'nome2', {
      toolbar: [
      { name: 'document' }, 
      { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
      ],
      height: '80px',
      enterMode : CKEDITOR.ENTER_BR
    });
    </script> 
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="alter_ego">Alter Ego</label>  
  <div class="col-md-4">
    <?php echo $FORM->alter_ego;?>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="energia_inicial">Energia</label>  
  <div class="col-md-2">
  <?php echo $FORM->energia_inicial;?>
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="escudo">Escudo</label>  
  <div class="col-md-2">
  <?php echo $FORM->escudo;?>
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="afiliacao">Colecao</label>
  <div class="col-md-4">
    <?php echo $FORM->colecao;?>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="afiliacao">Afiliacao</label>
  <div class="col-md-4">
      <?php echo $FORM->afiliacao;?>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="numero">Numero</label>  
  <div class="col-md-2">
  <?php echo $FORM->numero;?>
    
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="raridade">Raridade</label>
  <div class="col-md-4">
    <?php echo $FORM->raridade;?>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="afiliacao">Alinhamento</label>
  <div class="col-md-4">
    <?php echo $FORM->alinhamento;?>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="texto_permanente">Texto Permanente</label>
  <div class="col-md-4">   
    <div style="width:650px;">                  
    <textarea class="form-control" id="texto_permanente" name="texto_permanente" style=""><?php echo $Result['texto_permanente'];?></textarea>
    </div>
    <script>
       CKEDITOR.replace( 'texto_permanente' );
    </script>     
  </div>
</div>

<!-- Multiple Checkboxes -->
<div class="form-group">
  <label class="col-md-4 control-label" for="habilidade">Habilidade</label>
  <div class="col-md-4">  
      <?php 
      $n = count($FORM->habilidades);
      for ($i=0; $i < $n; $i++) { 
          echo $FORM->habilidades[$i];
      }
      ?>
  </div>		
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="salvar"></label>
  <div class="col-md-4">
    <button id="salvar" name="salvar" class="btn btn-primary">salvar</button>
  </div>
</div>

</fieldset>

<?php echo $FORM->idcarta;?>

</form>
<br><br><br>
</div> 
