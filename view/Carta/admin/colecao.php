 <script type="text/javascript">

  function salvarForm(acao){
    
      obj = document.forms["f1"];
      obj.acao.value = acao;    
      obj.submit();
  }
  </script>

  <div class="header-bar">
  <div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
    <h3>Cadastrar Colecao</h3>
  </div>          

  <div id="form-usuario" style='margin:0 auto; width:600px;margin-top:-20px;'>
    <form id="f1" id="f1"  onSubmit="return false" method="post" action="?task=AdminCarta&action=admColecaoAction" enctype="multipart/form-data">

      <table class='tb-form-usuario' border='0'>
        <tr>
          <td width="100px;">Nome da colecao:</td>
          <td class='tb-input'><input type="text" name='nome' size='40' min="5" value='<?php echo $Result['colecao']['nome'];?>'/></td>
        </tr>  
        <tr>
          <td width="100px;">Sigla da colecao:</td>
          <td class='tb-input'><input type="text" name='sigla' size='40' min="5" value='<?php echo $Result['colecao']['sigla'];?>'/></td>
        </tr>    
        <tr>
          <td width="100px;">Imagem da colecao:</td>
          <td class='tb-input'><input type="file" name='img' size='40' min="5"/></td>
        </tr>             
        <tr>
          <td class='tb-label'></td>
          <td class='tb-input'>
            <a href="javascript:salvarForm('salvar');"><span class="label label-primary">Salvar</span></a>  
            <a href="javascript:salvarForm('deletar');" onClick="return confirm('Tem certeza que quer excluir?')"><span class="label label-primary">Deletar</span></a>
            <a href="?task=AdminCarta&action=admColecaoPage"><span class="label label-primary">Novo</span></a>    
          </td>
        </tr>                           
      </table> 
      <input type='hidden' name='colecao' value='<?php echo $Result['colecao']['id'];?>'/> 
      <input type='hidden' name='acao' value=''/> 
    </form>
  </div>

<div id="imagem" style="margin: 0 auto;width:200px;"><br>
  <img src="<?php echo "img/colecoes/".$Result['colecao']['sigla'].".png" ?>" />
</div>

  <fieldset>
    <legend>Colecoes</legend>

    <table class="table table-hover" style="width:600px;margin:0 auto;">
      <tbody>
        <tr class="info">
          <th>Nome</th>
          <th>Sigla</th>
          <th>Acoes</th>
        </tr>
      </tbody>
      <?php 
      $n = count($Result['colecoes']);
      for ($i=0; $i < $n; $i++) { 
      
      ?>
      <tbody><tr>
        <th><?php echo $Result['colecoes'][$i]['nome'];?></th>
        <th><?php echo $Result['colecoes'][$i]['sigla'];?></th>
        <th>
            <a class="btn btn-info" type="button" href="?task=AdminCarta&action=admColecaoPage&colecao=<?php echo $Result['colecoes'][$i]['id'];?>">Editar</a>
            <a class="btn btn-info" type="button" href="?task=AdminCarta&action=deletarColecaoAction&colecao=<?php echo $Result['colecoes'][$i]['id'];?>" onClick="return confirm('Tem certeza que quer excluir?')">deletar</a>
        </th>

      </tr> 
      <?php } ?>        

    </table>


  </fieldset>

  </div>