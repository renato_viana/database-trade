<!--<center><a href="http://mtcgstore.com.br/" target="_blank"><img src="img/banner/pre.jpg" width="500"></a></center><br>-->

<!--[if lt IE 9]>
  <script type="text/javascript" src="external/html5/js/html5shiv.js"></script>
<![endif]-->
  <script type="text/javascript" src="assets/ygo/js/jquery-1.7.2.js"></script>
  <script type="text/javascript" src="assets/ygo/js/jquery-ui-1.8.16.custom.min.js"></script>
  <script type="text/javascript" src="assets/ygo/js/jquery.dropkick-1.0.0.js"></script>
  <script type="text/javascript" src="assets/ygo/js/common.js"></script>

  <script type="text/javascript" src="assets/ygo/js/scrolltopcontrol.js"></script>


  <link type="text/css" rel="stylesheet" href="assets/ygo/css/ui-lightness/jquery-ui-1.8.16.custom.css">
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css">
  <link type="text/css" rel="stylesheet" href="assets/ygo/css/common.css">

  <link rel="stylesheet" type="text/css" href="assets/ygo/css/CardTop.css">
  <link rel="stylesheet" type="text/css" href="assets/css/CardSearchCondition.css">
  <script type="text/javascript">
  <!--
  $(function(){

    
    $('input:text').val('');
    //$('#alinhamento').val('1');
    $('#ctype').val('');
    $('input:checkbox').prop('checked', false);
    $('#othercon_and').prop('checked', false).closest('a').addClass('radio_off_left').removeClass('radio_on_left').find('img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_off.png');
    $('#othercon_or').prop('checked', true).closest('a').addClass('radio_on_right').removeClass('radio_off_right').find('img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_on.png');

    
    $('#afiliacao').dropkick({
      theme: 'stype_en'
    });   
    $('#afiliacao2').dropkick({
      theme: 'stype_en'
    });       
    $('#colecao').dropkick({
      theme: 'stype_en'
    });
    $('#raridade').dropkick({
      theme: 'stype_en'
    });    
    $('#tipo_de_carta').dropkick({
      change: function (value, label) {
        ChangeCtype(value, label);
      }
    });
    $('#ctype').dropkick({
      change: function (value, label) {
        ChangeCtype(value, label);
      }
    });
  });

  //-->
  </script>

<div style="width:1000px;">


      <script type="text/javascript">
      <!--

      $(function(){

        
        $('#nome[value=]').css('color', '#bbb').val('Nome da Carta');
        $('#nome').focus(function(){
          if ($(this).val() == 'Nome da Carta') {
            $(this).val('').css('color', '#555');
          }
        }).blur(function(){
          if ($(this).val() == ''){
            $(this).css('color', '#bbb').val('Nome da Carta');
          }
          if ($(this).val() != 'Nome da Carta') {
            $(this).css('color', '#555');
          }
        });

       $('#texto_permanente[value=]').css('color', '#bbb').val('Descrição da Carta');
        $('#texto_permanente').focus(function(){
          if ($(this).val() == 'Descrição da Carta') {
            $(this).val('').css('color', '#555');
          }
        }).blur(function(){
          if ($(this).val() == ''){
            $(this).css('color', '#bbb').val('Descrição da Carta');
          }
          if ($(this).val() != 'Descrição da Carta') {
            $(this).css('color', '#555');
          }
        });        

        
        $('.search_box_tips').hover(
          function(e){

            if ($(this).closest('table').css('opacity') != 1) {
              return false;
            }

            var tips_txt = $(this).attr('alt');
            $(this).after('<span id="search_box_tips"><span></span></span>');
            $('#search_box_tips span').text(tips_txt);

            var offset = $(this).offset();
            var pos_x = offset.left - 30;
            var pos_y = offset.top - 34;

            $('#search_box_tips').css({
              'position': 'absolute',
              'top': pos_y,
              'left': pos_x,
              'display': 'inline-block',
              'background': 'url(http://www.db.yugioh-card.com/yugiohdb/external/image/parts/search_box_tips_bk.png) right bottom no-repeat',
              'height': '35px',
              'padding-right': '4px'
            });

            $('#search_box_tips span').css({
              'display': 'inline-block',
              'background': 'url(http://www.db.yugioh-card.com/yugiohdb/external/image/parts/search_box_tips_bk.png) left top no-repeat',
              'line-height': '23px',
              'height': '35px',
              'padding': '0 5px 0 20px',
              'font-weight': 'bold',
              'font-size': '12px',
              'color': '#6a3807'
            });
          },
          function(e){
            if ($(this).closest('table').css('opacity') != 1) {
              return false;
            }
            $('#search_box_tips').remove();
          }
        );

        
        $('.attreffe, .species, .other').click(function(){
          if ($(this).hasClass('button_off')) {
            $(this).addClass('button_on').removeClass('button_off');
            $(':checkbox', this).prop('checked', true);
          } else {
            $(this).addClass('button_off').removeClass('button_on');
            $(':checkbox', this).prop('checked', false);
          }
        });
        $('.othercon_left').click(function(){
          if ($(this).hasClass('radio_off_left')) {
            $(this).addClass('radio_on_left').removeClass('radio_off_left');
            $(':radio', this).prop('checked', true);
            $($(this).next()).addClass('radio_off_right').removeClass('radio_on_right');
            $(':radio', $(this).next()).prop('checked', false);
            $('img', this).attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_on.png');
            $('.othercon_right img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_off.png');
          }
        });
        $('.othercon_right').click(function(){
          if ($(this).hasClass('radio_off_right')) {
            $(this).addClass('radio_on_right').removeClass('radio_off_right');
            $(':radio', this).prop('checked', true);
            $($(this).prev()).addClass('radio_off_left').removeClass('radio_on_left');
            $(':radio', $(this).prev()).prop('checked', false);
            $('img', this).attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_on.png');
            $('.othercon_left img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_off.png');
          }
        });

        
        $('#condition_toggle').click(function(){
          $('#search_by_attack_and_defense_and_level, #search_by_attribute, #search_by_effect, #search_by_species, #search_by_other').toggle();
        });

        
        $('#form_search input:text').keypress(function(e){
          if (e.keyCode == 13) {
            Search();
          }
        });

      });

      
      function ChangeCtype(value, label) {
        
        switch (value) {

        case '':
        case '10':
          $('#search_by_alinhamento .search_body table').find(':input').prop('disabled', false);
          $('#search_by_alinhamento .search_body table').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);

          $('#search_by_effect .search_body table input:checkbox[value=21]').prop('disabled', false);
          $('#search_by_effect .search_body table input:checkbox[value=21]').closest('td').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);
          $('#search_by_effect .search_body table input:checkbox:not([value=20],[value=21],[value=24])').prop('disabled', false);
          $('#search_by_effect .search_body table input:checkbox:not([value=20],[value=21],[value=24])').closest('td').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);

          if (value == '') {
            $('#search_by_effect .search_body table').find(':input').prop('disabled', false);
            $('#search_by_effect .search_body table').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);
          } else {
            $('#search_by_effect .search_body table').find(':input').prop('disabled', true);
            $('#search_by_effect .search_body table').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);
          }

          $('#search_by_species .search_body table').find(':input').prop('disabled', false);
          $('#search_by_species .search_body table').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);;

          $('#search_by_other .search_body table').find(':input').prop('disabled', false);
          $('#search_by_other .search_body table').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);;
          break;

        case '20':
        case '30':
          $('#search_by_attack_and_defense_and_level .search_body table').find(':input').prop('disabled', true);
          $('#search_by_attack_and_defense_and_level .search_body table').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);

          $('#search_by_attribute .search_body table').find(':input').prop('disabled', true);
          $('#search_by_attribute .search_body table').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);

          $('#search_by_effect .search_body table').find(':input').prop('disabled', false);
          $('#search_by_effect .search_body table').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);
          $('#search_by_effect .search_body table input:checkbox[value=21]').prop('disabled', false);
          $('#search_by_effect .search_body table input:checkbox[value=21]').closest('td').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);
          $('#search_by_effect .search_body table input:checkbox:not([value=20],[value=21],[value=24])').prop('disabled', false);
          $('#search_by_effect .search_body table input:checkbox:not([value=20],[value=21],[value=24])').closest('td').fadeTo(0, 1.0).find('img').fadeTo(0, 1.0);

          $('#search_by_species .search_body table').find(':input').prop('disabled', true);
          $('#search_by_species .search_body table').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);

          $('#search_by_other .search_body table').find(':input').prop('disabled', true);
          $('#search_by_other .search_body table').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);

          if (value == '2') {
            $('#search_by_effect .search_body table input:checkbox[value=21]').prop('disabled', true);
            $('#search_by_effect .search_body table input:checkbox[value=21]').closest('td').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);
          } else {
            $('#search_by_effect .search_body table input:checkbox:not([value=20],[value=21],[value=24])').prop('disabled', true);
            $('#search_by_effect .search_body table input:checkbox:not([value=20],[value=21],[value=24])').closest('td').fadeTo(0, 0.2).find('img').fadeTo(0, 0.2);
          }

          break;

        default:
          break;
        }
      }

      
      function Search() {
        if ($('#nome').css('color') == '#bbb' || $('#nome').css('color') == 'rgb(187, 187, 187)') {
          $('#nome').val('');
        }
        
        form = $('#form_search').get(0);
        if(form.texto_permanente.value == "Descrição da Carta") form.texto_permanente.value = "";
        form.action = "?task=Carta&action=listarPage";
        
        form.submit();
      }


      //-->
      </script>
      <table width="1000" align="center" class="buscador">
        <tr><td>
      <form id="form_search" action="card_search.action" method="POST">
        <div id="search_box">

          <div id="search_by_keyword_and_type" style="padding:5px 8px;">
            <table style="">
 
              <tr valign="middle">
                <td>
                  <img src="assets/ygo/img/icon_search.png" alt="Search">
                </td>
                <td valign="top">
                  <input type="text" id="nome" name="nome" style="width:350px;height:32px" maxlength="50">
                </td>
                <td valign="top">
                  <input type="text" id="texto_permanente" name="texto_permanente" style="width:400px;height:32px" maxlength="50">
                </td>                
                <td valign="top">
                    <?php echo $FORM->tipo;?>
                </td>
                <td>
                  
                </td>
              </tr>
            </table>
          </div>

          <div id="search_by_keyword_and_type" style="padding:5px 8px;">
            <table style="">
 
              <tr valign="middle">
                <td width="34">
                  
                </td>
                <td valign="top">
                  <?php echo $FORM->colecao;?>
                </td>
                <td valign="top">
                  <?php echo $FORM->raridade;?>
                </td>                
                <td valign="top">

                </td>
                <td>
                  
                </td>
              </tr>
            </table>
          </div>          

          <div id="condition_toggle">
            Filtro
          </div><!--#condition_toggle-->

          <div id="search_by_afiliacao" style="border-bottom:solid 1px #414a56;" >
            <div class="search_body" style="height:43px;">
              <table>
                <tr>
                  <td>
                    <span id="level_rank_title">
                      <b>Afilicão I</b>
                    </span>
                    <span>
                        <?php echo $FORM->afiliacao;?>
                    </span>
                    <!--
                    <span id="attack_title">
                      <b>Afilicão II</b>
                    </span>
                    <span id="attack_input">
                      <?php echo $FORM->afiliacao2;?>
                    </span>
                    <span id="pen_scale_title">
                    </span>
                  -->
                  </td>
                </tr>
              </table>
            </div><!--.search_body-->
          </div><!--#search_by_attack_and_defense_and_level-->          

          <div id="search_by_energia_escudo" style="border-bottom:solid 1px #414a56;" >
            <div class="search_body">
              <table>
                <tr>
                  <td>
                    <span id="level_rank_title">
                      <b>Energia </b><img src="img/icon/energia.png" width="21"/>
                    </span>
                    <span>
                      <input type="text" id="starfr" name="energia_inicial" maxlength="1">
                      <input type="text" id="starto" name="energia_fim"  maxlength="1">
                    </span>
                    <span id="attack_title">
                      <b>Escudo </b><img src="img/icon/escudo.png" width="21"/>
                    </span>
                    <span id="attack_input">
                      <input type="text" id="starfr" name="escudo_inicial" maxlength="1">
                      <input type="text" id="starto" name="escudo_fim" maxlength="1">
                    </span>
                    <span id="pen_scale_title">
                    </span>
                  </td>
                </tr>
              </table>
            </div><!--.search_body-->
          </div><!--#search_by_attack_and_defense_and_level-->

          <div id="search_by_alinhamento" style="margin-bottom:-7px;border-top:solid 1px #90a6b0;" >
            <div class="search_body">
              <table style="margin:5px 0px 5px 0px;">
                <col width="110">
                <col width="*">
                <tr valign="top">
                  <td>
                    <b>Alinhamento</b>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off">
  
                      <div style="background:url(external/image/parts/attribute/attribute_icon_dark.png) 8px no-repeat;">
                        <span style="padding-left:23px;">
                          Heroi
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="alinhamento[]" class="none" value="1">
                    </a>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off">
  
                      <div style="background:url(external/image/parts/attribute/attribute_icon_light.png) 8px no-repeat;">
                        <span style="padding-left:23px;">
                          Vilão
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="alinhamento[]" class="none" value="2">
                    </a>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off">
  
                      <div style="background:url(external/image/parts/attribute/attribute_icon_earth.png) 8px no-repeat;">
                        <span style="padding-left:23px;">
                          N/S
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="alinhamento[]" class="none" value="500">
                    </a>
                  </td>

                </tr>
              </table>
            </div><!--.search_body-->
          </div><!--#search_by_attribute-->



          <div id="search_by_effect" style="border-bottom:solid 1px #414a56;border-top: 1px solid #90A6B0;" >
            <div class="search_body" style="">
              <table style="margin-top: 5px;">
                <col width="110">
                <col width="*">
                <tr valign="top">
                  <td>
                    <b>Tipo de Ação</b>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_1_en">
  
                      <div>
                        <span style="padding-left:23px;">
                          Normal
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="tipo_acao[]" class="none" value="1">
                    </a>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_2_en">
  
                      <div style="background:url(img/icon/antecipar.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Antecipar
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="tipo_acao[]" class="none" value="2">
                    </a>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_3_en">
  
                      <div style="background:url(img/icon/imprevista.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Imprevista
                        </span>
                      </div>
                      <input type="checkbox" name="tipo_acao[]" class="none" value="3">
                    </a>
                  </td>

                </tr>
              </table>
            </div><!--.search_body-->
          </div><!--#search_by_effect-->


          <div id="search_by_effect" style="border-bottom:solid 1px #414a56;border-top: 1px solid #90A6B0;" >
            <div class="search_body" style="">
              <table style="margin-top: 5px;">
                <col width="110">
                <col width="*">
                <tr valign="top">
                  <td>
                    <b>Poderes</b>
                  </td>

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_1_en">
  
                      <div style="background:url(img/icon/1.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Agilidade
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="1">
                    </a>
                  </td>

                  <td>
                    <a style="width:160px;" href="javascript:void(0);" class="attreffe button_off effect_2_en">
  
                      <div style="background:url(img/icon/2.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Ataque a Distância
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="2">
                    </a>
                  </td>

                  <td>
                    <a style="width:160px;" href="javascript:void(0);" class="attreffe button_off effect_3_en">
  
                      <div style="background:url(img/icon/3.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Ataque Energetico
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="3">
                    </a>
                  </td>

                  <td>
                    <a style="width:120px;" href="javascript:void(0);" class="attreffe button_off effect_4_en">
  
                      <div style="background:url(img/icon/4.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Elasticidade
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="4">
                    </a>
                  </td>

                  <td>
                    <a style="width:120px;" href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/5.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Genialidade
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="5">
                    </a>
                  </td>
                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_6_en">
  
                      <div style="background:url(img/icon/6.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Lâminas
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="6">
                    </a>
                  </td>                  
                  </tr><tr>
                  <td></td>

                  <td>
                    <a style="width:120px;" href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/7.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Regeneração
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="7">
                    </a>
                  </td>

                  <td>
                    <a style="width:120px;" href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/8.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Super Força
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="8">
                    </a>
                  </td>   

                  <td>
                    <a  href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/9.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Telecinesia
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="9">
                    </a>
                  </td>   

                  <td>
                    <a  href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/10.png) 8px no-repeat">

                        <span style="padding-left:23px;">
                          Telepatia
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="10">
                    </a>
                  </td>    

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/11.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Vôo
                        </span>
                      </div>
  
  
                      <input type="checkbox" name="habilidades[]" class="none" value="11">
                    </a>
                  </td>  

                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url(img/icon/12.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Magia
                        </span>
                      </div>
  
                      <input type="checkbox" name="habilidades[]" class="none" value="12">
                    </a>
                  </td>    
                </tr><tr>
                  <td></td>
                  <td>
                    <a style="width:160px;" href="javascript:void(0);" class="attreffe button_off effect_2_en">

                      <div style="background:url(img/icon/13.png) 8px no-repeat">
                        <span style="padding-left:23px;">
                          Poder Cósmico
                        </span>
                      </div>


                      <input type="checkbox" name="habilidades[]" class="none" value="13">
                    </a>
                  </td>                   
                  <td>
                    <a href="javascript:void(0);" class="attreffe button_off effect_5_en">
  
                      <div style="background:url('img/icon/0.png') 8px no-repeat">
                        <span style="padding-left:23px;">
                          N/S
                        </span>
                      </div>
  
                      <input type="checkbox" name="habilidades[]" class="none" value="0">
                    </a>
                  </td> 


                </tr>
              </table>
            </div><!--.search_body-->
          </div><!--#search_by_effect-->          
          <center><a href="javascript:Search();" class="btn btn-primary btn-sm"><b>Buscar</b></a></center>
      </form><!--#form_search-->

</div>

</br>

<style type="text/css">
#update-db{
  
}
.update-cards{
  float: left;
}
.update-decklists{
  float: right;
}
.content-card{
  margin: 5px;
}
.content-card img{
  margin-top:5px;
}
</style>

<div id="update-db">


<div class="panel panel-default update-cards" style="width:460px;">
  <div class="panel-heading">
    <h3 class="panel-title"><b>Últimas Cartas Cadastradas</b></h3>
  </div>
  <div class="panel-body">
            <table>
            <tr>
            <?php 
            $n = count($Result['last_cards']);
            //print_r($Result['last_cards'][0]);
            for ($i=0; $i < $n; $i++) { 
            ?>
            <td>
                    <div class="content-card" align="center">
                      <center>
                      <a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['last_cards'][$i]['id'];?>"><img src="<?php echo "img/cartas/".$Result['last_cards'][$i]['sigla']."/thumb/".$Result['last_cards'][$i]['numero'].".png" ;?>" /></a>
                    </center>
                    </div>  
            </td>
            <?php } ?>                                                                 
            </tr>
        </table>
  </div>
</div>

<div class="panel panel-default update-decklists" style="width:460px;">
  <div class="panel-heading">
    <h3 class="panel-title"><b>Últimas Deck Lists</b></h3>
  </div>
  <div class="panel-body">
            <table width="100%">
            <tr>
            <th align="left">Autor</th>                                                      
            <th align="left">Deck</th>
            <th align="left">Data</th>
            </tr>
            <?php 
            $n = count($Result['last_decks']);
            for ($i=0; $i < $n; $i++) { 
            ?>             
            <tr height="28" style="border-bottom:1px solid #CCC;">             
            <td align="left">
                <a href="?task=Deck&action=deckPage&deck=<?php echo $Result['last_decks'][$i]['id'];?>"><?php echo $Result['last_decks'][$i]['usuario_nome'];?></a>
            </td>                                                      
            <td align="left">
                <a href="?task=Deck&action=deckPage&deck=<?php echo $Result['last_decks'][$i]['id'];?>"><?php echo $Result['last_decks'][$i]['deck_nome'];?></a>
            </td>   
            <td align="left">
               <a href="?task=Deck&action=deckPage&deck=<?php echo $Result['last_decks'][$i]['id'];?>"><?php echo $Result['last_decks'][$i]['criado'];?></a>
            </td>
            </tr>  
            <?php } ?>               
        </table>
  </div>
</div>

</div>
    </td></tr>
</table>
        </br></br>
