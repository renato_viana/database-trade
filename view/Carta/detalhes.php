<style type="text/css">
.container{
  margin-left: -200px;
  margin-top: 20px;
}
.panel{
  width: 850px;
}
</style>

<div class="header-bar" style="margin-bottom:50px;">
<div>

<div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
  <h3>Detalhes - <?php echo $Result[0]['nome'];?></h3>
</div> 

<?php if($Login['tipo_usuario'] == 1){ ?>
<center>
<div style="width:93px;">
  <table>
      <tr>
        <td><a class="btn btn-info" type="button" href="?task=AdminCarta&action=admCartaPage&carta=<?php echo $_GET['carta'];?>">Editar</a></td>
        <td><a class="btn btn-info" type="button" href="?task=AdminCarta&action=admDeleteCartaAction&carta=<?php echo $_GET['carta'];?>" onClick="return confirm('Tem certeza que quer excluir?')">deletar</a></td>
      </tr>
  </table>
</div>
</center>  
<?php } ?>

<ul class="pager">
  <li class="previous"><a href="<?php echo "javascript:history.back(-2)";?>">&larr; Voltar</a></li>
</ul>

<div class="container">

  <div class="row">
    <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >


      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title"><b><?php echo $Result[0]['nome'];?></b></h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-3 col-lg-3 " align="center"> <img src="<?php echo "img/cartas/".$Result[0]['sigla']."/".$Result[0]['numero'].".png";?>" style="position: relative; display: inline;" alt="<?php echo $Result[0]['nome'];?>" title="<?php echo $Result[0]['nome'];?>" class="none ui-draggable"> </div>

            <div  style="margin-left:50px;float:left;"> 
              <table class="table table-user-information" style="width:500px;">
                <tbody>  
		 <?php if($Result['link'] != ""){ ?>
                  <tr>
                    <td>Valor da Carta</td>
                    <td><a href="<?php echo $Result['link'];?>" target="_blank">Clique aqui</a></td>
                  </tr>  
		 <?php } ?>                
                  <tr>
                    <td>Nome</td>
                    <td><?php echo $Result[0]['nome'];?></td>
                  </tr>                      
                  <tr>
                    <td>Tipo de Carta</td>
                    <td><?php echo $Result[0]['carta'];?></td>
                  </tr>  
                  <?php if($Result[0]['alinhamento'] != "NADA"){ ?>
                  <tr>
                    <td>Alinhamento:</td>
                    <td><?php echo $Result[0]['alinhamento'];?></td>
                  </tr>
                  <?php } ?>
                  <?php if ($Result[0]['idTipoCarta'] < 3){ ?>
                  <tr>
                    <td>Afiliação</td>
                    <td><?php echo $Result[0]['afiliacao'];?></td>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td>Coleção</td>
                    <td><a href="?task=Carta&action=listarColecaoPage&colecao=<?php echo $Result[0]['colecao_id'];?>"><?php echo $Result[0]['colecao'];?></a></td>
                  </tr>   
                  <tr>
                    <td>Raridade</td>
                    <td><?php echo $Result[0]['raridade'];?></td>
                  </tr>     
                  <?php if($Result[0]['acao'] != "NADA"){ ?>
                  <tr>
                    <td>Tipo de Ação</td>
                    <td><?php echo $Result[0]['acao'];?></td>
                  </tr>   
                   <?php } ?>  
                  <?php if ($Result[0]['idTipoCarta'] < 3){ ?>   
                  <tr>
                    <td>Energia <img src="img/icon/energia.png" width="20" /></td>
                    <td><?php echo $Result[0]['energia_inicial'];?></td>
                  </tr>   
                  <tr>
                    <td>Escudo <img src="img/icon/escudo.png" width="19" /></td>
                    <td><?php echo $Result[0]['escudo'];?></td>
                  </tr>     
                  <tr>
                    <td>Poderes</td>
                    <td>
                      <?php 
                      $n = count($Result['habilidade']);
                      for ($i=0; $i < $n; $i++) { ?>
                      <img alt="" src="<?php echo "img/icon/".$Result['habilidade'][$i]['habilidade_id'].".png"?>">
                      <?php }?>
                    </td>
                  </tr>                                                                                       
                  <?php } ?>
                  <tr>
                    <td>Texto da Carta</td>
                    <td><?php echo $Result[0]['texto_permanente'];?></td>
                  </tr>                  
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="panel-footer" style="padding-bottom:35px" >
        

    </div>

  </div>
</div>
</div>

</div>
</div>
