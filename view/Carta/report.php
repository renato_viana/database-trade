<link type="text/css" rel="stylesheet" href="assets/css/common.css">
<link rel="stylesheet" type="text/css" href="assets/css/CardDetail.css">

<script src="libs/ckeditor/ckeditor.js"></script>

<div class="header-bar">
<fieldset>
<legend>Report</legend> 

<div style="width:600px;margin:0 auto;">
<center><a class="btn btn-info" type="button" href="?task=Carta&action=detalhesPage&carta=<?php echo $_GET['carta'];?>">Voltar</a></center>
	
<div id="article_body" aling="center" style="border-bottom:1px solid #CCC;width:758px;margin-top:20px;" >
<table style="margin:0 8px 10px 8px;" border="0">
				<tbody><tr>
				<td style="padding-right:8px;" valign="top">
					<div id="card_frame_list">
<center>						<img src="<?php echo "img/cartas/".$Result['sigla']."/".$Result['numero'].".png";?>" style="position: relative; display: inline;width:80px;"></center>
					</div>
				</td>
				<td valign="top">

	
					<table id="details" border="0">
						<colgroup><col width="50%">
						<col width="50%">
						</colgroup><tbody>
						<tr>
							<td colspan="2">
								<div class="item_box t_center" style="margin-top:10px;">
		
								<center><a href="?task=Carta&action=detalhesPage&carta=<?php echo $_GET['carta'];?>"><b><?php echo $Result['nome'];?> - <?php if($_GET['type'] == 1) echo "Erratas"; else echo "Jogadas"; ?></b></a></center>
		
								</div>
							</td>
						</tr>					
					</tbody></table><!--#details--></table>
				
</div>

<div id="article_body" aling="center" style="border-bottom:1px solid #CCC;width:758px;margin-top:20px;padding-bottom:20px;" >

<!-- Place this in the body of the page content -->
<form method="post" action="?task=Carta&action=reportAction">
    <textarea name="texto"><?php echo $Result['report']['texto'];?></textarea>

    <script>
   		 CKEDITOR.replace( 'texto' );
    </script>    

<!-- Button -->
<div class="form-group"></br></br>
  <label class="col-md-4 control-label" for="salvar"></label>
  <div class="col-md-4">
    <button id="salvar" name="salvar" class="btn btn-primary" value="submit">Salvar</button>
  </div>
</div>
<input type="hidden" name="carta" value="<?php echo $_GET['carta'];?>">    
<input type="hidden" name="tipo" value="<?php echo $_GET['type'];?>">
<input type="hidden" name="report" value="<?php echo $_GET['report'];?>">
<input type="hidden" name="acao" value="salvar">
</form>
</br></br></br>

</div>

<div>
</fieldset>
</div>