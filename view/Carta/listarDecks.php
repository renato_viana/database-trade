<div class="header-bar">
<fieldset>
<legend>Detalhes</legend>  

<div id="article_body" aling="center" style="border:0px solid;width:758px;margin-top:20px;">
<table style="margin:0 8px 10px 8px;">
				<tbody><tr>
				<td style="padding-right:8px;" valign="top">
					<div id="card_frame">
						<img src="<?php echo "img/cartas/".$Result[0]['sigla']."/".$Result[0]['numero'].".png";?>" style="position: relative; display: inline;" alt="<?php echo $Result[0]['nome'];?>" title="<?php echo $Result[0]['nome'];?>" class="none ui-draggable">
					</div>
	
				</td>

				<td valign="top">

	
					<table id="details" border="0">
						<colgroup><col width="50%">
						<col width="50%">
						</colgroup><tbody>
						<tr>
							<td colspan="2">
								<div class="item_box t_center" style="margin-bottom:10px;">
									<span class="item_box_title">
										
									</span>
		
<b>									<?php echo $Result[0]['nome'];?></b>
		
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="item_box" style="margin-right:5px;">
									<span class="item_box_title">
										<b>Card</b>
									</span>
									<span class="item_box_value">
		
										<?php echo $Result[0]['carta'];?>
		
									</span>
								</div>
							</td>
							<?php if ($Result[0]['idTipoCarta'] < 3){ ?>
							<td>
								<div class="item_box" style="margin-left:5px;">
									<span class="item_box_title">
		
		
										<b>Alinhamento</b>
		
									</span>
									<span class="item_box_value">
										<?php echo $Result[0]['alinhamento'];?>
									</span>
								</div>
							</td>
							<?php } ?>
						</tr>
		
		
						<?php if ($Result[0]['idTipoCarta'] < 3){ ?>
						<tr>
							<td colspan="2">
								<div class="item_box t_center" style="margin-top:10px;">
									<span class="item_box_title">
										<b>Afiliacao</b>
									</span>
		
									<?php echo $Result[0]['afiliacao'];?>
		
								</div>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<td>
								<div class="item_box" style="margin-top:10px;margin-right:5px;">
									<span class="item_box_title">
										<b>Colecao</b>
									</span>
									<span class="item_box_value">
		
										<?php echo $Result[0]['colecao'];?>
		
									</span>
								</div>
							</td>
							<td>
								<div class="item_box" style="margin-left:5px;margin-top:10px">
									<span class="item_box_title">
		
		
										<b>Raridade</b>
		
									</span>
									<span class="item_box_value">
										<?php echo $Result[0]['raridade'];?>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="item_box t_center" style="margin-top:10px;">
									<span class="item_box_title">
										<b>Tipo de Acao</b>
									</span>
									<!--
		
									 --><?php echo $Result[0]['acao'];?><!--
		
		
		
		
									 -->
								</div>
							</td>
						</tr>
						<?php if ($Result[0]['idTipoCarta'] < 3){ ?>
						<tr>
							<td>
								<div class="item_box" style="margin-top:10px;margin-right:5px;">
									<span class="item_box_title">
										<b>Energia</b>
									</span>
									<span class="item_box_value">
										<?php echo $Result[0]['energia_inicial'];?>
									</span>
								</div>
							</td>
							<td>
								<div class="item_box" style="margin-top:10px;margin-left:5px;">
									<span class="item_box_title">
										<b>Escudo</b>
									</span>
									<span class="item_box_value">
										<?php echo $Result[0]['escudo'];?>
									</span>
								</div>
							</td>
						</tr>
						<?php } ?>
						<?php if ($Result[0]['idTipoCarta'] != 3){ ?>
						<tr>
							<td colspan="2">
								<div class="item_box t_center" style="margin-top:10px;">
									<span class="item_box_title">
										<b>Poderes</b>
									</span>
										<?php 
										$n = count($Result['habilidade']);
										for ($i=0; $i < $n; $i++) { ?>
											<img alt="" src="<?php echo "img/icon/".$Result['habilidade'][$i]['habilidade_id'].".png"?>">
										<?php }?>
								</div>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<td colspan="2">
								<div class="item_box_text" style="margin-top:10px;">
									<div class="item_box_title">
										<b>Texto da Carta</b>
									</div>
									</br>
									<?php echo $Result[0]['texto_permanente'];?>
		
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="item_box" style="margin-top:10px;margin-right:5px;">
									
										<a href="regras.php?cod=<?php echo $_GET['cod'];?>" style="color:blue;"><b>- Regras</b></a>
									
								</div>
							</td>
							<td>
								<div class="item_box" style="margin-left:5px;margin-top:10px">
		
										<a href="jogadas.php?cod=<?php echo $_GET['cod'];?>" style="color:blue;"><b>- Jogadas</b></a>
		
								</div>
							</td>
						</tr>						
					</tbody></table><!--#details-->
					</table>
</div>

</fieldset>
</div>