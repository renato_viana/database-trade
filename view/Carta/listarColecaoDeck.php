<link type="text/css" rel="stylesheet" href="assets/css/common.css">
<link rel="stylesheet" type="text/css" href="assets/css/CardDetail.css">

<style>
<!--
#right_bar{
  
}
.open-hand{
  width: 635px;
  margin: 0 auto;
  margin-top: 10px;
}
.comentarios{
  width: 600px;
  margin: 0 auto;
  margin-top: 35px;
}
#deck{
  width:620px;
  height:510px;
  margin-left:70px;
  margin-top:10px;
}
.selected_card{
  width:60px;
  height:83px;
  float:left;
  border:1px solid;
}
.img_deck{
  width:60px;
  height:83px;
}
#txt{
  margin-top:20px;padding-bottom:20px;
  
}
#sobre{
  margin-top:20px;padding-bottom:20px;
  width:550px;
}
.tool-box{
  width:180px;
  float: right;
}
.master{
  width: 600px;
  margin: 0 auto;
}
-->
</style>

<div class="header-bar" style="margin-bottom:50px;">

<fieldset>
<legend>Coleção - <?php echo $Result['colecao']['nome'];?></legend>

    <ul class="pager">
      <li class="previous"><a href="<?php echo "javascript:history.back(-2)";?>">&larr; Voltar</a></li>
    </ul>


<fieldset>
<legend>Lista de cards</legend>

<div id="deck" style="margin:0 auto;">
  <?php 

  if(isset($Result['mydeck'])){

    foreach($Result['mydeck'] as $row){
      ?>

      <div class="selected_card">
        <a href="?task=Carta&action=detalhesPage&carta=<?php echo $row['id'];?>"><img alt="" src="<?php echo "img/cartas/".$row['sigla']."/thumb/".$row['numero'].".png";?>" alt="<?php echo $row['nome'];?>" title="<?php echo $row['nome'];?>" class="img_deck" border="0"/></a>
      </div>
      <?php }} ?>
    </div>

</fieldset>


  <?php 
    
    if(count($Result['lista']) > 0){

  ?>

<fieldset>

<legend>Cartas Exclusivas</legend>

<?php for ($i=0; $i < count($Result['lista']); $i++) { ?>

<div id="article_body" aling="center" style="border-bottom:1px solid #CCC;width:758px;margin-top:20px;" >
<table style="margin:0 8px 10px 8px;" border="0">
        <tbody><tr>
        <td style="padding-right:8px;" valign="top">
          <div id="card_frame_list">
<center>            <a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['lista'][$i]['id'];?>"><img src="<?php echo "img/cartas/".$Result['lista'][$i]['sigla']."/".$Result['lista'][$i]['numero'].".png";?>" style="position: relative; display: inline;" alt="<?php echo $Result['lista'][$i]['nome'];?>" title="<?php echo $Result['lista'][$i]['nome'];?>" class="none ui-draggable"></a></center>
          </div>
        </td>
        <td valign="top">

  
          <table id="details" border="0">
            <colgroup><col width="50%">
            <col width="50%">
            </colgroup><tbody>
            <tr>
              <td colspan="2">
                <div class="item_box t_center" style="margin-top:10px;">
    
                <center><a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['lista'][$i]['id'];?>"><b><?php echo $Result['lista'][$i]['nome'];?></b></a></center>
    
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item_box" style="margin-top:10px;margin-right:5px;">
                  <span class="item_box_title">
                    <b>Coleção</b>
                  </span>
                  <span class="item_box_value">
    
                    <?php echo $Result['lista'][$i]['colecao'];?>
    
                  </span>
                </div>
              </td>
              <td>
                <div class="item_box" style="margin-left:5px;margin-top:10px">
                  <span class="item_box_title">
    
    
                    <b>Card</b>
    
                  </span>
                  <span class="item_box_value">
                    <?php echo $Result['lista'][$i]['carta'];?>
                  </span>
                </div>
              </td>
            </tr>
          </tbody></table><!--#details--></table>
</div>


<?php }?>
</fieldset>

<?php }?>
</div>

</fieldset>
</div>