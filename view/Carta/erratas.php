<link type="text/css" rel="stylesheet" href="assets/css/common.css">
<link rel="stylesheet" type="text/css" href="assets/css/CardDetail.css">

<div class="header-bar">
<fieldset>
<legend>Erratas</legend> 

<div style="width:600px;margin:0 auto;">
<center><a class="btn btn-info" type="button" href="?task=Carta&action=detalhesPage&carta=<?php echo $_GET['carta'];?>">Voltar</a></center>
	
<div id="article_body" aling="center" style="border-bottom:1px solid #CCC;width:758px;margin-top:20px;" >
<table style="margin:0 8px 10px 8px;" border="0">
				<tbody><tr>
				<td style="padding-right:8px;" valign="top">
					<div id="card_frame_list">
<center>						<img src="<?php echo "img/cartas/".$Result['sigla']."/".$Result['numero'].".png";?>" style="position: relative; display: inline;width:80px;"></center>
					</div>
				</td>
				<td valign="top">

	
					<table id="details" border="0">
						<colgroup><col width="50%">
						<col width="50%">
						</colgroup><tbody>
						<tr>
							<td colspan="2">
								<div class="item_box t_center" style="margin-top:10px;">
		
								<center><a href="?task=Carta&action=detalhesPage&carta=<?php echo $_GET['carta'];?>"><b><?php echo $Result['nome'];?> - Erratas</b></a></center>
		
								</div>
							</td>
						</tr>					
					</tbody></table><!--#details--></table>
					
					<center><b>Para criar uma Errata <a href="?task=Carta&action=reportPage&carta=<?php echo $_GET['carta'];?>&type=1">Clique Aqui</a></b></center></br>
</div>

<div id="article_body" aling="center" style="border-bottom:1px solid #CCC;width:758px;margin-top:20px;padding-bottom:20px;" >

	<ul>
		<?php 
		
		$n = count($Result['erratas']);		

		if($n == 0){
			echo "<center>Nao foi encontrado nenhuma errata!</center>";
		}else{
		 for ($i=0; $i < $n; $i++) { 
		?>	
		<li style="border-bottom:1px solid #CCC;"> 
				<?php echo $Result['erratas'][$i]['texto'];?><br>
		<?php if($Login['tipo_usuario'] == 1){ ?>
				<a href="?task=Carta&action=reportPage&type=1&carta=<?php echo $_GET['carta']?>&report=<?php echo $Result['erratas'][$i]['id'];?>"><span class="label label-primary">Editar</span></a>  
				<a href="?task=Carta&action=deleteReportAction&carta=<?php echo $_GET['carta']?>&report=<?php echo $Result['erratas'][$i]['id'];?>&acao=deletar" onClick="return confirm('Tem certeza que quer excluir?')"><span class="label label-primary">Deletar</span></a>  
		<?php } ?>
		</li><br>
		<?php }} ?>
	</ul>
</div>

<div>
</fieldset>
</div>