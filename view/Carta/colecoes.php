<style type="text/css">
.offer{
  background:#fff; border:1px solid #ddd; box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2); margin: 15px 0; overflow:hidden;
}
.offer-radius{
  border-radius:7px;
}
.offer-primary {  border-color: #428bca; }
.offer-content{
  padding:0 20px 10px;
}
.offer-content{
  color: #000;
}
</style>

<div class="header-bar">

<fieldset>
<legend>Coleções</legend>


    <div class="container" style="border:0px solid;margin-left:80px;margin-bottom:50px;">
    <div class="row" style="border:0px solid;">
      <?php for ($i=0,$j=1; $i < count($Result); $i++,$j++) { ?>
      <div class="col-xs-3">
        <div class="offer offer-radius offer-primary">

          <div class="offer-content">
            <h3 class="lead">
                <center><a href="?task=Carta&action=listarColecaoPage&colecao=<?php echo $Result[$i]['id'];?>"><b><?php echo $Result[$i]['nome'];?></b></a></center>
            </h3>           
            <p>
              <center><a href="?task=Carta&action=listarColecaoPage&colecao=<?php echo $Result[$i]['id'];?>"><img src="<?php echo "img/colecoes/".$Result[$i]['sigla'].".png";?>" style="position: relative; display: inline;" alt="<?php echo $Result[$i]['$nome']?>" title="<?php echo $Result[$i]['nome']?>"></a></center>
            </p>
            
          </div>
        </div>
      </div>
      <?php 
        if($j == 3){
          echo "</div><div class='row'>";
          $j=0;
        }

       }?>
    </div>
  </div>
  

</fieldset>
</div>