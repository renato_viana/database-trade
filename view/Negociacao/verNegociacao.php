<script src="libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

function mudarSituacao(){
      situacao = document.getElementById("situacao").value;
      negociacao = document.getElementById("negociacao").value;
      url = "index.php?task=Negociacao&action=mudarSituacaoAction&negociacao="+negociacao+"&situacao="+situacao;
      window.location.assign(url);
}

function addReputacao(n){
   
      if(n == 1)
        reputacao = document.getElementById("reputacao_comprador").value;
         

      if(n == 2)
         reputacao = document.getElementById("reputacao_vendedor").value;    
       
      url = "index.php?task=Negociacao&action=addReputacaoAction&reputacao="+reputacao;
      window.location.assign(url);
}
</script>

<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">

<fieldset>
<legend>Ver Negociacao</legend>

<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                  <?php if($Result['proprietario']['id'] != NULL || $Result['login']['tipo_usuario'] == 1 || $Result['login']['tipo_usuario'] == 2){ ?>
                  <div style="float:right;">
                      <div class="form-group">
                        <div class="col-md-12">
                          <form>
                          <?php if($Result['negociacao'][0]['status-id'] != 10 || $Tvar->verificarAdmin()){ ?>    
                          <select name="situacao" class="form-control" onchange="mudarSituacao()" id="situacao">  
                            <option value="" selected="selected"> Mudar Situação </option>  
                            <option value="1"> Pendente </option>  
                            <option value="2"> Ativo </option>  
                            <option value="3"> Concluido </option>  
                            <option value="4"> Cencelado </option> 
                            <?php if($Tvar->verificarAdmin()){?> 
                            <option value="10"> Finalizado pela Administração </option> 
                            <?php }?>
                          </select>   
                          <?php } ?>
                          <input type="hidden" value="<?php echo $_GET['negociacao']?>" name="negociacao" id="negociacao">
                         </form>
                        </div>
                      </div>
                  </div>
                  <?php } ?>                  
                <!-- Form Name -->
                <h3>Sobre a Negociação</h3>
                <!-- Text input-->

                <div class="control-group">
                  <b>Codigo:</b> <?php echo $Result['negociacao'][0]['id'];?><br>
                  <b>de:</b>  <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['solicitante']['id'];?>"><?php echo $Result['solicitante']['login'];?></a> 
                  
                   <?php if($Result['negociacao'][0]['status-id'] >= 3){?>
                   <small><b>Reputação:</b></small>
                   <?php }?>

                   <?php if($Result['negociacao'][0]['status-id'] == 3 && $Tvar->verificarAdmin()){?>
                   
                      <select name="reputacao_comprador" id="reputacao_comprador">
                          <option value="<?php echo $Result['solicitante']['id'].",".$_GET['negociacao'].",1,0,1";?>">Sem Reputação</option>
                          <option value="<?php echo $Result['solicitante']['id'].",".$_GET['negociacao'].",1,1,1";?>">Positiva</option>
                          <option value="<?php echo $Result['solicitante']['id'].",".$_GET['negociacao'].",1,2,1";?>">Negativa</option>
                      </select>
                      <a href="#" onclick="addReputacao(1)"><span class="label label-primary">Add</span></a>  
                  <?php } ?>     
                      <?php echo $Result['rep-comprador'];?>
                  <br>
                  <b>Com a Estande:</b><a href="?task=Estande&action=estandePage&estande=<?php echo $Result['negociacao'][0]['estande_id'];?>"> <?php echo $Result['negociacao'][0]['estande']['nome'];?></a><br>
                  <b>Que tem o dono:</b> <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['negociacao'][0]['dono']['id'];?>"><?php echo $Result['negociacao'][0]['dono']['login'];?></a>
                   
                   <?php if($Result['negociacao'][0]['status-id'] >= 3){?>
                   <small><b>Reputação:</b></small>
                   <?php }?>

                   <?php if($Result['negociacao'][0]['status-id'] == 3 && $Tvar->verificarAdmin()){?>
                   
                      <select name="reputacao_vendedor" id="reputacao_vendedor">
                          <option value="<?php echo $Result['negociacao'][0]['dono']['id'].",".$_GET['negociacao'].",1,0,2";?>">Sem Reputação</option>
                          <option value="<?php echo $Result['negociacao'][0]['dono']['id'].",".$_GET['negociacao'].",1,1,2";?>">Positiva</option>
                          <option value="<?php echo $Result['negociacao'][0]['dono']['id'].",".$_GET['negociacao'].",1,2,2";?>">Negativa</option>
                      </select>
                      <a href="#" onclick="addReputacao(2)"><span class="label label-primary">Add</span></a>
                      <?php } ?>     
                      <?php echo $Result['rep-vendedor'];?>
                  <br>
                  <b>Com a proposta inicial:</b> <?php echo $Result['negociacao'][0]['proposta'];?><br>
                  <b>Situação:</b> <?php echo $Result['negociacao'][0]['status'];?><br>
                  <!--<b>Perguntas:</b> 31<br>-->
                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

</div>

<div class="row">

      <div class="col-md-12 col-xs-12">
        
                <fieldset class="bs-callout bs-callout-warning">  
        <div class="comentarios">

            <fieldset>
              <legend>Chat de Negociação</legend>
            <?php 
            
            if( $Result['negociacao'][0]['status-id'] == 2 || $Tvar->verificarAdmin() ){ ?>    
            <!-- Place this in the body of the page content -->
            <form method="post" action="?task=ChatNegociacao&action=addMensagemAction" name="comentario" enctype="multipart/form-data">
              <textarea name="mensagem"></textarea>

              <script> 
              CKEDITOR.replace( 'mensagem', {
                toolbar: [
                { name: 'document' }, 
                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
                ],
                height: '80px',
                enterMode : CKEDITOR.ENTER_BR
              });
              </script> 
              <div class="form-group">
                <label class="col-md-12 control-label" for="tipo_carta">Anexar imagem <small>obs: apenas imagem é permitido</small></label>
                <div class="col-md-12">
                  <input type="file" name='img' size='40' min="5"/>
                </div>
              </div>              
              
            
              <!-- Button -->

                <div class="" style="margin-top:3px;margin-bottom:3px;">
                  <center><button id="salvar" name="salvar" class="btn btn-primary" value="submit">Enviar</button></center>
                </div>
                <input type="hidden" name="negociacao" value="<?php echo $_GET['negociacao'];?>">
            </form>
          <?php }else{ ?>
                <?php if( $Result['negociacao'][0]['status-id'] != 10 ){ ?>    
                      <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        Para abrir o chat é necessario ativar a negociação!!!
                      </div>          
                <?php } ?>  
          <?php } ?>              
            </fieldset>

          <table width="100%">
            <?php 
            $mensagem = $Result['mensagens'];

            $n = count($mensagem);
          
            for($i=0;$i<$n;$i++){ ?>            
            <tr><td>
              <div class="panel panel-default">
                <div class="panel-body">

                  <div id="imagem" style="width:55px;float:left;border:0px solid;margin-top:-10px;"><br>
                    <?php if(file_exists("upload/usuario/".$mensagem[$i]['usuario_id'].".".$mensagem[$i]['usuario_img'])){?>
                        <img src="<?php echo "upload/usuario/".$mensagem[$i]['usuario_id'].".".$mensagem[$i]['usuario_img'] ?>" width="50px"/>
                    <?php } else{ ?>
                        <img src="upload/usuario/foto.jpg"  width="50px"/>
                    <?php }?>
                  </div>    
                                
                  <h5><b><?php echo $mensagem[$i]['usuario_login'];?><span class="says"> diz:</span></b></h5>
                  <div class="comment-content">
                    <p><h6><?php echo $mensagem[$i]['mensagem'];?></h6></p>
                  </div><!-- .comment-content -->                 
                    <?php if($mensagem[$i]['anexo'] == 1){ ?>
                          <small><a href="?task=Negociacao&action=baixarAnexoAction&negociacao=<?php echo $Result['negociacao'][0]['id'];?>&msg=<?php echo $mensagem[$i]['id'];?>">clique aqui para baixar anexo</a></small>
                    <?php } ?>                  
                </div>
                <div class="panel-footer">
                  <time datetime="2014-06-10T14:38:30+00:00">
                    <?php echo implode ( "/",array_reverse(explode("-", $mensagem[$i]['data'])))." ".$mensagem[$i]['hora'];?>       
                  </time>
                </div>
              </div>

            </td></tr>
            <?php } ?>                       
          </table>
        </div>
      </fieldset>            
        </div>
</div>
<fieldset>