<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Propostas Recebidas</legend>

<style type="text/css">
#mytable a{
	color: black;
}

<?php 
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>

</style>

  <div class="row">
    <div class="col-md-11">
      <form class="form-horizontal" role="form" enctype="multipart/form-data" name="f1" action="?task=Negociacao&action=propostasRecebidasPage" method="post">
        <fieldset>

          <!-- Text input-->
          <div class="form-group">

            <label class="col-sm-2 control-label" for="textinput">Codigo:</label>
            <div class="col-sm-3">
              <input type="text" placeholder="Codigo" class="form-control" name="codigo">
            </div>            

            <label class="col-sm-2 control-label" for="frete">Situação</label>
            <div class="col-sm-3">
              <select id="selectbasic" name="situacao" class="form-control">
                            <option value="" selected="selected"> Mudar Situação </option>  
                            <option value="1"> Pendente </option>  
                            <option value="2"> Ativo </option>  
                            <option value="3"> Concluido </option>  
                            <option value="4"> Cencelado </option>  
                            <option value="4"> Finalizado pela Administração</option>
              </select>
            </div>

            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Buscar</button>
            </div>                                 
          

          </div>   
        </fieldset>
      </form>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

<div class="row">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Negociacao&action=propostasRecebidasPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Negociacao&action=propostasRecebidasPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Negociacao&action=propostasRecebidasPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>

<div class="row">
      <div class="col-md-12 col-xs-12" style="height:590px;">
                <fieldset class="bs-callout bs-callout-danger">
				
                <!-- Text input-->
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Codigo</th>
                                     <th>Proposta</th>
                                      <th>de</th>
                                      <th>Data</th>
                                      <th>Situação</th>
                                      <th></th>
                                     </thead>            
                                <tbody >
                                  <?php
                                        $pergunta = $Result['negociacoes'];
                                        $n = count($pergunta);
                                        $n = ($n >= 10) ? 10 : $n;      
                                        for($i=$paginacao->primeiro_registro;$i<$paginacao->primeiro_registro+$n;$i++){
                                          $negociacao  = $Result['negociacoes'][$i];  
                                          $estande     = $negociacao['estande'];  
                                          $dono        = $negociacao['dono'];  
                                          $solicitante = $negociacao['solicitante'];
                                         ?>
                                      <tr>
                                          <td><a href="?task=Negociacao&action=verNegociacaoPage&negociacao=<?php echo $negociacao['id'];?>">#<?php echo $negociacao['id'];?><a></td>
                                          <td><a href="?task=Estande&action=estandePage&estande=<?php echo $estande['id']?>"><?php echo $negociacao['proposta'];?><a></td>
                                          <td><a href="?task=Usuario&action=perfilPage&id=<?php echo $dono['id']?>"><?php echo $solicitante['login'];?><a></td>
                                          <td><?php echo implode ( "/",array_reverse(explode("-", $negociacao['data'])));?></td>
                                          <td><?php echo $negociacao['status'];?></td>
                                          <td><a href="?task=Negociacao&action=verNegociacaoPage&negociacao=<?php echo $negociacao['id'];?>" class="btn-sm btn-danger" style="color:white;"><b>Ver<b></a></td>
                                      </tr>
                                      <?php } ?>
                                </tbody>
                            </table>         
                              
                              
                          </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
</div>

<div class="row">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Negociacao&action=propostasRecebidasPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Negociacao&action=propostasRecebidasPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Negociacao&action=propostasRecebidasPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>

<fieldset>

