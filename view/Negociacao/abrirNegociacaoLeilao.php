<fieldset>
<legend>Abrir Negociação Leilão</legend>

<ul class="pager">
  <li class="previous"><a href="?task=Leilao&action=leilaoPage&leilao=<?php echo $_GET['leilao'];?>">&larr; Voltar</a></li>
</ul>


<div class="container">
	<div class="row">
      <div class="col-md-10">
        <div class="well well-sm">
          <form class="form-horizontal" action="?task=Negociacao&action=abrirNegociacaoLeilaoAction" method="post">
          <fieldset>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Leilão</label>
              <div class="col-md-9">
                <input id="name" name="estande_nome" type="text" placeholder="Your name" class="form-control" value="<?php echo $Result['leilao']['nome'];?>" readonly>
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Vendedor</label>
              <div class="col-md-9">
                <input id="email" name="dono" type="text" placeholder="Your email" class="form-control" value="<?php echo $Result['dono']['nome'];?>" readonly>
              </div>
            </div>
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="mensagem">mensagem</label>
              <div class="col-md-9">
                <textarea class="form-control" id="mensagem" name="mensagem" placeholder="..." rows="5"></textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-sm">Enviar</button>
              </div>
            </div>
          </fieldset>
          <input id="estande" name="leilao" type="hidden" value="<?php echo $Result['leilao']['id'];?>">
          </form>
        </div>
      </div>
	</div>
</div>

<fieldset>

