<link href="libs/ratting/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="libs/ratting/js/star-rating.min.js" type="text/javascript"></script>

<style>
<!--
#right_bar{
	
}
.open-hand{
	width: 635px;
	margin: 0 auto;
	margin-top: 10px;
}
.comentarios{
	width: 600px;
	margin: 0 auto;
	margin-top: 35px;
}
#deck{
	width:620px;
	height:510px;
	margin-left:70px;
	margin-top:10px;
}
.selected_card{
	width:60px;
	height:83px;
	float:left;
	border:1px solid;
}
.img_deck{
	width:60px;
	height:83px;
}
#txt{
	margin-top:20px;padding-bottom:20px;
	
}
#sobre{
	margin-top:20px;padding-bottom:20px;
	width:550px;
}
.tool-box{
	width:180px;
	float: right;
}
.master{
	width: 600px;
	margin: 0 auto;
}
-->
</style>	

<div class="header-bar" style="margin-bottom:50px;">
	<fiedset>	
		<legend>Deck - <?php echo $Result['deck']['nome'];?></legend>
    <ul class="pager">
      <li class="previous"><a href="<?php echo "javascript:history.back(-2)";?>">&larr; Voltar</a></li>
    </ul>
		<div id="right_bar" style="margin-top:20px;">

			<table style='margin:0 auto;'>		
				<tr>
					<td align='right'><b>Autor: </b></td>
					<td style='padding-left:15px;'><?php echo $Result['deck']['usuario_nome'];?></td>
				</tr>
				<tr>
					<td align='right'><b>Criado em: </b></td>
					<td style='padding-left:15px;'><?php if($Result['deck']['criado']) echo implode ( "/",array_reverse(explode("-",$Result['deck']['criado']))); else echo '-';?></td>
				</tr>		
				<tr>
					<td align='right'><b>Atualizado em: </b></td>
					<td style='padding-left:15px;'><?php if($Result['deck']['atualizado']) echo implode ( "/",array_reverse(explode("-",$Result['deck']['atualizado']))); else echo '-'; ?></td>
				</tr>	
				<tr>
					<td align='right'><b>Rating: </b></td>
					<td style='padding-left:15px;'>
						<?php echo $Result['rating_soma'];?> <img src="img/template/star.png" />
					</td>
				</tr>	
				<tr>
					<td align='right'><b>Views: </b></td>
					<td style='padding-left:15px;'>
						<?php echo $Result['deck']['views'];?>
					</td>
				</tr>											
			</table>
			<div class="open-hand">
				<?php 
				$openhand = $Result['openhand'];
				$login = $Result['login'];
				$coment = $Result['coment'];
				$rating_user = $Result['rating_user'];
				unset($Result['openhand']);
				unset($Result['login']);
				unset($Result['coment']);
				unset($Result['rating_user']);
				unset($Result['rating_soma']);
				?>
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title"><b>Open Hand</b></h3>
					</div>
					<div class="panel-body">
						<?php for ($i=0; $i < 10; $i++) { ?>

						<div class="selected_card">
							<a href="?task=Carta&action=detalhesPage&carta=<?php echo $openhand[$i]['id'];?>"><img alt="" src="<?php echo "img/cartas/".$openhand[$i]['sigla']."/thumb/".$openhand[$i]['numero'].".png";?>" alt="<?php echo $openhand[$i]['nome'];?>" title="<?php echo $openhand[$i]['nome'];?>" class="img_deck" border="0"/></a>
						</div>
						<?php } ?>
					</br>
					<center>
						<h2><small>Proximas 5 cartas</small></h2>
					</center>
					<div style="width:300px;margin:0 auto;">
						<?php for ($i=10; $i < 15; $i++) { ?>

						<div class="selected_card">
							<a href="?task=Carta&action=detalhesPage&carta=<?php echo $openhand[$i]['id'];?>"><img alt="" src="<?php echo "img/cartas/".$openhand[$i]['sigla']."/thumb/".$openhand[$i]['numero'].".png";?>" alt="<?php echo $openhand[$i]['nome'];?>" title="<?php echo $openhand[$i]['nome'];?>" class="img_deck" border="0"/></a>
						</div>
						<?php } ?>	
					</div>
				</div>
				<div style="margin:10px;border:0px solid">
					<center><a href="?task=Deck&action=deckPage&deck=<?php echo $_GET['deck']?>" class="label label-primary">Nova Mão</a></center>
				</div>
			</div>
		</div>

		<div class="master">
		<div class="tool-box">
			<form action="?task=Coment&action=addRatingAction" method="post" name="rating">
				<table>
					<tr>
						<td>
							<center><!-- <b>Avalie este Deck:--></b> <input id="input-23" value="<?php echo $rating_user[0]['star']?>" class="rating" name="estrela" data-size="xs" data-step="1" data-show-caption="false"></center>
						</td>
						<?php if(isset($login['id'])){ ?>
						<td>
							<input type="submit" value="Avaliar"> 
						</td>
						<?php } ?>
					</tr>	
				</table>
								<input type="hidden" name="deck" value="<?php echo $_GET['deck'];?>">
								<input type="hidden" name="usuario" value="<?php echo $login['id'];?>">				
			</form>			
		</div>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a href="#home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Deck</a></li>
				<li><a href="#profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list"></span> Texto</a></li>
				<li><a href="#sobre-nav" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-align-justify"> Sobre</a></li>
			</ul>


			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="home">
					<div id="deck" style="margin:0 auto;">
						<?php 

						if(isset($Result['mydeck'])){

							foreach($Result['mydeck'] as $row){
								?>

								<div class="selected_card">
									<a href="?task=Carta&action=detalhesPage&carta=<?php echo $row['id'];?>"><img alt="" src="<?php echo "img/cartas/".$row['sigla']."/thumb/".$row['numero'].".png";?>" alt="<?php echo $row['nome'];?>" title="<?php echo $row['nome'];?>" class="img_deck" border="0"/></a>
								</div>
								<?php }} ?>
							</div>
						</div>

						<div class="tab-pane" id="profile">

							<div id="txt">
								<ul>
									<?php 
									$aux=NULL; $i=0; $j=0;$CT=array(0 => 0, 1 => 0, 2 => 0,3 => 0,4 => 0);

									for ($j=0; $j < count($Result['t']); $j++) { 
										if($aux != $Result['t'][$j]['tipo']) {
											$i++;
										}
										$CT[$i] += $Result['t'][$j]['qtd'];
										$aux = $Result['t'][$j]['tipo'];
									}	
									$aux=NULL; $i=0; 

									for ($j=0; $j < count($Result['txt']); $j++) { ?>
									<?php if($aux != $Result['t'][$j]['tipo']){ 
										$i++; 
										?>
										<li><b><?php echo $Result['t'][$j]['tipo'];?> (<?php echo $CT[$i];?>)</b></li>
										<?php } ?>
										<li><?php echo  $Result['t'][$j]['qtd']."x "?><a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['t'][$j]['id'];?>"><?php echo ucwords(strtolower($Result['t'][$j]['nome']))." - ".$Result['t'][$j]['sigla'];?></a></li>
										<?php $aux = $Result['t'][$j]['tipo']; } ?>
									</ul>
								</div>

							</div>

							<div class="tab-pane" id="sobre-nav">

								<div id="sobre" aling="center">
									<?php echo $Result['deck']['sobre'];?>
								</div>

							</div>

						</div><!-- fim Content -->
			</div><!-- fim master -->
						<script src="libs/ckeditor/ckeditor.js"></script>


				<div class="comentarios">

						<fieldset>
							<legend>Comentarios</legend>
						<?php if(isset($login['id'])){ ?>
						<!-- Place this in the body of the page content -->
						<form method="post" action="?task=Coment&action=addComentAction" name="comentario">
							<textarea name="texto"><?php echo $Result['report']['texto'];?></textarea>

							<script> 
							CKEDITOR.replace( 'texto', {
								toolbar: [
								{ name: 'document' },	
								{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
								],
								height: '80px',
								enterMode : CKEDITOR.ENTER_BR
							});
							</script> 

						
							<!-- Button -->

								<div class="" style="float:right;margin-top:3px;margin-bottom:3px;">
									<button id="salvar" name="salvar" class="btn btn-primary" value="submit">Comentar</button>
								</div>
								<input type="hidden" name="deck" value="<?php echo $_GET['deck'];?>">
								<input type="hidden" name="usuario" value="<?php echo $login['id'];?>">
							
						</form>
						<?php }else{ ?>
							<center><h5>Para comentar é necessario fazer o <a href="?task=Usuario&action=loginPage">login!</a></h5></center>
						<?php } ?>
						</fieldset>

					<table width="100%">
						<?php 
						$n = count($coment);
						for ($i=0; $i < $n; $i++) { 
						?>						
						<tr><td>
							<div class="panel panel-default">
								<div class="panel-body">
									<h5><b><?php echo $coment[$i]['usuario_login'];?><span class="says"> diz:</span></b></h5>
									<div class="comment-content">
										<p><?php echo $coment[$i]['texto'];?></p>
									</div><!-- .comment-content -->									
								</div>
								<div class="panel-footer">
									<time datetime="2014-06-10T14:38:30+00:00">
										<?php echo $coment[$i]['data']." ".$coment[$i]['hora'];?>				
										<?php if($login['id'] == $coment[$i]['usuario_id']){ ?>
										<div style="width:50px;float:right;">
											<a href="?task=Coment&action=removeComentAction&deck=<?php echo $_GET['deck'];?>&coment=<?php echo $coment[$i]['id'];?>" style="text-decoration:none;"><span class="label label-danger">deletar</span></a>
										</div>
										<?php }?>
									</time>
								</div>
							</div>

						</td></tr>
						<?php }?>												
					</table>	
				</div>



			</div>		

		</fiedset>	


	</div>



