

<!--[if lt IE 9]>
  <script type="text/javascript" src="external/html5/js/html5shiv.js"></script>
<![endif]-->
  <script type="text/javascript" src="assets/ygo/js/jquery-1.7.2.js"></script>
  <script type="text/javascript" src="assets/ygo/js/jquery-ui-1.8.16.custom.min.js"></script>
  <script type="text/javascript" src="assets/ygo/js/jquery.dropkick-1.0.0.js"></script>
  <script type="text/javascript" src="assets/ygo/js/common.js"></script>

  <script type="text/javascript" src="assets/ygo/js/scrolltopcontrol.js"></script>


  <link type="text/css" rel="stylesheet" href="assets/ygo/css/ui-lightness/jquery-ui-1.8.16.custom.css">
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css">
  <link type="text/css" rel="stylesheet" href="assets/ygo/css/common.css">

  <link rel="stylesheet" type="text/css" href="assets/ygo/css/CardTop.css">
  <link rel="stylesheet" type="text/css" href="assets/css/CardSearchCondition.css">
  
  <script type="text/javascript">
  <!--
  $(function(){

    
    $('input:text').val('');
    //$('#alinhamento').val('1');
    $('#ctype').val('');
    $('input:checkbox').prop('checked', false);
    $('#othercon_and').prop('checked', false).closest('a').addClass('radio_off_left').removeClass('radio_on_left').find('img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_off.png');
    $('#othercon_or').prop('checked', true).closest('a').addClass('radio_on_right').removeClass('radio_off_right').find('img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_on.png');

    
    $('#autor').dropkick({
      theme: 'stype_en'
    });   
   
  });

  //-->
  </script>

<div style="width:1000px;">


      <script type="text/javascript">
      <!--

      $(function(){

        
        $('#nome[value=]').css('color', '#bbb').val('Nome do Deck');
        $('#nome').focus(function(){
          if ($(this).val() == 'Nome do Deck') {
            $(this).val('').css('color', '#555');
          }
        }).blur(function(){
          if ($(this).val() == ''){
            $(this).css('color', '#bbb').val('Nome do Deck');
          }
          if ($(this).val() != 'Nome do Deck') {
            $(this).css('color', '#555');
          }
        });

       $('#cartanome[value=]').css('color', '#bbb').val('Nome da Carta');
        $('#cartanome').focus(function(){
          if ($(this).val() == 'Nome da Carta') {
            $(this).val('').css('color', '#555');
          }
        }).blur(function(){
          if ($(this).val() == ''){
            $(this).css('color', '#bbb').val('Nome da Carta');
          }
          if ($(this).val() != 'Nome da Carta') {
            $(this).css('color', '#555');
          }
        });        

        
        $('.search_box_tips').hover(
          function(e){

            if ($(this).closest('table').css('opacity') != 1) {
              return false;
            }

            var tips_txt = $(this).attr('alt');
            $(this).after('<span id="search_box_tips"><span></span></span>');
            $('#search_box_tips span').text(tips_txt);

            var offset = $(this).offset();
            var pos_x = offset.left - 30;
            var pos_y = offset.top - 34;

            $('#search_box_tips').css({
              'position': 'absolute',
              'top': pos_y,
              'left': pos_x,
              'display': 'inline-block',
              'background': 'url(http://www.db.yugioh-card.com/yugiohdb/external/image/parts/search_box_tips_bk.png) right bottom no-repeat',
              'height': '35px',
              'padding-right': '4px'
            });

            $('#search_box_tips span').css({
              'display': 'inline-block',
              'background': 'url(http://www.db.yugioh-card.com/yugiohdb/external/image/parts/search_box_tips_bk.png) left top no-repeat',
              'line-height': '23px',
              'height': '35px',
              'padding': '0 5px 0 20px',
              'font-weight': 'bold',
              'font-size': '12px',
              'color': '#6a3807'
            });
          },
          function(e){
            if ($(this).closest('table').css('opacity') != 1) {
              return false;
            }
            $('#search_box_tips').remove();
          }
        );

        
        $('.attreffe, .species, .other').click(function(){
          if ($(this).hasClass('button_off')) {
            $(this).addClass('button_on').removeClass('button_off');
            $(':checkbox', this).prop('checked', true);
          } else {
            $(this).addClass('button_off').removeClass('button_on');
            $(':checkbox', this).prop('checked', false);
          }
        });
        $('.othercon_left').click(function(){
          if ($(this).hasClass('radio_off_left')) {
            $(this).addClass('radio_on_left').removeClass('radio_off_left');
            $(':radio', this).prop('checked', true);
            $($(this).next()).addClass('radio_off_right').removeClass('radio_on_right');
            $(':radio', $(this).next()).prop('checked', false);
            $('img', this).attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_on.png');
            $('.othercon_right img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_off.png');
          }
        });
        $('.othercon_right').click(function(){
          if ($(this).hasClass('radio_off_right')) {
            $(this).addClass('radio_on_right').removeClass('radio_off_right');
            $(':radio', this).prop('checked', true);
            $($(this).prev()).addClass('radio_off_left').removeClass('radio_on_left');
            $(':radio', $(this).prev()).prop('checked', false);
            $('img', this).attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_on.png');
            $('.othercon_left img').attr('src', 'http://www.db.yugioh-card.com/yugiohdb/external/image/parts/check_off.png');
          }
        });

        
        $('#condition_toggle').click(function(){
          $('#search_by_attack_and_defense_and_level, #search_by_attribute, #search_by_effect, #search_by_species, #search_by_other').toggle();
        });

        
        $('#form_search input:text').keypress(function(e){
          if (e.keyCode == 13) {
            Search();
          }
        });

      });

      
      function Search() {
        
        if ($('#nome').css('color') == '#bbb' || $('#nome').css('color') == 'rgb(187, 187, 187)') {
        //  $('#nome').val('');
        //  $('#carta').val('');
        }
        
        form = $('#form_search').get(0);
        if(form.nome.value == "Nome do Deck") form.nome.value = "";
        if(form.cartanome.value == "Nome da Carta") form.cartanome.value = "";
        
        form.action = "?task=Deck&action=listarDecksPage";
        form.submit();
      }


      //-->
      </script>


<style type="text/css">
.label-deck{
	font-weight: bold;
	font-size: 14px;
}
</style>

<?php 
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>

<div class="header-bar">
<fiedset>	
<legend>Deck Lists</legend>

     <form id="form_search" action="card_search.action" method="POST">
        <div id="search_box">

          <div id="search_by_keyword_and_type" style="padding:5px 8px;">
            <table style="" border="0">
 
              <tr valign="middle">
                <td>
                  <img src="http://www.db.yugioh-card.com/yugiohdb/external/image/parts/icon_search.png" alt="Search">
                </td>
                <td valign="top" width="250">
                  <span class="label-deck">Nome do Deck</span>
                  <input type="text" id="nome" name="nome" style="width:250px;height:32px" maxlength="50">
                </td>
                <td valign="top" width="250">
                  <span class="label-deck">Carta contida no deck</span>
                  <input type="text" id="cartanome" name="cartanome" style="width:250px;height:32px" maxlength="50">
                </td>   
                <td valign="top" width="300">
                	<span class="label-deck">Autor</span>
                    <?php echo $FORM->autor;?>
                </td>                             
                <td valign="top"></br>
                    <center><a href="javascript:Search();" class="btn btn-primary btn-sm"><b>Buscar</b></a></center>
                </td>
                <td>
                  
                </td>
              </tr>
            </table>
          </div>
      </br></br>
      </div>
	</form>

</br></br>

<style type="text/css">
.decklists li{
	list-style: none;
}
</style>

<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Deck&action=listarDecksPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Deck&action=listarDecksPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Deck&action=listarDecksPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>


<ul class="decklists">
	<?php 
	$aux = NULL;
	$i = 0;$j=0;
	$n = count($Result);
	$n = ($n >= 100) ? 100 : $n;
	
	for($k=$paginacao->primeiro_registro;$k<$paginacao->primeiro_registro+$n;$k++){
		
	if($aux != $Result[$k]['usuario']){

	$j++;
	$i = 0;
	?>
		<li><b><?php echo "<a href='?task=Usuario&action=perfilPage&id=".$Result[$k]['usuario_id']."'>".ucwords($Result[$k]['usuario'])."</a>";?><b></li>
	<?php } ?>		
		<li style="padding-left:30px;"><h5><a href="?task=Deck&action=deckPage&deck=<?php echo $Result[$k]['id'];?>"><?php echo $Result[$k]['deck'];?></a> <?php if($i == 0) {echo "- [novo]";$i=1;}?></h5></li>
	<?php 
	$aux = $Result[$k]['usuario'];
	}?>
</ul>
</fiedset>	

<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Deck&action=listarDecksPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Deck&action=listarDecksPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Deck&action=listarDecksPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>

<br><br><br>
</div>