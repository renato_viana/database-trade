<style>
<!--
#right_bar{
	
}
.open-hand{
	width: 635px;
	margin: 0 auto;
	margin-top: 10px;
}
.comentarios{
	width: 600px;
	margin: 0 auto;
	margin-top: 35px;
}
#deck{
	width:620px;
	height:510px;
	margin-left:70px;
	margin-top:10px;
}
.selected_card{
	width:60px;
	height:83px;
	float:left;
	border:1px solid;
}
.img_deck{
	width:60px;
	height:83px;
}
#txt{
	margin-top:20px;padding-bottom:20px;
	
}
#sobre{
	margin-top:20px;padding-bottom:20px;
	width:550px;
}
.tool-box{
	width:180px;
	float: right;
}
.master{
	width: 600px;
	margin: 0 auto;
}
-->
</style>	



<div class="header-bar" style="margin-bottom:50px;">
	<fiedset>	
		<legend>My Deck - <?php echo $Result['deck']['nome'];?></legend>
    <ul class="pager">
      <li class="previous"><a href="<?php echo "javascript:history.back(-2)";?>">&larr; Voltar</a></li>
    </ul>
		<div id="right_bar" style="margin-top:20px;">

			<table style='margin:0 auto;'>		
				<tr>
					<td align='right'><b>Autor: </b></td>
					<td style='padding-left:15px;'><?php echo $Result['deck']['usuario_nome'];?></td>
				</tr>
				<tr>
					<td align='right'><b>Criado em: </b></td>
					<td style='padding-left:15px;'><?php if($Result['deck']['criado']) echo implode ( "/",array_reverse(explode("-",$Result['deck']['criado']))); else echo '-';?></td>
				</tr>		
				<tr>
					<td align='right'><b>Atualizado em: </b></td>
					<td style='padding-left:15px;'><?php if($Result['deck']['atualizado']) echo implode ( "/",array_reverse(explode("-",$Result['deck']['atualizado']))); else echo '-'; ?></td>
				</tr>	
				<tr>
					<td align='right'><b>Rating: </b></td>
					<td style='padding-left:15px;'>
						<?php echo $Result['rating_soma'];?> <img src="img/template/star.png" />
					</td>
				</tr>	
				<tr>
					<td align='right'><b>Link Publico: </b></td>
					<td style='padding-left:15px;'>
						http://mtcgdatabase.com.br/?task=Deck&action=deckPage&deck=<?php echo $_GET['deck'];?>
					</td>
				</tr>								
			</table>
			<div class="open-hand">
				<?php 
				$openhand = $Result['openhand'];
				$login = $Result['login'];
				$coment = $Result['coment'];
				$rating_user = $Result['rating_user'];
				unset($Result['openhand']);
				unset($Result['login']);
				unset($Result['coment']);
				unset($Result['rating_user']);
				unset($Result['rating_soma']);
				?>
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title"><b>Open Hand</b></h3>
					</div>
					<div class="panel-body">
						<?php for ($i=0; $i < 10; $i++) { ?>

						<div class="selected_card">
							<a href="?task=Carta&action=detalhesPage&carta=<?php echo $openhand[$i]['id'];?>"><img alt="" src="<?php echo "img/cartas/".$openhand[$i]['sigla']."/thumb/".$openhand[$i]['numero'].".png";?>" alt="<?php echo $openhand[$i]['nome'];?>" title="<?php echo $openhand[$i]['nome'];?>" class="img_deck" border="0"/></a>
						</div>
						<?php } ?>
					</br>
					<center>
						<h2><small>Proximas 5 cartas</small></h2>
					</center>
					<div style="width:300px;margin:0 auto;">
						<?php for ($i=10; $i < 15; $i++) { ?>

						<div class="selected_card">
							<a href="?task=Carta&action=detalhesPage&carta=<?php echo $openhand[$i]['id'];?>"><img alt="" src="<?php echo "img/cartas/".$openhand[$i]['sigla']."/thumb/".$openhand[$i]['numero'].".png";?>" alt="<?php echo $openhand[$i]['nome'];?>" title="<?php echo $openhand[$i]['nome'];?>" class="img_deck" border="0"/></a>
						</div>
						<?php } ?>	
					</div>
				</div>
				<div style="margin:10px;border:0px solid">
					<center><a href="?task=Deck&action=deckPage&deck=<?php echo $_GET['deck']?>" class="label label-primary">Nova Mão</a></center>
				</div>
			</div>
		</div>

		<div class="master">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a href="#home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Deck</a></li>
				<li><a href="#profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list"></span> Texto</a></li>
			</ul>


			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="home">
					<div id="deck" style="margin:0 auto;">
						<?php 

						if(isset($Result['mydeck'])){

							foreach($Result['mydeck'] as $row){
								?>

								<div class="selected_card">
									<a href="?task=Carta&action=detalhesPage&carta=<?php echo $row['id'];?>"><img alt="" src="<?php echo "img/cartas/".$row['sigla']."/thumb/".$row['numero'].".png";?>" alt="<?php echo $row['nome'];?>" title="<?php echo $row['nome'];?>" class="img_deck" border="0"/></a>
								</div>
								<?php }} ?>
							</div>
						</div>

						<div class="tab-pane" id="profile">

							<div id="txt">
								<ul>
									<?php 
									$aux=NULL; $i=0; $j=0;$CT=array(0 => 0, 1 => 0, 2 => 0,3 => 0,4 => 0);

									for ($j=0; $j < count($Result['t']); $j++) { 
										if($aux != $Result['t'][$j]['tipo']) {
											$i++;
										}
										$CT[$i] += $Result['t'][$j]['qtd'];
										$aux = $Result['t'][$j]['tipo'];
									}	
									$aux=NULL; $i=0; 

									for ($j=0; $j < count($Result['txt']); $j++) { ?>
									<?php if($aux != $Result['t'][$j]['tipo']){ 
										$i++; 
										?>
										<li><b><?php echo $Result['t'][$j]['tipo'];?> (<?php echo $CT[$i];?>)</b></li>
										<?php } ?>
										<li><?php echo  $Result['t'][$j]['qtd']."x "?><a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['t'][$j]['id'];?>"><?php echo ucwords(strtolower($Result['t'][$j]['nome']))." - ".$Result['t'][$j]['sigla'];?></a></li>
										<?php $aux = $Result['t'][$j]['tipo']; } ?>
									</ul>
								</div>

							</div>

						</div><!-- fim Content -->
			</div><!-- fim master -->
						<script src="libs/ckeditor/ckeditor.js"></script>


				<div class="comentarios">

						<fieldset>
							<legend>Comentarios Sobre o Deck</legend>
						
						<!-- Place this in the body of the page content -->
						<form method="post" action="?task=Deck&action=addSobreAction" name="comentario">
							<textarea name="texto"><?php echo $Result['deck']['sobre'];?></textarea>

							<script> 
							CKEDITOR.replace( 'texto', {
								height: '600px',
								enterMode : CKEDITOR.ENTER_BR
							});
							</script> 

						
							<!-- Button -->

								<div class="" style="float:right;margin-top:3px;margin-bottom:3px;">
									<button id="salvar" name="salvar" class="btn btn-primary" value="submit">Enviar</button>
								</div>
								<input type="hidden" name="deck" value="<?php echo $_GET['deck'];?>">
								<input type="hidden" name="usuario" value="<?php echo $login['id'];?>">							
						</form>
						</fieldset>
	
				</div>



			</div>		

		</fiedset>	


	</div>



