<link rel="stylesheet" type="text/css" href="assets/css/3dbuttons.css">   
<script type="text/javascript" src="assets/js/functions.js"></script>
<script>
function enviarDeck(acao){
        obj = document.forms["form_deck"];
    obj.acao.value = acao;
    if(obj.nome.value != ''){
      obj.submit();
    }else{
      alert("O Deck deve ter um nome!");
    }
}
function carregarDeck(){
  obj  = document.forms["form_deck"];
  deck = obj.lista_deck.value;
  window.location = "?task=Deck&action=construtorPage&deck="+deck;
}
</script>
<style>
<!--
#deck_build{
  border:1px solid;
  width:1000px;
  height:700px;
  margin:0 auto;
  background:url('img/template/deck_maker.jpg');
}
#left_bar{
  width:230px;
  height:100%;
  float:right;
  margin-right:20px; 

}
#right_bar{
  width:747px;
  height:700px;
  float:right;
}
#main_card{
  width:225px;
  height:331px;
  float:left;
  margin-left:13px;
  margin-top:10px;
}
#mainCard{
  width:225px;
  height:331px;
}
#listar_card{
  width:225px;
  height:331px;
  float:left;
  margin-left:13px;
  margin-top:10px;
}
#listar_card_header{
  width:225px;
  height:35px;
  float:left;
  
}
#bandeja_de_cartas{
  width:216px;
  height:296px; 
  float:left;
  margin-left:4px;
}
#deck{
  width:620px;
  height:510px;
  float:left;
  margin-left:70px;
  margin-top:10px;
}
.selected_card{
  width:60px;
  height:83px;
  float:left;
  border:1px solid;
}
.cards_listed{
  width:52px;
  height:72px;
  float:left;
  border:1px solid;
}
#buscar{
  width:100%;
  height:130px;
  float:left;
  margin-top:50px;
}
.img_listed{
  width:52px;
  height:72px;
}
.img_deck{
  width:60px;
  height:83px;
}
#command{
  width:50px;
  height:50px;
  float:left;
  margin-top:10px;
  margin-left:3px;
}
#nav_bar{
  width:100%;
  margin:0 auto;
  height: 40px;
}
-->
</style>


<div class="header-bar">
<div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
  <h3>Construtor de Deck</h3>
</div>          

<div id="nav_bar">
<form method='post' name='form_deck' action="?task=Deck&action=acaoDeckAction">
      <div style='float:left;'>
      <table border=0>
        <tr>
          <td>
            <div class="form-group">
              
                <div>Nome do deck:
                  <?php echo $FORM->nome ?>
                </div>
            </div>
          </td>
          <td valign="top" style="padding: 2px 5px 0px 5px;">
            <div>
                <div >
                <a href="javascript:enviarDeck('salvar_deck');"><span class="label label-primary">Salvar</span></a>  
                </div>
            </div>
          </td> 
                    
          <td valign="top" style="padding: 2px 5px 0px 5px;">
            <!-- Select Basic -->
            <div>
              <a href="javascript:enviarDeck('deletar_deck');" onClick="return confirm('Tem certeza que quer excluir?')"><span class="label label-primary">Deletar</span></a>  
            </div>
          </td>
          
          <td valign="top" style="padding: 2px 5px 0px 5px;">
            <!-- Select Basic -->
            <div>
              <a href="?task=Deck&action=construtorPage"><span class="label label-primary">Novo</span></a> 
            </div>
          </td>
          </table>
        </div>
        <div style='float:right;'>
          <table>
          <td>
            <!-- Select Basic -->
            <div class="form-group" style="float:right;">
                <div >
              Abrir Deck:
                <?php echo $FORM->lista_deck;?>
                </div>
            </div>
          </td>         
        </tr>
</table>
</div>
<?php echo $FORM->iddeck ;?>
<input type="hidden" name="acao" value="" />
</form>
</div>

</div>



<div id="deck_build" style="margin-bottom:20px;">
  <div id="right_bar">
    <div id="deck">
      <?php       
      if(isset($_SESSION['mydeck'])){
        foreach($_SESSION['mydeck'] as $row){?>
        <div class="selected_card">
          <img alt="" src="<?php echo "img/cartas/".$row['sigla']."/thumb/".$row['numero'].".png";?>" alt="<?php echo $row['nome'];?>" title="<?php echo $row['nome'];?>" class="img_deck" border="0" ondblclick="<?php echo "rmvCardInDeck(".$row['key'].");"?>" onMouseOver="<?php echo "mudarMainCard('"."img/cartas/".$row['sigla']."/".$row['numero'].".png',".$row['id'].");"?>" />
        </div>
        <?php }} ?>
    </div>

    <div id="command">
      <a href="#" onclick="javascript:updateDeck();"><img src="img/template/bot_az.png" border="0"/></a></br>
      <a href="#" onclick="javascript:rmvAll();"><img src="img/template/bot_limpeza.png" border="0"/></a>
    </div>

    <div id="deck_name">
      
    </div>

    <div id="buscar">
      <form method="post" name="buscar">
        <table style="margin:0 auto;margin-top:10px;" border='0'>
          <tr style="height:35px;">
            <td>
              <div class="form-group">
                <div class="col-md-5">
                  <input id="nome" name="nome" placeholder="Nome" type="text">
                </div>
              </div>
            </td>
            <td>
              <div class="form-group">
                <div class="col-md-5">
                  <input id="texto" name="texto_permanente" placeholder="Descricao" type="text">
                </div>
              </div>
            </td>         
            <td>
              <div class="col-md-4">
                    <?php echo $FORM->tipo;?>
              </div>
            </td> 
            <td>
              <!-- Select Basic -->
              <div class="form-group">
                <div class="col-md-4">
                    <?php echo $FORM->alinhamento;?>
                    </div>
                  </div>
                </td>         
              </tr>
              <tr>
                <td>
                  <!-- Text input-->
                  <div class="form-group">
                    <div class="col-md-1">
                      <table>
                        <tr>
                          <td><img src="img/icon/energia.png" style="width:22px;"/></td>              
                          <td><input id="energia_inicial" name="energia_inicial" placeholder="" type="text" style="width:35px"></td>
                          <td><b>>=</b></td>
                          <td><input id="energia_fim" name="energia_fim" placeholder="" type="text" style="width:35px"></td>
                        </tr> 
                      </table>
                    </div>
                  </div>
                </td>
                <td>
                  <!-- Text input-->
                  <div class="form-group">
                    <div class="col-md-1">
                      <table>
                        <tr>
                          <td><img src="img/icon/escudo.png" style="width:20px;"/></td>             
                          <td><input id="escudo_inicial" name="escudo_inicial" placeholder="" type="text" style="width:35px"></td>
                          <td><b>>=</b></td>
                          <td><input id="escudo_fim" name="escudo_fim" placeholder="" type="text" style="width:35px"></td>
                        </tr> 
                      </table>  
                    </div>
                  </div>
                </td>         
                <td>
                  <!-- Select Basic -->
                  <div class="form-group">
                    <div class="col-md-4">
                      <?php echo $FORM->afiliacao;?>
                        </div>
                      </div>
                    </td> 
                    <td>
                      <!-- Select Basic -->
                      <div class="form-group">
                        <div class="col-md-4">
                          <?php echo $FORM->tipo_acao;?>
                            </div>
                          </div>
                        </td>           
                      </tr> 
                      <tr>
                        <td colspan="5">
                          <!-- Multiple Checkboxes (inline) -->
                          <div>
                            <div>
                              <label class="checkbox-inline" for="habilidades-0">
                                <input name="habilidades[]" id="habilidades0" value="1" type="checkbox">
                                <img src="img/icon/1.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-1">
                                <input name="habilidades[]" id="habilidades-1" value="2" type="checkbox">
                                <img src="img/icon/2.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-2">
                                <input name="habilidades[]" id="habilidades-2" value="3" type="checkbox">
                                <img src="img/icon/3.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-3">
                                <input name="habilidades[]" id="habilidades-3" value="4" type="checkbox">
                                <img src="img/icon/4.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-5">
                                <input name="habilidades[]" id="habilidades-5" value="5" type="checkbox">
                                <img src="img/icon/5.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-6">
                                <input name="habilidades[]" id="habilidades-6" value="6" type="checkbox">
                                <img src="img/icon/6.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-7">
                                <input name="habilidades[]" id="habilidades-7" value="7" type="checkbox">
                                <img src="img/icon/7.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-8">
                                <input name="habilidades[]" id="habilidades-8" value="8" type="checkbox">
                                <img src="img/icon/8.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-9">
                                <input name="habilidades[]" id="habilidades-9" value="9" type="checkbox">
                                <img src="img/icon/9.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-10">
                                <input name="habilidades[]" id="habilidades-10" value="10" type="checkbox">
                                <img src="img/icon/10.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-11">
                                <input name="habilidades[]" id="habilidades-11" value="11" type="checkbox">
                                <img src="img/icon/11.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-12">
                                <input name="habilidades[]" id="habilidades-12" value="12" type="checkbox">
                                <img src="img/icon/12.png" />
                              </label>     
                              <label class="checkbox-inline" for="habilidades-13">
                                <input name="habilidades[]" id="habilidades-13" value="13" type="checkbox">
                                <img src="img/icon/13.png" />
                              </label>
                              <label class="checkbox-inline" for="habilidades-100">
                                <input name="habilidades[]" id="habilidades-100" value="0" type="checkbox">
                                <img src="img/icon/0.png" />
                              </label>                                                                 
                            </div>
                          </div>
                        </td>         
                      </tr>
                      <tr>
                        <td colspan="4"> 
                          <center><button type="button" class="btn btn-ky text-capitalize btn-xs" onclick="buscarCards('listar_card');">Buscar</button></center>
                        </td>
                      </tr>
                    </table>
          </form>
  </div>    

  </div>

  <div id="left_bar">
    <div id="main_card">
      <a href="#" id="main_link" target="_blank"><img id="mainCard" border="0"/></a>
    </div>
    
    <div id="listar_card">
      <div id="listar_card_header">
        <table border="0" align="center">
          <tr>
            <td><img src="img/template/bot_left.png" border="0"/></td>
            <td width="100" align="center">1/1</td>
            <td><img src="img/template/bot_right.png"/></td>
          </tr>
        </table>
      </div>
      <div id="bandeja_de_cartas">
      </div>    
    </div>
    
    <div id="painel">
    </div>    
  </div>

</div>
