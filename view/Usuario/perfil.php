  <link href="assets/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css"/>
  <link href="assets/css/mystyle.css" media="screen" rel="stylesheet" type="text/css"/>
  <script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript">
function mudarTipo(){
      tipo = document.getElementById("tipo").value;
      usuario = document.getElementById("usuario").value;
      url = "index.php?task=Usuario&action=mudarTipoUsuarioAction&tipo="+tipo+"&usuario="+usuario;
      window.location.assign(url);
}
</script>

<?php 
$Tvar  = new TratamentoVar();
$perfil = $Tvar->getSession('login');
?>
<style type="text/css">
.p-container{
  margin: 0 auto;
  margin-top: 20px;
  margin-bottom:30px;
}
.panel{
  width: 750px;
}
#p{
  margin: 0 auto;
  width: 1100px;
  height: 720px;

}
#p-r{
  float:left;
  width: 300px;
  height: 600px;

}
#p-l{
  float:left;
  width: 750px;
  height: 600px;
}
#profile {
  height: 250px;
  position: relative;
  overflow: auto ;
  overflow-x: hidden; 
  height: 550px;
}
#p-photo{
  width: 200px;
  height: 200px;
  margin-right: 20px;
  float: right;
}
#inbox{
  width: 127px;
  float: right;
}
</style>
<div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
  <h3>Meu Perfil</h3>
</div> 

<div class="p-container">
  <div id="tools">
       <div id="inbox">
            <a class="btn btn-info btn-circle text-uppercase" href="?task=Mensagem&action=escreverMensagemPage&usuario=<?php echo $_GET['id'];?>" id="reply"><span class="glyphicon glyphicon-envelope"></span> <small>Mandar Menssagem</small></a>
       </div>
  </div>
  <div id="p">

  <div id="p-r">
    <div id="p-photo">
        <?php if(file_exists("upload/usuario/".$Result['usuario'][0]['id'].".".$Result['usuario'][0]['img'])){?>
            <img src="<?php echo "upload/usuario/".$Result['usuario'][0]['id'].".".$Result['usuario'][0]['img'] ?>" style="border:2px solid #CCC;"/>
        <?php } else{ ?>
            <img src="upload/usuario/foto.jpg" />
        <?php }?>      
    </div>
  </div>

  <div id="p-l">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#home" role="tab" data-toggle="tab"></span><span class="glyphicon glyphicon-list"></span> Sobre</a></li>
        <li><a href="#profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"> </span> Decks</a></li>
        <li><a href="#positiva" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-plus"> </span> Reputações Positivas</a></li>
        <li><a href="#negativa" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-minus"></span> Reputações Negativas</a></li>
      </ul>

<!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="home">      

          <div class="" >

                <div class="panel panel-info">
                  <div class="panel-heading">
                    <h3 class="panel-title"><b><?php echo $Result['usuario'][0]['nome'];?></b></h3>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <!--<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xfa1/t1.0-9/p417x417/264723_4095802326198_2030804694_n.jpg?z=100" class="img-circle" width='120'> </div>-->

                      <div class=" col-md-9 col-lg-9 "> 
                        <table class="table table-user-information">
                          <tbody>
                            <tr>
                              <td>Tipo de Usuario</td>
                              <td><?php echo $Result['usuario'][0]['tipo_usuario'];?>
                                <?php if($Tvar->verificarAdmin()){?>
                                <form method="post">
                                  <select name="tipo" class="form-control" onchange="mudarTipo()" id="tipo">  
                                    <option value="" selected="selected"> Mudar Tipo </option>  
                                    <option value="0"> Comum </option>  
                                    <?php if($Login['tipo_usuario'] == 1){?>
                                    <option value="1"> Super Administrador </option> 
                                    <?php } ?> 
                                    <option value="2"> Administrador </option>  
                                    <option value="10"> Cencelado </option> 
                                    <input type="hidden" value="<?php echo $Result['usuario'][0]['id'];?>" name="usuario" id="usuario" />
                                  </select>  
                                </form>
                                <?php } ?>
                              </td>
                            </tr>                             
                            <tr>
                              <td><b>Negociações</b></td>
                              <td></td>
                            </tr>  
                            <tr>
                              <td>Reputação</td>
                              <td><?php echo $Result['rep-soma'];?> <span class="glyphicon glyphicon-star"></span></td>
                            </tr>  
                            <tr>
                              <td>Leilões</td>
                              <td><?php echo $Result['leilao'];?></td>
                            </tr>                                                                                    
                            <tr>
                              <td>Estande</td>
                              <td><?php echo $Result['estande'];?></td>
                            </tr>                                               
                            <tr>
                              <td><b>Dados de Acesso</b></td>
                              <td></td>
                            </tr>   
                            <tr>
                              <td>Registro:</td>
                              <td><?php echo $Tvar->dateBr($Result['usuario'][0]['data_cadastro']);?></td>
                            </tr>                                           
                            <tr>
                              <td>Ultimo Login:</td>
                              <td><?php echo $Tvar->dateBr($Result['usuario'][0]['ultimo_login'])." ".$Result['usuario'][0]['hora_ultimo_login'];?></td>
                            </tr>
                            <tr>
                              <td>Numero de Login:</td>
                              <td><?php echo $Result['usuario'][0]['qtd_login'];?></td>
                            </tr>              
                            <tr>
                              <td><b>Sobre</b></td>
                              <td></td>
                            </tr>                  
                            <!--  
                            <tr>
                              <td>Email</td>
                              <td><?php echo $Result['usuario'][0]['email'];?></td>
                            </tr> 
                                               
                            <tr>
                              <td>Telefone</td>
                              <td><?php echo $Result['usuario'][0]['fone'];?></td>
                            </tr>
                            <tr>
                              <td>Data de Nascimento:</td>
                              <td><?php echo $Tvar->dateBr($Result['usuario'][0]['nascimento']);?></td>
                            </tr>-->
                            <tr>
                              <td>Sexo:</td>
                              <td><?php echo $Result['usuario'][0]['sexo'];?></td>
                            </tr>                
                            <tr>
                              <td>Estado</td>
                              <td><?php echo $Result['usuario'][0]['estado'];?></td>
                            </tr>
                            <tr>
                              <td>Cidade</td>
                              <td><?php echo $Result['usuario'][0]['cidade'];?></td>
                            </tr>
                            <!--
                            <tr>
                              <td>Rua</td>
                              <td><?php echo $Result['usuario'][0]['rua'];?></td>
                            </tr>
                              <td>Complemento</td>
                              <td><?php echo $Result['usuario'][0]['complemento'];?>
                            </td>
                            </tr>
                              <td>Bairro</td>
                              <td><?php echo $Result['usuario'][0]['bairro'];?>
                            </td>  
                            </tr>
                              <td>Cep</td>
                              <td><?php echo $Result['usuario'][0]['cep'];?>
                            </td>                                   
                          </tr>
                          -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>


          <div class="tab-pane" id="profile">
                  <div class="container">
                    <div class="row">
                          <div class="col-md-8">
                          <div class="table-responsive">
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                     <th>Nome</th>
                                      <th>Criado</th>
                                      <th>Atualizado</th>
                                      <th>Rating</th>
                                      <th>Visualizações</th>
                                     </thead>            
                      <tbody >
                        <?php for ($i=0; $i < count($Result['deck']); $i++) { ?>
                      <tr>
                      <td><a href="?task=Deck&action=deckPage&deck=<?php echo $Result['deck'][$i]['id']?>"><?php echo $Result['deck'][$i]['deck'];?><a></td>
                      <td><?php echo implode ( "/",array_reverse(explode("-",$Result['deck'][$i]['criado'])));?></td>
                      <td><?php echo implode ( "/",array_reverse(explode("-",$Result['deck'][$i]['atualizado'])));?></td>
                      <td><?php echo $Result['deck'][$i]['rating'];?></td>
                      <td><?php echo $Result['deck'][$i]['views'];?></td>
                      </tr>
                      <?php } ?>
                      </tbody>
                  </table>
                  <!--
                  <div class="clearfix"></div>
                  <ul class="pagination pull-right">
                    <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                  </ul>
                      -->            
                              </div>
                              
                          </div>
                    </div>
                  </div>
          </div>

          <div class="tab-pane" id="positiva">
                  <div class="container">
                    <div class="row">
                          <div class="col-md-8">
                          <div class="table-responsive">
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Negociou com:</th>
                                      <th>Tipo</th>
                                      <th>Atuação</th>
                                      <th>Data</th>
                                     </thead>            
                      <tbody >
                        <?php for ($i=0; $i < count($Result['rep-positiva']); $i++) { ?>
                      <tr>
                      <td><a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['rep-positiva'][$i]['negociado-id'];?>"><?php echo $Result['rep-positiva'][$i]['negociado'];?><a></td>
                      <td><?php echo $Result['rep-positiva'][$i]['tipo_negociacao'];?></td>
                      <td><?php echo $Result['rep-positiva'][$i]['atuacao-label'];?></td>
                      <td><?php echo implode ( "/",array_reverse(explode("-",$Result['rep-positiva'][$i]['data'])));?></td>
                      </tr>
                      <?php } ?>
                      </tbody>
                  </table>           
                              </div>
                              
                          </div>
                    </div>
                  </div>
          </div>    

          <div class="tab-pane" id="negativa">
                  <div class="container">
                    <div class="row">
                          <div class="col-md-8">
                          <div class="table-responsive">
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                      <th>Negociou com:</th>
                                      <th>Tipo</th>
                                      <th>Atuação</th>
                                      <th>Data</th>
                                     </thead>            
                      <tbody >
                        <?php for ($i=0; $i < count($Result['rep-negativa']); $i++) { ?>
                      <tr>
                      <td><a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['rep-negativa'][$i]['negociado-id'];?>"><?php echo $Result['rep-negativa'][$i]['negociado'];?><a></td>
                      <td><?php echo $Result['rep-negativa'][$i]['tipo_negociacao'];?></td>
                      <td><?php echo $Result['rep-negativa'][$i]['atuacao-label'];?></td>
                      <td><?php echo implode ( "/",array_reverse(explode("-",$Result['rep-negativa'][$i]['data'])));?></td>
                      </tr>
                      <?php } ?>
                      </tbody>
                  </table>           
                              </div>
                              
                          </div>
                    </div>
                  </div>
          </div>                 

       </div>   

  </div>
</div>

</div>