<script type="text/javascript">
function Trim(str){
  return str.replace(/^\s+|\s+$/g,"");
}

function validarForm(form){
      form.submit();
}

  </script>

  <div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
    <h3>Atualizar Perfil</h3>
  </div>          

  <div id="form-usuario" style='margin:0 auto; width:600px;margin-top:-20px;'>
    <form id="f1" id="f1"  onSubmit="return false" method="post" >

      <table class='tb-form-usuario' border='0'>
        <tr>
          <td class='tb-label'></td>
          <td class='tb-input'><span style="color:red;"><p id="validate_message">&nbsp;</p></span></td>
        </tr>  
        <tr>
          <td class='tb-label'>Sexo:</td>
          <td class='tb-input'>Maculino <input type="radio" name='sexo' class="required" value='masculino'/> Feminino <input type="radio" name='sexo' class="required" value='feminino'/></td>
        </tr>                                                                                     
        <tr>
          <td class='tb-label'>Telefone:</td>
          <td class='tb-input'><input type="text" name='fone' id="fone" size='15' class="required" value=''/></td>
        </tr>
        <tr>
          <td class='tb-label'>Estado:</td>
          <td class='tb-input'>
            <select id="estado" name="estado">
              <option></option>
              <option value="1">Acre</option>
              <option value="2">Alagoas</option>
              <option value="3">Amapa</option>
              <option value="4">Amazonas</option>
              <option value="5">Bahia</option>
              <option value="6">Ceara</option>
              <option value="7">Distrito Federal</option>
              <option value="8">Goios</option>
              <option value="9">Espirito Santo</option>
              <option value="10">Maranhao</option>
              <option value="11">Mato Grosso</option>
              <option value="12">Mato Grosso do Sul</option>
              <option value="13">Minas Gerais</option>
              <option value="14">Para</option>
              <option value="15">Paraiba</option>
              <option value="16">Parana</option>
              <option value="17">Pernambuco</option>
              <option value="18">Piaui</option>
              <option value="19">Rio de Janeiro</option>
              <option value="20">Rio Grande do Norte</option>
              <option value="21">Rio Grande do Sul</option>
              <option value="22">Rondonia</option>
              <option value="23">Roraima</option>
              <option value="24">Sao Paulo</option>
              <option value="25">Santa Catarina</option>
              <option value="26">Sergipe</option>
              <option value="27">Tocantins</option>
            </select>
          </td>
        </tr> 
        <tr>
          <td class='tb-label'>Cidade:</td>
          <td class='tb-input'><input type="text" name='cidade' size='40' class="required" value='<?php echo $form['cidade'];?>'/></td>
        </tr>   
        <tr>
          <td class='tb-label'>Rua/Avenida:</td>
          <td class='tb-input'><input type="text" name='rua' size='40' class="required" value='<?php echo $form['rua'];?>'/></td>
        </tr> 
        <tr>
          <td class='tb-label'>Complemento:</td>
          <td class='tb-input'><input type="text" name='complemento' size='40' class="required" value='<?php echo $form['complemento'];?>'/></td>
        </tr>                                                                            
        <tr>
          <td class='tb-label'>Bairro:</td>
          <td class='tb-input'><input type="text" name='bairro' size='40' class="required" value='<?php echo $form['bairro'];?>'/></td>
        </tr>                            
        <tr>
          <td class='tb-label'>CEP:</td>
          <td class='tb-input'><input type="text" name='cep' id='cep' size='10' class="required" value='<?php echo $form['cep'];?>'/></td>
        </tr> 
        <tr>
          <td class='tb-label'></td>
          <td class='tb-input'>
            <input type='submit' value='Enviar' onclick="return validarForm(this.form)"> 
          </td>
        </tr>                           
      </table> 
      <input type='hidden' name='task' value='Usuario'/> 
      <input type='hidden' name='action' value='updatePerfilAction'/> 
    </form>
  </div>