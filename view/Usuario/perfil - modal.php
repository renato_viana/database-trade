<?php 
$Tvar  = new TratamentoVar();
$perfil = $Tvar->getSession('login');
?>
<style type="text/css">
.p-container{
  margin: 0 auto;
  margin-top: 20px;
  margin-bottom:30px;
}
.panel{
  width: 680px;
}
#p{
  margin: 0 auto;
  width: 1100px;
  height: 600px;

}
#p-r{
  float:left;
  width: 350px;
  height: 600px;

}
#p-l{
  float:left;
  width: 750px;
  height: 600px;
}



</style>
<script type="text/javascript">
$(document).ready(function(){
    //Referances 
    //jQuery Cookie : https://github.com/carhartl/jquery-cookie
    //Modal : http://getbootstrap.com/javascript/#modals
    var my_cookie = $.cookie($('.modal-check').attr('name'));
    if (my_cookie && my_cookie == "true") {
        $(this).prop('checked', my_cookie);
        console.log('checked checkbox');
    }
    else{
        $('#myModal').modal('show');
        console.log('uncheck checkbox');
    }

    $(".modal-check").change(function() {
        $.cookie($(this).attr("name"), $(this).prop('checked'), {
            path: '/',
            expires: 1
        });
    });
});
</script>
<div class="page-header" style='border:0px solid;margin:0px;margin-top:-20px;'>
  <h3>Meu Perfil</h3>
</div> 

<div class="p-container">


<!--
Referances 
jQuery Cookie : https://github.com/carhartl/jquery-cookie
Modal : http://getbootstrap.com/javascript/#modals
-->
<script src="http://frontend.reklamor.com/jquery.cookie/jquery.cookie.js"></script>

<div class="container">
  <div class="row">
        <div class='modal fade' id='myModal'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class='modal-title'>
                          <strong>MODAL WITH JQUERY COOKIE</strong>
                        </h4>
                    </div>
                    <!-- / modal-header -->
                    <div class='modal-body'>
                        <img class="img-responsive" src="http://placehold.it/600x350&text=MODAL" />
                    </div>
                    <!-- / modal-body -->
                   <div class='modal-footer'>
                       <div class="checkbox pull-right">
                            <label>
                              <input class='modal-check' name='modal-check' type="checkbox"> Don't Show
                            </label>
                        </div>
                        <!--/ checkbox -->
                  </div>
                  <!--/ modal-footer -->
                </div>
                <!-- / modal-content -->
          </div>
          <!--/ modal-dialog -->
        </div>
        <!-- / modal -->
  </div>
    <!-- / row -->
</div>
<!-- / container -->



  <div id="p">

  <div id="p-r">

  </div>

  <div id="p-l">

          <div class="" >

                <div class="panel panel-info">
                  <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $perfil['nome'];?></h3>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <!--<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xfa1/t1.0-9/p417x417/264723_4095802326198_2030804694_n.jpg?z=100" class="img-circle" width='120'> </div>-->

                      <div class=" col-md-9 col-lg-9 "> 
                        <table class="table table-user-information">
                          <tbody>
          <!--
                            <tr>
                              <td><b>Dados de Acesso</b></td>
                              <td></td>
                            </tr>                  
                            <tr>
                              <td>Ultimo Login:</td>
                              <td><?php echo $Tvar->dateBr($perfil['ultimo_login'])." ".$perfil['hora_ultimo_login'];?></td>
                            </tr>
                            <tr>
                              <td>Numero de Login:</td>
                              <td><?php echo $perfil['qtd_login'];?></td>
                            </tr>                  -->
                            <tr>
                              <td><b>Dados Pessoais</b></td>
                              <td></td>
                            </tr>                  
                            <tr>
                              <td>Email</td>
                              <td><?php echo $perfil['email'];?></td>
                            </tr>                      
                            <tr>
                              <td>Telefone</td>
                              <td><?php echo $perfil['fone'];?></td>
                            </tr>                      
                            <tr>
                              <td>Data de Nascimento:</td>
                              <td><?php echo $Tvar->dateBr($perfil['nascimento']);?></td>
                            </tr>
                            <tr>
                              <td>Sexo:</td>
                              <td><?php echo $perfil['sexo'];?></td>
                            </tr>
                            <tr>
                              <td><b>Endereco</b></td>
                              <td></td>
                            </tr>                  
                            <tr>
                              <td>Estado</td>
                              <td><?php echo $perfil['estado'];?></td>
                            </tr>
                            <tr>
                              <td>Cidade</td>
                              <td><?php echo $perfil['cidade'];?></td>
                            </tr>
                            <tr>
                              <td>Rua</td>
                              <td><?php echo $perfil['rua'];?></td>
                            </tr>
                              <td>Complemento</td>
                              <td><?php echo $perfil['complemento'];?>
                            </td>
                            </tr>
                              <td>Bairro</td>
                              <td><?php echo $perfil['bairro'];?>
                            </td>  
                            </tr>
                              <td>Cep</td>
                              <td><?php echo $perfil['cep'];?>
                            </td>                                   
                          </tr>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="panel-footer" style="padding-bottom:35px" >
                  <a href="edit.html"><i class="glyphicon glyphicon-edit2"></i></a>
                  <span class="pull-right">
                    <a href="?task=Usuario&action=updatePerfilPage" data-original-title="Edit this user" data-toggle="tooltip" title='Editar' type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                  </span>
                </div>

              </div>
            </div>

  </div>
</div>

</div>