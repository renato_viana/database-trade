<!-- Header -->
<!DOCTYPE html>
<html lang="en">
<head>
  <link href='img/template/favicon.png' rel='icon' type='image/x-icon'/>
  <title>MTCG - DataBase</title>  
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css"/>
  <link href="assets/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css"/>
  <link href="assets/css/jquery.smartmenus.bootstrap.css" media="screen" rel="stylesheet" type="text/css"/>
  <link href="assets/css/mystyle.css" media="screen" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="assets/css/default.css">   
  <script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.js"></script>
  <script type="text/javascript" src="assets/js/jquery.smartmenus.js"></script>
  <script type="text/javascript" src="assets/js/jquery.smartmenus.bootstrap.js"></script>
  <script type="text/javascript" src="assets/js/validate.js"></script>    
  <script type="text/javascript" src="assets/js/jquery.maskedinput.js"></script>    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51183994-1', 'mtcgdatabase.com.br');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
      jQuery(function($){
       $("#fone").mask("(99) 9999-9999");
       $("#nascimento").mask("99/99/9999");
       $("#data_inicio").mask("99/99/9999");
       $("#data_fim").mask("99/99/9999");
       $("#hora_fim").mask("99:99");
       $("#cep").mask("99999-999");
       });

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</head>
<body>


  <?php if(isset($Login) && $Login != false){?>
  <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" href="#">Database</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="#">Mensagem PM</a>
                    <ul class="dropdown-menu" role="menu">
                          <li><a href="?task=Mensagem&action=entradaPage">Caixa de Entrada</a></li>
                          <li><a href="?task=Mensagem&action=enviadasPage">Enviadas</a></li>
                    </ul>
              </li>               
              <li><a href="#">Estande</a>
                    <ul class="dropdown-menu" role="menu">
                          <li><a href="?task=Estande&action=configPage">Minha Estande</a></li>
                          <li><a href="?task=Estande&action=wantPage">Want</a></li>
                          <li><a href="?task=Estande&action=havePage">Have</a></li>
                    </ul>
              </li>              
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Negociações</span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?task=Negociacao&action=propostasEnviadasPage">Enviadas</a></li>
                  <li><a href="?task=Negociacao&action=propostasRecebidasPage">Recebidas</a></li>
                </ul>
              </li>                            
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Leilões</span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?task=Leilao&action=cadastroPage">Novo Leilão</a></li>
                  <li><a href="?task=Leilao&action=meusLeiloesPage">Meus Leilões</a></li>
                  <li><a href="?task=Leilao&action=leiloesGanhosPage">Leilões Ganhos</a></li>
                </ul>
              </li> 
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Anúncios</span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?task=Leilao&action=cadastrarAnuncioPage">Novo Anúncios</a></li>
                  <li><a href="?task=Leilao&action=meusAnunciosPage">Meus Anúncios</a></li>
                </ul>
              </li>                
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Deck</span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?task=Deck&action=construtorPage">Construtor</a></li>
                  <li><a href="?task=Deck&action=minhasDeckListPage">Minhas Deck Lists</a></li>
                  <li><a href="#">Leilões Ganhos</a></li>
                </ul>
              </li> 
              <li><a href="?task=Carta&action=cardMakerPage">Card Maker</a></li>                           
            </ul>
        <ul class="nav navbar-nav navbar-right">

          <li><a href="#"><?php echo $Login['login'];?></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="?task=Usuario&action=senhaPage">Mudar Senha</a></li>
              <li><a href="?task=Usuario&action=perfilPage&id=<?php echo $Login['id'];?>">Meus Dados</a></li>
              <li><a href="?task=Usuario&action=updatePerfilPage">Atualizar Perfil</a></li>
              <li><a href="?task=Usuario&action=fotoPerfilPage">Foto do Perfil</a></li>
            </ul>
          </li>

          <?php if(isset($Login) && $Login != false && $Login['tipo_usuario'] == 1 || $Login['tipo_usuario'] == 2){?>
          <li><a href="#">
              ADMINISTRACAO </a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="?task=AdminCarta&action=admColecaoPage">Colecao</a></li>
              <li><a href="?task=AdminCarta&action=admAlinhamentoPage">Alinhamento</a></li>
              <li><a href="?task=AdminCarta&action=admAfiliacaoPage">Afiliacao</a></li>
              <li><a href="?task=AdminCarta&action=admCartaPage">Carta</a></li> 
              <li class="divider"></li>
              <li><a href="?task=Negociacao&action=negociacoesAdminPage">Negociações Estande</a></li>
              <li><a href="?task=Negociacao&action=negociacoesLeilaoAdminPage">Negociações Leilões</a></li>                         
            </ul>
          </li>          
          <?php } ?>
          <li><a href="?task=Usuario&action=logoutAction"><b>Sair</b></a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <?php } ?>  
  <table width="1100" id="tb-main" <?php if($Login['tipo_usuario'] == 0) echo "style='margin-top: 0px;'";?>>
    <!-- Header-->
    <tr>
     <td>
      <div id='banner_bar' style="background-image: url('img/template/top.png');">
       <div id='logo'><a href="?task=Carta&action=buscadorPage"><img src="img/template/logo_bs.png"></a></div>
       <div id='login_bar'><a href="?task=Carta&action=buscadorPage"><img src="img/template/mtcg.png"></div>
     </div>	
  </td>
</tr>	