<tr>
 <td height='680' style="background-color:#FFF;">
  <div id='content'>

<nav class="navbar navbar-default" role="navigation" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="width:1150px;">
      <ul class="nav navbar-nav">
         <li><a href="?task=Carta&action=buscadorPage">Buscador</a></li>
         <li class="dropdown"><a href="#">Coleções</a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="?task=Carta&action=colecoesPage">Booster Box</a></li>
            <li><a href="?task=Carta&action=deckBoxPage">Deck Box</a></li>
            <li><a href="?task=Carta&action=promoPage">Cartas Promos</a></li>
            <li><a href="?task=Carta&action=especiaisPage">Especiais</a></li>
          </ul>
         </li>
         <li class="dropdown"><a href="#">Card/Deck</a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="?task=Deck&action=listarDecksPage">Deck Lists</a></li>
            <li><a href="?task=Carta&action=top10Page">Top 10 Cards</a></li>
          </ul>
         </li>
         <li><a href="?task=Estande&action=defaultPage">Estandes</a></li>
         <li><a href="?task=Leilao&action=defaultPage">Leilões</a></li>
         <li><a href="?task=Leilao&action=defaultAnuncioPage">Anúncios</a></li>
        <?php if(isset($Login) && $Login != false){?>

        <?php } else { ?>
        <li><a href="?task=Usuario&action=loginPage">Login</a></li>
        <?php }  ?>
      </ul>
      <ul class="nav navbar-nav navbar-right" style="">
      <form class="navbar-form navbar-left" role="search" method="post" action="?task=Carta&action=listarPage">
        <div class="form-group">
          <input name="nome" type="text" class="form-control" placeholder="Nome da Carta" style="width:250px;" required>
        </div>
        <button type="submit" class="btn btn-default">Pesquisar</button>
      </form>        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>           

<div id='<?php if(!isset($col3)){ echo 'column-01';} else{ echo 'column-03';}?>'>
          <?php if(isset($alert)){ ?>
<div class="header-bar" style="margin-bottom:50px;">
          <div class="<?php echo "alert alert-".$alert['tipo'];?>" role="alert" style="margin-bottom:40px;"><?php echo $alert['msg'];?></div>
</div>
          <?php } ?>
    <div class="header-bar">
          <?php require_once 'view/'.$task.'/'.$action.'.php';?>
    </div>
    </div>

    <?php if(!isset($col3)){?>
    <div id='column-02'>
        <?php include("view/Template/menu.php");?>
    </div>
    <?php } ?>
  </div>
</td>
</tr>
