
<a name="topo"></a>
<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">   
<script src="libs/ckeditor/ckeditor.js"></script>
<fieldset>
<legend>
		<div class="col-md-4">
    		<?php echo $Result['estande']['nome'];?>
    	</div>
</legend>
<br><br>
<style type="text/css">
.comentarios{
	width: 940px;
	margin: 0 auto;
	margin-top: 35px;
	margin-bottom: 50px;
}
</style>

<div class="row">
      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                <!-- Form Name -->
                <h3>Estande</h3>
				
                <!-- Text input-->
                <div class="control-group">
                	<b>Última Atualização:</b> <?php echo implode ( "/",array_reverse(explode("-",$Result['estande']['atualizado'])))." ".$Result['estande']['atualizado-hora'];?><br>
                	<b>Visitas:</b> <?php echo $Result['estande']['views'];?><br>
                	<!--<b>Perguntas:</b> 31<br>-->
                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->

      <div class="col-md-6 col-xs-12">
                <fieldset class="bs-callout bs-callout-warning">
                <h3>Dono</h3>   
                <!-- Text input-->
                 <b>Login:</b> <a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['usuario']['id'];?>"><?php echo $Result['usuario']['login'];?></a><br>
                 <b>Reputação:</b> <?php echo $Result['usuario']['reputacao'];?> <span class="glyphicon glyphicon-star"></span><br>
                 <b>Estado:</b> <?php echo $Result['usuario']['cidade']." - ".$Result['usuario']['estado'];?><br>
                 <b>Registrado em:</b> <?php echo  implode ( "/",array_reverse(explode("-",$Result['usuario']['data_cadastro'])));?><br>
				<div class="" style="float:left;margin-top:3px;margin-bottom:3px;">
					<a href="?task=Negociacao&action=abrirNegociacaoPage&estande=<?php echo $Result['estande']['id'];?>" id="salvar" name="salvar" class="btn btn-primary" value="submit">Abrir Negociação</a>
				</div>                 
              </fieldset>
              

        </div>

</div>


<div class="row">
      <div class="col-md-12 col-xs-12">
                <fieldset class="bs-callout bs-callout-danger">
                <!-- Form Name -->
                <h3>Informações</h3>
					<?php echo $Result['estande']['informacoes']; ?>			
                <!-- Text input-->
                <div class="control-group">

                </div>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
        
</div>


<div style="">
<ul class="pager">
  <li class="previous"><a href="#perguntas">&dArr; Ir para perguntas</a></li>
</ul>
</div>

<div class="row">

      <div class="col-md-6 col-xs-12">


         <div class="col-md-12">
				<div class="col-md-12">
				 	<div class="panel panel-primary">
				 		<div class="panel-heading">
				 			<h3 class="panel-title"><b>HAVE (tenho) LIST</b></h3>

				 		</div>
				 		<div class="panel-body">

			    	<?php for ($j=6; $j >= 1; $j--) { 
			    	if(count($Result['havelist'][$j]) == 0)
			    		continue;
			    	switch ($j) {
			    			case 6:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Exclusiva</b></h5></center></div>";
			    				break;			    							    		
			    			case 5:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Ultra Rara</b></h5></center></div>";
			    				break;
			    			case 4:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Super Rara</b></h5></center></div>";
			    				break;

			    			case 3:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Rara</b></h5></center></div>";
			    				break;			    
			    			case 2:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Incomum</b></h5></center></div>";
			    				break;			    		
			    			case 1:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Comum</b></h5></center></div>";
			    				break;
			    		}	
			    	?>				 			

			  <p>	 			
              <table id="mytable" class="" border="0">

				    <tbody>
						<tr>
			    	<?php 
			    	$c=0;
			    	for ($i=0; $i < count($Result['havelist'][$j]) && $i < 50; $i++) { 
			    	?>
				    	<th wigth="2">
				    		<a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['havelist'][$j][$i]['id'];?>"><img src="<?php echo 'img/cartas/'.$Result['havelist'][$j][$i]['sigla'].'/thumb/'.$Result['havelist'][$j][$i]['numero'].".png";?>" width="50"></a>
				    		<center><?php echo $Result['havelist'][$j][$i]['qtd']."x";?></center>
				    	</th>

				    <?php 
				    $c++;
				    if($c==6){
				    	echo "</tr><tr>";
				    	$c=0;
				    }
				    } ?>
				    	</tr>				    
				    </tbody>
				</table>
			   </p>

				    <?php } ?>
				 		</div>
				 	</div>
				 </div>

         </div>
              

        </div>


      <div class="col-md-6 col-xs-12">

         <div class="col-md-12">
				<div class="col-md-12">
				 	<div class="panel panel-primary">
				 		<div class="panel-heading">
				 			<h3 class="panel-title"><b>WANT (Quero) LIST</b></h3>
				 		</div>
				 		<div class="panel-body">


			    	<?php for ($j=6; $j >= 1; $j--) { 
			    	if(count($Result['wantlist'][$j]) == 0)
			    		continue;
			    	switch ($j) {
			    			case 6:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Exclusiva</b></h5></center></div>";
			    				break;			    							    		
			    			case 5:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Ultra Rara</b></h5></center></div>";
			    				break;
			    			case 4:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Super Rara</b></h5></center></div>";
			    				break;

			    			case 3:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Rara</b></h5></center></div>";
			    				break;			    
			    			case 2:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Incomum</b></h5></center></div>";
			    				break;			    		
			    			case 1:
			    				echo "<div style='border-bottom:1px solid #ccc;'><center><h5><b>Comum</b></h5></center></div>";
			    				break;
			    		}	
			    	?>				 			

			  <p>	 			
              <table id="mytable" class="" border="0">

				    <tbody>
						<tr>
			    	<?php 
			    	$c=0;
			    	for ($i=0; $i < count($Result['wantlist'][$j]) && $i < 50; $i++) { 
			    	?>
				    	<th wigth="2">
				    		<a href="?task=Carta&action=detalhesPage&carta=<?php echo $Result['wantlist'][$j][$i]['id'];?>"><img src="<?php echo 'img/cartas/'.$Result['wantlist'][$j][$i]['sigla'].'/thumb/'.$Result['wantlist'][$j][$i]['numero'].".png";?>" width="50"></a>
				    		<center><?php echo $Result['wantlist'][$j][$i]['qtd']."x";?></center>
				    	</th>

				    <?php 
				    $c++;
				    if($c==6){
				    	echo "</tr><tr>";
				    	$c=0;
				    }
				    } ?>
				    	</tr>				    
				    </tbody>
				</table>
			   </p>

				    <?php } ?>
				 		</div>
				 	</div>
				 </div>

         </div>


      </div><!--\div col-md6 col-sm-12 -->        

</div>


<?php 
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>

<a name="perguntas"></a>
<div class="row">
				<div class="comentarios">

						<fieldset>
							<legend>Perguntas ao Estandista</legend>
						<?php if(isset($Result['login']['id'])){ ?>
						<!-- Place this in the body of the page content -->
						<form method="post" action="?task=Pergunta&action=addPerguntaAction" name="comentario">
							<textarea name="pergunta"></textarea>

							<script> 
							CKEDITOR.replace( 'pergunta', {
								toolbar: [
								{ name: 'document' },	
								{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
								],
								height: '80px',
								enterMode : CKEDITOR.ENTER_BR
							});
							</script> 

						
							<!-- Button -->

								<div class="" style="float:right;margin-top:3px;margin-bottom:3px;">
									<button id="salvar" name="salvar" class="btn btn-primary" value="submit">Enviar Pergunta</button>
								</div>
								<input type="hidden" name="estande" value="<?php echo $_GET['estande'];?>">
						</form>
						<?php }else{ ?>
							<center><h5>Para perguntar é necessario fazer <a href="?task=Usuario&action=loginPage">login!</a></h5></center>
						<?php } ?>
						</fieldset>

<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Estande&action=estandePage&estande='.$_GET['estande'].'&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Estande&action=estandePage&estande='.$_GET['estande'].'&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Estande&action=estandePage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>


					<table width="100%">
						<?php 
						$pergunta = $Result['perguntas'];
						$n = count($pergunta);
						$n = ($n >= 9) ? 9 : $n;						
						for($i=$paginacao->primeiro_registro;$i<$paginacao->primeiro_registro+$n;$i++){

							if($pergunta[$i]['usuario_login'] != ""){
						?>						
						<tr><td>
							<div class="panel panel-default">
								<div class="panel-body">
									<div id="imagem" style="width:55px;float:left;border:0px solid;margin-top:-10px;"><br>
									  <?php if(file_exists("upload/usuario/".$pergunta[$i]['usuario_id'].".".$pergunta[$i]['usuario_img'])){?>
									      <img src="<?php echo "upload/usuario/".$pergunta[$i]['usuario_id'].".".$pergunta[$i]['usuario_img'] ?>" width="50px"/>
									  <?php } else{ ?>
									      <img src="upload/usuario/foto.jpg"  width="50px"/>
									  <?php }?>
									</div>									
									<h5><b><a href="?task=Usuario&action=perfilPage&id=<?php echo $pergunta[$i]['usuario_id'];?>"><?php echo $pergunta[$i]['usuario_login'];?></a><span class="says"> pergunta:</span></b></h5>
									<div class="comment-content">
										<p><h6><?php echo $pergunta[$i]['pergunta'];?></h6></p>
									</div><!-- .comment-content -->									
								</div>
								<div class="panel-footer">
									<time datetime="2014-06-10T14:38:30+00:00">
										<?php echo $pergunta[$i]['data']." ".$pergunta[$i]['hora'];?>				
										<?php if($Result['login']['id'] == $pergunta[$i]['usuario_id']){ ?>
										<div style="width:50px;float:right;">
											<a href="?task=Pergunta&action=removePerguntaAction&estande=<?php echo $_GET['estande'];?>&pergunta=<?php echo $pergunta[$i]['id'];?>" style="text-decoration:none;"><span class="label label-danger">deletar</span></a>
										</div>
										<?php }?>
									</time>
								</div>
							</div>

						</td></tr>
						<?php } }?>												
					</table>
<div style="float:right;">
<ul class="pager">
  <li class="previous"><a href="#topo">&uArr; Ir para topo</a></li>
</ul>
</div>

<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Estande&action=estandePage&estande='.$_GET['estande'].'&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Estande&action=estandePage&estande='.$_GET['estande'].'&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Estande&action=estandePage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>



				</div>

</div>

<fieldset>

