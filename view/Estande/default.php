<link rel="stylesheet" type="text/css" href="http://fapto.org.br/cesicard/assets/css/estilo.css">
<fieldset>
<legend>Estandes</legend>

<style type="text/css">
#mytable a{
	color: black;
}
</style>

<?php 
$paginacao = $Result['paginacao'];
unset($Result['paginacao']);
?>


  <div class="row">
    <div class="col-md-11">
      <form class="form-horizontal" role="form" enctype="multipart/form-data" name="f1" action="?task=Estande&action=defaultPage" method="post">
        <fieldset>

          <!-- Text input-->
          <div class="form-group">

            <label class="col-sm-2 control-label" for="textinput">Quem quer:</label>
            <div class="col-sm-2">
              <input type="text" placeholder="Quem quer" class="form-control" name="quer">
            </div>            

            <label class="col-sm-2 control-label" for="frete">E/OU</label>
            <div class="col-sm-2">
              <select id="selectbasic" name="tipo" class="form-control">
                <option value="">Nada</option>
                  <option value="1">E</option>
                  <option value="2">OU</option>
              </select>
            </div>

            <label class="col-sm-2 control-label" for="textinput">Quem tem: </label>
            <div class="col-sm-2">
              <input type="text" placeholder="Quem tem" class="form-control" name="tem">
            </div>

          </div>                                   
          
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary">Buscar</button>
              </div>
            </div>
          </div>


        </fieldset>
      </form>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

<div class="row">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Estande&action=defaultPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Estande&action=defaultPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Estande&action=defaultPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>

<div class="row">
            <div class="col-md-12 col-xs-12" style="height:590px;">
                <fieldset class="bs-callout bs-callout-danger">
				
                <!-- Text input-->
                          <div class="col-md-12">
                          
                                <table id="mytable" class="table table-bordred table-striped">
                                     <thead>
                                     <th>Nome da estande</th>
                                      <th>Dono</th>
                                      <th>Visualizações</th>
                                      <th>Última Atualização</th>
                                      <th></th>
                                     </thead>            
                      <tbody >
                        <?php 
                            $pergunta = $Result['estandes'];
                            $n = count($pergunta);
                            $n = ($n >= 10) ? 10 : $n;      
                            for($i=$paginacao->primeiro_registro;$i<$paginacao->primeiro_registro+$n;$i++){
                          ?>
                      <tr>
                      	<td><a href="?task=Estande&action=estandePage&estande=<?php echo $Result['estandes'][$i]['id'];?>"><b><?php echo $Result['estandes'][$i]['nome'];?></b></a></td>
                      	<td><a href="?task=Usuario&action=perfilPage&id=<?php echo $Result['estandes'][$i]['usuario_id'];?>"><b><?php echo $Result['estandes'][$i]['usuario_login'];?></b></a></td>
                      	<td><?php echo $Result['estandes'][$i]['views'];?></td>
	                    <td><?php echo implode ( "/",array_reverse(explode("-",$Result['estandes'][$i]['atualizado'])))." ".$Result['estandes'][$i]['atualizado-hora'] ;?></td>
	                    <td><a href="?task=Estande&action=estandePage&estande=<?php echo $Result['estandes'][$i]['id'];?>" class="btn-sm btn-primary" style="color:white;"><b>entrar<b></a></td>
                      </tr>
                      <?php } ?>
                      </tbody>
                  </table>

                </fieldset>

      </div><!--\div col-md6 col-sm-12 -->
</div>


<div class="row" style="margin-bottom:20px;">
<center>
<ul class="pagination">
  <li><a href="<?php echo '?task=Estande&action=defaultPage&pagina=1';?>">&laquo;</a></li>
  <?php for($i=1;$i<=$paginacao->paginas;$i++) { ?>
  <li <?php if(!isset($_GET['pagina']) && $i == 1) echo "class='active'";?> <?php if($_GET['pagina'] == $i) echo "class='active'";?> ><a href="<?php echo '?task=Estande&action=defaultPage&pagina='.$i;?>"><?php echo $i;?></a></li>
  <?php } ?>
  <li><a href="<?php echo '?task=Estande&action=defaultPage&pagina='.$paginacao->paginas;?>">&raquo;</a></li>
</ul>
</center>
</div>

<fieldset>

