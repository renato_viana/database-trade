<?php

class RecuperarSenha
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $login;
var $key;


// **********************
// CONSTRUCTOR METHOD
// **********************

function RecuperarSenha()
{
$this->database = new medoo();

}


// **********************
// SELECT METHOD / LOAD
// **********************

function select($filter = NULL)
{

	$row = $this->database->select("mtcg_recuperar_senha", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];
	
	$this->id = $row->id;
	$this->login = $row->login;
	$this->key = $row->key;

	return $r;

}

// **********************
// DELETE
// **********************

function delete($dados)
{
	$this->database->delete("mtcg_recuperar_senha", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados)
{
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("mtcg_recuperar_senha", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados)
{
	return $this->database->update("mtcg_recuperar_senha",$dados,array("carta_id" => $id));
}


} // class : end

?>
<!-- end of generated class -->
