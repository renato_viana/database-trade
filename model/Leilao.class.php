<?php

// **********************
// CLASS DECLARATION
// **********************

class Leilao
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Leilao(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_leilao", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

function time($id,$datatime){

  $sql = "";
  $sql .= "SELECT * FROM bs_leilao l WHERE '".$datatime."' < l.data_fim AND l.id=".$id;
  
  $data = $this->database->query($sql)->fetchAll();
  return $data;                

}

// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_leilao", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_leilao", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_leilao",$dados,array("id" => $id));
}

} // class : end

?>
