<?php

// **********************
// CLASS DECLARATION
// **********************

class Estande
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $usuario_id; 
var $informacoes; 
var $criado; 
var $atualizado; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Estande(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_estande", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

function searchEstande($filter = NULL){

		$sql = "";
		$sql .= "select estande.id,estande.nome,estande.usuario_id,usu.login,estande.views,estande.atualizado from bs_estande estande ";
		$sql .= "join bs_want w on w.estande_id=estande.id ";
		$sql .= "join bs_have h on h.estande_id=estande.id ";
		$sql .= "join bs_carta cw on cw.id=w.carta_id ";
		$sql .= "join bs_carta ch on ch.id=h.carta_id ";
		$sql .= "left join bs_usuario usu on usu.id=estande.usuario_id ";

		if($filter != NULL){

			$sql .= "WHERE ";

			if($filter['tem'] != "")
				$sql .= " cw.nome LIKE '%".$filter['tem']."%' ";

			if($filter['tipo'] == 1)
				$sql .= " AND ";

			if($filter['tipo'] == 2)
				$sql .= " OR ";		

			if($filter['tipo'] == "" && $filter['quer'] != "" && $filter['tem'] != "")
				$sql .= " OR ";				

			if($filter['quer'] != "")
				$sql .= " ch.nome LIKE '%".$filter['quer']."%' ";	

		}
		$sql .= " group by estande.id" ;
		$sql .= " ORDER BY estande.atualizado DESC " ;
  	
		$data = $this->database->query($sql)->fetchAll();
		return $data;                
}

// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_estande", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_estande", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_estande",$dados,array("id" => $id));
}


} // class : end

?>
