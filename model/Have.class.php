<?php

// **********************
// CLASS DECLARATION
// **********************

class Have
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $usuario_id; 
var $informacoes; 
var $criado; 
var $atualizado; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Have(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_have", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}


function getHaveList($estande,$raridade){

	$sql = "SELECT carta.id as carta_id,carta.nome,carta.numero,col.sigla, 
       rar.nome, have.qtd
FROM   bs_have have
       JOIN bs_carta carta
         ON carta.id = have.carta_id
       JOIN bs_colecao col
         ON col.id = carta.colecao
       JOIN bs_raridade rar
         ON rar.id = carta.raridade
WHERE  have.estande_id = ".$estande." AND rar.id = ".$raridade." 
ORDER  BY col.id,
          rar.id DESC ";

         $data = $this->database->query($sql)->fetchAll();
         
         return $data;          
}


// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_have", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_have", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_have",$dados,array("id" => $id));
}


} // class : end

?>
