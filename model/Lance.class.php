<?php

// **********************
// CLASS DECLARATION
// **********************

class Lance
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Lance(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_lances", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

function max($id){
  $sql = "";
  $sql .= "select MAX(valor) as lance from bs_lances l where l.leilao_id= ".$id;
  
  $data = $this->database->query($sql)->fetchAll();
  return $data;                

}


// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_lances", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_lances", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_lances",$dados,array("id" => $id));
}

} // class : end

?>
