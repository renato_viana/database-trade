<?php

// **********************
// CLASS DECLARATION
// **********************

class NegociacaoLeilao
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function NegociacaoLeilao(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_negociacao_leilao", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_negociacao_leilao", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_negociacao_leilao", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_negociacao_leilao",$dados,array("id" => $id));
}

} // class : end

?>
