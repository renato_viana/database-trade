<?php

class Frete
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $tipo;
var $descricao;
var $valor;
var $database;


// **********************
// CONSTRUCTOR METHOD
// **********************

function Frete()
{
$this->database = new medoo();

}


// **********************
// SELECT METHOD / LOAD
// **********************

function select($filter = NULL)
{

	$row = $this->database->select("mtcg_frete", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];
	
	$this->id = $row->id;
	$this->tipo = $row->tipo;
	$this->descricao = $row->descricao;
	$this->valor = $row->valor;

	return $r;

}

// **********************
// DELETE
// **********************

function delete($id)
{


}

// **********************
// INSERT
// **********************

function insert($dados)
{
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("mtcg_estoque", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados)
{
	return $this->database->update("mtcg_estoque",$dados,array("carta_id" => $id));
}


} // class : end

?>
<!-- end of generated class -->
