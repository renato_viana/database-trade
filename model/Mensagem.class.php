<?php

// **********************
// CLASS DECLARATION
// **********************

class Mensagem
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Mensagem(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_mensagem", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_mensagem", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_mensagem", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_mensagem",$dados,array("id" => $id));
}

} // class : end

?>
