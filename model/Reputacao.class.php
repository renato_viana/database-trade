<?php

// **********************
// CLASS DECLARATION
// **********************

class Reputacao
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Reputacao(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_reputacao", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}


// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_reputacao", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_reputacao", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_reputacao",$dados,array("id" => $id));
}

} // class : end

?>
