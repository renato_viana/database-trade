<?php

// **********************
// CLASS DECLARATION
// **********************

class Anuncio
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Anuncio(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_anuncio", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}


// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_anuncio", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_anuncio", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_anuncio",$dados,array("id" => $id));
}

} // class : end

?>
