<?php

// **********************
// CLASS DECLARATION
// **********************

class TagAnuncio
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $usuario_id; 
var $informacoes; 
var $criado; 
var $atualizado; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function TagAnuncio(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_tag_anuncio", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_tag_anuncio", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_tag_anuncio", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_tag_anuncio",$dados,array("id" => $id));
}


function getTagList($anuncio,$raridade){

	$sql = "SELECT carta.id as carta_id,carta.nome,carta.numero,col.sigla, 
       rar.nome, tag.qtd
FROM   bs_tag_anuncio tag
       JOIN bs_carta carta
         ON carta.id = tag.carta_id
       JOIN bs_colecao col
         ON col.id = carta.colecao
       JOIN bs_raridade rar
         ON rar.id = carta.raridade
WHERE  tag.anuncio_id = ".$anuncio." AND rar.id = ".$raridade." 
ORDER  BY col.id,
          rar.id DESC ";

         $data = $this->database->query($sql)->fetchAll();
         
         return $data;          

}


} // class : end

?>
