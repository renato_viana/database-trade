<?php

// **********************
// CLASS DECLARATION
// **********************

class Want
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $usuario_id; 
var $informacoes; 
var $criado; 
var $atualizado; 
var $database; 


// **********************
// CONSTRUCTOR METHOD
// **********************

function Want(){

$this->database = new medoo();

}

function select($filter){
	$row = $this->database->select("bs_want", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];

	return $r;
}

// **********************
// DELETE
// **********************

function delete($dados){
	$this->database->delete("bs_want", $dados);
}

// **********************
// INSERT
// **********************

function insert($dados){
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_want", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados){
	return $this->database->update("bs_want",$dados,array("id" => $id));
}


function getWantList($estande,$raridade){

	$sql = "SELECT carta.id as carta_id,carta.nome,carta.numero,col.sigla, 
       rar.nome, want.qtd
FROM   bs_want want
       JOIN bs_carta carta
         ON carta.id = want.carta_id
       JOIN bs_colecao col
         ON col.id = carta.colecao
       JOIN bs_raridade rar
         ON rar.id = carta.raridade
WHERE  want.estande_id = ".$estande." AND rar.id = ".$raridade." 
ORDER  BY col.id,
          rar.id DESC ";

         $data = $this->database->query($sql)->fetchAll();
         
         return $data;          

}


} // class : end

?>
