<?php

class Estado
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id; 
var $estado;
var $database;


// **********************
// CONSTRUCTOR METHOD
// **********************

function Estado()
{
	$this->database = new medoo();

}


// **********************
// SELECT METHOD / LOAD
// **********************

function select($filter = NULL)
{

	$row = $this->database->select("bs_estados", "*", $filter);
	$r   = $row;
	$row = (object) $row[0];
	
	$this->id = $row->id;
	$this->estado = $row->estado;

	return $r;

}

// **********************
// DELETE
// **********************

function delete($id)
{


}

// **********************
// INSERT
// **********************

function insert($dados)
{
	$this->id = ""; // clear key for autoincrement
	$this->id = $this->database->insert("bs_estados", $dados);
	return $this->id;
}

// **********************
// UPDATE
// **********************

function update($id,$dados)
{
	return $this->database->update("bs_estados",$dados,array("carta_id" => $id));
}


} // class : end

?>
<!-- end of generated class -->
