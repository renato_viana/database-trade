function mudarNome(){

	obj = document.form;
	
	document.getElementById("box-02").innerHTML = obj.nome.value;
	document.getElementById("footer_name").innerHTML = obj.nome.value;
	
	if(obj.nome.value == "")
		document.getElementById("box-02").innerHTML = "Card Name";
		
	if(obj.nome.value == "")
		document.getElementById("footer_name").innerHTML = "Card Name";		
}

function mudarEgo(){

	obj = document.form;
	
	document.getElementById("box-03").innerHTML = obj.aego.value;
	
	if(obj.aego.value == "")
		document.getElementById("box-03").innerHTML = "Alter Ego Name";
			
}

function mudarColecao(){

	obj = document.form;
	
	document.getElementById("m-colecao").innerHTML = obj.colecao.value;
	
	if(obj.colecao.value == "")
		document.getElementById("m-colecao").innerHTML = "XXXX";
			
}
function mudarNumero(){

	obj = document.form;
	
	document.getElementById("m-numero").innerHTML = obj.numero.value+"/100";
	
	if(obj.numero.value == "")
		document.getElementById("m-numero").innerHTML = "1/100";
			
}
function mudarAlinhamento(){

	obj = document.form;
	
	document.getElementById("box-04").innerHTML = obj.alinhamento.value;
	
	if(obj.alinhamento.value == "")
		document.getElementById("box-04").innerHTML = "N/A";
			
}

function mudarAfiliacao(){

	obj = document.form;
	
	document.getElementById("box-08").innerHTML = "<center>"+obj.afiliacao.value+"</center>";
	if(obj.afiliacao.value == "")
		document.getElementById("box-08").innerHTML = "<center>N/A</center>";
			
}

function mudarEnergia(){
	obj = document.form;
	url = "img/tpl_cardmaker/energia_"+obj.energia.value+".png";
	document.getElementById("img_energia").src=url;
			
}
function mudarEscudo(){
	obj = document.form;
	url = "img/tpl_cardmaker/escudo_"+obj.escudo.value+".png";
	document.getElementById("img_escudo").src=url;
			
}
function mudarPoder1(){
	obj = document.form;
	url = "img/tpl_cardmaker/p_"+obj.poder1.value+".png";
	document.getElementById("poder1").src=url;		
}
function mudarPoder2(){
	obj = document.form;
	url = "img/tpl_cardmaker/p_"+obj.poder2.value+".png";
	document.getElementById("poder2").src=url;			
}
function mudarPoder3(){
	obj = document.form;
	url = "img/tpl_cardmaker/p_"+obj.poder3.value+".png";
	document.getElementById("poder3").src=url;			
}
function mudarPoder4(){
	obj = document.form;
	url = "img/tpl_cardmaker/p_"+obj.poder4.value+".png";
	document.getElementById("poder4").src=url;			
}
function getPoder(str){
	str = str.replace("[p1]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_1.png' />");
	str = str.replace("[p2]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_2.png' />");
	str = str.replace("[p3]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_3.png' />");
	str = str.replace("[p4]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_4.png' />");
	str = str.replace("[p5]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_5.png' />");
	str = str.replace("[p6]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_6.png' />");
	str = str.replace("[p7]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_7.png' />");
	str = str.replace("[p8]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_8.png' />");
	str = str.replace("[p9]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_9.png' />");
	str = str.replace("[p10]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_10.png' />");
	str = str.replace("[p11]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_11.png' />");
	str = str.replace("[p12]","<img style='width: 14px; height: 14px;' src='img/tpl_cardmaker/p_12.png' />");
	str = str.replace("(x)","<img style='width: 12px; height: 12px;' src='img/tpl_cardmaker/cap_x.png' />");
    str = str.replace("(0)","<img style='width: 12px; height: 12px;' src='img/tpl_cardmaker/cap_0.png' />");
	str = str.replace("(1)","<img style='width: 12px; height: 12px;' src='img/tpl_cardmaker/cap_1.png' />");
	str = str.replace("(2)","<img style='width: 12px; height: 12px;' src='img/tpl_cardmaker/cap_2.png' />");
	str = str.replace("[b]","<b>");
	str = str.replace("[/b]","</b>");
	str = str.replace("[l]","</br>");
	return str;
}

function mudarTexto(){
	obj = document.form;
	document.getElementById("txt_permanente").innerHTML = getPoder(obj.texto.value);		
}
function mudarTextoAcao(){
	obj = document.form;
	document.getElementById("ac_body").innerHTML = getPoder(obj.efeito.value);		
}
function mudarNomeAcao(){
	obj = document.form;
	document.getElementById("titulo").innerHTML = getPoder(obj.nome_acao.value);		
}
function mudarAcao(obj){
	if(obj.value == 1){
		document.getElementById('box_acao').style.visibility = 'visible';
		acao = document.form;
		mudarTipoAcao(acao.tipo_acao);
	}
	else{ 
		document.getElementById('box_acao').style.visibility = 'hidden';
		document.getElementById('imprevista').style.visibility = 'hidden';
		document.getElementById('antecipar').style.visibility = 'hidden';
	}
}

function mudarTipoAcao(obj){
 //
	
	if(obj.value == 1 || obj.value == 3){
		document.getElementById('box_acao').style.borderColor = "black";
		document.getElementById("titulo").style.color = "black";
		document.getElementById("ac_body").style.color = "black";
	}	
	
	if(obj.value == 2 || obj.value == 4){
		document.getElementById('box_acao').style.borderColor = "red";
		document.getElementById("titulo").style.color = "red";
		document.getElementById("ac_body").style.color = "red";
	}
	
	
	if(obj.value == 1){
		document.getElementById('imprevista').style.visibility = 'hidden';
		document.getElementById('antecipar').style.visibility = 'hidden';
	}		
	
	if(obj.value == 2){
		document.getElementById('imprevista').style.visibility = 'hidden';
		document.getElementById('antecipar').style.visibility = 'visible';
	}	
	
	if(obj.value == 3){
		document.getElementById('imprevista').style.visibility = 'visible';
		document.getElementById("antecipar").style.visibility = 'hidden';
	}	
	
	if(obj.value == 4){
		document.getElementById('imprevista').style.visibility = 'visible';
		document.getElementById('antecipar').style.visibility = 'visible';
	}	
	
	acao = document.form;
	
	if(acao.tipo_acao.value == 2 || acao.tipo_acao.value == 4)
		url = "img/tpl_cardmaker/cap_"+acao.custo.value+"a.png";
	else
		url = "img/tpl_cardmaker/cap_"+acao.custo.value+".png";
	document.getElementById("img_custo").src=url;		
}

function mudarPoderAcao(obj){
	url = "img/tpl_cardmaker/ap_"+obj.value+".png";
	document.getElementById("img_poder").src=url;				
}
function mudarCusto(obj){
	
	acao = document.form;
	
	if(acao.tipo_acao.value == 2 || acao.tipo_acao.value == 4)
		url = "img/tpl_cardmaker/cap_"+obj.value+"a.png";
	else
		url = "img/tpl_cardmaker/cap_"+obj.value+".png";	
	
	document.getElementById("img_custo").src=url;				
}
