
var xmlHttp;
var Destino;

function GetXmlHttpObject(){
   var xmlHttp = null;

   try{
      //Firefox, Opera, Safari
	  xmlHttp = new XMLHttpRequest();

   }catch(e){
     // Internet Explore
	 try{
	    xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	 }catch(e){
	    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }  
   
   }

  return xmlHttp;
}

function stateChanged(){
    dest = Destino;
    if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
		document.getElementById(dest).innerHTML = xmlHttp.responseText;
	}
}

function alertas(id,tipo,div){	
	   busca = document.getElementById(id).value;
	   xmlHttp = GetXmlHttpObject();
	   if(xmlHttp == null){
	      alert("Seu Navegador não suporta Ajax");
		  return;
	   }   
	   Destino = div;   
	   xmlHttp.onreadystatechange = stateChanged;   
	   xmlHttp.open("get","consultas.php?tipo="+tipo+"&busca="+busca,true);   
	   xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");   
	   xmlHttp.send(null);



	}

function enviar_coment(url,post,dest){
   xmlHttp = GetXmlHttpObject();
   if(xmlHttp == null){
      alert("Seu Navegador não suporta Ajax");
	  return;
   }   
   
   Destino = dest;   
   xmlHttp.onreadystatechange = stateChanged;   
   xmlHttp.open("POST",url,true);   
   xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");   
   xmlHttp.send(post);
   

}

// Twitter	
function buscarCards(dest){

	 obj = document.buscar;
	 
	 hab = new Array();
	 j=0;

	 for (i=0;i<obj.elements.length;i++){
		if(obj.elements[i].type == "checkbox"){
		   if(obj.elements[i].checked == true){
			  hab[j] = obj.elements[i].value;
			  j++;
			  }
		}
	 }

     var post = "task=Carta&action=miniListAction&nome="+encodeURI(obj.nome.value)+"&texto_permanente="+encodeURI(obj.texto.value)+"&habilidades="+hab+"&acao=new&carta="+encodeURI(obj.carta.value)+"&alinhamento="+encodeURI(obj.alinhamento.value)+"&energia_inicial="+encodeURI(obj.energia_inicial.value)+"&energia_fim="+encodeURI(obj.energia_fim.value)+"&escudo_inicial="+encodeURI(obj.escudo_inicial.value)+"&escudo_fim="+encodeURI(obj.escudo_fim.value)+"&afiliacao="+encodeURI(obj.afiliacao.value)+"&tipo_acao="+encodeURI(obj.tipo_acao.value);
     
     enviar_coment("index.php",post,dest);	 
   }		
// Twitter  

function mudarMainCard(url,id){

	document.getElementById("main_link").href = '?task=Carta&action=detalhesPage&carta='+id;
	document.getElementById("mainCard").src=url;
}

function addCardInDeck(id){
	var post = "task=Carta&action=painelAction&card="+id+"&acao=add";
    enviar_coment("index.php",post,"deck");	 
}

function rmvCardInDeck(id){
	var post = "task=Carta&action=painelAction&card="+id+"&acao=remover";
    enviar_coment("index.php",post,"deck");
}

function updateDeck(){
	var post = "task=Carta&action=painelAction&acao=update";
    enviar_coment("index.php",post,"deck");
}

function rmvAll(){
	var post = "task=Carta&action=painelAction&acao=rmvall";
    enviar_coment("index.php",post,"deck");
}

function mudarPagina(f,pag){
     if(f == 1){
		pag--;
	 }else if(f == 2){
		pag++;
	 }
     var post = "task=Carta&action=miniListAction&pagina="+pag;
     enviar_coment("index.php",post,"listar_card");	 	
}
