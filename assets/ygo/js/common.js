/**
 * 共通
 */
(function($) {

	/**
	 * HTMLエスケープ
	 */
	$.escapeHTML = function (val) {
		return $('<div/>').text(val).html();
	};

})(jQuery);

$(function() {

	/** ■■画像のコピーガード■■ */
	/** ドラッグ抑止 */
	/** コンテキストメニュー表示抑止 */
	/** テキスト選択抑止 */
	/** コピー抑止 */
	$('img').draggable({
		'distance' : 999999
	}).bind('drag contextmenu selectstart copy', function() {
		return false;
	});

	/** 画面表示（JavaScript無効化対策） */
	$('#wrapper').show();

	/** bodyクリック時、カスタムselectを閉じる */
	$('body').click(function() {
		$('.dk_container').removeClass('dk_open');
	});

	/** テキスト上のEscキー防止 */
	$('input:text').keydown(function(e) {
		if (e.keyCode == 27) {
			return false;
		}
		return true;
	});

	$.support.cors = true;

});

$(document).ready(function(){

	/*- アイコンの挙動 -*/
	$(".icon_info").mouseenter(
		function(){
			text = $(this).attr("alt");
			/*if($(this).hasClass("r")){		type = "r";		}
			if($(this).hasClass("sr")){		type = "sr";		}
			if($(this).hasClass("ur")){		type = "ur";		}
			if($(this).hasClass("secret")){		type = "secret";	}
			if($(this).hasClass("ul")){		type = "ul";		}
			if($(this).hasClass("hologra")){	type = "hologra";	}
			if($(this).hasClass("gold")){		type = "gold";		}
			if($(this).hasClass("parallel")){	type = "parallel";	}
			if($(this).hasClass("cr")){		type = "cr";		}
			if($(this).hasClass("forbidden")){	type = "forbidden";		}
			if($(this).hasClass("limited")){	type = "limited";		}
			if($(this).hasClass("semilimited")){	type = "semilimited";		}
			if($(this).hasClass("unlimited")){	type = "unlimited";		}*/
			if($(this).hasClass("rid_2")){		type = "r";		}
			if($(this).hasClass("rid_3")){		type = "sr";		}
			if($(this).hasClass("rid_4")){		type = "ur";		}
			if($(this).hasClass("rid_5") || $(this).hasClass("rid_15")){		type = "secret";	}
			if($(this).hasClass("rid_6")){		type = "ul";		}
			if($(this).hasClass("rid_7")){	type = "hologra";	}
			if($(this).hasClass("rid_8") || $(this).hasClass("rid_14")){		type = "gold";		}
			if($(this).hasClass("rid_9") || $(this).hasClass("rid_10") || $(this).hasClass("rid_11") || $(this).hasClass("rid_12")){	type = "parallel";	}
			if($(this).hasClass("rid_16")){		type = "cr";		}
			if($(this).hasClass("fl_1")){	type = "forbidden";		}
			if($(this).hasClass("fl_2")){	type = "limited";		}
			if($(this).hasClass("fl_3")){	type = "semilimited";		}
			if($(this).hasClass("fl_4")){	type = "unlimited";		}

			icon_pos = $(this).offset();
			$("body").append('<div id="icon_balloon" class="type_'+ type +'"><b>'+ text +'</b><span></span>');
			balloonn_width = $("#icon_balloon b").width();

			icon_width = $(this).width();
			$("#icon_balloon").css({
				"top":icon_pos.top - 38,
				"left":icon_pos.left - balloonn_width + (icon_width/2)
				//"display":"none"
			});
			$("#icon_balloon").animate({"opacity":"0"},0);
			$("#icon_balloon").animate({"opacity":"100"},2000);
	});
	$(".icon_info").mouseleave(
		function(){
			$("#icon_balloon").remove();
			$("#dbg").text("doon");
		}
	);

	/*- 言語切り替えメニュー -*/
	$("#language span").click(function(){
		$(this).hide();
		$(this).next().show();
		language_menu = true;
	});
	$("#language").mouseleave(function(){
		$("#language ul").hide();
		$("#language span").show();
	});
});

/**
 * 言語切り替え
 */
function ChangeLanguage(lang) {
	var join = '?';
	// パラメータ取得
	var search = location.search;
	// 言語パラメータを削除
	search = search.replace(/.request_locale.+/, '');
	// パラメータ結合文字の決定
	if (search.substring(0) != '?' && search.substring(1).length > 0) {
		join = '&';
	}
	// 遷移URLを生成
	var href = location.pathname + search + join + 'request_locale=' + lang;
	// 遷移
	location.href = href;
}
