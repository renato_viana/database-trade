<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/CartaController.php';
require_once 'controller/PerguntaController.php';
require_once 'model/Carta.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Estande.class.php';
require_once 'model/Want.class.php';
require_once 'model/Have.class.php';
require_once 'model/Reputacao.class.php';
require_once 'model/Estado.class.php';



class EstandeController {

// PAGES

public function defaultPage(){

			$Tvar      = new TratamentoVar();
			$search    = $Tvar->PostReq();
			$dados     = $Tvar->GetReq();

			$estande = new Estande();

		    global $Result,$FORM;
//
			$lista = $Tvar->getSession('lista');

			if(!isset($dados['pagina'])){

				$res     = $estande->searchEstande($search);

				$usuario = new Usuario();

				$n = count($res);
				for ($i=0; $i < $n; $i++) { 
					$r = $usuario->select(array("id" => $res[$i]['usuario_id']));
					$res[$i]['usuario_login'] = $r[0]['login'];
					$aux = explode(" ",$res[$i]['atualizado']);
					$res[$i]['atualizado'] = $aux[0];
					$res[$i]['atualizado-hora'] = $aux[1];
				}

				$Result['estandes'] = $res;

				$Tvar->createSession('lista',$Result['estandes']);
				$total = count($Result['estandes']);
			}else{
				$total = count($lista);
				$Result['estandes'] = $lista;			
			}

			$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
			$paginacao    = new PaginacaoController($pagina,10,$total);		
			$Result['paginacao'] = $paginacao;
//

			$template       =  new TemplateController();
			$template->renderTemplate('Estande','default',$alert,true);			
}

public function estandePage(){

		$Tvar    = new TratamentoVar();
		$d     = $Tvar->GetReq();
		$dados     = $Tvar->GetReq();
		
		$estande = new Estande();

		global $Result,$FORM;
		$estande->update($dados['estande'],array("views[+]" => 1));
		$res = $estande->select(array("id" => $dados['estande']));
		$aux = explode(" ",$res[0]['atualizado']);
		$res[0]['atualizado'] = $aux[0];
		$res[0]['atualizado-hora'] = $aux[1];	
		unset($aux);	
		$Result['estande'] = $res[0];

		$usuario = new Usuario();
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));
		$estado  = new Estado();
		$t       = $estado->select(array("id" => $r[0]['estado']));
		$r[0]['estado'] = $t[0]['estado'];

		$login = $Tvar->getSession('login');
		$Result['login'] = $login;

		$carta  = new CartaController();
		$want   = new Want();
		
		
		if(isset($d['msg']))
			$alert  = $Tvar->createAlert($d['msg'],$d['tipo']);			

		$login = $Tvar->getSession('login');
		$Result['usuario'] = $r[0];

		$std = $dados['estande'];

		for ($k=6; $k >= 1 ; $k--) { 
			$wantlist   = $want->getWantList($std,$raridade = $k);
				$j=0;
				for ($i=0; $i < count($wantlist); $i++) { 
					$c = $wantlist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $wantlist[$i]['qtd'];	
					$j++;	
				}
			$WANTLIST[$k] = $aux;
			unset($aux);
		}

		$Result['wantlist'] = $WANTLIST;


		$have   = new Have();

		for ($k=6; $k >= 1 ; $k--) { 
			$havelist   = $have->getHaveList($std,$raridade = $k);
				$j=0;
				for ($i=0; $i < count($havelist); $i++) { 
					$c = $havelist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $havelist[$i]['qtd'];	
					$j++;	
				}
			$HAVELIST[$k] = $aux;
			unset($aux);
		}

		$Result['havelist'] = $HAVELIST;		



		$perguntas = new PerguntaController();
		$lista = $Tvar->getSession('lista');

		if(!isset($dados['pagina'])){
			$res    = $perguntas->buscarPerguntaByEstandeIdAction($d['estande']);
			$Result['perguntas'] = $res;

			$Tvar->createSession('lista',$Result['perguntas']);
			$total = count($Result['perguntas']);
		}else{
			$total = count($lista);
			$Result['perguntas'] = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,9,$total);		
		$Result['paginacao'] = $paginacao;			

		//Reputação
		$reputacao = new Reputacao();
		$n = 0;
		//Positivas
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['estande']['usuario_id'],"tipo_reputacao" => 1)));
		$n += count($res);
		//Negativas		
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['estande']['usuario_id'],"tipo_reputacao" => 2)));
		$n -= count($res);
		$Result['usuario']['reputacao'] = $n;		

		$template       =  new TemplateController();
		$template->renderTemplate('Estande','estande',$alert,true);
}

public function configPage(){

		$Tvar    = new TratamentoVar();
		$d     = $Tvar->GetReq();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');
		$estande = new Estande();

		global $Result,$FORM;

		$res = $estande->select(array("usuario_id" => $login['id']));
		
		if(!$res){
			$template       =  new TemplateController();
			$template->renderTemplate('Estande','ativar',$alert,true);			
			exit();
		}

		$aux = explode(" ",$res[0]['atualizado']);
		$res[0]['atualizado'] = $aux[0];
		$res[0]['atualizado-hora'] = $aux[1];		
		unset($aux);
		$Result['estande'] = $res[0];		
		$Result['estande'] = $res[0];

		$carta  = new CartaController();
		$want   = new Want();

		
		
		if(isset($d['msg']))
			$alert  = $Tvar->createAlert($d['msg'],$d['tipo']);			

		$login = $Tvar->getSession('login');
		$Result['login'] = $login;

		$std = $this->getEstandeId();

		for ($k=6; $k >= 1 ; $k--) { 
			$wantlist   = $want->getWantList($std,$raridade = $k);
				$j=0;
				for ($i=0; $i < count($wantlist); $i++) { 
					$c = $wantlist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $wantlist[$i]['qtd'];	
					$j++;	
				}
			$WANTLIST[$k] = $aux;
			unset($aux);
		}

		$Result['wantlist'] = $WANTLIST;


		$have   = new Have();

		for ($k=6; $k >= 1 ; $k--) { 
			$havelist   = $have->getHaveList($std,$raridade = $k);
				$j=0;
				for ($i=0; $i < count($havelist); $i++) { 
					$c = $havelist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $havelist[$i]['qtd'];	
					$j++;	
				}
			$HAVELIST[$k] = $aux;
			unset($aux);
		}

		$Result['havelist'] = $HAVELIST;		



		$perguntas = new PerguntaController();
		$lista = $Tvar->getSession('lista');

		if(!isset($dados['pagina'])){
			$res    = $perguntas->buscarPerguntaByEstandeIdAction($d['estande']);
			$Result['perguntas'] = $res;

			$Tvar->createSession('lista',$Result['perguntas']);
			$total = count($Result['perguntas']);
		}else{
			$total = count($lista);
			$Result['perguntas'] = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,9,$total);		
		$Result['paginacao'] = $paginacao;			

		$template       =  new TemplateController();
		$template->renderTemplate('Estande','config',$alert,true);
}

public function wantPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();	
		$d     = $Tvar->GetReq();

		$carta  = new CartaController();
		$want   = new Want();
		$estande = new Estande();
		
		if(isset($d['msg']))
			$alert  = $Tvar->createAlert($d['msg'],$d['tipo']);			

		$login = $Tvar->getSession('login');
		$std = $estande->select(array("usuario_id" => $login['id']));			

		global $Result,$FORM;

		$Result['cartas'] = $carta->buscarCartasAction(array("nome" => $this->trocarcaracter($dados['nome'])),"nome");	


		for ($k=6; $k >= 1 ; $k--) { 
			$wantlist   = $want->getWantList($std[0]['id'],$raridade = $k);
				$j=0;
				for ($i=0; $i < count($wantlist); $i++) { 
					$c = $wantlist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $wantlist[$i]['qtd'];	
					$j++;	
				}
			$WANTLIST[$k] = $aux;
			unset($aux);
		}

		$Result['wantlist'] = $WANTLIST;

		$template       =  new TemplateController();
		$template->renderTemplate('Estande','want',$alert,true);	
}

public function havePage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();	
		$d     = $Tvar->GetReq();

		$carta  = new CartaController();
		$have   = new Have();
		$estande = new Estande();


		if(isset($d['msg']))
			$alert  = $Tvar->createAlert($d['msg'],$d['tipo']);			

		$login = $Tvar->getSession('login');
		$std = $estande->select(array("usuario_id" => $login['id']));			

		global $Result,$FORM;

		$Result['cartas'] = $carta->buscarCartasAction(array("nome" => $this->trocarcaracter($dados['nome'])),"nome");	


		for ($k=6; $k >= 1 ; $k--) { 
			$havelist   = $have->getHaveList($std[0]['id'],$raridade = $k);
				$j=0;
				for ($i=0; $i < count($havelist); $i++) { 
					$c = $havelist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $havelist[$i]['qtd'];	
					$j++;	
				}
			$HAVELIST[$k] = $aux;
			unset($aux);
		}

		$Result['havelist'] = $HAVELIST;

		$template       =  new TemplateController();
		$template->renderTemplate('Estande','have',$alert,true);	
}

// ACTIONS

public function ativarAction(){

		$Tvar    = new TratamentoVar();
		$login = $Tvar->getSession('login');	
		$estande = new Estande();
		$res = $estande->insert(array("usuario_id" => $login['id'],"criado" => date("Y-m-d")));

		$template =  new TemplateController();
		$template->redirectUrl("?task=Estande&action=configPage&estande=".$res,"Estande ativada com sucesso!!!","success");					
}


public function addWantListAction(){

		$Tvar  = new TratamentoVar();
		$want  = new Want();
		$carta = new Carta();		
		$estande = new Estande();

		$dados = $Tvar->GetReq();		
		$login = $Tvar->getSession('login');

		$std = $estande->select(array("usuario_id" => $login['id']));		


		$res = $want->select(array("AND" => array( "carta_id" => $dados['carta'],"estande_id" => $std[0]['id'])));
		
		$template =  new TemplateController();
		if(!$res){
			$res = $want->insert(array("carta_id" => $dados['carta'],"qtd" => $dados['qtd'],"estande_id" => $std[0]['id']));
			$template->redirectUrl("?task=Estande&action=wantPage","Carta inserida na Want List com sucesso!!!","success");								
		}else{
			$res = $want->update($res[0]['id'],array("qtd" => $dados['qtd']));
			$template->redirectUrl("?task=Estande&action=wantPage","Quantidade da Carta foi atualizada com sucesso!!!","success");								
		}		
}

public function addHaveListAction(){

		$Tvar  = new TratamentoVar();
		$have  = new Have();
		$carta = new Carta();		
		$estande = new Estande();

		$dados = $Tvar->GetReq();		
		$login = $Tvar->getSession('login');

		$std = $estande->select(array("usuario_id" => $login['id']));		


		$res = $have->select(array("AND" => array( "carta_id" => $dados['carta'],"estande_id" => $std[0]['id'])));
		
		$template =  new TemplateController();
		if(!$res){
			$res = $have->insert(array("carta_id" => $dados['carta'],"qtd" => $dados['qtd'],"estande_id" => $std[0]['id']));
			$template->redirectUrl("?task=Estande&action=havePage","Carta inserida na Have List com sucesso!!!","success");								
		}else{
			$res = $want->update($res[0]['id'],array("qtd" => $dados['qtd']));
			$template->redirectUrl("?task=Estande&action=havePage","Quantidade da Carta foi atualizada com sucesso!!!","success");								
		}		
}


public function rmvCartaWantAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();
		$std   = $this->getEstandeId();

		$want  = new Want();
		$want->delete(array('AND' =>  array('carta_id' => $dados['carta'],'estande_id' => $std)));
		
		$template =  new TemplateController();
		$template->redirectUrl("?task=Estande&action=wantPage","Carta removida com sucesso!!!","success");								
}

public function rmvCartaHaveAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();
		$std   = $this->getEstandeId();

		$have  = new Have();
		$have->delete(array('AND' =>  array('carta_id' => $dados['carta'],'estande_id' => $std)));
		
		$template =  new TemplateController();
		$template->redirectUrl("?task=Estande&action=havePage","Carta removida com sucesso!!!","success");								
}

public function updateInfoEstandeAction(){

		$Tvar    = new TratamentoVar();	
		$dados   = $Tvar->PostReq();
		$estande = new Estande();

		$std = $this->getEstandeId();

		$estande->update($std,array("nome" => $dados['nome'],"informacoes" => $dados['informacoes'],"atualizado" => date('Y-m-d H:i:s')));
		
		$template =  new TemplateController();
		$template->redirectUrl("?task=Estande&action=configPage","Estande atualizada com sucesso!!!","success");										


}

	function trocarcaracter($char){


		$carac_esp  = array("á","Á","â","Â","à","À","ã","Ã","ç","Ç","é","É","ê","Ê","í","Í","ó","Ó","ô","Ô","õ","Õ","ú","Ú","ü","Ü","º","ª","-");
		$carac_html = array("&aacute;","&Aacute;","&acirc;","&Acirc;","&agrave;","&Agrave;","&atilde;","&Atilde;","&ccedil;","&Ccedil;","&eacute;","&Eacute;","&ecirc;","&Ecirc;","&iacute;","&Iacute;","&oacute;","&Oacute;","&ocirc;","&Ocirc;","&otilde;","&Otilde;","&uacute;","&Uacute;","&uuml;","&Uuml;","&ordm","&ordf","&#45;");
		$char = str_replace($carac_esp,$carac_html,$char);
		return $char;
	}

public function getEstandeId(){

		$Tvar    = new TratamentoVar();	
		$estande = new Estande();

		$login = $Tvar->getSession('login');

		$std = $estande->select(array("usuario_id" => $login['id']));		

		return $std[0]['id'];	
}

}