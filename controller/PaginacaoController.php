<?php 


class PaginacaoController {

	var $pagina;
	var $paginas;
	var $total_por_paginas;
	var $primeiro_registro;
	var $total_de_registros;

// PAGES


// ACTIONS

	public function __construct($pagina,$total_por_pagina,$total_de_registros){

		$this->pagina           =   $pagina;
		$this->total_por_paginas 	=   $total_por_pagina;
		$this->total_de_registros  =   $total_de_registros;
		$this->paginas = ceil($total_de_registros/$total_por_pagina);
		$this->primeiro_registro = ($pagina*$total_por_pagina) - $total_por_pagina;
	}

}
