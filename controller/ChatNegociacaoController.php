<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/MensagemController.php';
require_once 'model/ChatNegociacao.class.php';
require_once 'model/NegociacaoLeilao.class.php';
require_once 'model/ChatNegociacaoLeilao.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Notificacao.class.php';

class ChatNegociacaoController {

	var $deSys = 24;

// PAGES


// ACTIONS

public function addMensagemAction(){
	
	
	$chatNegociacao    = new ChatNegociacao();
	$Tvar        = new TratamentoVar();
	$login = $Tvar->getSession('login');	

	$dados = $Tvar->PostReq();

	$file = $_FILES['img'];
	$ponto = explode(".", $file['name']);
	$ext = $ponto[count($ponto)-1];		

	unset($dados['salvar']);

	$login = $Tvar->getSession('login');
	
	$d['data']          =  date('Y-m-d');
	$d['hora']          =  date('H:i:s');
	$d['usuario_id']    =  $login['id'];
	$d['negociacao_id']    =  $dados['negociacao'];
	$d['mensagem']      =  $dados['mensagem'];
	$d['img']      =  $ext;
	
	$res = $chatNegociacao->insert($d);
	unset($d);

	if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
		
		include("libs/wide/lib/WideImage.php");
		$img = WideImage::load($file['tmp_name']);
		$img->saveToFile("upload/negociacoes/".$dados['negociacao']."/".$res.".".$ext);
	}		

	$template =  new TemplateController();

	//Enviar Mensagem de notificação
	$negociacao = new Negociacao();
	$x          = $negociacao->select(array("id" => $dados['negociacao']));
	$estande = new Estande();
	$r     = $estande->select(array("id" => $x[0]['estande_id']));

	if($login['id'] == $r[0]['usuario_id'])
		$d['para']               =   $x[0]['usuario_id'];
	else
		$d['para']               =   $r[0]['usuario_id'];

	$d['de']                 =   $this->deSys;
	$d['assunto']            =   "Nova resposta na negociação ".$dados['negociacao'];
	$d['mensagem']           =   "
	Ola,<br>Uma nova reposta foi enviada na negociação ".$dados['negociacao']."
	<a href='?task=Negociacao&action=verNegociacaoPage&negociacao=".$dados['negociacao']."'>clique aqui</a> para visualizar.
	";		

	$mensagem = new MensagemController();
	$mensagem->enviarMensagemSistemaAction($d);
	
	if($res){
		$template->redirectUrl("?task=Negociacao&action=verNegociacaoPage&negociacao=".$dados['negociacao'],"Mensagem enviada com sucesso!!!","success");
	}else{
		$template->redirectUrl("?task=Negociacao&action=verNegociacaoPage&negociacao=".$dados['negociacao'],"Erro ao enviado a mensagem!!!","danger");
	}
}


public function addMensagemLeilaoAction(){
	
	
	$chatNegociacaoLeilao    = new ChatNegociacaoLeilao();
	$negociacaoLeilao    = new NegociacaoLeilao();
	$Tvar        = new TratamentoVar();
	$template =  new TemplateController();

	$dados = $Tvar->PostReq();

	$file = $_FILES['img'];
	$ponto = explode(".", $file['name']);
	$ext = $ponto[count($ponto)-1];		

	unset($dados['salvar']);

	$login = $Tvar->getSession('login');

	$res   = $negociacaoLeilao->select(array("AND" => array("id" => $dados['negociacao_leilao'],"leilao_id" => $dados['leilao'])));

	if(count($res) == 0)
		$template->redirectUrl("?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$dados['leilao'],"Erro ao enviado a mensagem!!!","danger");
	
	$d['data']          =  date('Y-m-d');
	$d['hora']          =  date('H:i:s');
	$d['usuario_id']    =  $login['id'];
	$d['negociacao_leilao_id']    =  $dados['negociacao_leilao'];
	$d['mensagem']      =  $dados['mensagem'];
	$d['img']      =  $ext;
	
	$res = $chatNegociacaoLeilao->insert($d);
	unset($d);
    echo "upload/negociacoes/leilao/".$dados['negociacao_leilao']."/".$res.".".$ext;
    
	if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
		
		include("libs/wide/lib/WideImage.php");
		$img = WideImage::load($file['tmp_name']);
		$img->saveToFile("upload/negociacoes/leilao/".$dados['negociacao_leilao']."/".$res.".".$ext);
	}		

	if($res){
		$template->redirectUrl("?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$dados['leilao'],"Mensagem enviada com sucesso!!!","success");
	}else{
		$template->redirectUrl("?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$dados['leilao'],"Erro ao enviado a mensagem!!!","danger");
	}
}

	public function removePerguntaAction(){
		
		$pergunta = new Pergunta();
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$d['pergunta'] = $dados['pergunta'];
		
		$res = $pergunta->delete(array("id" => $d));
		
		$template =  new TemplateController();		
		$template->redirectUrl("?task=Estande&action=estandePage&estande=".$dados['estande'],"Pergunta deletada com sucesso!!!","success");

	}	

	public function buscarPerguntaByEstandeIdAction($id){

		$pergunta  = new Pergunta();
		$usuario = new Usuario();
		$Tvar  = new TratamentoVar();
        
		$res     = $pergunta->select(array("estande_id" => $id,"ORDER" => "id DESC"));
		

		$n       = count($res);
		for ($i=0; $i < $n; $i++) { 
			$aux = $usuario->select(array("id" => $res[$i]));
			$res[$i]['data']          = $Tvar->dateBr($res[$i]['data']);
			$res[$i]['usuario_nome']  = $aux[0]['nome'];
			$res[$i]['usuario_login'] = $aux[0]['login'];
		}
		return $res;
	}

}
