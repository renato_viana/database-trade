<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/EstandeController.php';
require_once 'model/Usuario.class.php';
require_once 'model/Mensagem.class.php';


class MensagemController {


// PAGES

public function defaultPage(){

		$Tvar      = new TratamentoVar();
	    $mensagem  = new Mensagem();

		$login     = $Tvar->getSession('login');

		global $Result;

		$res                = $mensagem->select(array("AND" => array("de" => $login['id'],"tipo" => 2)));
		$Result['enviadas'] = $res;

		$res                = $mensagem->select(array("AND" => array("para" => $login['id'],"tipo" => 1)));
		
		$n = count($res);
		$usuario = new Usuario();
		for ($i=0; $i < $n; $i++) { 
			$r = $usuario->select(array("id" => $res[$i]['de']));
			$res[$i]['de'] = $r[0];
		}		

		$Result['entrada']  = $res;

		$template       =  new TemplateController();
		$template->renderTemplate('Mensagem','default',$alert,true);

}

public function entradaPage(){

		$Tvar      = new TratamentoVar();
	    $mensagem  = new Mensagem();

		$login     = $Tvar->getSession('login');
		$dados     = $Tvar->GetReq();

		global $Result;

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);				

		$lista = $Tvar->getSession('lista');

		if(!isset($dados['pagina'])){
			$res                = $mensagem->select(array("AND" => array("para" => $login['id'],"tipo" => 1),"ORDER" => "id DESC"));
			
			$n = count($res);
			$usuario = new Usuario();
			for ($i=0; $i < $n; $i++) { 
				$r = $usuario->select(array("id" => $res[$i]['de']));
				$res[$i]['de'] = $r[0];
			}	

			$Result['entrada']  = $res;

			$Tvar->createSession('lista',$Result['entrada']);
			$total = count($Result['entrada']);

		}else{
			$total = count($lista);
			$Result['entrada'] = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,25,$total);		
		$Result['paginacao'] = $paginacao;			

		$template       =  new TemplateController();
		$template->renderTemplate('Mensagem','entrada',$alert,true);

}

public function enviadasPage(){

		$Tvar      = new TratamentoVar();
	    $mensagem  = new Mensagem();

		$login     = $Tvar->getSession('login');
		$dados     = $Tvar->GetReq();

		global $Result;

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);				

		$lista = $Tvar->getSession('lista');

		if(!isset($dados['pagina'])){
			$res                = $mensagem->select(array("AND" => array("de" => $login['id'],"tipo" => 2),"ORDER" => "id DESC"));
			
			$n = count($res);
			$usuario = new Usuario();
			for ($i=0; $i < $n; $i++) { 
				$r = $usuario->select(array("id" => $res[$i]['para']));
				$res[$i]['para'] = $r[0];

				$r = $usuario->select(array("id" => $res[$i]['de']));
				$res[$i]['de'] = $r[0];				
			}	

			$Result['entrada']  = $res;

			$Tvar->createSession('lista',$Result['entrada']);
			$total = count($Result['entrada']);

		}else{
			$total = count($lista);
			$Result['entrada'] = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,25,$total);		
		$Result['paginacao'] = $paginacao;			

		$template       =  new TemplateController();
		$template->renderTemplate('Mensagem','enviadas',$alert,true);

}

public function escreverMensagemPage(){

		$Tvar      = new TratamentoVar();
		$login = $Tvar->getSession('login');

		$dados     = $Tvar->GetReq();

		global $Result;

		$usuario = new Usuario();
		$res     = $usuario->select(array("id" => $login['id']));

		$Result['de'] = $res[0];		

		$usuario = new Usuario();
		$res     = $usuario->select(array("id" => $dados['usuario']));

		$Result['para'] = $res[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Mensagem','escreverMensagem',$alert,true);

}

public function verMensagemPage(){

		$Tvar      = new TratamentoVar();
		$usuario = new Usuario();

		$login = $Tvar->getSession('login');

		$dados     = $Tvar->GetReq();

		global $Result;		

		$mensagem = New Mensagem();
		$res      = $mensagem->select(array("id" => $dados['mensagem']));

		//Preparar para ver uma mensangem da caixa de entrada
		if($dados['tipo'] == "entrada" && $res[0]['tipo'] == 1){
			$res      = $mensagem->select(array("AND" => array("id" => $dados['mensagem'],"para" => $login['id'])));
			if(count($res) == 0)
				exit;
			$res[0]['de'] = $usuario->select(array("id" => $res[0]['de']));
			$res[0]['para'] = $usuario->select(array("id" => $res[0]['para']));

			if($res[0]['aberto'] == 0)
				$mensagem->update($dados['mensagem'],array("aberto" => 1));

			$Result['responder'] = $res[0]['de'][0]['id'];
		}

		//Preparar para ver uma mensangem da caixa de enviadas

		if($dados['tipo'] == "enviada" && $res[0]['tipo'] == 2){

			$res      = $mensagem->select(array("AND" => array("id" => $dados['mensagem'],"de" => $login['id'])));
			if(count($res) == 0)
				exit;

			$res[0]['de'] = $usuario->select(array("id" => $res[0]['de']));
			$res[0]['para'] = $usuario->select(array("id" => $res[0]['para']));

			$Result['responder'] = $res[0]['para'][0]['id'];
		}

		
 		$Result['mensagem'] = $res[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Mensagem','verMensagem',$alert,true);

}

// ACTIONS

public function enviarMensagemAction(){

		$Tvar      = new TratamentoVar();
	    $mensagem  = new Mensagem();

		$dados     = $Tvar->PostReq();
		$login     = $Tvar->getSession('login');

		// Enviar para caixa de Entrada do "para (destinatario)"
		$mensagem->insert(
			array(
				"de" => $login['id'],
				"para" => $dados['para'],
				"assunto" => $dados['assunto'],
				"mensagem" => $dados['mensagem'],
				"tipo" => 1,
				"data" => date("Y-m-d"),
				"hora" => date("H:i:s"),
				"aberto" => 0
				)
		);

		// Enviar para caixa de Enviadas do "de (remetente)"
		$mensagem->insert(
			array(
				"de" => $login['id'],
				"para" => $dados['para'],
				"assunto" => $dados['assunto'],
				"mensagem" => $dados['mensagem'],
				"tipo" => 2,
				"data" => date("Y-m-d"),
				"hora" => date("H:i:s"),
				"aberto" => 1
				)
		);		


		$template =  new TemplateController();		
		$template->redirectUrl("?task=Mensagem&action=enviadasPage","Mensagem enviada com sucesso!!!","success");

}

public function enviarMensagemSistemaAction($dados,$grupo=NULL){

		$Tvar      = new TratamentoVar();
	    $mensagem  = new Mensagem();
	    $usuario   = new Usuario();

	    $n = count($grupo);
	    if($n > 0){
		    for ($i=0; $i < $n; $i++) { 
				$res = $usuario->select(array("tipo_usuario" => $grupo[$i]));	    	
				if(count($res) > 0){
					$mensagem->insert(
						array(
							"de" => $dados['id'],
							"para" => $res[0]['id'],
							"assunto" => $dados['assunto'],
							"mensagem" => $dados['mensagem'],
							"tipo" => 1,
							"data" => date("Y-m-d"),
							"hora" => date("H:i:s"),
							"aberto" => 0
							)
					);	
				}	
			}
	    }else{
			$mensagem->insert(
				array(
					"de" => $dados['de'],
					"para" => $dados['para'],
					"assunto" => $dados['assunto'],
					"mensagem" => $dados['mensagem'],
					"tipo" => 1,
					"data" => date("Y-m-d"),
					"hora" => date("H:i:s"),
					"aberto" => 0
					)
			);		    	
	    }
		// Enviar para caixa de Entrada do "para (destinatario)"
}

public function deletarMensagemAction(){

		$Tvar      = new TratamentoVar();
	    $mensagem  = new Mensagem();
	    $template =  new TemplateController();		

	    $login     = $Tvar->getSession('login');

		$dados     = $Tvar->PostReq();

		$n = count($dados['msgs']);
		for ($i=0; $i < $n; $i++) {

			if($dados['tipo'] == "entrada"){
				$res = $mensagem->select(array("AND" => array("id" =>$dados['msgs'][$i],"para" => $login['id'])));
				if(count($res) > 0)
					$mensagem->delete(array("id" => $dados['msgs'][$i]));
				else
					$template->redirectUrl($_SERVER['HTTP_REFERER'],"Erro ao apagar a(s) Mensagem(ns)!!!","danger");		
			}

			if($dados['tipo'] == "enviada"){
				$res = $mensagem->select(array("AND" => array("id" =>$dados['msgs'][$i],"de" => $login['id'])));
				if(count($res) > 0)
					$mensagem->delete(array("id" => $dados['msgs'][$i]));
				else
					$template->redirectUrl($_SERVER['HTTP_REFERER'],"Erro ao apagar a(s) Mensagem(ns)!!!","danger");		
			}

		}

		if($dados['ver'] == "truee"){
			
			if($dados['tipo'] == "entrada"){
				$template->redirectUrl("?task=Mensagem&action=entradaPage","Mensagem(ns) deleta(s) com sucesso!!!","success");		
				exit();
			}

			if($dados['tipo'] == "enviada"){
				$template->redirectUrl("?task=Mensagem&action=enviadasPage","Mensagem(ns) deleta(s) com sucesso!!!","success");		
				exit();
			}

		}

		$template->redirectUrl($_SERVER['HTTP_REFERER'],"Mensagem(ns) deleta(s) com sucesso!!!","success");		
}

}
