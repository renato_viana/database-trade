<?php 

require_once 'controller/TemplateController.php';
require_once 'model/Coment.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Rating.class.php';

class ComentController {


// PAGES


// ACTIONS

	public function addComentAction(){
		
		$coment = new Coment();
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();

		unset($dados['salvar']);

		
		$d['data']       = date('Y-m-d');
		$d['hora']       = date('H:i:s');
		$d['usuario_id'] = $dados['usuario'];
		$d['deck_id']    = $dados['deck'];
		$d['texto']      = $dados['texto'];
		$d['tipo']       = 1;
		
		$res = $coment->insert($d);
		
		$template =  new TemplateController();

		if($res){
			$template->redirectUrl("?task=Deck&action=deckPage&deck=".$dados['deck'],"Comentario enviado com sucesso!!!","success");
		}else{
			$template->redirectUrl("?task=Deck&action=deckPage&deck=".$dados['deck'],"Erro ao enviado o comentario!!!","danger");
		}
	}

	public function removeComentAction(){
		
		$coment = new Coment();
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$d['coment'] = $dados['coment'];
		
		$res = $coment->delete(array("id" => $d));
		
		$template =  new TemplateController();		
		$template->redirectUrl("?task=Deck&action=deckPage&deck=".$dados['deck'],"Comentario deletado com sucesso!!!","success");

	}	

	public function buscarComentsByDeckIdAction($id){

		$coment  = new Coment();
		$usuario = new Usuario();
		$Tvar  = new TratamentoVar();

		$res     = $coment->select(array("deck_id" => $id));

		$n       = count($res);
		for ($i=0; $i < $n; $i++) { 
			$aux = $usuario->select(array("id" => $res[$i]));
			$res[$i]['data']          = $Tvar->dateBr($res[$i]['data']);
			$res[$i]['usuario_nome']  = $aux[0]['nome'];
			$res[$i]['usuario_login'] = $aux[0]['login'];
		}

		return $res;
	}

	public function addRatingAction(){
		
		$rating = new Rating();
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();

		$d['usuario_id'] = $dados['usuario'];
		$d['deck_id'] = $dados['deck'];
		$d['star'] = $dados['estrela'];
		
		$res = $rating->select(array( "AND" => array("usuario_id" => $d['usuario_id'],"deck_id" => $d['deck_id'])));
		
		if(count($res) > 0){
			$rating->update($res[0]['id'],array("star" => $d['star']));
		}else{
			$rating->insert($d);
		}

		$template =  new TemplateController();		
		$template->redirectUrl("?task=Deck&action=deckPage&deck=".$dados['deck'],"Deck avaliado com sucesso!!!","success");		

	}

	public function getRatingbyDeckAction($deck,$usuario){
		
		$rating = new Rating();
		
		$res = $rating->select(array( "AND" => array("usuario_id" => $usuario,"deck_id" => $deck)));

		return $res;
	}	

	public function getSomaAction($deck){
		
		$rating = new Rating();
		
		$res = $rating->getSoma($deck);
		
		return $res;
	}	

}
