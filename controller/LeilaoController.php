<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/MensagemController.php';
require_once 'model/NegociacaoLeilao.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Leilao.class.php';
require_once 'model/Lance.class.php';
require_once 'model/Estado.class.php';
require_once 'model/Anuncio.class.php';
require_once 'model/TagAnuncio.class.php';


class LeilaoController {

	var $deSys = 24;

// PAGES

public function defaultPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$search    = $Tvar->PostReq();


		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		global $Result;
//
		$lista = $Tvar->getSession('lista');

		if(!isset($dados['pagina'])){

			$leilao  = new Leilao();

			if($search['nome'] == "")
				$res     = $leilao->select(array("status[>]" => 1, "ORDER" => "data_fim DESC"));
			else{
				
				$array = array(
					"status[>]" => 1,
					"LIKE" => array("OR" => array(
							"informacoes" => $search['nome'],
							"nome" => $search['nome'],
					)),			
					"ORDER" => "data_fim DESC"
				);				
				$res     = $leilao->select($array);
			}

			$lance   = new Lance();

			$usuario = new Usuario();
			$n = count($res);
			for ($i=0; $i < $n; $i++) { 
				$r = $usuario->select(array("id" => $res[0]['usuario_id']));
				$aux = explode(" ", $res[$i]['data_fim']);
				$res[$i]['data_fim'] = $aux[0];
				$res[$i]['data_fim-hora'] = $aux[1];

				$aux = explode(" ", $res[$i]['criado']);
				$res[$i]['criado'] = $aux[0];
				$res[$i]['criado-hora'] = $aux[1];

				$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);

				$res[$i]['vendedor'] = $r[0]['login'];

				$res[$i]['maior_lance'] = $lance->max($res[$i]['id']);
			}

			$Result['leiloes'] = $res;

			$Tvar->createSession('lista',$Result['leiloes']);
			$total = count($Result['leiloes']);
		}else{
			$total = count($lista);
			$Result['leiloes'] = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,10,$total);		
		$Result['paginacao'] = $paginacao;	
//

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','default',$alert,true);

}


public function defaultAnuncioPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$search    = $Tvar->PostReq();


		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		global $Result;
//
		$lista = $Tvar->getSession('lista');

		if(!isset($dados['pagina'])){

			$anuncio  = new Anuncio();

			if($search['nome'] == "")
				$res     = $anuncio->select(array("status[>]" => 1, "ORDER" => "criado DESC"));
			else{
				
				$array = array(
					"status[>]" => 1,
					"LIKE" => array("OR" => array(
							"informacoes" => $search['nome'],
							"nome" => $search['nome'],
					)),			
					"ORDER" => "data_fim DESC"
				);				
				$res     = $leilao->select($array);
			}

			$usuario = new Usuario();
			$n = count($res);
			for ($i=0; $i < $n; $i++) { 
				$r = $usuario->select(array("id" => $res[0]['usuario_id']));
				$aux = explode(" ", $res[$i]['expiraem']);
				$res[$i]['expiraem'] = implode ( '/',array_reverse(explode('-',$aux[0])));
				$res[$i]['expiraem-hora'] = $aux[1];

				$aux = explode(" ", $res[$i]['criado']);
				$res[$i]['criado'] = $aux[0];
				$res[$i]['criado-hora'] = $aux[1];
				$res[$i]['dono'] = $r[0];

				$txt = explode(" ", $res[$i]['informacoes']);
				$m   = count($txt);
				$t   = "";
				for ($j=0; $j < 35; $j++) { 
					$t .= " ".$txt[$j];
				}
				$t .= "...";
				$res[$i]['informacoes'] = $t;
			}

			$Result['anuncio'] = $res;

			$Tvar->createSession('lista',$Result['anuncio']);
			$total = count($Result['anuncio']);
		}else{
			$total = count($lista);
			$Result['anuncio'] = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,10,$total);		
		$Result['paginacao'] = $paginacao;	
//
		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','defaultAnuncio',$alert,true);

}

public function meusLeiloesPage(){

		$Tvar      = new TratamentoVar();
		$negociacaoLeilao      = new NegociacaoLeilao();

		$login = $Tvar->getSession('login');
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$leilao  = new Leilao();
		$res     = $leilao->select( array("usuario_id" => $login['id'],"ORDER" => "criado DESC"));

		$n = count($res);
		for ($i=0; $i < $n; $i++) { 

			$aux = explode(" ", $res[$i]['data_fim']);
			$res[$i]['data_fim'] = $aux[0];
			$res[$i]['data_fim-hora'] = $aux[1];

			$aux = explode(" ", $res[$i]['criado']);
			$res[$i]['criado'] = $aux[0];
			$res[$i]['criado-hora'] = $aux[1];

			if($res[$i]['status'] == 3){
					$p = $negociacaoLeilao->select(array( "leilao_id" => $res[$i]['id']));

					if(count($p) == 0)
						$res[$i]['negociacao'] = "<a href='?task=Negociacao&action=abrirNegociacaoLeilaoDonoPage&leilao=".$res[$i]['id']."' class='btn-sm btn-primary' style='color:white;'><span class='glyphicon glyphicon-plus'></span> Abrir Negociação</a>";
					else
						$res[$i]['negociacao'] = "<a href='?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$res[$i]['id']."'  class='btn-sm btn-primary' style='color:white;'><span class='glyphicon glyphicon-eye-open'></span> Ver Negociação</a>";										
			}

			$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);
		}

		global $Result;

		$Result['leiloes'] = $res;
		$Result['login']   = $login;

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','meusLeiloes',$alert,true);

}


public function meusAnunciosPage(){

		$Tvar      = new TratamentoVar();
		$negociacaoLeilao      = new NegociacaoLeilao();

		$login = $Tvar->getSession('login');
		$dados     = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$anuncio  = new Anuncio();
		$res     = $anuncio->select( array("usuario_id" => $login['id'],"ORDER" => "criado DESC"));

		$n = count($res);
		for ($i=0; $i < $n; $i++) { 

			$aux = explode(" ", $res[$i]['expiraem']);
			
			$res[$i]['expiraem'] = $aux[0];
			$res[$i]['expiraem-hora'] = $aux[1];

			$aux = explode(" ", $res[$i]['criado']);
			$res[$i]['criado'] = $aux[0];
			$res[$i]['criado-hora'] = $aux[1];

			if($res[$i]['status'] == 3){
					$p = $negociacaoLeilao->select(array( "leilao_id" => $res[$i]['id']));

					if(count($p) == 0)
						$res[$i]['negociacao'] = "<a href='?task=Negociacao&action=abrirNegociacaoLeilaoDonoPage&leilao=".$res[$i]['id']."' class='btn-sm btn-primary' style='color:white;'><span class='glyphicon glyphicon-plus'></span> Abrir Negociação</a>";
					else
						$res[$i]['negociacao'] = "<a href='?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$res[$i]['id']."'  class='btn-sm btn-primary' style='color:white;'><span class='glyphicon glyphicon-eye-open'></span> Ver Negociação</a>";										
			}

			$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);
		}

		global $Result;

		$Result['leiloes'] = $res;
		$Result['login']   = $login;

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','meusAnuncios',$alert,true);

}

public function leiloesGanhosPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$leilao  = new Leilao();
		$res     = $leilao->select( array("ganhador" => $login['id'],"ORDER" => "criado DESC"));

		$n = count($res);
		$usuario = new Usuario();
		$lance   = new Lance();
		$negociacaoLeilao = new NegociacaoLeilao();

		for ($i=0; $i < $n; $i++) { 

			$r = $usuario->select(array("id" => $res[$i]['usuario_id']));

			$res[$i]['vendedor'] = $r[0];
			$res[$i]['lance']    = $lance->max($res[$i]['id']);
			$r                   = $lance->select(array("valor" => $res[$i]['lance'][0]['lance']));	
			$res[$i]['data_ultimo_lance'] = $r[0]['data'];

			$aux = explode(" ", $res[$i]['data_ultimo_lance']);
			$res[$i]['data_ultimo_lance'] = $aux[0];
			$res[$i]['data_ultimo_lance-hora'] = $aux[1];

			$aux = explode(" ", $res[$i]['criado']);
			$res[$i]['criado'] = $aux[0];
			$res[$i]['criado-hora'] = $aux[1];

			$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);

			
			$p = $negociacaoLeilao->select(array("AND" => array( "leilao_id" => $res[$i]['id'],"usuario_id" => $login['id'])));
			if(count($p) == 0)
				$res[$i]['negociacao'] = "<a href='?task=Negociacao&action=abrirNegociacaoLeilaoPage&leilao=".$res[$i]['id']."' class='btn-sm btn-primary' style='color:white;'>Abrir Negociação</a>";
			else
				$res[$i]['negociacao'] = "<a href='?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$res[$i]['id']."'  class='btn-sm btn-primary' style='color:white;'>Ver Negociação</a>";					
		}

		global $Result;

		$Result['leiloes'] = $res;
		$Result['login']   = $login;

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','leiloesGanhos',$alert,true);

}

public function meuLeilaoAdminPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$leilao  = new Leilao();
		$lance   = new Lance();
		if(!$Tvar->verificarAdmin())
			$res     = $leilao->select(array("AND" => array("id" => $dados['leilao'],"usuario_id" => $login['id'])));
		else
			$res     = $leilao->select(array("id" => $dados['leilao']));

		$template       =  new TemplateController();
		$n = count($res);
		if($n == 0)
			$template->renderTemplate('Leilao','404',$alert,true);		

		$n = count($res);
		for ($i=0; $i < $n; $i++) { 

			$aux = explode(" ", $res[$i]['criado']);
			$res[$i]['criado'] = $aux[0];
			$res[$i]['criado-hora'] = $aux[1];

			$aux = explode(" ", $res[$i]['data_fim']);
			$res[$i]['data_fim'] = $aux[0];
			$res[$i]['data_fim-hora'] = $aux[1];			

			$res[$i]['status-id'] = $res[$i]['status'];
			$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);

			$lnc     = $lance->max($res[$i]['id']);
			$res[$i]['lance_inicial-home'] = $res[$i]['lance_inicial'];
			if($lnc[0]['lance'] != ""){

				$res[$i]['lance_inicial'] = $lnc[0]['lance']+$res[$i]['incremento'];
			}
		}

		global $Result;

		$Result['leilao'] = $res[0];
		$Result['login']   = $login;

		//Pegar Lances

		$res     = $lance->select(array("leilao_id" => $dados['leilao'],"ORDER" => "data DESC"));

		$usuario = new Usuario();
		$n = count($res);
		for ($i=0; $i < $n; $i++) { 
			$r  = $usuario->select(array("id" => $res[$i]['usuario_id']));
			$res[$i]['usuario']  = $r[0];

			$aux = explode(" ", $res[$i]['data']);
			$res[$i]['data'] = $aux[0];
			$res[$i]['data-hora'] = $aux[1];			
		}

		$Result['lances']   = $res;

		//Buscar Ultimo Lance

		$lnc     = $lance->max($dados['leilao']);
		$res     = $lance->select(array("valor" => $lnc[0]['lance']));		
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));

		$res[0]['usuario'] = $r[0];
		$Result['ultimo-lance'] = $res[0];

		//Verificar se já se encerrou o leilão e quem é o ganhador
		$Result['vencedor'] = false;
		$res = $leilao->time($dados['leilao'],date("Y-m-d H:i:s"));
		
		if(count($res) == 0){
			$Result['vencedor'] = 10;
			if($Result['ultimo-lance']['usuario']['id'] != 0){				
				$leilao->update($dados['leilao'],array("ganhador" => $Result['ultimo-lance']['usuario']['id'],"status" => 3));
			}else{
				if($Result['leilao']['status-id'] != 4){
					$leilao->update($dados['leilao'],array("status" => 5));
					$Result['vencedor'] = 9;
				}
			}

		}

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','meuLeilaoAdmin',$alert,true);

}


public function meuAnuncioAdminPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$anuncio  = new Anuncio();
		$res     = $anuncio->select(array("AND" => array("id" => $dados['anuncio'],"usuario_id" => $login['id'])));


		if(!$Tvar->verificarAdmin())
			$res     = $anuncio->select(array("AND" => array("id" => $dados['anuncio'],"usuario_id" => $login['id'])));
		else
			$res     = $anuncio->select(array("id" => $dados['anuncio']));

		$template       =  new TemplateController();
		$n = count($res);
		if($n == 0)
			$template->renderTemplate('Leilao','404',$alert,true);		

		$n = count($res);
		for ($i=0; $i < $n; $i++) { 

			$aux = explode(" ", $res[$i]['criado']);
			$res[$i]['criado'] = $aux[0];
			$res[$i]['criado-hora'] = $aux[1];

			$aux = explode(" ", $res[$i]['expiraem']);
			$res[$i]['expiraem'] = $aux[0];
			$res[$i]['expiraem-hora'] = $aux[1];			

			$res[$i]['status-id'] = $res[$i]['status'];
			$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);

		}

		global $Result;

		$Result['anuncio'] = $res[0];
		$Result['login']   = $login;
		$estado = new Estado();
		$est    = $estado->select(array("id" => $Result['login']['estado']));
		$Result['login']['estado'] = $est[0]['estado'];		

		//Reputação
		$reputacao = new Reputacao();
		$n = 0;
		//Positivas
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['anuncio']['usuario_id'],"tipo_reputacao" => 1)));
		$n += count($res);
		//Negativas		
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['anuncio']['usuario_id'],"tipo_reputacao" => 2)));
		$n -= count($res);
		$Result['reputacao'] = $n;			


		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','meuAnuncioAdmin',$alert,true);

}

public function leilaoPage(){

		$Tvar      = new TratamentoVar();
		$template       =  new TemplateController();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$leilao  = new Leilao();
		$lance   = new Lance();

		$array = array(
			"AND" => array(
				"OR" => array(
					"status" => array(2,3,5)
				),
				"id" => $dados['leilao']
			)
		);		

		$res     = $leilao->select($array);

		$n = count($res);
		if($n == 0)
			$template->renderTemplate('Leilao','404',$alert,true);


		for ($i=0; $i < $n; $i++) { 

			$aux = explode(" ", $res[$i]['criado']);
			$res[$i]['criado'] = $aux[0];
			$res[$i]['criado-hora'] = $aux[1];

			$aux = explode(" ", $res[$i]['data_fim']);
			$res[$i]['data_fim'] = $aux[0];
			$res[$i]['data_fim-hora'] = $aux[1];			

			$res[$i]['status'] = $this->getSituacaoAction($res[$i]['status']);

			$lnc     = $lance->max($res[$i]['id']);
			$res[$i]['lance_inicial-home'] = $res[$i]['lance_inicial'];
			if($lnc[0]['lance'] != ""){

				$res[$i]['lance_inicial'] = $lnc[0]['lance']+$res[$i]['incremento'];
			}
		}

		global $Result;
		$auxr = $leilao->select(array("id" => $dados['leilao']));
		$usuario = new Usuario();
		$auxr    = $usuario->select(array("id" => $auxr[0]['usuario_id']));

		$estado = new Estado();
		$est    = $estado->select(array("id" => $auxr[0]['estado']));
		$auxr[0]['estado'] = $est[0]['estado'];
		
		$Result['leilao'] = $res[0];
		$Result['login']   = $auxr[0];

		//Pegar Lances

		$res     = $lance->select(array("leilao_id" => $dados['leilao'],"ORDER" => "data DESC"));

		
		$n = count($res);
		for ($i=0; $i < $n; $i++) { 
			$r  = $usuario->select(array("id" => $res[$i]['usuario_id']));
			$res[$i]['usuario']  = $r[0];

			$aux = explode(" ", $res[$i]['data']);
			$res[$i]['data'] = $aux[0];
			$res[$i]['data-hora'] = $aux[1];			
		}

		$Result['lances']   = $res;

		//Buscar Ultimo Lance

		$lnc     = $lance->max($dados['leilao']);
		$res     = $lance->select(array("valor" => $lnc[0]['lance']));		
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));

		$res[0]['usuario'] = $r[0];
		$Result['ultimo-lance'] = $res[0];

		//Verificar se já se encerrou o leilão e quem é o ganhador
		$Result['vencedor'] = false;
		$res = $leilao->time($dados['leilao'],date("Y-m-d H:i:s"));
		
		if(count($res) == 0){
			$Result['vencedor'] = 10;
			if($Result['ultimo-lance']['usuario']['id'] != 0){				
				$leilao->update($dados['leilao'],array("ganhador" => $Result['ultimo-lance']['usuario']['id'],"status" => 3));
				//-------------
				if($login['id'] == $Result['ultimo-lance']['usuario']['id']){
					$negociacaoLeilao = new NegociacaoLeilao();
					$res = $negociacaoLeilao->select(array("AND" => array( "leilao_id" => $dados['leilao'],"usuario_id" => $Result['ultimo-lance']['usuario']['id'])));
					if(count($res) == 0)
						$Result['negociacao'] = "<a href='?task=Negociacao&action=abrirNegociacaoLeilaoPage&leilao=".$Result['leilao']['id']."' id='salvar' name='salvar' class='btn-sm btn-primary' value='submit'>Abrir Negociação</a>";
					else
						$Result['negociacao'] = "<a href='?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$Result['leilao']['id']."' id='salvar' name='salvar' class='btn-sm btn-primary' value='submit'>Ver Negociação</a>";
				}
				//-------------
			}else{
				$leilao->update($dados['leilao'],array("status" => 5));
				$Result['vencedor'] = 9;
			}

		}		


		//Reputação
		$reputacao = new Reputacao();
		$n = 0;
		//Positivas
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['leilao']['usuario_id'],"tipo_reputacao" => 1)));
		$n += count($res);
		//Negativas		
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['leilao']['usuario_id'],"tipo_reputacao" => 2)));
		$n -= count($res);
		$Result['usuario']['reputacao'] = $n;	


		$template->renderTemplate('Leilao','leilao',$alert,true);

}


public function cadastroPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','cadastro',$alert,true);

}

public function cadastrarAnuncioPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		global $Result;

		$data = date("d/m/Y",strtotime("+7 days"));
		$Result['expiraem'] = $data;

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','cadastrarAnuncio',$alert,true);

}

public function editarPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		$leilao    = new Leilao();
		$res       = $leilao->select(array("id" => $dados['leilao']));
		
		if($res[0]['status'] == 2 || $res[0]['status'] == 3 || $res[0]['status'] == 10){
			$template       =  new TemplateController();
			$template->redirectUrl("?task=Leilao&action=meusLeiloesPage","Não é possivel editar um leilão que esteja ativo ou encerrado, cancele o leilão para poder edita-lo!!!","danger");
			exit();
		}

		$aux       = explode(" ", $res[0]['data_fim']);
		$res[0]['data_fim'] = $aux[0];
		$aux       = explode(":",$aux[1]);
		$res[0]['hora'] = $aux[0].":".$aux[1];

		global $Result;

		$Result['leilao'] = $res[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','editar',$alert,true);

}


public function editarAnuncioPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		$anuncio    = new Anuncio();
		$res       = $anuncio->select(array("AND" => array("id" => $dados['anuncio'],"usuario_id" => $login['id'])));
		
		if($res[0]['status'] == 2 || $res[0]['status'] == 3 || $res[0]['status'] == 10){
			$template       =  new TemplateController();
			$template->redirectUrl("?task=Leilao&action=meusAnunciosPage","Não é possivel editar um Anúncio que esteja ativo, cancelado ou expirado!","danger");
			exit();
		}
		
		$aux       = explode(" ", $res[0]['expiraem']);
		$res[0]['expiraem'] = $aux[0];

		global $Result;

		$Result['anuncio'] = $res[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','editarAnuncio',$alert,true);

}

public function confirmarLancePage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();	
		$login = $Tvar->getSession('login');

		$leilao  = new Leilao();

		$res     = $leilao->select(array("id" => $dados['leilao']));

		$usuario = new Usuario();
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));

		$template =  new TemplateController();	

		if($res[0]['usuario_id'] == $login['id']){
			$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Você não pode dar lance no seu próprio leilão!!!","danger");	
		}		

		//Verificar se o leilão ainda esta dentro do prazo final

		$res = $leilao->time($dados['leilao'],date("Y-m-d H:i:s"));
		if(count($res) == 0){
			$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Não foi possivel enviar seu lance, o prazo do leilão já se encerrou!!!","danger");	
		}

		//print_r($res);exit();

		//Verificar se o lance é maior igual que o minimo	
		$lance  = new Lance();
		$lnc     = $lance->max($dados['leilao']);

		if($lnc[0]['lance'] != ""){
			$res = $leilao->select(array("id" => $dados['leilao']));
			$mim = number_format($res[0]['incremento']+$lnc[0]['lance'], 2, ',', '.');
			$dados['lance'] = str_replace(",", ".", $dados['lance']);
			
			if(number_format($dados['lance'], 2, ',', '.') < $mim){
					$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Você não pode dar um lance menor que o minimo!!!","danger");	
					exit();
			}
		}		

		global $Result;

		$Result['leilao']  = $res[0];
		$Result['usuario'] = $r[0];
		$Result['login'] = $login;
		$Result['lance'] = $dados['lance'];

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','confirmarLance',$alert,true);

}


public function mudarSituacaoConfirmarPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();	
		$login = $Tvar->getSession('login');


		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','mudarSituacaoConfirmar',$alert,true);

}


public function mudarSituacaoConcluirPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();	
		$login = $Tvar->getSession('login');


		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','mudarSituacaoConcluir',$alert,true);

}

public function solicitarCancelamentoPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();	
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);				

		$leilao = new Leilao();
		$res    = $leilao->select(array("id" => $dados['leilao']));

		$usuario = new Usuario();
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));

		global $Result;

		$Result['leilao']   = $res[0];
		$Result['vendedor'] = $r[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','solicitarCancelamento',$alert,true);

}


public function addTagsAnuncioPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();	
		$d     = $Tvar->GetReq();

		$carta  = new CartaController();
		$tagAnuncio   = new TagAnuncio();
		$anuncio = new Anuncio();
		
		if(isset($d['msg']))
			$alert  = $Tvar->createAlert($d['msg'],$d['tipo']);			

		$login = $Tvar->getSession('login');
		$std = $anuncio->select(array("AND" => array("usuario_id" => $login['id'],"id" => $d['anuncio'])));			

		global $Result,$FORM;

		$Result['cartas'] = $carta->buscarCartasAction(array("nome" => $this->trocarcaracter($d['nome'])),"nome");	

		for ($k=6; $k >= 1 ; $k--) { 
			$wantlist   = $tagAnuncio->getTagList($std[0]['id'],$raridade = $k);
				$j=0;
				for ($i=0; $i < count($wantlist); $i++) { 
					$c = $wantlist[$i]['carta_id'];
					$a = $carta->buscarCartasByIdAction($c);
					$aux[$j]['tipo']        =  $a[0]['carta'];
					$aux[$j]['id']          =  $a[0]['id'];
					$aux[$j]['nome']        =  $a[0]['nome'];
					$aux[$j]['numero']      =  $a[0]['numero'];
					$aux[$j]['sigla']       =  $a[0]['sigla'];
					$aux[$j]['qtd']         =  $wantlist[$i]['qtd'];	
					$j++;	
				}
			$WANTLIST[$k] = $aux;
			unset($aux);
		}

		$Result['wantlist'] = $WANTLIST;

		$template       =  new TemplateController();
		$template->renderTemplate('Leilao','addTagsAnuncio',$alert,true);	
}

// ACTIONS

public function cadastrarAction(){
		$Tvar      = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados     = $Tvar->PostReq();
		
		$file       = $_FILES['imagem'];
		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];						
		
		$leilao    = new Leilao();
		$dados['usuario_id'] = $login['id'];
		$dados['lance_inicial'] = str_replace(",", ".",$dados['lance_inicial']);
		$dados['incremento'] = str_replace(",", ".",$dados['incremento']);
		$dados['frete'] = str_replace(",", ".",$dados['frete']);
		$dados['data_inicio']   = $Tvar->dateEn($dados['data_inicio']);
		$dados['data_fim']      = $Tvar->dateEn($dados['data_fim'])." ".$dados['hora_fim'].":00";
		$dados['criado']      = date("Y-m-d H:i:s");
		$dados['imagem']      = $ext;		
		unset($dados['hora_fim']);
		
		$res = $leilao->insert($dados);
		
		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			$img = WideImage::load($file['tmp_name']);
			$img->resize(350,350)->saveToFile("upload/leilao/thumb/".$res.".".$ext);
			$img->saveToFile("upload/leilao/".$res.".".$ext);
		}		

		$template =  new TemplateController();		
		$template->redirectUrl("?task=Leilao&action=meusLeiloesPage&leilao=".$res,"Leilão Cadastrado com secesso!!!","success");
}


public function cadastrarAnuncioAction(){
		$Tvar      = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados     = $Tvar->PostReq();
		
		$file       = $_FILES['imagem'];
		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];						
		
		$anuncio    = new Anuncio();
		$dados['usuario_id'] = $login['id'];
		$dados['oferta'] = str_replace(",", ".",$dados['oferta']);
		//$dados['expiraem']   = $Tvar->dateEn($dados['expiraem'])." 23:59:00";
		$dados['expiraem'] = date("Y/m/d",strtotime("+7 days"))." 23:59:00";
		$dados['criado']      = date("Y-m-d H:i:s");
		$dados['img']      = $ext;		
		$dados['status']      = 1;	
		
		$res = $anuncio->insert($dados);
		
		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			$img = WideImage::load($file['tmp_name']);
			$img->resize(350,350)->saveToFile("upload/anuncio/thumb/".$res.".".$ext);
			$img->saveToFile("upload/anuncio/".$res.".".$ext);
		}		

		$template =  new TemplateController();		
		$template->redirectUrl("?task=Leilao&action=meusAnunciosPage&anuncio=".$res,"Anúncio Cadastrado com secesso!!!","success");
}


public function editarAction(){
		$Tvar      = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados     = $Tvar->PostReq();
		
		$file       = $_FILES['imagem'];
		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];						
		
		$leilao    = new Leilao();
		$dados['usuario_id'] = $login['id'];
		$dados['lance_inicial'] = str_replace(",", ".",$dados['lance_inicial']);
		$dados['incremento'] = str_replace(",", ".",$dados['incremento']);
		$dados['frete'] = str_replace(",", ".",$dados['frete']);
		$dados['data_inicio']   = $Tvar->dateEn($dados['data_inicio']);
		$dados['data_fim']      = $Tvar->dateEn($dados['data_fim'])." ".$dados['hora_fim'].":00";
		$dados['criado']      = date("Y-m-d H:i:s");
		$dados['imagem']      = $ext;		
		$dados['status']      = 1;
		unset($dados['hora_fim']);

		$res   = $leilao->select(array("usuario_id" => $login['id']));

		if($res[0]['status'] == 2 || $res[0]['status'] == 3 || $res[0]['status'] == 10){
			$template       =  new TemplateController();
			$template->redirectUrl("?task=Leilao&action=meusLeiloesPage","Não é possivel editar um leilão que esteja ativo ou encerrado, cancele o leilão para poder edita-lo!!!","danger");
			exit();
		}		

		if(count($res) == 0){
			$template =  new TemplateController();		
			$template->redirectUrl("?task=Leilao&action=editarPage&leilao=".$res,"Erro ao editar o Leilão!!!","danger");			
		}
		$idleilao = $dados['leilao'];
		unset($dados['leilao']);

		$res = $leilao->update($idleilao,$dados);

		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			$img = WideImage::load($file['tmp_name']);
			$img->resize(350,350)->saveToFile("upload/leilao/thumb/".$res.".".$ext);
			$img->saveToFile("upload/leilao/".$res.".".$ext);
		}		

		$template =  new TemplateController();		
		$template->redirectUrl("?task=Leilao&action=editarPage&leilao=".$idleilao,"Leilão Editado com secesso!!!","success");
}


public function editarAnuncioAction(){
		$Tvar      = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados     = $Tvar->PostReq();
		
		$file       = $_FILES['imagem'];
		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];						
		
		$anuncio    = new Anuncio();
		$dados['oferta'] = str_replace(",", ".",$dados['oferta']);
		$dados['img']      = $ext;	
		$dados['expiraem'] = date("Y/m/d",strtotime("+7 days"));

		
		$res       = $anuncio->select(array("AND" => array("id" => $dados['anuncio'],"usuario_id" => $login['id'])));
		
		if($res[0]['status'] == 2 || $res[0]['status'] == 3 || $res[0]['status'] == 10){
			$template       =  new TemplateController();
			$template->redirectUrl("?task=Leilao&action=meusAnunciosPage","Não é possivel editar um Anúncio que esteja ativo, cancelado ou expirado!","danger");
			exit();
		}	

		if(count($res) == 0){
			$template =  new TemplateController();		
			$template->redirectUrl("?task=Leilao&action=meusAnunciosPage&anuncio=".$dados['anuncio'],"Erro ao editar Anúncio!!!","danger");
			exit();
		}

		$idanuncio = $dados['anuncio'];
		unset($dados['anuncio']);

		$res = $anuncio->update($idanuncio,$dados);

		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			$img = WideImage::load($file['tmp_name']);
			$img->resize(350,350)->saveToFile("upload/leilao/thumb/".$res.".".$ext);
			$img->saveToFile("upload/leilao/".$res.".".$ext);
		}		

		$template =  new TemplateController();		
		$template->redirectUrl("?task=Leilao&action=editarAnuncioPage&anuncio=".$idanuncio,"Anúncio Editado com sucesso!!!","success");
}


public function getSituacaoAction($status){

	switch ($status) {
		case 1:
			$status = "<span class='label label-warning'>Pendente</span>";
			break;
		case 2:
			$status = "<span class='label label-info'>Ativo</span>";
			break;			
		case 3:
			$status = "<span class='label label-success'>Encerrado</span>";
			break;					
		case 4:
			$status = "<span class='label label-danger'>Cancelado</span>";
			break;	
		case 5:
			$status = "<span class='label label-default'>Sem Vencedor</span>";
			break;				
		case 10:
			$status = "<span class='label label-default'>Finalizado pela Administração</span>";
			break;				
	}
	return $status;
}

public function addLanceAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();	
		$login = $Tvar->getSession('login');
		$lance      = new Lance();
		$leilao     = new Leilao();

		$template =  new TemplateController();		

		//Verificar se o leilão ainda esta dentro do prazo final

		$res = $leilao->time($dados['leilao'],date("Y-m-d H:i:s"));
		if(count($res) == 0){
			$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Não foi possivel enviar seu lance, o prazo do leilão já se encerrou!!!","danger");	
		}

		//Verificar se o lance é maior igual que o minimo
		$lnc     = $lance->max($dados['leilao']);
		if($lnc[0]['lance'] != ""){
			$res = $leilao->select(array("id" => $dados['leilao']));
			$mim = number_format($res[0]['incremento']+$lnc[0]['lance'], 2, ',', '.');
			$dados['lance'] = str_replace(",", ".", $dados['lance']);
			if($dados['lance'] < $mim){
					$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Você não pode dar um lance menor que o minimo!!!","danger");	
					exit();
			}
		}		
		
		$res       = $lance->insert(array("leilao_id" => $dados['leilao'],"usuario_id" => $login['id'],"data" => date("Y-m-d H:i:s"),"valor" => $dados['lance']));		

		//Enviar Mensagem de notificação
		$leilao = new Leilao();
		$r      = $leilao->select(array("id" => $dados['leilao']));

		$d['de']                 =   $this->deSys;
		$d['para']               =   $r[0]['usuario_id'];
		$d['assunto']            =   "Novo Lance no Leilão #".$dados['leilao'];
		$d['mensagem']           =   "
		Ola,<br>Um novo lance foi dado no seu leilão #".$dados['leilao']." 
		<a href='?task=Leilao&action=leilaoPage&leilao=".$dados['leilao']."#lances'>clique aqui</a> para visualizar.
		";		

		$mensagem = new MensagemController();
		$mensagem->enviarMensagemSistemaAction($d);


		$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Lance enviado com sucesso!!!","success");	

}

public function mudarSituacaoAtivarAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();
		$login     = $Tvar->getSession('login');

		$leilao    = new Leilao();
		$res       = $leilao->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['leilao']))); 

		$template =  new TemplateController();	
		if(count($res) == 0){
			$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Erro ao ativar o leilao!!!","danger");
			exit;
		}

		$leilao->update($dados['leilao'],array("status" => 2));
		$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Leilão ativado com secesso!!!","success");
}


public function mudarSituacaoAnuncioAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login     = $Tvar->getSession('login');

		$anuncio    = new Anuncio();
		$res       = $anuncio->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['anuncio']))); 

		$template =  new TemplateController();	
		if(count($res) == 0){
			$template->redirectUrl("?task=Leilao&action=meuAnuncioAdminPage&anuncio=".$dados['anuncio'],"Erro ao ativar o anúncio!!!","danger");
			exit;
		}

		$anuncio->update($dados['anuncio'],array("status" => $dados['situacao']));
		$template->redirectUrl("?task=Leilao&action=meuAnuncioAdminPage&anuncio=".$dados['anuncio'],"Anúncio ativado com secesso!!!","success");
}

public function mudarSituacaoConcluidoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();
		$login     = $Tvar->getSession('login');

		$leilao    = new Leilao();
		$res       = $leilao->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['leilao']))); 

		$template =  new TemplateController();	
		if(count($res) == 0){
			$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Erro ao ativar o leilao!!!","danger");
			exit;
		}
		$leilao->update($dados['leilao'],array("status" => 3));
		$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Leilão ativado com secesso!!!","success");
}

public function cancelarAction(){

		$Tvar      = new TratamentoVar();
		$lance     = new Lance();
		$dados     = $Tvar->GetReq();
		$login     = $Tvar->getSession('login');

		$leilao    = new Leilao();

		if(!$Tvar->verificarAdmin())
			$res       = $leilao->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['leilao']))); 
		else
			$res       = $leilao->select(array("id" => $dados['leilao'])); 

		if(count($res) == 0){
			$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Erro ao cancelar o leilao!!!","danger");
			exit;
		}

		if(!$Tvar->verificarAdmin())
			$lnc     = $lance->max($dados['leilao']);
		
		$template =  new TemplateController();	
		if($lnc[0]['lance'] != ""){
			$template->redirectUrl("?task=Leilao&action=solicitarCancelamentoPage&leilao=".$dados['leilao'],"Não foi possivel cancelar seu Leilão porque existe lance(s) dados nele, caso queria cancela-lo mesmo assim, faça uma solicitação justificando o motivo do cancelamento para a administração pelo formulario abaixo!!!","danger");
			exit;
		}

		$leilao->update($dados['leilao'],array("status" => 4));
		$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Leilão cancelado com secesso!!!","success");
}


public function enviarSolicitacaoCancelamentoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();
		$login     = $Tvar->getSession('login');

		$leilao    = new Leilao();
		$res       = $leilao->select(array("AND" => array("id" => $dados['leilao'],"usuario_id" => $login['id'])));

		$usuario   = new Usuario();
		$r         = $usuario->select(array("id" => $res[0]['usuario_id']));

		$dados['id']         =     $login['id'];
		$dados['assunto']    =  "Solicitação de Cancelamento de Leilão";
		$dados['mensagem']   =  "
		<b>Usuario:</b> <a href='?task=Usuario&action=perfilPage&id=".$r[0]['id']."'>".$r[0]['login']."</a></br>
		<b>Solicitou o cancelamento do Leilão:</b> <a href='?task=Leilao&action=leilaoPage&leilao=".$res[0]['id']."'>".$res[0]['nome']."</a></br>
		<b>Pelo Seguinte motivo:</b> ".$dados['mensagem']."</br>
		";	
		$mensagemController = new MensagemController();
		$mensagemController->enviarMensagemSistemaAction($dados,array(1,2));
		
		$template =  new TemplateController();	
		$template->redirectUrl("?task=Leilao&action=meuLeilaoAdminPage&leilao=".$dados['leilao'],"Solicitação enviada com sucesso, aguarde uma respota de um administrador!!!","success");		
}

function trocarcaracter($char){


	$carac_esp  = array("á","Á","â","Â","à","À","ã","Ã","ç","Ç","é","É","ê","Ê","í","Í","ó","Ó","ô","Ô","õ","Õ","ú","Ú","ü","Ü","º","ª","-");
	$carac_html = array("&aacute;","&Aacute;","&acirc;","&Acirc;","&agrave;","&Agrave;","&atilde;","&Atilde;","&ccedil;","&Ccedil;","&eacute;","&Eacute;","&ecirc;","&Ecirc;","&iacute;","&Iacute;","&oacute;","&Oacute;","&ocirc;","&Ocirc;","&otilde;","&Otilde;","&uacute;","&Uacute;","&uuml;","&Uuml;","&ordm","&ordf","&#45;");
	$char = str_replace($carac_esp,$carac_html,$char);
	return $char;
}

public function addAnuncioListAction(){

		$Tvar  = new TratamentoVar();
		$tag  = new TagAnuncio();
		$carta = new Carta();		
		$anuncio = new Anuncio();

		$dados = $Tvar->GetReq();		
		$login = $Tvar->getSession('login');

		$std = $anuncio->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['anuncio'])));		

		$res = $tag->select(array("AND" => array( "carta_id" => $dados['carta'],"anuncio_id" => $dados['anuncio'])));
		
		$template =  new TemplateController();
		if(!$res){
			$res = $tag->insert(array("carta_id" => $dados['carta'],"qtd" => $dados['qtd'],"anuncio_id" => $dados['anuncio']));
			$template->redirectUrl("?task=Leilao&action=addTagsAnuncioPage&anuncio=".$dados['anuncio'],"Carta inserida no Conteúdo do Anúncio com sucesso!!!","success");								
		}else{
			$res = $tag->update($res[0]['id'],array("qtd" => $dados['qtd']));
			$template->redirectUrl("?task=Leilao&action=addTagsAnuncioPage&anuncio=".$dados['anuncio'],"Quantidade da Carta foi atualizada com sucesso!!!","success");								
		}		
}

public function rmvCartaAnuncioAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();

		$anuncio  = new TagAnuncio();
		$anuncio->delete(array('AND' =>  array('carta_id' => $dados['carta'],'anuncio_id' => $dados['anuncio'])));
		
		$template =  new TemplateController();
		$template->redirectUrl("?task=Leilao&action=addTagsAnuncioPage&anuncio=".$dados['anuncio'],"Carta removida com sucesso!!!","success");								
}

}
