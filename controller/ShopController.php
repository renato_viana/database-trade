<?php

require_once 'controller/CartaController.php';
require_once 'controller/TemplateController.php';
require_once 'controller/PaginacaoController.php';
require_once 'controller/MailerController.php';
require_once 'model/Estoque.class.php';
require_once 'model/Frete.class.php';
require_once 'model/Estado.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Pagamento.class.php';
require_once 'model/Pedido.class.php';
require_once 'model/PedidoDetalhes.class.php';

class ShopController {

	function access403Page(){
		
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		$template =  new TemplateController();
		$template->renderTemplate("Shop","403",$alert);	
	}

	function defaultPage(){

		$Tvar  = new TratamentoVar();
		$carta  = new CartaController();

		global $Result,$FORM;
  		
		$Result = $carta->buscarCartasEspecificasAction(
			array(	
			"AND" => array(	 
				"mtcg_estoque.qtd[>]" => 0,	
			"OR" => 
				array("raridade" => array(2,3))
				)
			)
			);

		shuffle($Result);

		$estoque  = new Estoque();

		$n = count($Result);
		for($i=0;$i<$n;$i++){
			$id  = $Result[$i]['id'];
			$res = $estoque->select(array( "carta_id" => $Result[$i]['id']));
			
			$Result[$i]['qtd']    =  $res[0]['qtd'];
			$Result[$i]['preco']  =  number_format($res[0]['preco'], 2, ',', '.');
			$Result[$i]['oferta'] =  $res[0]['oferta'];
		}


		$template =  new TemplateController();
		$template->renderTemplate("Shop","default");	

	}

	function contatoPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		$template =  new TemplateController();
		$template->renderTemplate("Shop","contato",$alert);	

	}	


	function estoquePage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$carta  = new CartaController();

		global $Result,$FORM;
		$Result = $carta->buscarCartasAction(array("colecao" => $dados['categoria']),"numero");

		$estoque  = new Estoque();

		$n = count($Result);
		for($i=0;$i<$n;$i++){
			$id  = $Result[$i]['id'];
			$res = $estoque->select(array( "carta_id" => $Result[$i]['id']));
			
			if(count($res) != 0){
				$Result[$i]['qtd']   =  $FORM->textCreate(array("qtd-".$id => $res[0]['qtd']),array("style" =>"width:70px;"));
				$Result[$i]['preco'] =  $FORM->textCreate(array("preco-".$id => $res[0]['preco']),array("style" =>"width:70px;"));
				$Result[$i]['oferta'] =  $FORM->textCreate(array("oferta-".$id => $res[0]['oferta']),array("style" =>"width:70px;"));
			}else{
				$d['carta_id']  = $id;
				$d['qtd']  = 0;
				$d['preco']  = 0;
				$d['oferta']  = 0;
				$estoque->insert($d);
			}
			

		}

		$template =  new TemplateController();
		$template->renderTemplate("Shop","estoque");
	}	


	function produtosPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		$lista = $Tvar->getSession('lista');

		$carta  = new CartaController();

		global $Result,$FORM;

		$d['colecao'] = $dados['categoria'];
		if(isset($dados['raridade']))
			$d['raridade'] = $dados['raridade'];

		if(!isset($dados['pagina'])){
			$Result = $carta->buscarCartasAction($d,"numero");
			$Tvar->createSession('lista',$Result);
			$total = count($Result);
			
		}else{
			$total = count($lista);
			$Result = $lista;
		}			

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,18,$total);

		$estoque  = new Estoque();

		$n = count($Result);
		for($i=0;$i<$n;$i++){
			$id  = $Result[$i]['id'];
			$res = $estoque->select(array( "carta_id" => $Result[$i]['id']));
			
			$Result[$i]['qtd']    =  $res[0]['qtd'];
			$Result[$i]['preco']  =  number_format($res[0]['preco'], 2, ',', '.');
			$Result[$i]['oferta'] =  $res[0]['oferta'];
		}

		$Result['paginacao'] = $paginacao;
		$template =  new TemplateController();
		$template->renderTemplate("Shop","produtos");
	}

	function viewProdutoPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$carta  = new CartaController();

		global $Result,$FORM;
		$Result = $carta->buscarCartasAction(array("id" => $dados['produto']));

		$estoque  = new Estoque();

		$n = count($Result);
		for($i=0;$i<$n;$i++){
			$id  = $Result[$i]['id'];
			$res = $estoque->select(array( "carta_id" => $Result[$i]['id']));
			
			$Result[$i]['qtd']    =  $res[0]['qtd'];
			$Result[$i]['preco']  =  number_format($res[0]['preco'], 2, ',', '.');
			$Result[$i]['oferta'] =  $res[0]['oferta'];
		}

		$template =  new TemplateController();
		$template->renderTemplate("Shop","viewProduto");
	}		


	function carrinhoPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		if(!$this->validarCompraAction()){	
			$template->redirectUrl("?task=Shop&action=carrinhoPage","Alguns Item nao estao mais disponiveis e foram retirados do carrinho!","danger");
			exit;
		}

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$dados = $_SESSION['carrinho'];
		$key   = array_keys($dados);
		
		$carta    =  new CartaController();
		$estoque  =  new Estoque();

		global $Result,$FORM;

		$Result['total'] = 0;
		$n = count($key);
		for ($i=0; $i < $n; $i++) { 

			$id   = $key[$i];
			$R = $carta->buscarCartasAction(array("id" => $id));
			$res = $estoque->select(array( "carta_id" => $id));
			$Result['total'] += $res[0]['preco']*$dados[$id]['qtd'];
			$R['qtd']        =  $FORM->textCreate(array("qtd-".$id => $dados[$id]['qtd']),array("style" =>"width:40px;","id" => "item_".$id));
			$R['preco']      =  number_format($res[0]['preco'], 2, ',', '.');
			$R['precototal'] =  number_format($res[0]['preco']*$dados[$id]['qtd'], 2, ',', '.');

			$Result[$i] = $R;
		}
		
		// Listar Itens Invalidos {
		if(isset($_SESSION['invalido'])){

			$dados = $_SESSION['invalido'];
			$key   = array_keys($dados);
			$n = count($key);
			for ($i=0; $i < $n; $i++) { 

				$id   = $key[$i];
				$res = $carta->buscarCartasAction(array("id" => $id));
				$r[$i]['nome']     =  $res[0]['nome'];
				
			}
			$Result['invalido'] = $r;
			unset($_SESSION['invalido']);
		}
		// Listar Itens Invalidos }

		$template =  new TemplateController();
		$template->renderTemplate("Shop","carrinho",$alert);
	}	



	function fretePage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();		

		if(!isset($_SESSION['login'])){
			$template =  new TemplateController();
			$template->redirectUrl("?task=Shop&action=logincadastroPage");
			exit();
		}

		$frete = new Frete();
		global $FORM;

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			
		
		$res   = $frete->select();
		$n     = count($res);
		for ($i=0; $i < $n; $i++) { 
			$d['frete'] = $res[$i]['id'];
			$FORM->radio[]     = $FORM->radioCreate($d,array("id" => "frete-".$d['frete']),"<b>".$res[$i]['tipo'].": </b>".number_format($res[$i]['valor'], 2, ',', '.')." - ".$res[$i]['descricao'],"frete-".$d['frete']);
		}

		$template =  new TemplateController();
		$template->renderTemplate("Shop","frete",$alert);
	}	


	function enderecoPage(){

		global $FORM;
		$template =  new TemplateController();
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();

		$frete = new Frete();
		$res   = $frete->select(array("id" => $dados['frete']));

		if(count($res) == 0){
			$template->redirectUrl("?task=Shop&action=fretePage","Selecione uma forma de entrega!","danger");
			exit();
		}

		$_SESSION['frete'] = $dados['frete'];

		$login = $Tvar->getSession('login');
		
		$usuario = new Usuario();
		$res     = $usuario->select(array("id" => $login['id']));

		$estado = new Estado();
		$estado = $estado->select();
		
		$FORM->nome     = $FORM->textCreate(array("nome" => $res[0]['nome']));
		$FORM->fone     = $FORM->textCreate(array("fone" => $res[0]['fone']),array("id" => "fone"));
		$FORM->cep     = $FORM->textCreate(array("cep" => $res[0]['cep']),array("id" => "cep"));
		for ($i=0; $i < count($estado); $i++) { 
			$aux[$estado[$i]['id']] = $estado[$i]['estado'];
		}
		$FORM->estado = $FORM->selectCreate(array("estado" => $aux),NULL,$res[0]['estado']);
		$FORM->cidade     = $FORM->textCreate(array("cidade" => $res[0]['cidade']));
		$FORM->rua     = $FORM->textCreate(array("rua" => $res[0]['rua']));
		$FORM->complemento     = $FORM->textCreate(array("complemento" => $res[0]['complemento']));
		$FORM->bairro     = $FORM->textCreate(array("bairro" => $res[0]['bairro']));

		$template->renderTemplate("Shop","endereco");
	}	

	function pagamentoPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		$dados = $Tvar->PostReq();
		unset($dados['confirma-envio']);		

		$login = $Tvar->getSession('login');
		$usuario = new Usuario();
		$res     = $usuario->update($login['id'],$dados);

		$pag = new Pagamento();
		global $FORM;		
		
		$res   = $pag->select();
		
		$n     = count($res);
		for ($i=0; $i < $n; $i++) { 
			$d['pagamento'] = $res[$i]['id'];
			$FORM->radio[]     = $FORM->radioCreate($d,array("id" => "pag-".$d['pagamento']),$res[$i]['tipo'],"pag-".$d['pagamento']);
		}		
		$template =  new TemplateController();
		$template->renderTemplate("Shop","pagamento",$alert);
	}	


	function finalizarPage(){
		

		$template =  new TemplateController();

		if(!isset($_SESSION['carrinho'])){
			$template->redirectUrl("?task=Shop&action=carrinhoPage");
			exit();
		}

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();

		$pag = new Pagamento();
		$res   = $pag->select(array("id" => $dados['pagamento']));

		if(count($res) == 0){
			$template->redirectUrl("?task=Shop&action=pagamentoPage","Selecione uma forma de pagamento!","danger");
			exit();
		}

		//Verificar se os produtos ainda estao em estoque {


		if(!$this->validarCompraAction()){	
			$template->redirectUrl("?task=Shop&action=carrinhoPage","Alguns Item nao estao mais disponiveis e foram retirados do carrinho!","danger");
			exit;
		}
		//Verificar se os produtos ainda estao em estoque }

		$_SESSION['pagamento'] = $dados['pagamento'];

		$pedido   = new Pedido();
		$detalhes = new PedidoDetalhes();


		$dados = $_SESSION['carrinho'];
		$key   = array_keys($dados);
		$carta    =  new CartaController();
		$estoque  =  new Estoque();

		global $Result,$FORM;

		$Result['total'] = 0;
		$n = count($key);
		for ($i=0; $i < $n; $i++) { 
			$id   = $key[$i];
			$res = $estoque->select(array( "carta_id" => $id));
			$Result['total'] += $res[0]['preco']*$dados[$id]['qtd'];
		}		


		$login = $Tvar->getSession('login');
		$usuario = new Usuario();
		$res     = (array) $usuario->select(array("id" => $login['id']));
		unset($res['database']);

		$Tvar->createSession('login',$res[0]);
		$login = $Tvar->getSession('login');

		$Result['login']     = $login;
		$Result['pagamento'] = $_SESSION['pagamento'];

		$estado = new Estado();
		$res    = $estado->select(array("id" => $login['estado']));
		$Result['login']['estado'] = $res[0]['estado'];

		$frete = new Frete();
		$r           = $frete->select(array("id" => $_SESSION['frete']));
		$valorfrete  = $r[0]['valor'];

		$d['usuario_id'] = $login['id'];
		$d['valor'] = $Result['total']+$valorfrete;
		$d['pagamento'] = $_SESSION['pagamento'];
		$d['frete'] = $_SESSION['frete'];
		$d['status'] = 1;
		$d['data'] = date('Y-m-d');
		$d['hora'] = date('H:i:s');

		$res = $pedido->insert($d);
		$idpedido = $res;

		unset($d);
		$n = count($key);
		for ($i=0; $i < $n; $i++) { 
			$id   = $key[$i];
			$r = $estoque->select(array( "carta_id" => $id));
			$d['carta_id']  = $id;
			$d['qtd']       = $dados[$id]['qtd'];
			$d['preco']     = $r[0]['preco'];
			$d['pedido_id'] = $res;
			$estoque->update(array( "carta_id" => $id),array("qtd[-]" => $dados[$id]['qtd']));
			$aux = $detalhes->insert($d);
		}


		$dados = $_SESSION['carrinho'];
		$key   = array_keys($dados);
		
		$carta    =  new CartaController();
		$estoque  =  new Estoque();

		global $Result,$FORM;

		$Result['total'] = 0;
		$n = count($key);
		unset($R);
		for ($i=0; $i < $n; $i++) { 

			$id   = $key[$i];
			$R = $carta->buscarCartasAction(array("id" => $id));
			$res = $estoque->select(array( "carta_id" => $id));
			
			$Result['total'] += $res[0]['preco']*$dados[$id]['qtd'];

			$R['qtd']        =  $dados[$id]['qtd'];
			$R['preco']      =  number_format($res[0]['preco'], 2, ',', '.');
			$R['precototal'] =  number_format($res[0]['preco']*$dados[$id]['qtd'], 2, ',', '.');

			$Result[$i] = $R;
		}

		$frete = new Frete();
		$res   = $frete->select(array("id" => $_SESSION['frete']));
		$Result['frete'] = $res[0]['valor'];
		$Result['total'] += $res[0]['valor'];
		$Result['idpedido'] = $idpedido;

		$this->emailPedidoAction($idpedido);		
		
		unset($_SESSION['frete']);
		unset($_SESSION['pagamento']);
		unset($_SESSION['carrinho']);

		$template =  new TemplateController();
		$template->renderTemplate("Shop","finalizar");
		
	}	

	function meusPedidosPage(){

		$Tvar  = new TratamentoVar();
		$login = $Tvar->getSession('login');

		$pedido = new Pedido();
		$res    = $pedido->select(array("usuario_id" => $login['id'],"ORDER" => "id DESC"));

		global $Result;

		$Result = $res;

		$template =  new TemplateController();
		$template->renderTemplate("Shop","meusPedidos");		

	}


	function detalhesPedidoPage(){

		$Tvar  = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados = $Tvar->GetReq();

		$usuario = new Usuario();
		$res     = $usuario->select(array("id" => $login['id']));

		$pedido = new Pedido();

		if($res[0]['tipo_usuario'] == 1){
		   $res    = $pedido->select(array("id" => $dados['pedido']));
		}else{
		   $res    = $pedido->select(array("AND" => array("id" => $dados['pedido'], "usuario_id" => $login['id'])));
		}

		if(count($res) == 0){
			$template =  new TemplateController();
			$template->redirectUrl("?task=Shop&action=access403Page","Acesso Proibido!!!","danger");
			exit;
		}


		$detalhes = new PedidoDetalhes();
		$r        = $detalhes->select(array("pedido_id" => $res[0]['id']));

		$carta = new CartaController();
		$n = count($r);
		global $Result;

		for ($i=0; $i < $n; $i++) { 

			$id   = $r[$i]['carta_id'];
			$aux  =  $carta->buscarCartasAction(array("id" => $id));
			$Result['lista'][$i] = $aux;
			$Result['lista'][$i]['preco'] = number_format($r[$i]['preco'], 2, ',', '.');
			$Result['lista'][$i]['qtd'] = $r[$i]['qtd'];
			$Result['lista'][$i]['valor-total'] = number_format($r[$i]['qtd']*$r[$i]['preco'], 2, ',', '.');
		}		

		$Result['pedido'] = $res[0];
		
		$estado = new Estado();
		$res    = $estado->select(array("id" => $login['estado']));
		$login['estado']  = $res[0]['estado'];
		$Result['login']  = $login;

		$template =  new TemplateController();
		$template->renderTemplate("Shop","detalhesPedido");	

	}	

	function buscaPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();

		$carta  = new CartaController();

		global $Result,$FORM;

		$Result = $carta->buscarCartasAction(array("nome" => $this->trocarcaracter($dados['nome'])),"nome");

		$estoque  = new Estoque();

		$n = count($Result);
		for($i=0;$i<$n;$i++){
			$id  = $Result[$i]['id'];
			$res = $estoque->select(array( "carta_id" => $Result[$i]['id']));
			
			$Result[$i]['qtd']    =  $res[0]['qtd'];
			$Result[$i]['preco']  =  number_format($res[0]['preco'], 2, ',', '.');
			$Result[$i]['oferta'] =  $res[0]['oferta'];
		}


		$template =  new TemplateController();
		$template->renderTemplate("Shop","busca");	

	}

	function logincadastroPage(){

		$template =  new TemplateController();
		$template->renderTemplate("Shop","logincadastro");	
	}


	function adminPedidosPage(){

		$Tvar  = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados = $Tvar->GetReq();
		$d = NULL;
		if(isset($dados['status']) && $dados['status'] != 6)
			$d['status'] = $dados['status'];

		
		$d['ORDER'] = "id DESC";

		$pedido = new Pedido();
		$res    = $pedido->select($d);

		global $Result;

		$Result = $res;

		$template =  new TemplateController();
		$template->renderTemplate("Shop","adminPedidos");		

	}


	function addProdutoAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$carta  = new CartaController();
		$estoque  = new Estoque();

		global $Result;
		$Result = $carta->buscarCartasAction(array("id" => $dados['produto']));
		$res = $estoque->select(array( "carta_id" => $dados['produto']));

		$_SESSION['carrinho'][$dados['produto']]['qtd'] = 1;

		$template =  new TemplateController();
		$template->redirectUrl("?task=Shop&action=carrinhoPage");
	}	

	function updateProdutoAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$estoque  = new Estoque();

		global $Result;
		$res = $estoque->select(array( "carta_id" => $dados['produto']));


		$template =  new TemplateController();

		if($dados['qtd'] <= $res[0]['qtd']){
			$_SESSION['carrinho'][$dados['produto']]['qtd'] = $dados['qtd'];

			$template->redirectUrl("?task=Shop&action=carrinhoPage","Produto Atualizado com sucesso!","success");
		}else{
			$template->redirectUrl("?task=Shop&action=carrinhoPage","So possuimos ".$res[0]['qtd']." unidades em estoque deste produto!","danger");
		}		
	}		

	function delProdutoAction(){
		
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		unset($_SESSION['carrinho'][$dados['produto']]);

		$template =  new TemplateController();
		$template->redirectUrl("?task=Shop&action=carrinhoPage","Produto deletado com sucesso!","success");

	}	

	function updateAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $_POST;

		$estoque  = new Estoque();

		foreach($dados['id_card'] as $id){
			$d['qtd']    = @$dados['qtd-'.$id];
			$d['preco']  = @$dados['preco-'.$id];
			$d['oferta'] = @$dados['oferta-'.$id];

			$res = $estoque->update($id,$d);
		}
		$template =  new TemplateController();
		$template->redirectUrl("?task=Shop&action=estoquePage&categoria=".$dados['categoria']);
	}

	function validarCompraAction(){

		$dados = $_SESSION['carrinho'];
		$key   = array_keys($dados);
		
		$estoque  =  new Estoque();
		global $Result,$FORM;
		$invalido = NULL;

		$n = count($key);
		for ($i=0; $i < $n; $i++) { 

			$id   = $key[$i];
			$res = $estoque->select(array( "carta_id" => $id));
			$R['qtd']        =  $dados[$id]['qtd'];
			if($dados[$id]['qtd'] > $res[0]['qtd']){
				$invalido[$id] = $res[0]['qtd'];
				if($res[0]['qtd'] > 0)
					$_SESSION['carrinho'][$id]['qtd'] = $res[0]['qtd'];
				else
					unset($_SESSION['carrinho'][$id]);
			}
		}
		if($invalido != NULL){
			$_SESSION['invalido'] = $invalido;
			return false;
		} 
		return true;
	}	


	function mudarStatusAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		$pedido = new Pedido();

		if($dados['status'] == 5){
			$login = $Tvar->getSession('login');

			$res    = $pedido->select(array("id" => $dados['pedido']));

			$detalhes = new PedidoDetalhes();
			$r        = $detalhes->select(array("pedido_id" => $res[0]['id']));

			$estoque  = new Estoque();

			$n = count($r);
			for ($i=0; $i < $n; $i++) { 
				$carta_id   = $r[$i]['carta_id'];
				$qtd        = $r[$i]['qtd'];

				$estoque->update(array( "carta_id" => $carta_id),array("qtd[+]" => $qtd));
			}
		}

		$res    = $pedido->update($dados['pedido'],array("status" => $dados['status']));

		$template =  new TemplateController();
		$template->redirectUrl("?task=Shop&action=adminPedidosPage");

	}

	function contatoAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();	

		$mailer = new MailerController();
		
		$msg    = $mailer->createMsgContatoAction($dados['nome'],$dados['email'],$dados['fone'],$dados['assunto'],$dados['msg']);
		$para   = "f4rr3ll.ftc@gmail.com";
		$nome   = "Atacadao TCG";
		$assunto   = "Contato Atacadão TCG";

		$template =  new TemplateController();

		if($mailer->enviarEmailAction($para,$nome,$assunto,$msg)){
			$template->redirectUrl("?task=Shop&action=contatoPage","Email enviado com sucesso, entraremos em contato em breve!!!","success");
		}else{
			$template->redirectUrl("?task=Shop&action=contatoPage","Nao foi possivel enviar o email!!!","danger");
		}
	}	


	function emailPedidoAction($idpedido){
		
		$Tvar  = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados = $Tvar->GetReq();
		
		$pedido = new Pedido();
		$res    = $pedido->select(array("id" => $idpedido));
		
		$detalhes = new PedidoDetalhes();
		$r        = $detalhes->select(array("pedido_id" => $res[0]['id']));

		$carta = new CartaController();
		$n = count($r);

		for ($i=0; $i < $n; $i++) { 

			$id   = $r[$i]['carta_id'];
			$aux  =  $carta->buscarCartasAction(array("id" => $id));
			$Result['lista'][$i] = $aux;
			$Result['lista'][$i]['preco'] = number_format($r[$i]['preco'], 2, ',', '.');
			$Result['lista'][$i]['qtd'] = $r[$i]['qtd'];
			$Result['lista'][$i]['valor-total'] = number_format($r[$i]['qtd']*$r[$i]['preco'], 2, ',', '.');
		}		

		$Result['pedido'] = $res[0];
		
		$frete  = new Frete();
		$f      = $frete->select(array("id" => $res[0]['frete-id']));

		$Result['frete'] = $f[0];

		$estado = new Estado();
		$res    = $estado->select(array("id" => $login['estado']));
		$login['estado']  = $res[0]['estado'];
		$Result['login']  = $login;

		$mailer = new MailerController();
		$msg    = $mailer->createMsgPedidoAction($Result);
		
		if($mailer->enviarEmailAction($login['email'],$login['nome'],"Atacadao TCG - Novo Pedido #".$idpedido,$msg)){
			//echo "ok<pre>";
			
			//$template->redirectUrl("?task=Shop&action=contatoPage","Email enviado com sucesso, entraremos em contato em breve!!!","success");
		}else{
			//echo "error";
			//$template->redirectUrl("?task=Shop&action=contatoPage","Nao foi possivel enviar o email!!!","danger");
		}		

	}	


	function pagseguroAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$pedido = new Pedido();
		$res    = $pedido->select(array("id" => $dados['pedido']));
		
		$detalhes = new PedidoDetalhes();
		$r        = $detalhes->select(array("pedido_id" => $res[0]['id']));

		$carta = new CartaController();
		$n = count($r);
		$itens = "";

		for ($i=0; $i < $n; $i++) { 

			$id   = $r[$i]['carta_id'];
			$aux  = $carta->buscarCartasAction(array("id" => $id));
			$itens .= "<item>
			<id>".$aux[0]['id']."</id>
			<description>".$this->trocarcaracterHE($aux[0]['nome'])."</description>
			<amount>".number_format($r[$i]['preco'], 2, '.', '')."</amount>
			<quantity>".$r[$i]['qtd']."</quantity>
			</item>";
		}	
		$itens .= "<item>
		<id>".$res[0]['frete-id']."</id>
		<description>Frete</description>
		<amount>".number_format($res[0]['frete-valor'], 2, '.', '')."</amount>
		<quantity>1</quantity>
		</item>";


		$email = 'f4rr3ll.ftc@gmail.com';
		$token = 'A3B9A9C2AF52498AA3F3233ED3D20AD3';

		$url = 'https://ws.pagseguro.uol.com.br/v2/checkout/?email=' . $email . '&token=' . $token;

		$xml = "<?xml version='1.0' encoding='ISO-8859-1' standalone='yes'?>
		<checkout>
		<currency>BRL</currency>
		<redirectURL>http://www.atacadaotcg.com.br</redirectURL>
		<items>".$itens."</items>
		<reference>".$dados['pedido']."</reference>
		</checkout>";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/xml; charset=ISO-8859-1'));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
		$xml= curl_exec($curl);

		if($xml == 'Unauthorized'){
        //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção

			echo "Erro em criar o XML";
        exit;//Mantenha essa linha
    }
    
    curl_close($curl);
    
    $xml= simplexml_load_string($xml);
    if(count($xml -> error) > 0){
        //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção, talvez seja útil enviar os códigos de erros.

    	echo "Erro retorno do XML";
    	exit;
    }
    header('Location: https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml -> code);

	}


	function trocarcaracter($char){


		$carac_esp  = array("á","Á","â","Â","à","À","ã","Ã","ç","Ç","é","É","ê","Ê","í","Í","ó","Ó","ô","Ô","õ","Õ","ú","Ú","ü","Ü","º","ª","-");
		$carac_html = array("&aacute;","&Aacute;","&acirc;","&Acirc;","&agrave;","&Agrave;","&atilde;","&Atilde;","&ccedil;","&Ccedil;","&eacute;","&Eacute;","&ecirc;","&Ecirc;","&iacute;","&Iacute;","&oacute;","&Oacute;","&ocirc;","&Ocirc;","&otilde;","&Otilde;","&uacute;","&Uacute;","&uuml;","&Uuml;","&ordm","&ordf","&#45;");
		$char = str_replace($carac_esp,$carac_html,$char);
		return $char;
	}	

	function trocarcaracterHE($char){


		$carac_esp  = array("á","Á","â","Â","à","À","ã","Ã","ç","Ç","é","É","ê","Ê","í","Í","ó","Ó","ô","Ô","õ","Õ","ú","Ú","ü","Ü","º","ª","-");
		$carac_html = array("&aacute;","&Aacute;","&acirc;","&Acirc;","&agrave;","&Agrave;","&atilde;","&Atilde;","&ccedil;","&Ccedil;","&eacute;","&Eacute;","&ecirc;","&Ecirc;","&iacute;","&Iacute;","&oacute;","&Oacute;","&ocirc;","&Ocirc;","&otilde;","&Otilde;","&uacute;","&Uacute;","&uuml;","&Uuml;","&ordm","&ordf","&#45;");
		$char = str_replace($carac_html,$carac_esp,$char);
		return $char;
	}		

}

