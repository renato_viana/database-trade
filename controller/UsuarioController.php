<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/MailerController.php';
require_once 'model/Usuario.class.php';
require_once 'model/Deck.class.php';
require_once 'model/Estande.class.php';
require_once 'model/Leilao.class.php';
require_once 'model/RecuperarSenha.class.php';
require_once 'model/Estado.class.php';
require_once 'model/Reputacao.class.php';
require_once 'model/Negociacao.class.php';
require_once 'model/NegociacaoLeilao.class.php';


class UsuarioController {


// PAGES	

	public function registerPage() 
	{
		$task     = $_GET['task'];
		$page   = 'register';
		

		$template =  new TemplateController();
		$template->renderTemplate($task,$page,NULL,true);
	}

	public function perfilPage() 
	{

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->GetReq();

		global $Result;

		$usuario = new Usuario();
		$res     = $usuario->select(array("id" => $dados['id']));

		$estado = new Estado();
		$r      = $estado->select(array("id" => $res[0]['estado']));
		$res[0]['estado'] = $r[0]['estado'];
		$res[0]['tipo_usuario-id'] = $res[0]['tipo_usuario'];
		$res[0]['tipo_usuario']    = $this->getTipoUsuarioAction($res[0]['tipo_usuario']);

		$deck    = new Deck();
		$deck    = $deck->getListDeck($dados['id']);

		$Result['usuario'] = $res;
		$Result['deck'] = $deck;

		$estande = new Estande();
		$res     = $estande->select(array("usuario_id" => $dados['id']));
		
		if(count($res) > 0){
			$estande = $res[0]['id'];
			$Result['estande'] = "<a href='?task=Estande&action=estandePage&estande=$estande'>Veja minha estande</a>";
		}else
			$Result['estande'] = "Não ativo";

		$leilao = new Leilao();
		$res     = $leilao->select(array("usuario_id" => $dados['id']));
		
		if(count($res) > 0){
			$leilao = $res[0]['id'];
			$Result['leilao'] = "<a href='?task=Leilao&action=meusLeiloesPublicosPage&leilao=$leilao'>Veja meus leilões</a>";
		}else
			$Result['leilao'] = "Não possui nenhum leilao";			

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		//Pegar Reputação

		$reputacao = new Reputacao();

		$n = 0;
		//Positivas
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['usuario'][0]['id'],"tipo_reputacao" => 1)));
		$n += count($res);
		
		$Result['rep-positiva'] = $this->getListReputacaoAction($res,$dados['id']);
		//Negativas		
		$res       = $reputacao->select(array("AND" => array("usuario_id" => $Result['usuario'][0]['id'],"tipo_reputacao" => 2)));
		$n -= count($res);

		$Result['rep-negativa'] = $this->getListReputacaoAction($res,$dados['id']);
		$Result['rep-soma'] = $n;

		$template =  new TemplateController();
		$template->renderTemplate('Usuario','perfil',$alert,true);
	}	

	public function updatePerfilPage() 
	{
		$task     = $_GET['task'];
		$page   = 'updateperfil';

		$template =  new TemplateController();
		$template->renderTemplate($task,$page,NULL,true);
	}	

	public function senhaPage() 
	{
		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		$template =  new TemplateController();
		$template->renderTemplate('Usuario','senha',$alert,true);
	}	

	public function loginPage() 
	{
		$Tvar    = new TratamentoVar();
		$Tvar->destroySession('form');
		$dados   = $Tvar->GetReq();

		
		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);

		$task     = $_GET['task'];
		$page   = 'login';

		$template =  new TemplateController();
		$template->renderTemplate($task,$page,$alert,true);
	}	

	function recuperarSenhaPage(){
		
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		
		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);		

		$template =  new TemplateController();
		$template->renderTemplate("Usuario","recuperarSenha",$alert,true);
	}

	function contatoPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		$template =  new TemplateController();
		$template->renderTemplate("Usuario","contato",$alert,true);	

	}	

	function fotoPerfilPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		$login   = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		global $Result;

		$usuario = new Usuario();
		$res     = $usuario->select(array("id" => $login['id']));

		$Result['usuario'] = $res[0];

		$template =  new TemplateController();
		$template->renderTemplate("Usuario","fotoPerfil",$alert,true);	

	}	


	// ACTIONS

	function contatoAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();	

		$mailer = new MailerController();
		
		$msg    = $mailer->createMsgContatoAction($dados['nome'],$dados['email'],$dados['fone'],$dados['assunto'],$dados['msg']);
		$para   = "f4rr3ll.ftc@gmail.com";
		$nome   = "Atacadao TCG";
		$assunto   = "Contato MTCG Database";

		$template =  new TemplateController();

		if($mailer->enviarEmailAction($para,$nome,$assunto,$msg)){
			$template->redirectUrl("?task=Usuario&action=contatoPage","Email enviado com sucesso, entraremos em contato em breve!!!","success");
		}else{
			$template->redirectUrl("?task=Usuario&action=contatoPage","Nao foi possivel enviar o email!!!","danger");
		}
	}		

	public function registerAction(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();

		$dados['nascimento']     = $Tvar->dateEn($dados['nascimento']);
		$dados['data_cadastro']  = date('Y-m-d');
		$dados['hora_cadastro']  = date('H:i:s');
		$dados['qtd_login']      = 0;
		$dados['status']         = 1;
		$dados['tipo_usuario']   = 0;
		$dados['tipo_conta']     = 0;
		$dados['senha']          = md5($dados['senha']);
		$Tvar->setSessionRegisterForm($dados);		

		$usuario = new Usuario($dados);
		$array   = get_object_vars($usuario);
		unset($array['id']);
		unset($array['database']);

		$usuario->insert($array);
		$template =  new TemplateController();

		if($usuario->insert($array)){
			$template->redirectTemplate('Usuario','loginPage',"Usuario Cadastrado com sucesso!!!","success");		
		}else{			
			$error  = $usuario->database->error();
			$alert  = $Tvar->createAlert("Nao foi possivel concluir o cadastro no bando de dados: ".$error[2],"danger");
			$template->renderTemplate('Usuario','register',$alert);		
		}
	}	

	public function authenticationAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();

		$usuario = new Usuario();
		$res = $usuario->validaUsuario($Tvar->trocarcaracter($dados['login']),md5($dados['senha']));
		
		$template =  new TemplateController();

		if(isset($res)){
			if($res['login'] == $dados['login'] && $res['senha'] == md5($dados['senha'])){

				$usuario = new Usuario($res);
				$d['ultimo_login']      = date('Y-m-d');
				$d['hora_ultimo_login'] = date('H:i:s');
				$d['qtd_login[+]']      = 1;
				$usuario->update($usuario->id,$d);
				$Tvar->createSession('login',$res);
				$template->redirectUrl("?task=Carta&action=buscadorPage");
			}else{
				$template->redirectTemplate('Usuario','loginPage',"Usuario ou senha nao conferem!!! ","danger");	
			}
		}else{
			$template->redirectTemplate('Usuario','loginPage',"Usuario ou senha nao conferem!!! ","danger");
		}	
	}


	public function logoutAction(){
		
		$Tvar    = new TratamentoVar();
		$Tvar->destroySession('login');		

		$template =  new TemplateController();
		$template->redirectUrl("?task=Carta&action=buscadorPage");
	}	

	public function updatePerfilAction(){
		
		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();
		$Login = $Tvar->getSession('login');

		if($dados['sexo'] != ""){
			switch ($dados['sexo']) {
				case 'masculino':
					$d['sexo']      =  $dados['sexo'];	
					break;
				case 'feminino':
					$d['sexo']      =  $dados['sexo'];	
					break;					
			}
		}

		if($dados['fone'] != "")
			$d['fone']      =  $dados['fone'];
		if($dados['estado'] != "")
			$d['estado']    =  $dados['estado'];
		if($dados['cidade'] != "")
			$d['cidade']    =  $dados['cidade'];
		if($dados['rua'] != "")
			$d['rua']       =  $dados['rua'];
		if($dados['complemento'] != "")
			$d['complemento']    =  $dados['complemento'];
		if($dados['bairro'] != "")
			$d['bairro']         =  $dados['bairro'];
		if($dados['cep'] != "")
			$d['cep']            =  $dados['cep'];
		
		$usuario = new Usuario($Login);
		$usuario->update($usuario->id,$d);

		$filtro['id'] = $usuario->id;
		$usuario->select($filtro);
		$res  = (array) $usuario;
		unset($res['database']);
		$Tvar->createSession('login',$res);

		$template =  new TemplateController();
		$template->redirectUrl("?task=Usuario&action=perfilPage&id=".$Login['id'],"Perfil Atualizado com secesso!!!","success");					

	}		

	public function senhaAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();
		$perfil  = $Tvar->getSession('login');

		$f['id']    = $perfil['id'];
		$f['senha'] = md5($dados['senha_atual']);
		$filtro['AND']   = $f;

		$usuario = new Usuario();
		$r = (array) $usuario->select($filtro);

		$template =  new TemplateController();		

		if(count($r) == 1){
			$d['senha'] = md5($dados['senha']);
			$usuario->update($usuario->id,$d);
			$template->redirectTemplate('Usuario','senhaPage',"Senha Atualizada com secesso!!! ","success");
		}else{
			$alert  = $Tvar->createAlert("A senha digitada nao confere com a senha atual!","danger");
			$template->renderTemplate('Usuario','senha',$alert,true);					
		}

	}


		function recuperarSenhaAction(){

			$Tvar    = new TratamentoVar();
			$dados   = $Tvar->PostReq();	

			$usuario = new Usuario();
			$res     = $usuario->select(array("email" => $dados['email']));

			if(count($res) > 0){

				//$recuperar = new RecuperarSenha();
				//$recuperar->delete(array("login" => $res[0]['login']));

				$key = md5($res[0]['login'].date('d-m-Y H:i:s'));
				$key = strtoupper(substr($key,0,10));
				$usuario->update($res[0]['id'],array("senha" => md5($key)));
				//$recuperar->insert(array("login" => $res[0]['login'],"key" => $key));


				$mailer = new MailerController();
				
				
				$msg = "Prezado ".$res[0]['nome'].",<br><br>
				
				Foi solicitada uma nova senha para a sua conta:<br><br>
				
				Seu login: ".$res[0]['login']."<br><br>
				Nova Senha: $key <br><br>
				
				Ao fazer o proximo login em nosso site favor trocar a senha em Mudar Senha
				
				Atenciosamente,
				MTCG Database
				";					

				$template =  new TemplateController();
				
				if($mailer->enviarEmailAction($res[0]['email'],$res[0]['nome'],"MTCG Database - Recuperacao de Senha",$msg)){				
					$template->redirectUrl("?task=Usuario&action=recuperarSenhaPage","Uma nova senha foi enviado para o seu email!","success");
				}else{
					$template->redirectUrl("?task=Usuario&action=recuperarSenhaPage","Erro ao enviar nova senha para o seu email!","danger");
				}

			}	

		}

		public function fotoPerfilAction(){

			$Tvar    = new TratamentoVar();
			$dados   = $Tvar->PostReq();	
			$login   = $Tvar->getSession('login');

			$foto    = $_FILES['img'];

			$ponto = explode(".", $foto['name']);
			$ext = $ponto[count($ponto)-1];

			if($foto['type'] == "image/png" || $foto['type'] == "image/jpeg"){
				
				include("libs/wide/lib/WideImage.php");
				$img = WideImage::load($foto['tmp_name']);
				$img->resize(200,200)->saveToFile("upload/usuario/".$login['id'].".".$ext);
			}

			$usuario = new Usuario();
			$usuario->update($login['id'],array("img" => $ext));

			$template =  new TemplateController();
			$template->redirectUrl("?task=Usuario&action=fotoPerfilPage","Sua Foto foi enviada com sucesso!","success");
		}

		public function getListReputacaoAction($res,$user){

				$dados['id'] = $user;
				$usuario = new Usuario();
				$m = count($res);
				for ($i=0; $i < $m; $i++) { 

					//Buscar Negociações do tipo Estande
					if($res[$i]['tipo_negociacao'] == 1){
					$negociacao = new Negociacao();
					$neg = $negociacao->select(array("id" => $res[$i]['negociacao_id']));	
						//Buscar Negociações do tipo Estande e como comprador
						if($res[$i]['atuacao'] == 1){
							$estande = new Estande();
							$std     = $estande->select(array("id" => $neg[0]['estande_id']));
							$use2    = $usuario->select(array("id" => $std[0]['usuario_id']));
							$res[$i]['negociado']    = $use2[0]['login'];
							$res[$i]['negociado-id'] = $use2[0]['id'];
							$res[$i]['atuacao-label']      = "Comprador";
							$res[$i]['data']         = $neg[0]['data'];
						}
						//Buscar Negociações do tipo Estande e como vendedor
						if($res[$i]['atuacao'] == 2){
							$use2    = $usuario->select(array("id" => $neg[0]['usuario_id']));
							$res[$i]['negociado']    = $use2[0]['login'];
							$res[$i]['negociado-id'] = $use2[0]['id'];
							$res[$i]['atuacao-label']      = "Vendedor";
							$res[$i]['data']         = $neg[0]['data'];
						}				
						$res[$i]['tipo_negociacao'] = "Estande";
					}

					//Buscar Negociações do tipo Leilão
					if($res[$i]['tipo_negociacao'] == 2){
					$negociacao = new NegociacaoLeilao();
					$neg = $negociacao->select(array("id" => $res[$i]['negociacao_id']));

					//Buscar Negociações do tipo Estande e como comprador
						if($res[$i]['atuacao'] == 1){
							$leilao = new Leilao();
							$std     = $leilao->select(array("id" => $neg[0]['leilao_id']));
							$use2    = $usuario->select(array("id" => $std[0]['usuario_id']));
							$res[$i]['negociado']    = $use2[0]['login'];
							$res[$i]['negociado-id'] = $use2[0]['id'];
							$res[$i]['atuacao-label']      = "Comprador";
							$res[$i]['data']         = $neg[0]['data'];

						}
						//Buscar Negociações do tipo Estande e como vendedor
						if($res[$i]['atuacao'] == 2){
							$leilao = new Leilao();
							$std     = $leilao->select(array("id" => $neg[0]['leilao_id']));
							$use2    = $usuario->select(array("id" => $std[0]['usuario_id']));
							$res[$i]['negociado']    = $use2[0]['login'];
							$res[$i]['negociado-id'] = $use2[0]['id'];
							$res[$i]['atuacao-label']      = "Vendedor";
							$res[$i]['data']         = $neg[0]['data'];
						}				
						$res[$i]['tipo_negociacao'] = "Leilão";
					}			
		}	
		return $res;		
		}


		public function getTipoUsuarioAction($n){
			switch ($n) {
				case 0:
					return "Comum";
					break;
				case 1:
					return "Super Administrador";
					break;					
				case 2:
					return "Administrador";
					break;	
				case 10:
					return "Cancelado";
					break;														
			}
		}

		public function mudarTipoUsuarioAction(){

			$Tvar    = new TratamentoVar();
			$dados   = $Tvar->GetReq();	
			$login   = $Tvar->getSession('login');	

			$usuario  = new Usuario();
			$usuario->update($dados['usuario'],array("tipo_usuario" => $dados['tipo']));

			$template =  new TemplateController();
			$template->redirectUrl("?task=Usuario&action=perfilPage&id=".$dados['usuario'],"Tipo de usuario mudado com sucesso!","success");			
		}
}
