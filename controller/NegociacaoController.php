<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/EstandeController.php';
require_once 'controller/MensagemController.php';
require_once 'model/Estande.class.php';
require_once 'model/Leilao.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Negociacao.class.php';
require_once 'model/NegociacaoLeilao.class.php';
require_once 'model/ChatNegociacao.class.php';
require_once 'model/ChatNegociacaoLeilao.class.php';
require_once 'model/Lance.class.php';
require_once 'model/Reputacao.class.php';


class NegociacaoController {

	var $deSys = 24;

// PAGES

public function abrirNegociacaoPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		$estande = new Estande();
		$res     = $estande->select(array("id" => $dados['estande']));

		$usuario = new Usuario();
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));

		global $Result;

		$Result['estande'] = $res[0];
		$Result['dono']    = $r[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','abrirNegociacao',$alert,true);

}

public function abrirNegociacaoLeilaoPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		$leilao = new Leilao();
		$res     = $leilao->select(array("id" => $dados['leilao']));

		$usuario = new Usuario();
		$r       = $usuario->select(array("id" => $res[0]['usuario_id']));

		global $Result;

		$Result['leilao'] = $res[0];
		$Result['dono']    = $r[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','abrirNegociacaoLeilao',$alert,true);

}

public function abrirNegociacaoLeilaoDonoPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		$leilao = new Leilao();
		$res     = $leilao->select(array("id" => $dados['leilao']));

		$usuario = new Usuario();
		$r       = $usuario->select(array("id" => $res[0]['ganhador']));

		global $Result;

		$Result['leilao'] = $res[0];
		$Result['ganhador']    = $r[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','abrirNegociacaoLeilaoDono',$alert,true);

}

public function propostasEnviadasPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$search     = $Tvar->PostReq();
		$login = $Tvar->getSession('login');		

		global $Result;		

		//Paginação
		$lista = $Tvar->getSession('lista');

			if(!isset($dados['pagina'])){

				$negociacao = new Negociacao();

				if($search['codigo'] != "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("AND" => array("usuario_id" => $login['id'],"status" => $search['situacao'],"id" => $search['codigo']),"ORDER" => "id DESC"));
				}

				if($search['codigo'] != "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("AND" => array("usuario_id" => $login['id'],"id" => $search['codigo']),"ORDER" => "id DESC"));
				}				

				if($search['codigo'] == "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("AND" => array("usuario_id" => $login['id'],"status" => $search['situacao']),"ORDER" => "id DESC"));
				}	
				
				if($search['codigo'] == "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("usuario_id" => $login['id'],"ORDER" => "id DESC"));
				}	

				//$res        = $negociacao->select(array("usuario_id" => $login['id'],"ORDER" => "id DESC"));
				
				$estande    = new Estande();	
				$usuario    = new Usuario();
				$n = count($res);
				for ($i=0; $i < $n; $i++) { 
					$r          = $estande->select(array("id" => $res[$i]['estande_id']));
					$res[$i]['estande']     =  $r[0];
					$r          = $usuario->select(array("id" => $r[0]['usuario_id']));
					$res[$i]['dono']     =  $r[0];
					$res[$i]['status']   = $this->getSituacaoAction($res[$i]['status']);
				}

				$Result['negociacoes'] = $res;

				$Tvar->createSession('lista',$Result['negociacoes']);
				$total = count($Result['negociacoes']);
			}else{
				$total = count($lista);
				$Result['negociacoes'] = $lista;			
			}

			$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
			$paginacao    = new PaginacaoController($pagina,10,$total);		
			$Result['paginacao'] = $paginacao;		
		//Paginação	

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','propostasEnviadas',$alert,true);
}


public function propostasRecebidasPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$search     = $Tvar->PostReq();

		$estandeController = new EstandeController();
		$std               = $estandeController->getEstandeId();

		global $Result;

		//Paginação
		$lista = $Tvar->getSession('lista');

			if(!isset($dados['pagina'])){

				$negociacao = new Negociacao();

				if($search['codigo'] != "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("AND" => array("estande_id" => $std,"status" => $search['situacao'],"id" => $search['codigo']),"ORDER" => "id DESC"));
				}

				if($search['codigo'] != "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("AND" => array("estande_id" => $std,"id" => $search['codigo']),"ORDER" => "id DESC"));
				}				

				if($search['codigo'] == "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("AND" => array("estande_id" => $std,"status" => $search['situacao']),"ORDER" => "id DESC"));
				}	
				
				if($search['codigo'] == "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("estande_id" => $std,"ORDER" => "id DESC"));
				}	


				
				$estande    = new Estande();	
				$usuario    = new Usuario();
				$n = count($res);
				for ($i=0; $i < $n; $i++) { 
					$r          = $estande->select(array("id" => $res[$i]['estande_id']));
					$res[$i]['estande']     =  $r[0];
					$r          = $usuario->select(array("id" => $r[0]['usuario_id']));
					$res[$i]['dono']     =  $r[0];
					$r          = $usuario->select(array("id" => $res[$i]['usuario_id']));
					$res[$i]['solicitante']     =  $r[0];			
					$res[$i]['status']   = $this->getSituacaoAction($res[$i]['status']);
					$aux = explode(" ",$res[$i]['proposta']);
					$res[$i]['proposta'] = $aux[0]." ".$aux[1]." ".$aux[2]." ".$aux[3]."... ";
				}

				$Result['negociacoes'] = $res;

				$Tvar->createSession('lista',$Result['negociacoes']);
				$total = count($Result['negociacoes']);
			}else{
				$total = count($lista);
				$Result['negociacoes'] = $lista;			
			}

			$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
			$paginacao    = new PaginacaoController($pagina,10,$total);		
			$Result['paginacao'] = $paginacao;		
		//Paginação		

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','propostasRecebidas',$alert,true);
}


public function verNegociacaoPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			
	

		$negociacao = new Negociacao();
		$res        = $negociacao->select(array("id" => $dados['negociacao']));
		$comprador  = $res[0]['usuario_id'];

		$estande    = new Estande();
		$r          = $estande->select(array("id" => $res[0]['estande_id']));
		$res[0]['estande']     =  $r[0];
		$dono       = $r[0]['usuario_id'];

		//-------------------------
		if($login['id'] != $dono && $login['id'] != $comprador && !$Tvar->verificarAdmin()){
			$template =  new TemplateController();		
			$template->renderTemplate('Negociacao','404',$alert,true);
		}
		//-------------------------		

		$usuario    = new Usuario();
		$r          = $usuario->select(array("id" => $r[0]['usuario_id']));
		$res[0]['dono']     =  $r[0];

		$r          = $usuario->select(array("id" => $res[0]['usuario_id']));
		$res[0]['solicitante']     =  $r[0];		

		$res[0]['status-id'] = $res[0]['status'];
		$res[0]['status']   = $this->getSituacaoAction($res[0]['status']);	

		global $Result;
		//Verificar se é o dono da estande	

		$estandeController = new EstandeController();
		$std               = $estandeController->getEstandeId();
		$aux               = $negociacao->select(array("AND" => array("estande_id" => $std,"id" => $dados['negociacao'])));
		$Result['proprietario'] = $aux[0];

		$Result['negociacao'] = $res;
		$Result['solicitante']       = $res[0]['solicitante'];

		//Buscar Mensagens

		$chatNegociacao = new ChatNegociacao();
		$res            = $chatNegociacao->select(array("negociacao_id" => $dados['negociacao']));
		$n = count($res);
		for ($i=0; $i < $n; $i++) { 
			$r = $usuario->select(array("id" => $res[$i]['usuario_id']));
			$res[$i]['usuario_login'] = $r[0]['login'];
			$res[$i]['usuario_id'] = $r[0]['id'];
			$res[$i]['usuario_img'] = $r[0]['img'];
			
			if(is_file("upload/negociacoes/".$dados['negociacao']."/".$res[$i]['id'].".".$res[$i]['img'])){
				$res[$i]['anexo']      = 1;
			}
		}
		
		$Result['mensagens'] = $res;
		$Result['login'] = $login;

		//Pegar Reputação
		if($Result['negociacao'][0]['status-id'] >= 3){

			$reputacao = new Reputacao();

			//Comprador			
			$res       = $reputacao->select(array("AND" => array("negociacao_id" => $dados['negociacao'],"atuacao" => 1)));

			if($res[0]['tipo_reputacao'] == 1)
				$txt = "<span class='label label-success'>Positiva</span>";
			else if($res[0]['tipo_reputacao'] == 2)
				$txt = "<span class='label label-danger'>Negativa</span>";

			$Result['rep-comprador'] = $txt;

			//Vendedor
			$res       = $reputacao->select(array("AND" => array("negociacao_id" => $dados['negociacao'],"atuacao" => 2)));

			if($res[0]['tipo_reputacao'] == 1)
				$txt = "<span class='label label-success'>Positiva</span>";
			else if($res[0]['tipo_reputacao'] == 2)
				$txt = "<span class='label label-danger'>Negativa</span>";	

			$Result['rep-vendedor'] = $txt;		
		}

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','verNegociacao',$alert,true);

}


public function verNegociacaoLeilaoPage(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');

		global $Result;

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);				

		$leilao  = new Leilao();
		$res     = $leilao->select(array("id" => $dados['leilao']));

		$Result['leilao'] = $res[0];
		$dono       =  $res[0]['usuario_id'];

		$negociacaoLeilao = new NegociacaoLeilao();
		$res        = $negociacaoLeilao->select(array("leilao_id" => $Result['leilao']['id']));

		$comprador  =  $res[0]['usuario_id'];		

		//---------------------------
		
		if($login['id'] != $dono && $login['id'] != $comprador && !$Tvar->verificarAdmin()){
				$template =  new TemplateController();		
				$template->renderTemplate('Negociacao','404',$alert,true);
		}
		
		//---------------------------			

		$res[0]['status-id'] = $res[0]['status'];
		$res[0]['status']    = $this->getSituacaoAction($res[0]['status']);

		$Result['negociacaoLeilao'] = $res[0];	

		$usuario    = new Usuario();
		$res        = $usuario->select(array("id" => $Result['leilao']['usuario_id']));

		$Result['vendedor'] = $res[0];	

		$usuario    = new Usuario();
		$res        = $usuario->select(array("id" => $Result['leilao']['ganhador']));

		$Result['ganhador'] = $res[0];			

		$lance    = new Lance();
		$Result['meulance']    = $lance->max($Result['leilao']['id']);
		$Result['lance']       = $Result['meulance'][0]['lance'];
	
		$Result['login']       = $login;

		//Buscar Mensagens

		$chatNegociacaoLeilao = new ChatNegociacaoLeilao();
		$res            = $chatNegociacaoLeilao->select(array("negociacao_leilao_id" => $Result['negociacaoLeilao']['id']));

		$n = count($res);
		for ($i=0; $i < $n; $i++) { 
			$r = $usuario->select(array("id" => $res[$i]['usuario_id']));
			$res[$i]['usuario_login'] = $r[0]['login'];
			$res[$i]['usuario_id'] = $r[0]['id'];
			$res[$i]['usuario_img'] = $r[0]['img'];
			
			if(is_file("upload/negociacoes/".$dados['negociacao']."/".$res[$i]['id'].".".$res[$i]['img'])){
				$res[$i]['anexo']      = 1;
			}
		}
		$Result['mensagens'] = $res;

		//Pegar Reputação

		if($Result['negociacaoLeilao']['status-id'] >= 3){

			$reputacao = new Reputacao();

			//Comprador			
			$res       = $reputacao->select(array("AND" => array("negociacao_id" => $Result['negociacaoLeilao']['id'],"atuacao" => 1)));

			if($res[0]['tipo_reputacao'] == 1)
				$txt = "<span class='label label-success'>Positiva</span>";
			else if($res[0]['tipo_reputacao'] == 2)
				$txt = "<span class='label label-danger'>Negativa</span>";

			$Result['rep-comprador'] = $txt;

			//Vendedor
			$res       = $reputacao->select(array("AND" => array("negociacao_id" => $Result['negociacaoLeilao']['id'],"atuacao" => 2)));

			if($res[0]['tipo_reputacao'] == 1)
				$txt = "<span class='label label-success'>Positiva</span>";
			else if($res[0]['tipo_reputacao'] == 2)
				$txt = "<span class='label label-danger'>Negativa</span>";	

			$Result['rep-vendedor'] = $txt;		
		}		

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','verNegociacaoLeilao',$alert,true);

}


public function negociacoesAdminPage(){

		$Tvar       = new TratamentoVar();
		$dados      = $Tvar->GetReq();
		$search     = $Tvar->PostReq();
		$login = $Tvar->getSession('login');		

		global $Result;

		if(!$Tvar->verificarAdmin()){
				$template =  new TemplateController();
				$template->redirectUrl("?task=Usuario&action=LoginPage","Acesso Negado!!!","danger");			
		}

//
			$lista = $Tvar->getSession('lista');

			if(!isset($dados['pagina'])){

				$negociacao = new Negociacao();

				if($search['codigo'] != "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("AND" => array("status" => $search['situacao'],"id" => $search['codigo']),"ORDER" => "id DESC"));
				}

				if($search['codigo'] != "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("id" => $search['codigo'],"ORDER" => "id DESC"));
				}				

				if($search['codigo'] == "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("status" => $search['situacao'],"ORDER" => "id DESC"));
				}	
				
				if($search['codigo'] == "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("ORDER" => "id DESC"));
				}					
				
				$estande    = new Estande();	
				$usuario    = new Usuario();
				$n = count($res);
				for ($i=0; $i < $n; $i++) { 
					$r          = $usuario->select(array("id" => $res[$i]['usuario_id']));
					$res[$i]['comprador']     =  $r[0];
					
					$r          = $estande->select(array("id" => $res[$i]['estande_id']));
					$res[$i]['estande']     =  $r[0];
					$r          = $usuario->select(array("id" => $r[0]['usuario_id']));
					$res[$i]['dono']     =  $r[0];
					$res[$i]['status']   = $this->getSituacaoAction($res[$i]['status']);
				}

				$Result['negociacoes'] = $res;

				$Tvar->createSession('lista',$Result['negociacoes']);
				$total = count($Result['negociacoes']);
			}else{
				$total = count($lista);
				$Result['negociacoes'] = $lista;			
			}

			$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
			$paginacao    = new PaginacaoController($pagina,10,$total);		
			$Result['paginacao'] = $paginacao;
//		

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','negociacoesAdmin',$alert,true);

}


public function negociacoesLeilaoAdminPage(){

		$Tvar       = new TratamentoVar();
		$dados      = $Tvar->GetReq();
		$search     = $Tvar->PostReq();
		$login = $Tvar->getSession('login');		

		global $Result;

		if(!$Tvar->verificarAdmin()){
				$template =  new TemplateController();
				$template->redirectUrl("?task=Usuario&action=LoginPage","Acesso Negado!!!","danger");			
		}

//
			$lista = $Tvar->getSession('lista');

			if(!isset($dados['pagina'])){

				$negociacao = new NegociacaoLeilao();

				if($search['codigo'] != "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("AND" => array("status" => $search['situacao'],"id" => $search['codigo']),"ORDER" => "id DESC"));
				}

				if($search['codigo'] != "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("id" => $search['codigo'],"ORDER" => "id DESC"));
				}				

				if($search['codigo'] == "" && $search['situacao'] != ""){
					$res        = $negociacao->select(array("status" => $search['situacao'],"ORDER" => "id DESC"));
				}	
				
				if($search['codigo'] == "" && $search['situacao'] == ""){
					$res        = $negociacao->select(array("ORDER" => "id DESC"));
				}					
				
				$leilao    = new Leilao();	
				$usuario    = new Usuario();
				$n = count($res);
				for ($i=0; $i < $n; $i++) { 
					$r          = $leilao->select(array("id" => $res[$i]['leilao_id']));
					$a          = $usuario->select(array("id" => $r[0]['ganhador']));
					$res[$i]['ganhador']     =  $a[0];
					$res[$i]['estande']     =  $r[0];
					$r          = $usuario->select(array("id" => $r[0]['usuario_id']));
					$res[$i]['dono']     =  $r[0];
					$res[$i]['status']   = $this->getSituacaoAction($res[$i]['status']);
				}

				$Result['negociacoes'] = $res;

				$Tvar->createSession('lista',$Result['negociacoes']);
				$total = count($Result['negociacoes']);
			}else{
				$total = count($lista);
				$Result['negociacoes'] = $lista;			
			}

			$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
			$paginacao    = new PaginacaoController($pagina,10,$total);		
			$Result['paginacao'] = $paginacao;
//		

		$template       =  new TemplateController();
		$template->renderTemplate('Negociacao','negociacoesLeilaoAdmin',$alert,true);

}

// ACTIONS


public function abrirNegociacaoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();

		$login = $Tvar->getSession('login');		

		$negociacao = new Negociacao();

		$res   =  $negociacao->insert(array("usuario_id" => $login['id'],"estande_id" => $dados['estande'],"proposta" => $dados['proposta'], "status" => 1,"data" => date('Y-m-d'),"hora" => date('H:i:s')));

		$estande = new Estande();
		$r     = $estande->select(array("id" => $dados['estande']));

		$d['de']                 =   $this->deSys;
		$d['para']               =   $r[0]['usuario_id'];
		$d['assunto']            =   "Solicitação de Negociação";
		$d['mensagem']           =   "
		Ola,<br>Uma nova proposta de negociação enviada por ".$login['login']." 
		<a href='?task=Negociacao&action=verNegociacaoPage&negociacao=".$res."'>clique aqui</a> para visualizar.
		";

		$mensagem = new MensagemController();
		$mensagem->enviarMensagemSistemaAction($d);		

		mkdir("upload/negociacoes/".$res);
		
		$template =  new TemplateController();		
		$template->redirectUrl("?task=Estande&action=estandePage&estande=".$dados['estande'],"Proposta de negociação enviada com sucesso!!!","success");
}



public function abrirNegociacaoLeilaoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();

		$login = $Tvar->getSession('login');		

		$negociacaoLeilao = new NegociacaoLeilao();

		$res   =  $negociacaoLeilao->insert(array("usuario_id" => $login['id'],"leilao_id" => $dados['leilao'],"mensagem" => $dados['mensagem'], "status" => 1,"data" => date('Y-m-d'),"hora" => date('H:i:s')));

		mkdir("upload/negociacoes/leilao/".$res);
		
		$template =  new TemplateController();		
		$template->redirectUrl("?task=Leilao&action=leilaoPage&leilao=".$dados['leilao'],"Negociação do leilão enviada com sucesso!!!","success");
}


public function abrirNegociacaoLeilaoDonoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->PostReq();

		$login = $Tvar->getSession('login');		

		$leilao  = new Leilao();
		$r       = $leilao->select(array("id" => $dados['leilao']));

		$negociacaoLeilao = new NegociacaoLeilao();

		$res   =  $negociacaoLeilao->insert(array("usuario_id" => $r[0]['ganhador'],"leilao_id" => $dados['leilao'],"mensagem" => $dados['mensagem'], "status" => 2,"data" => date('Y-m-d'),"hora" => date('H:i:s')));

		mkdir("upload/negociacoes/leilao/".$res);
		
		$template =  new TemplateController();		
		$template->redirectUrl("?task=Leilao&action=meusLeiloesPage","Negociação do leilão enviada com sucesso!!!","success");
}


public function mudarSituacaoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login     = $Tvar->getSession('login');	

		$estandeController = new EstandeController();
		$std               = $estandeController->getEstandeId();

		$negociacao = new Negociacao();
		
		if(!$Tvar->verificarAdmin())
			$res        = $negociacao->select(array('AND' => array("estande_id" => $std, "id" => $dados['negociacao'])));
		else
			$res = 1;

		$template =  new TemplateController();		
		if(count($res) > 0){

			//Enviar Mensagem de notificação
			$negociacao = new Negociacao();
			$x          = $negociacao->select(array("id" => $dados['negociacao']));
			$estande = new Estande();
			$r     = $estande->select(array("id" => $x[0]['estande_id']));

			if($login['id'] == $r[0]['usuario_id'])
				$d['para']               =   $x[0]['usuario_id'];
			else
				$d['para']               =   $r[0]['usuario_id'];

			$d['de']                 =   $this->deSys;
			$d['assunto']            =   "Negociação  #".$dados['negociacao']." mudou de situação";
			$d['mensagem']           =   "
			Ola,<br>A situação da negociação #".$dados['negociacao']." foi alterada para ".$this->getSituacaoAction($dados['situacao']).
			" <a href='?task=Negociacao&action=verNegociacaoPage&negociacao=".$dados['negociacao']."'>clique aqui</a> para visualizar.
			";		
			if($Tvar->verificarAdmin())
				$d['mensagem'] .= "</br></br>**Alterado Pela Administração**"; 

			$mensagem = new MensagemController();
			$mensagem->enviarMensagemSistemaAction($d);


			$negociacao->update($dados['negociacao'],array("status" => $dados['situacao']));
			
			$template->redirectUrl("?task=Negociacao&action=verNegociacaoPage&negociacao=".$dados['negociacao'],"Negociação atualizada com secesso!!!","success");
		}else{
			$template->redirectUrl("?task=Negociacao&action=verNegociacaoPage&negociacao=".$dados['negociacao'],"Erro ao atualizar a negociacão!!!","danger");
		}
}

public function mudarSituacaoLeilaoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();

		$login = $Tvar->getSession('login');		

		$leilao    = new Leilao();
		$res       = $leilao->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['negociacao'])));

		$template =  new TemplateController();	

		if($Tvar->verificarAdmin()){
			$res       = $leilao->select(array("id" => $dados['negociacao']));
		}

		$negociacaoLeilao = new NegociacaoLeilao();
		$res        = $negociacaoLeilao->select(array("leilao_id" => $res[0]['id']));		

		if(count($res) > 0){

			$leilao = new Leilao();
			$r     = $leilao->select(array("id" => $dados['negociacao']));
			
			if($login['id'] == $r[0]['usuario_id'])
				$d['para']               =   $r[0]['ganhador'];
			else
				$d['para']               =   $r[0]['usuario_id'];

			$d['de']                 =   $this->deSys;
			$d['assunto']            =   "Negociação do Leilão #".$dados['negociacao']." mudou de situação";
			$d['mensagem']           =   "
			Ola,<br>A situação da negociação do leilão #".$dados['negociacao']." foi alterada para ".$this->getSituacaoAction($dados['situacao']).
			" <a href='?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$res[0]['leilao_id']."'>clique aqui</a> para visualizar.
			";		
			if($Tvar->verificarAdmin())
				$d['mensagem'] .= "</br></br>**Alterado Pela Administração**"; 

			$mensagem = new MensagemController();
			$mensagem->enviarMensagemSistemaAction($d);


			$negociacaoLeilao->update($res[0]['id'],array("status" => $dados['situacao']));
			$template->redirectUrl("?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$dados['negociacao'],"Negociação atualizada com secesso!!!","success");
		}else{
			$template->redirectUrl("?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$dados['negociacao'],"Erro ao atualizar a negociacão!!!","danger");
		}
}

public function getSituacaoAction($status){

	switch ($status) {
		case 1:
			$status = "<span class='label label-warning'>Pendente</span>";
			break;
		case 2:
			$status = "<span class='label label-info'>Ativo</span>";
			break;			
		case 3:
			$status = "<span class='label label-success'>Concluido</span>";
			break;					
		case 4:
			$status = "<span class='label label-default'>Cancelado</span>";
			break;	
		case 10:
			$status = "<span class='label label-default'>Finalizado pela Administração</span>";
			break;				
	}
	return $status;
}

public function addReputacaoAction(){
		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$login = $Tvar->getSession('login');	

		if(!$Tvar->verificarAdmin()){
			exit();
		}

		// 0 -> ID usuario, 1 -> ID Negociacao, 2 -> Tipo de Negociacao (Estande, Leilao), 3 -> Tipo de Reputacao (Negativa, Positiva)
		// 4 -> Atuacao (Comprador, Vendedor)
		$aux       = explode(",", $dados['reputacao']); 
		
		$d['admin_id'] = $login['id'];
		$d['usuario_id'] = $aux[0];
		$d['negociacao_id'] = $aux[1];
		$d['tipo_negociacao'] = $aux[2];
		$d['tipo_reputacao'] = $aux[3];
		$d['atuacao'] = $aux[4];
		$leilao['leilao_id'] = $aux[5];

		$reputacao = new Reputacao();

		$res = $reputacao->select(array("AND" => array("usuario_id" => $d['usuario_id'],"tipo_negociacao" => $d['tipo_negociacao'],"negociacao_id" => $d['negociacao_id'])));

		if(count($res) == 0){

			if($d['tipo_reputacao'] == 1)
				$txt = "<span class='label label-success'>Positiva</span>";
			else if($d['tipo_reputacao'] == 2)
				$txt = "<span class='label label-danger'>Negativa</span>";			
			
			$dd['de']                 =   $this->deSys;
			$dd['para']               =   $d['usuario_id'];
			$dd['assunto']            =   "Você recebeu uma Reputação";
			$dd['mensagem']           =   "
			Ola,<br> Você recebeu uma nova reputação do tipo: $txt
			";

			$mensagem = new MensagemController();
			$mensagem->enviarMensagemSistemaAction($dd);

			$res = $reputacao->insert($d);
		}else{

			if($d['tipo_reputacao'] == 0)
				$res = $reputacao->delete(array("id" => $res[0]['id']));
			else
				$res = $reputacao->update($res[0]['id'],$d);
		}

		$template =  new TemplateController();	

		if($d['tipo_negociacao'] == 1)
			$template->redirectUrl("?task=Negociacao&action=verNegociacaoPage&negociacao=".$d['negociacao_id'],"Reputação adicionada com com secesso!!!","success");			
		elseif($d['tipo_negociacao'] == 2)
			$template->redirectUrl("?task=Negociacao&action=verNegociacaoLeilaoPage&leilao=".$leilao['leilao_id'],"Reputação adicionada com com secesso!!!","success");			

}

public function baixarAnexoAction(){

		$Tvar      = new TratamentoVar();
		$dados     = $Tvar->GetReq();
		$chatNegociacao      = new ChatNegociacao();
		$res = $chatNegociacao->select(array("id" => $dados['msg']) );
		
		$file_name = "upload/negociacoes/".$res[0]['negociacao_id']."/".$res[0]['id'].".".$res[0]['img'];

		// make sure it's a file before doing anything!
		if(is_file($file_name)) {

			// get the file mime type using the file extension
			switch($res[0]['img']) {
				case 'pdf': 
					$mime = 'application/pdf'; 
				break;
				case "png": 
					$mime = 'image/png'; 
				break;
				case 'jpeg': 
					$mime = 'image/jpg'; 
				break;
				case 'jpg': 
					$mime = 'image/jpg';
				break;
			}

			header('Pragma: public'); 	// required
			header('Expires: 0');		// no cache
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file_name)).' GMT');
			header('Cache-Control: private',false);
			header('Content-Type: '.$mime);
			header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '.filesize($file_name));	// provide file size
			header('Connection: close');
			readfile($file_name);		// push it out
			exit();

		}

}

}
