<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/CartaController.php';
require_once 'controller/ComentController.php';
require_once 'model/TipoCarta.class.php';
require_once 'model/Alinhamento.class.php';
require_once 'model/Afiliacao.class.php';
require_once 'model/TipoAcao.class.php';
require_once 'model/Deck.class.php';
require_once 'model/DeckList.class.php';
require_once 'model/Usuario.class.php';

class DeckController {


// PAGES

public function construtorPage()
{

		$Tvar  = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados = $Tvar->GetReq();	
		$deck  = new Deck();	
		unset($_SESSION['mydeck']);

		//verificar se tem alguem logado

		if($login['nome'] == ""){
			echo "Acesso não permitido";
			exit;
		}

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		global $Result,$FORM;

		//Criar formulario Deck e deck se tiver algum salvo ou selecionado

		$FORM->nome       = $FORM->textCreate(array("nome" => $res[0]['nome']),array("placeholder" => "Nome do deck"));

		if(isset($dados['deck'])){

			$res = $deck->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['deck'])));
			if(count($res) == 0){
				echo "Acesso não permitido";
				exit;				
			}

			$list  = new DeckList();	
			$carta  = new CartaController();

			$res = $deck->select(array("AND" => array("id" => $dados['deck'],"usuario_id" => $login['id'])));

			$FORM->nome       = $FORM->textCreate(array("nome" => $res[0]['nome']),array("placeholder" => "Nome do deck"));
			$FORM->iddeck     = $FORM->hiddenCreate(array("deck" => $res[0]['id']));
			$r   = $list->select(array("deck_id" => $dados['deck']));

			$j=0;
			for ($i=0; $i < count($r); $i++) { 
				$c = $r[$i]['carta_id'];
				$a = $carta->buscarCartasByIdAction($c);
				$aux[$j]['tipo']        =  $a[0]['carta'];
				$aux[$j]['id']          =  $a[0]['id'];
				$aux[$j]['nome']        =  $a[0]['nome'];
				$aux[$j]['numero']      =  $a[0]['numero'];
				$aux[$j]['sigla']       =  $a[0]['sigla'];
				$aux[$j]['key']         =  $j;
				$j++;				

			}
			$_SESSION['mydeck'] = @$aux;
		}

		//Buscar lista de decks do usuario
		
		$res = $deck->select(array("usuario_id" => $login['id']));
		$array['lista_deck'][0] =  "Escolha um deck";
		foreach ($res as $key => $value) {
			$array['lista_deck'][$value['id']] =  $value['nome'];
		}
		$FORM->lista_deck = $FORM->selectCreate($array,array("id" => "lista_deck","onchange" => "carregarDeck()"));
		unset($array);


		//Criar Select Tipo de Carta
		$tipo = new TipoCarta();
		$res  = $tipo->select();
		$array['carta'][0] =  "Tipo de Carta";
		foreach ($res as $key => $value) {
			$array['carta'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo = $FORM->selectCreate($array,array("id" => "tipo_de_carta"));
		unset($array);

		//Criar Select Alinhamento
		$tipo = new Alinhamento();
		$res  = $tipo->select();
		$array['alinhamento'][0] =  "Alinhamento";
		foreach ($res as $key => $value) {
			if($value['nome'] != "NADA")
				$array['alinhamento'][$value['id']] =  $value['nome'];
		}
		$FORM->alinhamento = $FORM->selectCreate($array,array("id" => "alinhamento"));		
		unset($array);

		//Criar Select Afiliacao
		$tipo = new Afiliacao();
		$res  = $tipo->select();
		$array['afiliacao'][0] =  "Afiliacao";
		foreach ($res as $key => $value) {
			if($value['nome'] != "NADA")
				$array['afiliacao'][$value['id']] =  $value['nome'];
		}
		$FORM->afiliacao = $FORM->selectCreate($array,array("id" => "afiliacao","style" => "width:140px;"));		
		unset($array);	

		//Criar Select Acao
		$tipo = new TipoAcao();
		$res  = $tipo->select();
		$array['tipo_acao'][0] =  "Tipo de Acao";
		foreach ($res as $key => $value) {
			if($value['nome'] != "NADA")
				$array['tipo_acao'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo_acao = $FORM->selectCreate($array,array("id" => "tipo_acao","style" => "width:140px;"));		
		unset($array);				
			
		$template       =  new TemplateController();
		$template->renderTemplate('Deck','construtor',$alert,true);
}

public function listarDecksPage(){

		$Tvar  = new TratamentoVar();
		$deck  = new Deck();
		$usuario  = new Usuario();
		$dados = $Tvar->GetReq();
		$busca = $Tvar->PostReq();

		$busca['cartanome'] = $Tvar->trocarcaracter($busca['cartanome']);
		$busca['nome'] = $Tvar->trocarcaracter($busca['nome']);

		$lista = $Tvar->getSession('lista');
		
		global $Result,$FORM;

		

		if(!isset($dados['pagina'])){
			$Result = $deck->getListDeck(NULL,$busca);
			
			$Tvar->createSession('lista',$Result);
			$total = count($Result);
		}else{
			$total = count($lista);
			$Result = $lista;			
		}

		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,100,$total);		
		$Result['paginacao'] = $paginacao;	

		$res = $deck->getAutorDecks();
		$array['autor'][0] =  "Autor";
		foreach ($res as $key => $value) {
			$array['autor'][$value['id']] =  $value['nome'];
		}
		$FORM->autor = $FORM->selectCreate($array,array("id" => "autor","class" => "form-control"));		
		unset($array);

		$template       =  new TemplateController();
		$template->renderTemplate('Deck','listarDecks',$alert,true);

}

public function minhasDeckListPage(){

		$Tvar  = new TratamentoVar();
		$deck  = new Deck();
		$usuario  = new Usuario();
		$login = $Tvar->getSession('login');

		if($login['nome'] == ""){
			echo "Acesso não permitido";
			exit;
		}		
		
		global $Result;

		$Result = $deck->getListDeck($login['id']);

		$template       =  new TemplateController();
		$template->renderTemplate('Deck','minhasdecklist',$alert,true);

}

public function deckPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();	
		$usuario  = new Usuario();
		$login = $Tvar->getSession('login');

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		global $Result;

		$Result['login'] = $login;

		if(isset($dados['deck'])){

			$deck = new Deck();
			$list  = new DeckList();	
			$carta  = new CartaController();

			$deck->update($dados['deck'],array("views[+]" => 1));

			$res = $deck->select(array("id" => $dados['deck']));
			$r   = $list->select(array("deck_id" => $dados['deck']));

			$openhand = $deck->openHand($dados['deck']);

			$Result['deck']     = $res[0];
			$Result['openhand'] = $openhand;

			$aux = $usuario->select(array("id" => $Result['deck']['usuario_id']));
			$Result['deck']['usuario_nome'] = $aux[0]['nome'];			
			
			$j=0;
			for ($i=0; $i < count($r); $i++) { 
				$c = $r[$i]['carta_id'];
				$a = $carta->buscarCartasByIdAction($c);
				$aux[$j]['tipo']        =  $a[0]['carta'];
				$aux[$j]['id']          =  $a[0]['id'];
				$aux[$j]['nome']        =  $a[0]['nome'];
				$aux[$j]['numero']      =  $a[0]['numero'];
				$aux[$j]['sigla']       =  $a[0]['sigla'];
				$aux[$j]['key']         =  $j;
				$j++;				

			}

			$Result['mydeck'] = @$aux;
			$Result['txt']    = $deck->gerarTxt($Result['deck']['id']);
			$Result['t']    = $deck->gerarTxt($Result['deck']['id']);


		}		

		//Pegar Comentarios

		$coment = new ComentController();
		$res    = $coment->buscarComentsByDeckIdAction($dados['deck']);
		$Result['coment'] = $res;

		//Pegar Rating

		$res    = $coment->getRatingbyDeckAction($dados['deck'],$login['id']);
		$Result['rating_user'] = $res;
		$Result['rating_user'][0]['star'] = (isset($Result['rating_user'][0]['star'])) ? $Result['rating_user'][0]['star'] : 0;

		$res    = $coment->getSomaAction($Result['deck']['id']);
		$Result['rating_soma'] = $res[0]['soma'];
		$Result['rating_soma'] = (isset($Result['rating_soma'])) ? $Result['rating_soma'] : 0;

		$template       =  new TemplateController();
		$template->renderTemplate('Deck','deck',$alert,true);
}


public function mydeckPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();	
		$usuario  = new Usuario();
		$deck  = new Deck();
		$login = $Tvar->getSession('login');

		if($login['nome'] == ""){
			echo "Acesso não permitido";
			exit;
		}	

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		global $Result;

		$Result['login'] = $login;

		if(isset($dados['deck'])){

			$res = $deck->select(array("AND" => array("usuario_id" => $login['id'],"id" => $dados['deck'])));
			if(count($res) == 0){
				echo "Acesso não permitido";
				exit;				
			}			

			$deck = new Deck();
			$list  = new DeckList();	
			$carta  = new CartaController();

			$res = $deck->select(array("id" => $dados['deck']));
			$r   = $list->select(array("deck_id" => $dados['deck']));

			$openhand = $deck->openHand($dados['deck']);

			$Result['deck']     = $res[0];
			$Result['openhand'] = $openhand;

			$aux = $usuario->select(array("id" => $Result['deck']['usuario_id']));
			$Result['deck']['usuario_nome'] = $aux[0]['nome'];			
			
			$j=0;
			for ($i=0; $i < count($r); $i++) { 
				$c = $r[$i]['carta_id'];
				$a = $carta->buscarCartasByIdAction($c);
				$aux[$j]['tipo']        =  $a[0]['carta'];
				$aux[$j]['id']          =  $a[0]['id'];
				$aux[$j]['nome']        =  $a[0]['nome'];
				$aux[$j]['numero']      =  $a[0]['numero'];
				$aux[$j]['sigla']       =  $a[0]['sigla'];
				$aux[$j]['key']         =  $j;
				$j++;				

			}

			$Result['mydeck'] = @$aux;
			$Result['txt']    = $deck->gerarTxt($Result['deck']['id']);
			$Result['t']    = $deck->gerarTxt($Result['deck']['id']);


		}		

		//Pegar Comentarios

		$coment = new ComentController();
		$res    = $coment->buscarComentsByDeckIdAction($dados['deck']);
		$Result['coment'] = $res;

		//Pegar Rating

		$res    = $coment->getRatingbyDeckAction($dados['deck'],$login['id']);
		$Result['rating_user'] = $res;
		$Result['rating_user'][0]['star'] = (isset($Result['rating_user'][0]['star'])) ? $Result['rating_user'][0]['star'] : 0;

		$res    = $coment->getSomaAction($Result['deck']['id']);
		$Result['rating_soma'] = $res[0]['soma'];
		$Result['rating_soma'] = (isset($Result['rating_soma'])) ? $Result['rating_soma'] : 0;

		$template       =  new TemplateController();
		$template->renderTemplate('Deck','mydeck',$alert,true);
}


// ACTIONS

public function acaoDeckAction(){

	$Tvar    = new TratamentoVar();
	$dados   = $Tvar->PostReq();

	switch ($dados['acao']) {
		case 'salvar_deck':

			$this->salvarDeckAction($dados);
			break;
		
		default:

			$this->deletarDeckAction($dados);
			break;
	}

}

public function salvarDeckAction($dados){
	
	$Tvar    = new TratamentoVar();
	$login   = $Tvar->getSession('login');
	$deck    = new Deck();
	$list    = new DeckList();		
	$template =  new TemplateController();

	if($dados['deck'] == ''){

		$d['nome']            =  $dados['nome'];
		$d['usuario_id']      =  $login['id'];
		$d['criado']          =  date('Y-m-d');

		$res = $deck->insert($d);
		unset($d);

		foreach($_SESSION['mydeck'] as $row){
			$d['carta_id']  = $row['id'];
			$d['deck_id'] = $res;			
			$r = $list->insert($d);
		}

		$template->redirectUrl("?task=Deck&action=construtorPage&deck=".$res,"Deck cadastrado com sucesso!!!","success");
		
	}else{

		$id = $dados['deck'];
		$list->delete(array("deck_id" => $id));

		foreach($_SESSION['mydeck'] as $row){
			$d['carta_id']  = $row['id'];
			$d['deck_id'] = $id;	

			$r = $list->insert($d);	
		}

		$res = $deck->update($id,array("nome" => $dados['nome'],"atualizado" => date('Y-m-d')));			

		$template->redirectUrl("?task=Deck&action=construtorPage&deck=".$id,"Deck atualizado com sucesso!!!","success");
	}


}

public function deletarDeckAction($dados){

	$list = new DeckList();
	$deck = new Deck();

	$list->delete(array("deck_id" => $dados['deck']));
	$deck->delete(array("id" => $dados['deck']));

	$template =  new TemplateController();
	$template->redirectUrl("?task=Deck&action=construtorPage","Deck deletado com sucesso!!!","success");

}

public function addSobreAction(){

	$Tvar    = new TratamentoVar();
	$dados   = $Tvar->PostReq();
	$deck = new Deck();

	$d['sobre'] = $dados['texto'];
	$deck->update($dados['deck'],$d);

	$template =  new TemplateController();
	$template->redirectUrl("?task=Deck&action=mydeckPage&deck=".$dados['deck'],"Comentario enviado com sucesso!!!","success");
}

}
