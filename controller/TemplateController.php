<?php 

require_once 'controller/NotificacaoController.php';

class TemplateController {

 public function renderTemplate($task,$action,$alert=NULL,$col3 = NULL) 
 {	
 	global $Result,$FORM;

	$NotificacaoController  = new NotificacaoController();
	$Result['notificacao'] = $NotificacaoController->getIconNotificacaoAction();	
 	
 	$Tvar  = new TratamentoVar();
 	$Login = $Tvar->getSession('login');
 	
 	require_once 'view/Template/header.php';
 	require_once 'view/Template/content.php';
 	require_once 'view/Template/footer.php';
 }

 public function renderTemplateFrame($task,$page,$paginacao) 
 {	 	 	
 	require_once "view/".$task."/".$page.".php";
 }


 public function redirectTemplate($task,$action,$msg=NULL,$tipo = NULL)
 {

 		$url = "?task=$task&action=$action";
 		
 		if($msg != NULL)
 			$url .= "&msg=$msg&tipo=$tipo";
 		header ("location: $url");
 }

 public function redirectUrl($url,$msg = NULL,$tipo=NULL)
 {
 		if($msg != NULL)
 			$url .= "&msg=$msg&tipo=$tipo";
 		header ("location: $url");
 } 

}
