<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/MensagemController.php';
require_once 'model/Pergunta.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Negociacao.class.php';
require_once 'model/NegociacaoLeilao.class.php';
require_once 'model/Estande.class.php';
require_once 'model/Leilao.class.php';

class PerguntaController {

	var $deSys = 24;


// PAGES


// ACTIONS

	public function addPerguntaAction(){
		
		
		$pergunta    = new Pergunta();
		$Tvar        = new TratamentoVar();
		$notificacao = new Notificacao();

		$dados = $Tvar->PostReq();

		unset($dados['salvar']);

		$login = $Tvar->getSession('login');

		
		$d['data']          =  date('Y-m-d');
		$d['hora']          =  date('H:i:s');
		$d['usuario_id']    =  $login['id'];
		$d['estande_id']    =  $dados['estande'];
		$d['pergunta']      =  $dados['pergunta'];
		
		$res = $pergunta->insert($d);
		unset($d);

		$d['data']          =  date('Y-m-d');
		$d['hora']          =  date('H:i:s');
		$d['usuario_id']    =  $login['id'];
		$d['tipo']          =  1; // 1 igual a pergunta, 2 comentario, 3 rating
		$d['reference']     =  $dados['estande'];		
		$d['status']        =  0;		
		//$r   = $notificacao->insert($d);
		unset($d);
		$estande = new Estande();
		$r       = $estande->select(array("id" => $dados['estande']));
		
		$d['de']                 =   $this->deSys;
		$d['para']               =   $r[0]['usuario_id'];
		$d['assunto']            =   "Nova Pergunta na Estande";
		$d['mensagem']           =   "
		Ola,<br> Você recebeu uma nova pergunta na sua estande <a href='?task=Estande&action=estandePage&estande=".$dados['estande']."#perguntas'>clique aqui</a> para visualizar.
		";
		$mensagem = new MensagemController();
		$mensagem->enviarMensagemSistemaAction($d);

		$template =  new TemplateController();

		if($res){
			$template->redirectUrl("?task=Estande&action=estandePage&estande=".$dados['estande'],"Pergunta enviada com sucesso!!!","success");
		}else{
			$template->redirectUrl("?task=Estande&action=estandePage&estande=".$dados['estande'],"Erro ao enviado a pergunta!!!","danger");
		}
	}

	public function removePerguntaAction(){
		
		$pergunta = new Pergunta();
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		$d['pergunta'] = $dados['pergunta'];
		
		$res = $pergunta->delete(array("id" => $d));
		
		$template =  new TemplateController();		
		$template->redirectUrl("?task=Estande&action=estandePage&estande=".$dados['estande'],"Pergunta deletada com sucesso!!!","success");

	}	

	public function buscarPerguntaByEstandeIdAction($id){

		$pergunta  = new Pergunta();
		$usuario = new Usuario();
		$Tvar  = new TratamentoVar();
        
		$res     = $pergunta->select(array("estande_id" => $id,"ORDER" => "id DESC"));
		

		$n       = count($res);
		for ($i=0; $i < $n; $i++) { 
			$aux = $usuario->select(array("id" => $res[$i]));
			$res[$i]['data']          = $Tvar->dateBr($res[$i]['data']);
			$res[$i]['usuario_nome']  = $aux[0]['nome'];
			$res[$i]['usuario_login'] = $aux[0]['login'];
			$res[$i]['usuario_img']   = $aux[0]['img'];
		}
		return $res;
	}

}
