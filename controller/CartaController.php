<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/PaginacaoController.php';
require_once 'controller/CartaController.php';
require_once 'model/Carta.class.php';
require_once 'model/TipoCarta.class.php';
require_once 'model/Alinhamento.class.php';
require_once 'model/Afiliacao.class.php';
require_once 'model/TipoAcao.class.php';
require_once 'model/Deck.class.php';
require_once 'model/DeckList.class.php';
require_once 'model/Habilidade.class.php';
require_once 'model/Colecao.class.php';
require_once 'model/Reports.class.php';
require_once 'model/Raridade.class.php';
require_once 'model/Link.class.php';

class CartaController {


// PAGES

public function buscadorPage()
{

		$Tvar  = new TratamentoVar();
		$login = $Tvar->getSession('login');
		$dados = $Tvar->GetReq();	
		$deck  = new Deck();	
		$carta  = new Carta();	

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);			

		global $Result,$FORM;


		//Criar Select Tipo de Carta
		$tipo = new TipoCarta();
		$res  = $tipo->select();
		$array['carta'][0] =  "Tipo de Carta";
		foreach ($res as $key => $value) {
			$array['carta'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo = $FORM->selectCreate($array,array("id" => "tipo_de_carta","class" => "form-control"));
		unset($array);

		//Criar Select Alinhamento
		$tipo = new Alinhamento();
		$res  = $tipo->select(array("ORDER" => "nome"));
		$array['alinhamento'][0] =  "Alinhamento";
		foreach ($res as $key => $value) {
			$array['alinhamento'][$value['id']] =  $value['nome'];
		}
		$FORM->alinhamento = $FORM->selectCreate($array,array("id" => "alinhamento","class" => "form-control"));		
		unset($array);

		//Criar Select Afiliacao
		$tipo = new Afiliacao();
		$res  = $tipo->select(array("ORDER" => "nome"));
		$array['afiliacao'][0] =  "Afiliação";
		foreach ($res as $key => $value) {
			$array['afiliacao'][$value['id']] =  $value['nome'];
		}
		$FORM->afiliacao = $FORM->selectCreate($array,array("id" => "afiliacao","class" => "form-control"));		
		unset($array);	

		//Criar Select Afiliacao
		$tipo = new Afiliacao();
		$res  = $tipo->select(array("ORDER" => "nome"));
		$array['afiliacao2'][0] =  "Afiliacao II";
		foreach ($res as $key => $value) {
			$array['afiliacao2'][$value['id']] =  $value['nome'];
		}
		$FORM->afiliacao2 = $FORM->selectCreate($array,array("id" => "afiliacao2","class" => "form-control"));		
		unset($array);			

		//Criar Select Acao
		$tipo = new TipoAcao();
		$res  = $tipo->select();
		$array['tipo_acao'][0] =  "Tipo de Acao";
		foreach ($res as $key => $value) {
			$array['tipo_acao'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo_acao = $FORM->selectCreate($array,array("id" => "tipo_acao","class" => "form-control"));		
		unset($array);			

		//Criar Select Alinhamento
		$tipo = new Colecao();
		$res  = $tipo->select();
		$array['colecao'][0] =  "Coleção";
		foreach ($res as $key => $value) {
			$array['colecao'][$value['id']] =  $value['nome'];
		}
		$FORM->colecao = $FORM->selectCreate($array,array("id" => "colecao","class" => "form-control"));		
		unset($array);	

		//Criar Select Alinhamento
		$tipo = new Raridade();
		$res  = $tipo->select();
		$array['raridade'][0] =  "Raridade";
		foreach ($res as $key => $value) {
			$array['raridade'][$value['id']] =  $value['nome'];
		}
		$FORM->raridade = $FORM->selectCreate($array,array("id" => "raridade","class" => "form-control"));		
		unset($array);	

		//Buscar Ultimas Cartas

		$res = $carta->getLastCards();			
		$Result['last_cards'] = $res;
		
		$res = $deck->getLastDecks();
		$Result['last_decks'] = $res;
		for ($i=0; $i < count($Result['last_decks']); $i++) { 
			$Result['last_decks'][$i]['criado'] = $Tvar->dateBr($Result['last_decks'][$i]['criado']);
		}

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','buscador',$alert,true);
}

public function listarPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->PostReq();	

		unset($dados['buscar']);

		$dados['nome']             = $Tvar->trocarcaracter($dados['nome']);
		$dados['texto_permanente'] = $Tvar->trocarcaracter($dados['texto_permanente']);

		//Preparar Alinhamento

		if(count($dados['alinhamento']) == 1)
			$dados['alinhamento'] = $dados['alinhamento'][0];
		else if(count($dados['alinhamento']) == 2){
			$soma = $dados['alinhamento'][0] + $dados['alinhamento'][1];
			if($soma == 3)
				$dados['alinhamento'] = $soma;
			else{
				$dados['alinhamento'] = 0;
			}
		}else{
			$dados['alinhamento'] = 0;		
		}

		//Preparar Alinhamento

		if(count($dados['tipo_acao']) == 1)
			$dados['tipo_acao'] = $dados['tipo_acao'][0];
		else if(count($dados['tipo_acao']) == 2){
			$soma = $dados['tipo_acao'][0] + $dados['tipo_acao'][1];
			if($soma == 5)
				$dados['tipo_acao'] = 4;
			else{
				$dados['tipo_acao'] = 0;
			}
		}else{
			$dados['tipo_acao'] = 0;		
		}		
		
		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		global $Result;
		$Result = $this->buscarCartasAction($dados);

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','listar',$alert,true);		

}

public function detalhesPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();	
		$link  = new Link();

		global $Result;

		$Result = $this->buscarCartasAction(array("id" => $dados['carta']));
		$aux    = $link->select(array("card_id" => $dados['carta']));

		$habilidade	= new Habilidade();
		$res        = $habilidade->select(array("carta_id" => $dados['carta']));
		
		$Result['habilidade'] = $res;
		$Result['link'] = $aux[0]['link'];

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','detalhes',$alert,true);

}

public function colecoesPage(){

		$Tvar  = new TratamentoVar();
		$colecao = new Colecao();

		global $Result;

		$Result = $colecao->select(array("tipo_colecao" => 1));

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','colecoes',$alert,true);

}

public function deckBoxPage(){

		$Tvar  = new TratamentoVar();
		$colecao = new Colecao();

		global $Result;

		$Result = $colecao->select(array("tipo_colecao" => 2,"ORDER" => "ordem"));

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','deckbox',$alert,true);

}

public function especiaisPage(){

		$Tvar  = new TratamentoVar();
		$colecao = new Colecao();

		global $Result;

		$Result = $colecao->select(array("tipo_colecao" => 4));

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','especiais',$alert,true);

}

public function promoPage(){

		$Tvar  = new TratamentoVar();
		$colecao = new Colecao();

		global $Result;

		$Result = $colecao->select(array("tipo_colecao" => 3));

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','promo',$alert,true);

}

public function top10Page(){

		$Tvar  = new TratamentoVar();
		$carta = new Carta();
		$dados = $Tvar->PostReq();
		
		if($dados['carta'] != '')
			$d['carta'] = $dados['carta'];

		if($dados['raridade'] != '')
			$d['raridade'] = $dados['raridade'];

		if($dados['colecao'] != '')
			$d['colecao'] = $dados['colecao'];

		global $Result, $FORM;

		//Criar Select Tipo de Carta
		$tipo = new TipoCarta();
		$res  = $tipo->select();
		$array['carta'][0] =  "Tipo de Carta";
		foreach ($res as $key => $value) {
			$array['carta'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo = $FORM->selectCreate($array,array("id" => "tipo_de_carta","class" => "form-control"));
		unset($array);

		//Criar Select Alinhamento
		$tipo = new Raridade();
		$res  = $tipo->select();
		$array['raridade'][0] =  "Raridade";
		foreach ($res as $key => $value) {
			$array['raridade'][$value['id']] =  $value['nome'];
		}
		$FORM->raridade = $FORM->selectCreate($array,array("id" => "raridade","class" => "form-control"));		
		unset($array);	

		//Criar Select Alinhamento
		$tipo = new Colecao();
		$res  = $tipo->select();
		$array['colecao'][0] =  "Coleção";
		foreach ($res as $key => $value) {
			$array['colecao'][$value['id']] =  $value['nome'];
		}
		$FORM->colecao = $FORM->selectCreate($array,array("id" => "colecao","class" => "form-control"));		
		unset($array);	

		$Result['top10'] = $carta->top10($d);

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','top10',$alert,true);

}

public function listarColecaoPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		
		global $Result;

		$Result = $this->buscarCartasAction($dados,"numero");

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','listarColecao',$alert,true);

}


public function listarColecaoDeckPage(){

		$Tvar     = new TratamentoVar();
		$colecao  = new Colecao();

		$dados = $Tvar->GetReq();
		global $Result;

		$Result['lista'] = $this->buscarCartasAction($dados,"numero");

		$res = $colecao->select(array("id" => $dados['colecao']));
		$Result['colecao'] = $res[0];

		$dados['deck'] = $res[0]['reference'];

		$deck = new Deck();
		$list  = new DeckList();	
		$carta  = new CartaController();

		$res = $deck->select(array("id" => $dados['deck']));
		$r   = $list->select(array("deck_id" => $dados['deck']));

		$openhand = $deck->openHand($dados['deck']);

		$Result['deck']     = $res[0];
		$Result['openhand'] = $openhand;	

		$j=0;
		for ($i=0; $i < count($r); $i++) { 
			$c = $r[$i]['carta_id'];
			$a = $carta->buscarCartasByIdAction($c);
			$aux[$j]['tipo']        =  $a[0]['carta'];
			$aux[$j]['id']          =  $a[0]['id'];
			$aux[$j]['nome']        =  $a[0]['nome'];
			$aux[$j]['numero']      =  $a[0]['numero'];
			$aux[$j]['sigla']       =  $a[0]['sigla'];
			$aux[$j]['key']         =  $j;
			$j++;				

		}

		$Result['mydeck'] = @$aux;

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','listarColecaoDeck',$alert,true);

}
public function erratasPage(){


		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		
		global $Result;

		$carta = new Carta();
		$res   = $carta->selectByid(array("id" => $dados['carta']));

		$colecao = new Colecao();
		$r       = $colecao->select(array("id" => $res[0]['colecao']));

		$Result = $res[0];
		$Result['sigla'] = $r[0]['sigla'];

		$reports = new Reports();
		$erratas = $reports->select(array("AND" => array("carta_id" => $dados['carta'],"tipo" => 1)));

		$Result['erratas'] = $erratas;

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','erratas',$alert,true);

}

public function jogadasPage(){


		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();
		
		global $Result;

		$carta = new Carta();
		$res   = $carta->selectByid(array("id" => $dados['carta']));

		$colecao = new Colecao();
		$r       = $colecao->select(array("id" => $res[0]['colecao']));

		$Result = $res[0];
		$Result['sigla'] = $r[0]['sigla'];

		$reports = new Reports();
		$erratas = $reports->select(array("AND" => array("carta_id" => $dados['carta'],"tipo" => 2)));

		$Result['erratas'] = $erratas;

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','jogadas',$alert,true);

}

public function reportPage(){

		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);	

		global $Result;

		$carta = new Carta();
		$res   = $carta->selectByid(array("id" => $dados['carta']));

		$colecao = new Colecao();
		$r       = $colecao->select(array("id" => $res[0]['colecao']));

		$Result = $res[0];
		$Result['sigla'] = $r[0]['sigla'];

		if(isset($dados['report'])){
			$reports = new Reports();
			$report = $reports->select(array("id" => $dados['report']));
		}
		
		$Result['report'] = $report[0];

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','report',$alert,true);

}


public function cardMakerPage(){

		$Tvar  = new TratamentoVar();

		$login = $Tvar->getSession('login');

		if($login['nome'] == ""){
			echo "Acesso não permitido";
			exit;
		}

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','cardmaker',$alert,true);

}

// ACTIONS

	public function miniListAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();
		$lista = $Tvar->getSession('lista');

		//if($dados['habilidade'])
		
		if($dados['acao'] == "new"){
			$cartas = $this->buscarCartasAction();
			$Tvar->createSession('lista',$cartas);
			$total = count($cartas);
		}else{
			$total = count($lista);
		}
		//Paginacao
		$pagina = (isset($dados['pagina'])) ? $dados['pagina'] : 1;
		$paginacao    = new PaginacaoController($pagina,16,$total);

		$template =  new TemplateController();
		$template->renderTemplateFrame('Carta','minilista',$paginacao);


	}

	public function verificarQtdByCard($id,$Deck,$n){
		$qtd = 0;
		foreach(@$Deck as $row){
			if($row['id'] == $id) $qtd++;
		//if(array_search($id,$row)) $qtd++;
		}
		if($qtd < 3) return true;
		else return false;
	}	

	public function tipoDeCard($tipo){

		switch($tipo){
			case 'Personagem':
			$tipo = 1;
			break;
			case 'Suporte':
			$tipo = 2;	
			break;
			case 'Cenério':
			$tipo = 3;
			break;	
			case 'Habilidade':
			$tipo = 4;
			break;		
		}
		return $tipo;
	}	

	function procurarID($id,$deck){
		
		$n = count($deck);
		
		$j=0;
		for($i=0;$i<$n;$i++){
			if($deck[$i]['id'] == $id) $j++;
		}
		if($j > 0){
			return $j;
		}
		return false;
	}	

	public function painelAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();		
		
		switch ($dados['acao']) {
			case 'add':
				$mydeck = $this->addCardPainelAction();
				break;

			case 'remover':
				$mydeck = $this->rmvCardPainelAction();
				break;	

			case 'update':
				$mydeck = $this->updateCardPainelAction();
				break;		

			case 'rmvall':
				$mydeck = $this->removeAllCardsPainelAction();
				break;									
			
			default:
				# code...
				break;
		}

		$template =  new TemplateController();
		$template->renderTemplateFrame('Carta','painel',$paginacao);


	}	

	public function addCardPainelAction(){
		
		$Tvar    = new TratamentoVar();
		$carta = $this->buscarCartasByIdAction();
		//$mydeck = $Tvar->getSession('mydeck');

		if(!isset($_SESSION['mydeck'])) $j = 0;
		$j = count(@$_SESSION['mydeck']);
		if($j < 60){
			if(@$this->verificarQtdByCard($carta[0]['id'],$_SESSION['mydeck'],$j)){
				$_SESSION['mydeck'][$j]['tipo']        =  $this->tipoDeCard($card_id);
				$_SESSION['mydeck'][$j]['id']          =  $carta[0]['id'];
				$_SESSION['mydeck'][$j]['nome']        =  $carta[0]['nome'];
				$_SESSION['mydeck'][$j]['numero']      =  $carta[0]['numero'];
				$_SESSION['mydeck'][$j]['sigla']       =  $carta[0]['sigla'];
				$_SESSION['mydeck'][$j]['key']         =  $j;
			}
		}
	}

	public function rmvCardPainelAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();
		//$mydeck = $Tvar->getSession('mydeck');
		
		$key = $dados['card'];
		$n = count($_SESSION['mydeck']);

		for($i=$key;$i<$n;$i++){
			$_SESSION['mydeck'][$i] = @$_SESSION['mydeck'][$i+1];
			@$_SESSION['mydeck'][$i]['key']--;
		}
		unset($_SESSION['mydeck'][$n-1]);
		
		//$Tvar->createSession('mydeck',$mydeck);
	}

	public function updateCardPainelAction(){

		$Tvar    = new TratamentoVar();
		$carta = new Carta();
		
		$res   = $carta->buscarCartaToOrdem();
		$j = 0;
		for($i=0;$i<count($res);$i++){
			
			if($n = @$this->procurarID($res[$i]['id'],$_SESSION['mydeck'])){
				for($t=0;$t<$n;$t++){
					$aux[$j]['tipo']        =  $res[$i]['card_id'];
					$aux[$j]['id']          =  $res[$i]['id'];
					$aux[$j]['nome']        =  $res[$i]['nome'];
					$aux[$j]['numero']      =  $res[$i]['numero'];
					$aux[$j]['sigla']       =  $res[$i]['sigla'];
					$aux[$j]['key']         =  $j;
					$j++;
				}
			}
		}
		$_SESSION['mydeck'] = @$aux;
	}

	public function removeAllCardsPainelAction(){
		unset($_SESSION['mydeck']);
	}	


	public function buscarCartasAction($busca=NULL,$orden = NULL)
	{


		$Tvar    = new TratamentoVar();
		if($busca == NULL){
			$dados   = $Tvar->PostReq();

			if(isset($dados['habilidades']) && $dados['habilidades'] != ""){
				$hab = explode(",",$dados['habilidades']);
			}

		}else{
			$dados   = $busca;
			$hab     = $busca['habilidades'];
		}
		unset($dados['habilidades']);
		unset($dados['task']);
		unset($dados['action']);
		unset($dados['acao']);
		unset($dados['buscar']);

		$dados['nome'] = $Tvar->trocarcaracter($dados['nome']);
		$dados['texto_permanente'] = $Tvar->trocarcaracter($dados['texto_permanente']);

		$login = $Tvar->getSession('login');

		if($login['tipo_usuario'] != 1)
			$dados['tosearch'] = 1;

		$tb = "bs_carta";
		foreach ($dados as $key => $value) {
			if($value == "") unset($dados[$key]);
			else $d[$tb.".".$key] = $dados[$key];
		}	
		
		$j['afiliacao'] = "id";
		$join['[><]bs_afiliacao'] = $j;
		unset($j);
		$j['alinhamento'] = "id";
		$join['[><]bs_alinhamento'] = $j;		
		unset($j);
		$j['colecao'] = "id";
		$join['[><]bs_colecao'] = $j;
		unset($j);
		$j['raridade'] = "id";
		$join['[><]bs_raridade'] = $j;
		unset($j);
		$j['tipo_de_acao'] = "id";
		$join['[><]bs_tipo_de_acao'] = $j;	
		unset($j);
		$j['tipo_de_carta'] = "id";
		$join['[><]bs_tipo_de_carta'] = $j;	
		unset($j);		

		if(isset($d[$tb.".nome"])){
			$a[$tb.".nome"]      = $d[$tb.".nome"];
			$filter['LIKE']	= $a;
			unset($d[$tb.".nome"]);
		}

		if(isset($d[$tb.".texto_permanente"])){
			$a[$tb.".texto_permanente"]      = $d[$tb.".texto_permanente"];
			$filter['LIKE']    	             = $a;
			unset($d[$tb.".texto_permanente"]);
		}		

		//Verificar energia
		
		if(isset($d[$tb.".energia_inicial"])){
			$d[$tb.".energia_inicial[>=]"] = $d[$tb.".energia_inicial"];
			if(isset($d[$tb.".energia_fim"])){
				$d[$tb.".energia_inicial[<]"] = $d[$tb.".energia_fim"]+1;
				unset($d[$tb.".energia_fim"]);
			}
			unset($d[$tb.".energia_inicial"]);
		}

		//Verificar escudo
		
		if(isset($d[$tb.".escudo_inicial"])){
			$d[$tb.".escudo[>=]"] = $d[$tb.".escudo_inicial"];
			if(isset($d[$tb.".escudo_fim"])){
				$d[$tb.".escudo[<]"] = $d[$tb.".escudo_fim"]+1;
				unset($d[$tb.".escudo_fim"]);
			}
			unset($d[$tb.".escudo_inicial"]);
		}		


		//Verificar tipo de carta
		if(isset($d[$tb.".carta"])){
			$d[$tb.".tipo_de_carta"] = $d[$tb.".carta"];
			unset($d[$tb.".carta"]);
		}	

		//Verificar tipo de acao
		if(isset($d[$tb.".tipo_acao"])){
			$d[$tb.".tipo_de_acao"] = $d[$tb.".tipo_acao"];
			unset($d[$tb.".tipo_acao"]);
		}			

		$alias[] = $tb.".id";
		$alias[] = $tb.".nome";
		$alias[] = $tb.".energia_inicial";
		$alias[] = $tb.".escudo";
		$alias[] = $tb.".texto_permanente";
		$alias[] = $tb.".numero";
		$alias[] = "bs_afiliacao.nome(afiliacao)";
		$alias[] = "bs_alinhamento.nome(alinhamento)";
		$alias[] = "bs_colecao.nome(colecao)";
		$alias[] = "bs_raridade.nome(raridade)";
		$alias[] = "bs_tipo_de_acao.nome(acao)";
		$alias[] = "bs_tipo_de_carta.nome(carta)";
		$alias[] = "bs_colecao.nome(colecao)";
		$alias[] = "bs_colecao.sigla(sigla)";
		$alias[] = "bs_colecao.id(colecao_id)";
		$alias[] = "bs_tipo_de_carta.id(idTipoCarta)";

		if($orden == NULL)
			$filter['ORDER'] = $tb.".nome";
		else
			$filter['ORDER'] = $tb.".".$orden;
		


		if(count($d) > 0)
			$filter['AND']	= $d;

		$carta = new Carta();
				
		$res = $carta->select($filter,$join,$alias);
		//var_dump($carta->database->last_query());

		if(count($hab) > 0){

			unset($join);
			$j['id'] = "carta_id";
			$join['[><]bs_habilidade_carta'] = $j;
			$ID_CARDS = $carta->buscarPorHabilidade($hab,$res,$join);				

			$hb    = true;
		}
		
        
		$n = count($ID_CARDS);
		$m = count($res);
		for ($i=0; $i < $m; $i++) { 
			if(@$hb != true || is_array($ID_CARDS)){
				if($n > 0){
					for($j=0;$j<$n;$j++){
						if($ID_CARDS[$j] == $res[$i]['id']){
							$all[] = $res[$i];
						}
					}
				}else{
					$all[] = $res[$i];
				}
			}			
		}

		return $all;

	}


	public function buscarCartasEspecificasAction($busca=NULL)
	{
		$Tvar    = new TratamentoVar();
		$filter   = $busca;

		$tb = "bs_carta";
			
		$j['afiliacao'] = "id";
		$join['[><]bs_afiliacao'] = $j;
		unset($j);
		$j['alinhamento'] = "id";
		$join['[><]bs_alinhamento'] = $j;		
		unset($j);
		$j['colecao'] = "id";
		$join['[><]bs_colecao'] = $j;
		unset($j);
		$j['raridade'] = "id";
		$join['[><]bs_raridade'] = $j;
		unset($j);
		$j['tipo_de_acao'] = "id";
		$join['[><]bs_tipo_de_acao'] = $j;	
		unset($j);
		$j['tipo_de_carta'] = "id";
		$join['[><]bs_tipo_de_carta'] = $j;	
		unset($j);
		$j['id'] = "carta_id";
		$join['[><]bs_estoque'] = $j;	
		unset($j);		

		if(isset($d[$tb.".nome"])){
			$a[$tb.".nome"]      = $d[$tb.".nome"];
			$filter['LIKE']	= $a;
			unset($d[$tb.".nome"]);
		}

		if(isset($d[$tb.".texto_permanente"])){
			$a[$tb.".texto_permanente"]      = $d[$tb.".texto_permanente"];
			$filter['LIKE']    	             = $a;
			unset($d[$tb.".texto_permanente"]);
		}		 

		$alias[] = $tb.".id";
		$alias[] = $tb.".nome";
		$alias[] = $tb.".energia_inicial";
		$alias[] = $tb.".escudo";
		$alias[] = $tb.".texto_permanente";
		$alias[] = $tb.".numero";
		$alias[] = "bs_afiliacao.nome(afiliacao)";
		$alias[] = "bs_alinhamento.nome(alinhamento)";
		$alias[] = "bs_colecao.nome(colecao)";
		$alias[] = "bs_raridade.nome(raridade)";
		$alias[] = "bs_tipo_de_acao.nome(acao)";
		$alias[] = "bs_tipo_de_carta.nome(carta)";
		$alias[] = "bs_colecao.nome(colecao)";
		$alias[] = "bs_colecao.sigla(sigla)";
		$alias[] = "bs_colecao.id(colecao_id)";
		
		if(count($d) > 0)
			$filter['AND']	= $d;

		$carta = new Carta();
				
		$res = $carta->select($filter,$join,$alias);

		
		//print_r($carta->database->last_query());exit();
		if(count($hab) > 0){

			unset($join);
			$j['id'] = "carta_id";
			$join['[><]bs_habilidade_carta'] = $j;
			$ID_CARDS = $carta->buscarPorHabilidade($hab,$res,$join);				
			$hb    = true;
		}
		
        
		$n = count($ID_CARDS);
		$m = count($res);
		for ($i=0; $i < $m; $i++) { 
			if(@$hb != true || is_array($ID_CARDS)){
				if($n > 0){
					for($j=0;$j<$n;$j++){
						if($ID_CARDS[$j] == $res[$i]['id']){
							$all[] = $res[$i];
						}
					}
				}else{
					$all[] = $res[$i];
				}
			}			
		}

		return $all;

	}


	public function buscarCartasByIdAction($id = NULL)
	{
		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();
		
		unset($dados['task']);
		unset($dados['action']);
		unset($dados['acao']);

		$tb = "bs_carta";	
		
		$j['afiliacao'] = "id";
		$join['[><]bs_afiliacao'] = $j;
		unset($j);
		$j['alinhamento'] = "id";
		$join['[><]bs_alinhamento'] = $j;		
		unset($j);
		$j['colecao'] = "id";
		$join['[><]bs_colecao'] = $j;
		unset($j);
		$j['raridade'] = "id";
		$join['[><]bs_raridade'] = $j;
		unset($j);
		$j['tipo_de_acao'] = "id";
		$join['[><]bs_tipo_de_acao'] = $j;	
		unset($j);
		$j['tipo_de_carta'] = "id";
		$join['[><]bs_tipo_de_carta'] = $j;	
		unset($j);		 

		$alias[] = $tb.".id";
		$alias[] = $tb.".nome";
		$alias[] = $tb.".energia_inicial";
		$alias[] = $tb.".escudo";
		$alias[] = $tb.".texto_permanente";
		$alias[] = $tb.".numero";
		$alias[] = "bs_afiliacao.nome(afiliacao)";
		$alias[] = "bs_alinhamento.nome(alinhamento)";
		$alias[] = "bs_colecao.nome(colecao)";
		$alias[] = "bs_raridade.nome(raridade)";
		$alias[] = "bs_tipo_de_acao.nome(acao)";
		$alias[] = "bs_tipo_de_carta.nome(carta)";
		$alias[] = "bs_colecao.nome(colecao)";
		$alias[] = "bs_colecao.sigla(sigla)";
		$alias[] = "bs_colecao.id(colecao_id)";
		
		if($id == NULL)
			$d[$tb.".id"]   = $dados['card'];
		else 
			$d[$tb.".id"]   = $id;

		$filter['AND']	= $d;

		$carta = new Carta();
		$res = $carta->select($filter,$join,$alias);

		return $res;

	}	

	// Report Actions

	function reportAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();

		switch ($dados['acao']) {
			case 'salvar':
			
				if($dados['report'] == "") $this->salvarReportAction();
				else $this->updateReportAction();
				
				break;

			case 'deletar':
					$this->deleteReportAction();
				break;					
			
			default:
				# code...
				break;
		}

	}

	function salvarReportAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();	

		$report = new Reports();

		$d['texto'] =  $dados['texto'];
		$d['carta_id'] =  $dados['carta'];
		$d['tipo'] =  $dados['tipo'];

		$res = $report->insert($d);		

		$template =  new TemplateController();
		$template->redirectUrl("?task=Carta&action=reportPage&type=".$dados['tipo']."&carta=".$dados['carta']."&report=".$res,"Salva com sucesso!!!","success");			

	}

	function updateReportAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->PostReq();	

		$report = new Reports();

		$d['texto'] =  $dados['texto'];

		$res = $report->update($dados['report'],$d);		

		$template =  new TemplateController();
		$template->redirectUrl("?task=Carta&action=reportPage&type=".$dados['tipo']."&carta=".$dados['carta']."&report=".$dados['report'],"Atualizado com sucesso!!!","success");			

	}

	function deleteReportAction(){

		$Tvar    = new TratamentoVar();
		$dados   = $Tvar->GetReq();			

		print_r($dados);
		$report = new Reports();
		$res = $report->delete(array("id" => $dados['report']));

		$template =  new TemplateController();
		$template->redirectUrl("?task=Carta&action=erratasPage&carta=".$dados['carta'],"Atualizado com sucesso!!!","success");			

	}


}
