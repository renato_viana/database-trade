<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/EstandeController.php';
require_once 'model/Estande.class.php';
require_once 'model/Usuario.class.php';
require_once 'model/Notificacao.class.php';


class NotificacaoController {


// PAGES

public function notificacaoPage(){

		$notificacao       = new Notificacao();
		$usuario       = new Usuario();
		$EstandeController = new EstandeController();

		global $Result;

		$std = $EstandeController->getEstandeId();

		$res = $notificacao->select(array("AND" => array("reference" => $std,"tipo" => 1),"ORDER" => "id DESC")); //Pegar a qtd de notificação na estande

		$n = count($res);
		for ($i=0; $i < $n; $i++) { 
			$notificacao->update($res[$i]['id'],array('status' => 1));
			$r = $usuario->select(array("id" => $res[$i]['usuario_id']));
					
			$res[$i]['usuario_login'] = $r[0]['login'];
		}

		$Result['notificacao-perguntas'] = $res;

		$template       =  new TemplateController();
		$template->renderTemplate('Notificacao','notificacao',$alert,true);

}


// ACTIONS

	public function getIconNotificacaoAction($user_id = NULL){
		
		$notificacao       = new Notificacao();
		$EstandeController = new EstandeController();

		$std = $EstandeController->getEstandeId();

		$note = 0;
		$res = $notificacao->select(array("AND" => array("reference" => $std,"tipo" => 1,"status" => 0))); //Pegar a qtd de notificação na estande
		$note += count($res);

		return $note;
		
	}
}
