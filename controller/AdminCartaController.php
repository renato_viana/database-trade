<?php 

require_once 'controller/TemplateController.php';
require_once 'controller/PaginacaoController.php';
require_once 'controller/CartaController.php';
require_once 'model/Carta.class.php';
require_once 'model/TipoCarta.class.php';
require_once 'model/Alinhamento.class.php';
require_once 'model/Afiliacao.class.php';
require_once 'model/TipoAcao.class.php';
require_once 'model/Deck.class.php';
require_once 'model/DeckList.class.php';
require_once 'model/Habilidade.class.php';
require_once 'model/HabilidadeCarta.class.php';
require_once 'model/Colecao.class.php';
require_once 'model/Raridade.class.php';

class AdminCartaController {


// PAGES

public function admCartaPage()
{
	
		$Tvar  = new TratamentoVar();
		$dados = $Tvar->GetReq();	

		//Verificar se admin esta logado
		$Tvar->validarAdmin();

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);

		global $Result,$FORM;

		if(isset($dados['carta'])){
			$carta = new Carta();
			$habilidade = new HabilidadeCarta();
			$colecao    = new Colecao();

			$carta = $carta->selectByid(array("id" => $dados['carta']));
			$hab = $habilidade->select(array("carta_id" => $carta[0]['id']));
			$colecao = $colecao->select(array("id" => $carta[0]['colecao']));

			$tipo = $res[0]['tipo_de_carta'];
			$Result['img'] = "<img src='img/cartas/".$colecao[0]['sigla']."/".$carta[0]['numero'].".png' width='80'/>";
			$Result['pequena'] = "<img src='img/cartas/".$colecao[0]['sigla']."/thumb/".$carta[0]['numero'].".png' width='50'/>";
		}



		$FORM->nome     = $FORM->textCreate(array("nome" => $carta[0]['nome']),array("id" => "nome","class" => "form-control input-md"));
		$FORM->tosearch     = $FORM->textCreate(array("tosearch" => $carta[0]['tosearch']),array("id" => "tosearch","class" => "form-control input-md"));
		$FORM->alter_ego     = $FORM->textCreate(array("alter_ego" => $carta[0]['alter_ego']),array("id" => "alter_ego","class" => "form-control input-md"));
		$FORM->energia_inicial     = $FORM->textCreate(array("energia_inicial" => $carta[0]['energia_inicial']),array("id" => "energia_inicial","class" => "form-control input-md"));
		$FORM->escudo     = $FORM->textCreate(array("escudo" => $carta[0]['escudo']),array("id" => "escudo","class" => "form-control input-md"));
		$FORM->numero     = $FORM->textCreate(array("numero" => $carta[0]['numero']),array("id" => "numero","class" => "form-control input-md"));
		$FORM->idcarta    = $FORM->hiddenCreate(array("carta" => $dados['carta']));
		$Result['nome'] = $carta[0]['nome'];
		$Result['texto_permanente'] = $carta[0]['texto_permanente'];
		

		for ($i=0; $i < 14; $i++) { 
			
			$checked = false;
			$n = count($hab);
			for ($j=0; $j < $n; $j++) { 
				if($i == $hab[$j]['habilidade_id']) $checked = true;
			}

			$FORM->habilidades[]     = $FORM->checkboxCreate(array("habilidades" => $i),array("class" => "checkbox-inline","style" => "margin:5px;"),"<img src='img/icon/".$i.".png' />",$checked);
		}

		//Criar Select Tipo de Carta
		$tipo = new TipoCarta();
		$res  = $tipo->select();
		$array['tipo_de_carta'][0] =  "Tipo de Carta";
		foreach ($res as $key => $value) {
			$array['tipo_de_carta'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo = $FORM->selectCreate($array,array("id" => "tipo_de_carta","class" => "form-control"),$carta[0]['tipo_de_carta']);
		unset($array);

		//Criar Select Alinhamento
		$tipo = new Alinhamento();
		$res  = $tipo->select();
		$array['alinhamento'][0] =  "Alinhamento";
		foreach ($res as $key => $value) {
			$array['alinhamento'][$value['id']] =  $value['nome'];
		}
		$FORM->alinhamento = $FORM->selectCreate($array,array("id" => "alinhamento","class" => "form-control"),$carta[0]['alinhamento']);		
		unset($array);

		//Criar Select Afiliacao
		$tipo = new Afiliacao();
		$res  = $tipo->select();
		$array['afiliacao'][0] =  "Afiliacao";
		foreach ($res as $key => $value) {
			$array['afiliacao'][$value['id']] =  $value['nome'];
		}
		$FORM->afiliacao = $FORM->selectCreate($array,array("id" => "afiliacao","class" => "form-control"),$carta[0]['afiliacao']);		
		unset($array);	

		//Criar Select Acao
		$tipo = new TipoAcao();
		$res  = $tipo->select();
		$array['tipo_de_acao'][0] =  "Tipo de Acao";
		foreach ($res as $key => $value) {
			$array['tipo_de_acao'][$value['id']] =  $value['nome'];
		}
		$FORM->tipo_acao = $FORM->selectCreate($array,array("id" => "tipo_de_acao","class" => "form-control"),$carta[0]['tipo_de_acao']);		
		unset($array);	

		//Criar Select Acao
		$tipo = new Colecao();
		$res  = $tipo->select();
		$array['colecao'][0] =  "Colecao";
		foreach ($res as $key => $value) {
			$array['colecao'][$value['id']] =  $value['nome'];
		}
		$FORM->colecao = $FORM->selectCreate($array,array("id" => "colecao","class" => "form-control"),$carta[0]['colecao']);		
		unset($array);	

		//Criar Select Acao
		$tipo = new Raridade();
		$res  = $tipo->select();
		$array['raridade'][0] =  "Raridade";
		foreach ($res as $key => $value) {
			$array['raridade'][$value['id']] =  $value['nome'];
		}
		$FORM->raridade = $FORM->selectCreate($array,array("id" => "raridade","class" => "form-control"),$carta[0]['raridade']);		
		unset($array);							
			
		$template       =  new TemplateController();
		$template->renderTemplate('Carta','admin/carta',$alert,true);
}

function admColecaoPage(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();		

		//Verificar se admin esta logado
		$Tvar->validarAdmin();			

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);		

		global $Result;

		$colecao = new Colecao();
		$Result['colecoes'] = $colecao->select();	

		if(isset($dados['colecao'])){
			$res = $colecao->select(array("id" => $dados['colecao']));				
			$Result['colecao'] = $res[0];
		}	

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','admin/colecao',$alert,true);	
}


function admAlinhamentoPage(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();	

		//Verificar se admin esta logado
		$Tvar->validarAdmin();				

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);		

		global $Result;

		$alinhamento = new Alinhamento();
		$Result['alinhamentos'] = $alinhamento->select();	

		if(isset($dados['alinhamento'])){
			$res = $alinhamento->select(array("id" => $dados['alinhamento']));				
			$Result['alinhamento'] = $res[0];
		}	

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','admin/alinhamento',$alert,true);	
}


function admAfiliacaoPage(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();		

		//Verificar se admin esta logado
		$Tvar->validarAdmin();			

		if(isset($dados['msg']))
			$alert  = $Tvar->createAlert($dados['msg'],$dados['tipo']);		

		global $Result;

		$afiliacao = new Afiliacao();
		$Result['afiliacoes'] = $afiliacao->select();	

		if(isset($dados['afiliacao'])){
			$res = $afiliacao->select(array("id" => $dados['afiliacao']));				
			$Result['afiliacao'] = $res[0];
		}	

		$template       =  new TemplateController();
		$template->renderTemplate('Carta','admin/afiliacao',$alert,true);	
}

// ACTIONS

// Coleções
function admColecaoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();	
		
		switch ($dados['acao']) {
			case 'salvar':
				if($dados['colecao'] == "")
					$this->cadastrarColecaoAction();
				else
					$this->atualizarColecaoAction();
				break;

			case 'deletar':
					$this->deletarColecaoAction($dados);
			break;
			
			default:
				# code...
				break;
		}
	

}

function cadastrarColecaoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();

		$file  = $_FILES['img'];
		
		unset($dados['acao']);
		unset($dados['colecao']);

		$colecao = new Colecao();
		$res = $colecao->insert($dados);

		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];

		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			$img = WideImage::load($file['tmp_name']);
			$img->resize(146,100)->saveToFile("img/colecoes/".$dados['sigla'].".".$ext);

			//move_uploaded_file($file['tmp_name'], "img/cartas/".$r[0]['sigla']."/".$dados['numero'].".".$ext);
		}
		mkdir("img/cartas/".$dados['sigla']);
		mkdir("img/cartas/".$dados['sigla']."/thumb");
//		chmod("img/cartas/".$dados['sigla'],777);

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admColecaoPage","Colecao cadastrada com sucesso","success");	


}

function atualizarColecaoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
		
		$file  = $_FILES['img'];
		$id    = $dados['colecao'];

		unset($dados['acao']);
		unset($dados['colecao']);

		$colecao = new Colecao();
		$res = $colecao->update($id,$dados);

		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];

		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			$img = WideImage::load($file['tmp_name']);
			$img->resize(146,100)->saveToFile("img/colecoes/".$dados['sigla'].".".$ext);

			//move_uploaded_file($file['tmp_name'], "img/cartas/".$r[0]['sigla']."/".$dados['numero'].".".$ext);
		}
		mkdir("img/cartas/".$dados['sigla']);
		mkdir("img/cartas/".$dados['sigla']."/thumb");		
		
		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admColecaoPage&colecao=".$id,"Colecao Atualizada com sucesso","success");	

}

function deletarColecaoAction($dados = NULL){

		$Tvar  = new TratamentoVar();
		if($dados == NULL)	
			$dados = $Tvar->GetReq();

		$colecao = new Colecao();
		$res = $colecao->delete(array("id" => $dados['colecao']));

		unlink("img/colecoes/".$dados['sigla'].".png");

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admColecaoPage","Colecao Deletada com sucesso","success");		

		

}

//Alinhamento

function admAlinhamentoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
		
		switch ($dados['acao']) {
			case 'salvar':
				if($dados['alinhamento'] == "")
					$this->cadastrarAlinhamentoAction();
				else
					$this->atualizarAlinhamentoAction();
				break;

			case 'deletar':
					$this->deletarAlinhamentoAction($dados);
			break;
			
			default:
				# code...
				break;
		}
	

}

function cadastrarAlinhamentoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
		
		unset($dados['acao']);
		unset($dados['alinhamento']);

		$alinhamento = new Alinhamento();
		$res = $alinhamento->insert($dados);

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admAlinhamentoPage","Alinhamento cadastrado com sucesso","success");	


}

function atualizarAlinhamentoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
	
		$id    = $dados['alinhamento'];

		unset($dados['acao']);
		unset($dados['alinhamento']);

		$colecao = new Alinhamento();
		$res = $colecao->update($id,$dados);
		
		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admAlinhamentoPage&colecao=".$id,"Alinhamento Atualizado com sucesso","success");	

}

function deletarAlinhamentoAction($dados = NULL){

		$Tvar  = new TratamentoVar();
		if($dados == NULL)	
			$dados = $Tvar->GetReq();

		$alinhamento = new Alinhamento();
		$res = $alinhamento->delete(array("id" => $dados['alinhamento']));

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admAlinhamentoPage","Alinhamento Deletado com sucesso","success");		

		

}

//Afiliacao

function admAfiliacaoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
		
		switch ($dados['acao']) {
			case 'salvar':
				if($dados['afiliacao'] == "")
					$this->cadastrarAfiliacaoAction();
				else
					$this->atualizarAfiliacaoAction();
				break;

			case 'deletar':
					$this->deletarAfiliacaoAction($dados);
			break;
			
			default:
				# code...
				break;
		}
	

}

function cadastrarAfiliacaoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
		
		unset($dados['acao']);
		unset($dados['afiliacao']);

		$afiliacao = new Afiliacao();
		$res = $afiliacao->insert($dados);

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admAfiliacaoPage","Afiliacao cadastrada com sucesso","success");	


}

function atualizarAfiliacaoAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->PostReq();
	
		$id    = $dados['afiliacao'];

		unset($dados['acao']);
		unset($dados['afiliacao']);

		$afiliacao = new Afiliacao();
		$res = $afiliacao->update($id,$dados);
		
		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admAfiliacaoPage&colecao=".$id,"Afiliacao Atualizada com sucesso","success");	

}

function deletarAfiliacaoAction($dados = NULL){

		$Tvar  = new TratamentoVar();
		if($dados == NULL)	
			$dados = $Tvar->GetReq();

		$afiliacao = new Afiliacao();
		$res = $afiliacao->delete(array("id" => $dados['afiliacao']));

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admAfiliacaoPage","Afiliacao Deletado com sucesso","success");		

		

}

//Carta

function admCartaAction(){

		$Tvar  = new TratamentoVar();	
		$carta = new Carta();
		$dados = $Tvar->PostReq();

		$dados['nome'] = $dados['nome2'];	
		unset($dados['nome2']);

		if($dados['carta'] != ""){
			$id = $dados['carta'];
		}

		$file  = $_FILES['img'];
		$pequena  = $_FILES['pequena'];

		$habilidade = $dados['habilidades'];
		
		unset($dados['salvar']);
		unset($dados['habilidades']);
		unset($dados['carta']);
		
		if($id != ""){
			$carta->update($id,$dados);
			$res = $id;
		}else{
			$res = $carta->insert($dados);
		}
		
		$colecao = new Colecao();

		$r = $colecao->select(array("id" => $dados['colecao']));

		//Imagem Grande
		$ponto = explode(".", $file['name']);
		$ext = $ponto[count($ponto)-1];

		if($file['type'] == "image/png" || $file['type'] == "image/jpeg"){
			
			include("libs/wide/lib/WideImage.php");
			
			$img = WideImage::load($file['tmp_name']);
			unlink("img/cartas/".$r[0]['sigla']."/".$dados['numero'].".".$ext);
			unlink("img/cartas/".$r[0]['sigla']."/thumb/".$dados['numero'].".".$ext);
			$img->resize(231,323)->saveToFile("img/cartas/".$r[0]['sigla']."/".$dados['numero'].".".$ext);
			$img->resize(61,85)->saveToFile("img/cartas/".$r[0]['sigla']."/thumb/".$dados['numero'].".".$ext);

			//move_uploaded_file($file['tmp_name'], "img/cartas/".$r[0]['sigla']."/".$dados['numero'].".".$ext);
		}

		
		$n = count($habilidade);
		
		if($n > 0){

			$hc = new HabilidadeCarta();
			$hc->delete(array("carta_id" => $res));
			
			for ($i=0; $i < $n; $i++) { 
				
				$d['habilidade_id'] = $habilidade[$i];
				$d['carta_id']      = $res;
				$aux = $hc->insert($d);
			}

		}
		
		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admCartaPage&carta=".$res,"Carta Cadastrada com sucesso","success");				

}

function admDeleteCartaAction(){

		$Tvar  = new TratamentoVar();	
		$dados = $Tvar->GetReq();
	
		$hc = new HabilidadeCarta();
		$hc->delete(array("carta_id" => $dados['carta']));

		$carta = new Carta();
		$res = $carta->delete(array("id" => $dados['carta']));

		$template       =  new TemplateController();
		$template->redirectUrl("?task=AdminCarta&action=admCartaPage","Carta Deletada com sucesso","success");				

}

}
