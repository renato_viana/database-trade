<?php 

class FormBuild {

	function beginForm($atributes){

		$field = "";
		$field .= "<form ";

		$atrr = array("method","action","class","id","placeholder","required","style","onsubmit");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
					$field .= " ".$value."='".$atributes[$value]."' ";	
		}	

		$field .= ">";

		return $field;

	}

	function endForm(){

		$field = "";
		$field .= " </form> ";

		return $field;
	}	

	function textCreate($array,$atributes = NULL){
		
		$field = "";
		$field .= "<input type='text'";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."' ";

		if($array[$key[0]] != "")	
			$field .= " value='".$array[$key[0]]."' ";

		$atrr = array("class","id","placeholder","required","style","onfocus","onblur","onclick");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
				$field .= " ".$value."='".$atributes[$value]."' ";	
		}	

		$field .= " />";
		
		return $field;
	}

	function passwordCreate($array,$atributes = NULL){
		
		$field = "";
		$field .= "<input type='password'";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."' ";

		if($array[$key[0]] != "")	
			$field .= " value='".$array[$key[0]]."' ";

		$atrr = array("class","id","placeholder","required","style","onfocus","onblur","onclick");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
				$field .= " ".$value."='".$atributes[$value]."' ";	
		}	

		$field .= " />";
		
		return $field;
	}	

	function emailCreate($array,$atributes = NULL){
		
		$field = "";
		$field .= "<input type='email'";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."' ";

		if($array[$key[0]] != "")	
			$field .= " value='".$array[$key[0]]."' ";

		$atrr = array("class","id","placeholder","style","required","onfocus","onblur","onclick");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
				$field .= " ".$value."='".$atributes[$value]."' ";	
		}	

		$field .= " />";
		
		return $field;
	}	

	function hiddenCreate($array,$atributes){
		
		$field = "";
		$field .= "<input type='hidden'";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."' ";

		if($array[$key[0]] != "")	
			$field .= " value='".$array[$key[0]]."' ";	

		$field .= " />";
		
		return $field;
	}	

	function checkboxCreate($array,$atributes,$label,$checked=NULL){

		$field = "";
		$field .= "<input type='checkbox'";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."[]' ";

			$field .= " value='".$array[$key[0]]."' ";	

		$atrr = array("class","id","style","onfocus","onblur","onclick");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
				$field .= " ".$value."='".$atributes[$value]."' ";	
		}		

		if($checked == true)
			$field .= " checked='checked' ";

		$field .= " /> ".$label;

		return $field;
	}	

	function radioCreate($array,$atributes,$label,$for,$checked=NULL){

		$field = "";
		$field = "<label class='checkbox-inline' for='".$for."'>";
		$field .= "<input type='radio'";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."' ";

			$field .= " value='".$array[$key[0]]."' ";	

		$atrr = array("class","id","style","onfocus","onblur","onclick");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
				$field .= " ".$value."='".$atributes[$value]."' ";	
		}		

		if($checked == true)
			$field .= " checked='checked' ";

		$field .= " /> ".$label;
		$field .= " </label>";

		return $field;
	}	

	function selectCreate($array,$atributes,$selected=NULL){

		$field = "";
		$field .= "<select ";

		$key    = array_keys($array);
			$field .= " name='".$key[0]."' ";		
		
		$atrr = array("class","id","style","onfocus","onblur","onclick","onchange");

		foreach ($atrr as $k => $value) {
				if(array_key_exists($value, $atributes))
				$field .= " ".$value."='".$atributes[$value]."' ";	
		}		

		$field .= " > ";

		$aux    = $array[$key[0]];
		$label  = array_keys($aux);
		$n      = count($aux);

		for($i=0;$i<$n;$i++){

		$field .= " <option";
		if($label[$i] != 0)
			$field .= " value='".$label[$i]."' ";		
		else
			$field .= " value='' ";		

		if($selected == $label[$i])
			$field .= " selected='selected' ";

		$field .= "> ";

		$field .= $aux[$label[$i]];

		$field .= " </option> ";

		}

		$field .= " </select> ";

		return $field;
	}

	function buttonCreate($label,$atributes){
		
		$field = "";
		$field .= "<button ";


		$atrr = array("value","class","id","style","onclick");

		foreach ($atrr as $key => $value) {
				if(array_key_exists($value, $atributes))
					$field .= " ".$value."='".$atributes[$value]."' ";	
		}	

		$field .= "> ";
		$field .= $label;
		$field .= " </button>";
		
		return $field;
	}	
}