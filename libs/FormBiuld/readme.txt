$f = new FormBuild();

echo $f->beginForm(["method" => "post","action" => "index.php"]);

$f->nome     = $f->textCreate(["nome" => "Renato"],["placeholder" => "Aqui"]);
echo $f->nome;

$f->pass     = $f->passwordCreate(["nome" => "Renato"],["class" => "Aqui"]);
echo $f->pass;

$f->hidden     = $f->hiddenCreate(["block" => "Renato"],["class" => "Aqui"]);
echo $f->hidden;

$f->checkbox     = $f->checkboxCreate(["block" => "Renato"],["class" => "box"],"Renato",true);
echo $f->checkbox;

$f->radio     = $f->radioCreate(["block" => "Renato"],["class" => "box"],"Renato");
echo $f->radio;

$Cidade['12'] = "Palmas";
$Cidade['22'] = "Gurupi";
$Cidade['43'] = "Miranorte";
$array['Cidade'] = $Cidade;

$f->select = $f->selectCreate($array,["id" => "teste"],43);
print_r($f->select);

echo $f->buttonCreate("Enviar",["class" => "btn btn-primary"]);
echo $f->endForm();