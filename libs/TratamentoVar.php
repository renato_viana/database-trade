<?php 

class TratamentoVar {

	public function check_postkeys($requiredKeys = array(), $ignoredKeys = array()){

		$_POSTKEYS = array_diff(array_keys($_POST),$ignoredKeys);
		sort($_POSTKEYS);
		sort($requiredKeys);

		return ($_POSTKEYS === $requiredKeys);
	}

	public function checkNum($num){

		$num = (is_numeric($num)) ? $num: false ;
		if($num == false){
			echo "checkNum Error: O dado enviado não e do tipo numerico: ".$num;
			exit;
		}
		return $num;
	}

	public function PostReq(){

		foreach($_POST AS $key => $value) { 
			$_POST[$key] = $value; 
		} 

		return $_POST;
	}

	public function GetReq(){

		foreach($_GET AS $key => $value) { 
			$_GET[$key] = $value; 
		} 

		return $_GET;
	}


	public function setSessionRegisterForm($dados){

		foreach($dados AS $key => $value) { 
			$_SESSION['form'][$key] = $value; 
		} 
	}	

	public function getSessionRegisterForm(){

		foreach($_SESSION['form'] AS $key => $value) { 
			$dados[$key] = $value; 
		} 

		return $dados;
	}	

	public function createSession($key,$dados){

		$_SESSION[$key] = $dados;
		return $dados;
	}

	public function destroySession($key){

		unset($_SESSION[$key]);
	}	

	public function getSession($key){

		if(isset($_SESSION[$key]))
			return $_SESSION[$key];
		else 
			return false;
	}		
	
	public function dateEn($date){
		return implode ( "-",array_reverse(explode("/",$date)));
	}	

	public function dateBr($date){
		return implode ( "/",array_reverse(explode("-",$date)));
	}		

	public function createAlert($msg,$tipo){
		$alert['msg']  = $msg;
		$alert['tipo'] = $tipo;
		return $alert;
	}


	public function trocarcaracter($char){


		$carac_esp  = array("á","Á","â","Â","à","À","ã","Ã","ç","Ç","é","É","ê","Ê","í","Í","ó","Ó","ô","Ô","õ","Õ","ú","Ú","ü","Ü","º","ª","-");
		$carac_html = array("&aacute;","&Aacute;","&acirc;","&Acirc;","&agrave;","&Agrave;","&atilde;","&Atilde;","&ccedil;","&Ccedil;","&eacute;","&Eacute;","&ecirc;","&Ecirc;","&iacute;","&Iacute;","&oacute;","&Oacute;","&ocirc;","&Ocirc;","&otilde;","&Otilde;","&uacute;","&Uacute;","&uuml;","&Uuml;","&ordm","&ordf","&#45;");

		$char = str_replace($carac_esp,$carac_html,$char);
		return $char;
	}	

	public function trocarcaracterHE($char){


		$carac_esp  = array("á","Á","â","Â","à","À","ã","Ã","ç","Ç","é","É","ê","Ê","í","Í","ó","Ó","ô","Ô","õ","Õ","ú","Ú","ü","Ü","º","ª","-");
		$carac_html = array("&aacute;","&Aacute;","&acirc;","&Acirc;","&agrave;","&Agrave;","&atilde;","&Atilde;","&ccedil;","&Ccedil;","&eacute;","&Eacute;","&ecirc;","&Ecirc;","&iacute;","&Iacute;","&oacute;","&Oacute;","&ocirc;","&Ocirc;","&otilde;","&Otilde;","&uacute;","&Uacute;","&uuml;","&Uuml;","&ordm","&ordf","&#45;");
		$char = str_replace($carac_html,$carac_esp,$char);
		return $char;
	}		

	public function validarAdmin(){
		
		$login = $this->getSession('login');
		if($login['tipo_usuario'] != 1){
			echo "Acesso não permitido";
			exit;
		}

	}

	public function verificarAdmin(){
		
		$login = $this->getSession('login');
		if($login['tipo_usuario'] == 1 || $login['tipo_usuario'] == 2)
			return true;
		return false;
	}	

	
}