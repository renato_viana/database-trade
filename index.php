<?php 
session_start();
error_reporting(0);
ini_set( 'default_charset', 'utf-8');
date_default_timezone_set('Brazil/DeNoronha');

require_once 'config.php';

//libs

require_once 'libs/medoo.php';
require_once 'libs/TratamentoVar.php';
require_once 'libs/FormBuild.php';
require_once 'libs/PHPMailer/PHPMailerAutoload.php';


$FORM = new FormBuild();
$Result = NULL;


$classe = (isset($_REQUEST['task'])) ? $_REQUEST['task'] : "Carta";
$metodo = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : "buscadorPage";

$classe .= 'Controller';

require_once 'controller/'.$classe.'.php';

$obj = new $classe();
$obj->$metodo();
